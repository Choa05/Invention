﻿/*! 10 !*/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;
using System.Reflection;
using Inv.Support;

namespace Inv
{
  public static class ServerShell
  {
    static ServerShell()
    {
      SocketHost = "127.0.0.1";
      SocketPort = 3717;
      FrameDurationMilliseconds = 16;
    }

    public static string SocketHost { get; set; }
    public static int SocketPort { get; set; }
    public static int FrameDurationMilliseconds { get; set; }

    public static ServerEngine NewEngine(Inv.Application HostApplication, Func<Guid, Inv.Application> InvFunction)
    {
      return new ServerEngine(HostApplication, InvFunction);
    }

    internal const long ProtocolVersion = 5;
  }

  public sealed class ServerEngine
  {
    internal ServerEngine(Inv.Application HostApplication, Func<Guid, Inv.Application> InvFunction)
    {
      this.HostApplication = HostApplication;
      this.TenantList = new Inv.DistinctList<ServerTenant>();
      this.AcceptTenantList = new Inv.DistinctList<Func<ServerTenant>>();
      this.RejectChannelList = new DistinctList<WebChannel>();

      this.WebServer = this.HostApplication.Web.NewServer(ServerShell.SocketHost, ServerShell.SocketPort, null);
      WebServer.AcceptEvent += (WebChannel) =>
      {
        Guid Identity;

        var ServerConnection = new ServerConnection(WebChannel.InputStream, WebChannel.OutputStream);
        var Packet = ServerConnection.TryReceivePacket();

        if (Packet == null)
        {
          WebChannel.Drop();
        }
        else
        {
          using (var Reader = Packet.ToReader())
          {
            var ClientMessage = (ClientMessage)Reader.ReadByte();
            if (ClientMessage != ClientMessage.Identification)
              throw new Exception("Invalid identification message: " + ClientMessage);

            // TODO: 64 bit protocol version number.
            var Version = Reader.ReadInt64();
            if (Version != ServerShell.ProtocolVersion)
            {
              WebChannel.Drop();
              return;
            }

            Identity = Reader.ReadGuid();
          }

          lock (AcceptTenantList)
            AcceptTenantList.Add(() => new ServerTenant(this, InvFunction(Identity), WebChannel, ServerConnection));
        }
      };
      WebServer.RejectEvent += (Channel) =>
      {
        lock (RejectChannelList)
          RejectChannelList.Add(Channel);
      };

      this.SleepHandle = new System.Threading.EventWaitHandle(false, System.Threading.EventResetMode.ManualReset);
    }

    public Inv.Application HostApplication { get; private set; }
    public event Action<ServerTenant> AcceptEvent;
    public event Action<ServerTenant> RejectEvent;
    public event Action ProcessEvent;

    public void Start()
    {
      if (!IsActive)
      {
        this.IsActive = true;

        this.LogWriter = null;
        //this.LogWriter = new StreamWriter(@"C:\Hole\InvServerEngine.log");
        this.ProcessStopwatch = new Stopwatch();

        WebServer.Connect();

        SleepHandle.Reset();

        this.Task = new Task(RunThread);
        Task.Start();
      }
    }
    public void Stop()
    {
      if (IsActive)
      {
        this.IsActive = false;

        SleepHandle.Set();

        if (Task != null)
        {
          Task.Wait();
          this.Task = null;
        }

        WebServer.Disconnect();

        if (LogWriter != null)
        {
          LogWriter.Flush();
          LogWriter.Dispose();
        }
      }
    }
    public IEnumerable<ServerTenant> GetTenants()
    {
      return TenantList;
    }
    
    internal ServerImage NextImage()
    {
      return new ServerImage(this, new ServerImageTag(++LastImageID));
    }
    internal ServerSound NextSound()
    {
      return new ServerSound(this, new ServerSoundTag(++LastSoundID));
    }

    private void RunThread()
    {
      try
      {
        var FrameDuration = Inv.ServerShell.FrameDurationMilliseconds;

        while (IsActive)
        {
          ProcessStopwatch.Restart();

          Process();

          var FrameMS = ProcessStopwatch.ElapsedMilliseconds;
          if (FrameMS < FrameDuration)
            SleepHandle.WaitOne(FrameDuration - (int)FrameMS);
        }
      }
      finally
      {
        foreach (var Tenant in TenantList)
          Tenant.Stop();

        TenantList.Clear();
      }
    }
    private void Process()
    {
      // rejecting tenants.
      WebChannel[] RejectChannelArray;

      lock (RejectChannelList)
      {
        if (RejectChannelList.Count > 0)
        {
          RejectChannelArray = RejectChannelList.ToArray();
          RejectChannelList.Clear();
        }
        else
        {
          RejectChannelArray = null;
        }
      }

      if (RejectChannelArray != null)
      {
        foreach (var RejectChannel in RejectChannelList)
        {
          var RejectTenant = TenantList.Find(T => T.WebChannel == RejectChannel);

          if (RejectTenant != null)
          {
            TenantList.Remove(RejectTenant);
            RejectTenant.Stop();

            if (RejectEvent != null)
              RejectEvent(RejectTenant);
          }
        }
      }

      // accepting tenants.
      Func<ServerTenant>[] AcceptTenantArray;
      lock (AcceptTenantList)
      {
        if (AcceptTenantList.Count > 0)
        {
          AcceptTenantArray = AcceptTenantList.ToArray();
          AcceptTenantList.Clear();
        }
        else
        {
          AcceptTenantArray = null;
        }
      }

      if (AcceptTenantArray != null)
      {
        foreach (var NewTenant in AcceptTenantArray)
        {
          var AcceptTenant = NewTenant();
          AcceptTenant.Start();

          TenantList.Add(AcceptTenant);

          if (AcceptEvent != null)
            AcceptEvent(AcceptTenant);
        }
      }

      // processing event.
      if (ProcessEvent != null)
        ProcessEvent();

      // processing tenants.
      foreach (var Tenant in TenantList)
      {
        Tenant.Process();

        if (Tenant.InvApplication.IsExit)
        {
          TenantList.Remove(Tenant);
          Tenant.Stop();
        }
      }
    }

    private WebServer WebServer;
    private Inv.DistinctList<ServerTenant> TenantList;
    private Inv.DistinctList<Func<ServerTenant>> AcceptTenantList;
    private Inv.DistinctList<WebChannel> RejectChannelList;
    private ushort LastImageID;
    private ushort LastSoundID;
    private Task Task;
    private Stopwatch ProcessStopwatch;
    private StreamWriter LogWriter;
    private bool IsActive;
    private EventWaitHandle SleepHandle;
  }

  public sealed class ServerTenant
  {
    internal ServerTenant(ServerEngine ServerEngine, Inv.Application InvApplication, WebChannel WebChannel, ServerConnection ServerConnection)
    {
      this.ServerEngine = ServerEngine;
      this.InvApplication = InvApplication;
      this.WebChannel = WebChannel;
      this.ServerConnection = ServerConnection;

      this.RouteArray = new Inv.EnumArray<PanelType, Func<ServerSurface, Panel, ServerPanel>>()
      {
        { Inv.PanelType.Browser, TranslateBrowser },
        { Inv.PanelType.Button, TranslateButton },
        { Inv.PanelType.Board, TranslateBoard },
        { Inv.PanelType.Dock, TranslateDock },
        { Inv.PanelType.Edit, TranslateEdit },
        { Inv.PanelType.Flow, TranslateFlow },
        { Inv.PanelType.Frame, TranslateFrame },
        { Inv.PanelType.Graphic, TranslateGraphic },
        { Inv.PanelType.Label, TranslateLabel },
        { Inv.PanelType.Memo, TranslateMemo },
        { Inv.PanelType.Overlay, TranslateOverlay },
        { Inv.PanelType.Canvas, TranslateCanvas },
        { Inv.PanelType.Scroll, TranslateScroll },
        { Inv.PanelType.Stack, TranslateStack },
        { Inv.PanelType.Table, TranslateTable },
      };

      this.ServerQueue = new ServerQueue("Tenant", ServerConnection);
      this.ServerSender = new ServerSender(this);
      this.ServerContract = ServerSender;

      this.SurfaceDictionary = new Dictionary<ServerSurfaceTag, WeakReference<Surface>>();
      this.PanelDictionary = new Dictionary<ServerPanelTag, WeakReference<Panel>>();
      this.ImageDictionary = new Dictionary<ServerImageTag, WeakReference<Image>>();
      this.SoundDictionary = new Dictionary<ServerSoundTag, WeakReference<Sound>>();
      this.AnimationDictionary = new Dictionary<ServerAnimationTag, Animation>();
      this.ClipDictionary = new Dictionary<ServerClipTag, AudioClip>();
      this.PickerDictionary = new Dictionary<ServerPickerTag, Picker>();
      this.PostActionList = new DistinctList<Action>();

      this.ClientArray = new EnumArray<ClientMessage, Action<ServerReader>>();

      var OwnerType = GetType().GetReflectionInfo();
      var MethodInfoArray = OwnerType.GetReflectionMethods().Where(M => !M.IsPublic && !M.IsStatic).ToArray();

      foreach (var MethodInfo in MethodInfoArray)
      {
        var ParameterInfoArray = MethodInfo.GetParameters();

        if (MethodInfo.Name.StartsWith("Receive") && ParameterInfoArray.Length == 1 && ParameterInfoArray[0].ParameterType == typeof(ServerReader))
        {
          var Message = Inv.Support.EnumHelper.Parse<ClientMessage>(MethodInfo.Name.Substring("Receive".Length));

          ClientArray[Message] = (Action<ServerReader>)MethodInfo.CreateDelegate(typeof(Action<>).MakeGenericType(typeof(ServerReader)), this);
        }
      }

      // will be replaced from the remote user.
      InvApplication.SetPlatform(new ServerPlatform(this));
      InvApplication.Device.Name = "";
      InvApplication.Device.Model = "";
      InvApplication.Device.System = "";
      InvApplication.Device.Keyboard = true;
      InvApplication.Device.Mouse = true;
      InvApplication.Device.Touch = true;
      InvApplication.Device.ProportionalFontName = "";
      InvApplication.Device.MonospacedFontName = "";
      InvApplication.Process.Id = 0;
      InvApplication.Window.Width = 1024;
      InvApplication.Window.Height = 768;

      this.CalendarTimeZoneName = TimeZoneInfo.Local.DisplayName;
    }

    public Application InvApplication { get; private set; }

    internal ServerEngine ServerEngine { get; private set; }
    internal WebChannel WebChannel { get; private set; }
    internal ServerConnection ServerConnection { get; private set; }
    internal ServerQueue ServerQueue { get; private set; }
    internal string CalendarTimeZoneName { get; private set; }
    internal bool PhoneIsSupported { get; private set; }
    internal bool LocationIsSupported { get; private set; }

    internal void Start()
    {
      var Packet = ServerConnection.ReceivePacket();

      using (var Reader = Packet.ToReader())
      {
        var ClientMessage = (ClientMessage)Reader.ReadByte();
        if (ClientMessage != ClientMessage.StartApplication)
          throw new Exception("Invalid introductory message: " + ClientMessage);

        InvApplication.Device.Name = Reader.ReadString();
        InvApplication.Device.Model = Reader.ReadString();
        InvApplication.Device.System = Reader.ReadString();
        InvApplication.Device.Keyboard = Reader.ReadBoolean();
        InvApplication.Device.Mouse = Reader.ReadBoolean();
        InvApplication.Device.Touch = Reader.ReadBoolean();
        InvApplication.Device.ProportionalFontName = Reader.ReadString();
        InvApplication.Device.MonospacedFontName = Reader.ReadString();

        InvApplication.Process.Id = Reader.ReadInt32();

        InvApplication.Window.Width = Reader.ReadInt32();
        InvApplication.Window.Height = Reader.ReadInt32();

        this.PhoneIsSupported = Reader.ReadBoolean();
        this.LocationIsSupported = Reader.ReadBoolean();
        this.CalendarTimeZoneName = Reader.ReadString();
      }

      ServerQueue.Start();

      InvApplication.StartInvoke();

      ProcessChanges();

      ServerSender.Return(ServerMessage.ConfirmStartApplication);
    }
    internal void Stop()
    {
      WebChannel.Drop();

      ServerQueue.Stop();

      InvApplication.StopInvoke();
    }
    internal void Reclamation()
    {
      ServerContract.MemoryReclamation();
    }
    internal void PlaySound(Sound Sound, float Volume, float Rate)
    {
      ServerContract.PlaySound(TranslateSoundTag(Sound), Volume, Rate);
    }
    internal void PlayClip(AudioClip Clip)
    {
      ServerContract.PlayClip(TranslateClipTag(Clip), TranslateSoundTag(Clip.Sound), Clip.Volume, Clip.Rate, Clip.Loop);
    }
    internal void StopClip(AudioClip Clip)
    {
      var ServerClip = Clip.Node as ServerClip;

      if (ServerClip != null)
        ServerContract.StopClip(ServerClip.Tag);
    }
    internal void WebLaunchUri(Uri Uri)
    {
      ServerContract.LaunchWebUri(Uri);
    }
    internal void MarketBrowse(string AppleiTunesID, string GooglePlayID, string WindowsStoreID)
    {
      ServerContract.BrowseMarket(AppleiTunesID, GooglePlayID, WindowsStoreID);
    }
    internal void CalendarShowPicker(CalendarPicker CalendarPicker)
    {
      var ServerPicker = (ServerPicker)CalendarPicker.Node;

      if (ServerPicker == null)
      {
        ServerPicker = new ServerPicker(new ServerPickerTag(++LastPickerID));

        PickerDictionary.Add(ServerPicker.Tag, CalendarPicker);

        CalendarPicker.Node = ServerPicker;
      }

      ServerContract.ShowCalendarPicker(ServerPicker.Tag, CalendarPicker.SetDate, CalendarPicker.SetTime, CalendarPicker.Value);
    }
    internal void DirectoryShowPicker(DirectoryFilePicker FilePicker)
    {
      var ServerPicker = (ServerPicker)FilePicker.Node;

      if (ServerPicker == null)
      {
        ServerPicker = new ServerPicker(new ServerPickerTag(++LastPickerID));

        PickerDictionary.Add(ServerPicker.Tag, FilePicker);

        FilePicker.Node = ServerPicker;
      }

      ServerContract.ShowDirectoryPicker(ServerPicker.Tag, FilePicker.Title, FilePicker.Type);
    }
    internal bool EmailSendMessage(EmailMessage EmailMessage)
    {
      var ToArray = EmailMessage.GetTos().ToArray();
      var ServerToArray = new ServerEmailTo[ToArray.Length];
      var ToIndex = 0;
      foreach (var To in ToArray)
      {
        ServerToArray[ToIndex++] = new ServerEmailTo()
        {
          Name = To.Name,
          Address = To.Address
        };
      }

      var AttachmentArray = EmailMessage.GetAttachments().ToArray();
      var ServerAttachmentArray = new ServerEmailAttachment[AttachmentArray.Length];
      var AttachmentIndex = 0;
      foreach (var Attachment in AttachmentArray)
      {
        ServerAttachmentArray[AttachmentIndex++] = new ServerEmailAttachment()
        {
          Name = Attachment.Name,
          Content = Attachment.File.ReadAllBytes()
        };
      }

      return ServerContract.SendEmailMessage(EmailMessage.Subject, EmailMessage.Body, ServerToArray, ServerAttachmentArray);
    }
    internal void PhoneDial(string PhoneNumber)
    {
      ServerContract.DialPhone(PhoneNumber);
    }
    internal void PhoneSMS(string PhoneNumber)
    {
      ServerContract.SMSPhone(PhoneNumber);
    }
    internal void LocationLookup(LocationLookup LocationLookup)
    {
      //ServerContract.LookupLocation(LocationLookup);
    }
    internal void WindowBrowse(File File)
    {
      throw new NotImplementedException();
      //ServerContract.WindowBrowse(File);
    }
    internal void Process()
    {
      try
      {
        Action[] PostActionArray;
        lock (PostActionList)
        {
          if (PostActionList.Count > 0)
          {
            PostActionArray = PostActionList.ToArray();

            PostActionList.Clear();
          }
          else
          {
            PostActionArray = null;
          }
        }

        if (PostActionArray != null)
        {
          foreach (var PostAction in PostActionArray)
            PostAction();
        }

        ProcessMessages();

        ProcessCompact();

        ServerSender.Send();
      }
      catch (Exception Exception)
      {
        InvApplication.HandleExceptionInvoke(Exception);
      }

      InvApplication.Window.DisplayRate.Calculate();
    }
    internal void ProcessMessages()
    {
      InvApplication.CheckThreadAffinity();

      var ReceivePacket = ServerQueue.TryReceivePacket();

      if (ReceivePacket == null)
      {
        ProcessChanges();
      }
      else
      {
        while (ReceivePacket != null)
        {
          using (var Reader = ReceivePacket.ToReader())
          {
            var Message = (ClientMessage)Reader.ReadByte();

            ProcessMessage(Message, Reader);
          }

          ReceivePacket = ServerQueue.TryReceivePacket();
        }
      }
    }
    internal void ProcessMessage(ClientMessage Message, ServerReader Reader)
    {
      InvApplication.CheckThreadAffinity();

      var ReceiveAction = ClientArray[Message];

      if (ReceiveAction == null)
        throw new Exception("ClientMessage not handled: " + Message);

      ReceiveAction(Reader);
    }
    internal void Post(Action Action)
    {
      lock (PostActionList)
        PostActionList.Add(Action);
    }
    internal void Call(Action Action)
    {
      throw new NotImplementedException();
    }
    internal IEnumerable<Inv.Image> GetImages()
    {
      foreach (var ImageReference in ImageDictionary.Values)
      {
        Inv.Image Image;
        if (ImageReference.TryGetTarget(out Image))
          yield return Image;
      }
    }
    internal IEnumerable<Inv.Sound> GetSounds()
    {
      foreach (var SoundReference in SoundDictionary.Values)
      {
        Inv.Sound Sound;
        if (SoundReference.TryGetTarget(out Sound))
          yield return Sound;
      }
    }
    
    private void ProcessChanges()
    {
      if (InvApplication.IsExit)
      {
        ServerContract.ExitApplication();
      }
      else
      {
        var InvWindow = InvApplication.Window;

        InvWindow.ProcessInvoke();

        if (InvWindow.ActiveTimerSet.Count > 0)
        {
          foreach (var InvTimer in InvWindow.ActiveTimerSet)
          {
            var ServerTimer = AccessTimer(InvTimer, S =>
            {
              var Result = new ServerTimer();
              Result.IntervalEvent += () => S.IntervalInvoke();
              return Result;
            });

            if (InvTimer.IsRestarting)
            {
              InvTimer.IsRestarting = false;
              ServerTimer.Stop();
            }

            if (ServerTimer.IntervalTime != InvTimer.IntervalTime)
              ServerTimer.IntervalTime = InvTimer.IntervalTime;

            if (InvTimer.IsEnabled && !ServerTimer.IsEnabled)
              ServerTimer.Start();
            else if (!InvTimer.IsEnabled && ServerTimer.IsEnabled)
              ServerTimer.Stop();
          }

          InvWindow.ActiveTimerSet.RemoveWhere(T => !T.IsEnabled);
        }

        var InvSurfaceActive = InvWindow.ActiveSurface;

        if (InvSurfaceActive != null)
        {
          var ServerSurface = AccessSurface(InvSurfaceActive);

          if (this.ActiveSurface != ServerSurface)
          {
            this.ActiveSurface = ServerSurface;

            InvSurfaceActive.ArrangeInvoke();
          }

          ProcessTransition(ServerSurface);

          InvSurfaceActive.ComposeInvoke();

          UpdateSurface(InvSurfaceActive, ServerSurface);

          if (InvSurfaceActive != null)
            ProcessAnimation(ServerSurface, InvSurfaceActive);
        }
        else
        {
          this.ActiveSurface = null;
        }

        if (InvWindow.Render())
        {
          if (InvWindow.Background.Render())
            ServerContract.SetWindowBackground(InvWindow.Background.Colour);
        }
      }
    }
    private void ProcessTransition(ServerSurface ServerSurface)
    {
      var InvWindow = InvApplication.Window;

      var InvTransition = InvWindow.ActiveTransition;
      if (InvTransition == null)
      {
        Debug.Assert(ActiveSurface == null || InvWindow.ActiveSurface == null || InvWindow.ActiveSurface.Node == ActiveSurface);
      }
      else
      {
        if (InvTransition.FromSurface != null && InvTransition.FromSurface.Node is Inv.ServerSurface)
          UpdateSurface(InvTransition.FromSurface, (Inv.ServerSurface)InvTransition.FromSurface.Node);

        ServerContract.TransitionSurface(ServerSurface.Tag, InvTransition.Animation, InvTransition.Duration);

        InvWindow.ActiveTransition = null;
      }
    }
    private void ProcessAnimation(ServerSurface ServerSurface, Inv.Surface InvSurface)
    {
      if (InvSurface.StopAnimationSet.Count > 0)
      {
        foreach (var StopAnimation in InvSurface.StopAnimationSet)
        {
          var AnimationTag = TranslateAnimationTag(StopAnimation);

          ServerContract.StopAnimation(AnimationTag);
        }

        InvSurface.StopAnimationSet.Clear();
      }
      else if (InvSurface.StartAnimationSet.Count > 0)
      {
        foreach (var StartAnimation in InvSurface.StartAnimationSet)
        {
          var StartAnimationID = TranslateAnimationTag(StartAnimation);

          var ServerTargetArray = new ServerAnimationTarget[StartAnimation.TargetCount];

          var TargetIndex = 0;
          foreach (var Target in StartAnimation.GetTargets())
          {
            var ServerCommandArray = new ServerAnimationCommand[Target.CommandCount];

            var CommandIndex = 0;
            foreach (var Command in Target.GetCommands())
            {
              var ServerCommand = new ServerAnimationCommand()
              {
                Type = Command.Type
              };

              switch (Command.Type)
              {
                case AnimationType.Opacity:
                  var OpacityCommand = (AnimationOpacityCommand)Command;
                  ServerCommand.OpacityOffset = OpacityCommand.Offset;
                  ServerCommand.OpacityDuration = OpacityCommand.Duration;
                  ServerCommand.OpacityFrom = OpacityCommand.From;
                  ServerCommand.OpacityTo = OpacityCommand.To;
                  break;

                default:
                  throw new Exception("AnimationType not handled: " + Command.Type);
              }

              ServerCommandArray[CommandIndex++] = ServerCommand;
            }

            ServerTargetArray[TargetIndex++] = new ServerAnimationTarget()
            {
              PanelTag = TranslatePanelTag(ServerSurface, Target.Panel),
              CommandArray = ServerCommandArray
            };;
          }

          ServerContract.StartAnimation(ServerSurface.Tag, StartAnimationID, ServerTargetArray);
        }

        InvSurface.StartAnimationSet.Clear();
      }
    }
    private void ProcessCompact()
    {
      CompactDictionary(SurfaceDictionary, T => ServerContract.DisposeSurface(T));
      CompactDictionary(PanelDictionary, T => ServerContract.DisposePanel(T));
      CompactDictionary(ImageDictionary, T => ServerContract.DisposeImage(T));
      CompactDictionary(SoundDictionary, T => ServerContract.DisposeSound(T));
    }
    private void UpdateSurface(Surface InvSurface, ServerSurface ServerSurface)
    {
      if (InvSurface.Render())
      {
        if (InvSurface.Background.Render())
          ServerContract.SetSurfaceBackground(ServerSurface.Tag, InvSurface.Background.Colour);

        var ContentPanel = TranslatePanel(ServerSurface, InvSurface.Content);
        ServerContract.SetSurfaceContent(ServerSurface.Tag, GetPanelTag(ContentPanel));
      }

      InvSurface.ProcessChanges(P => TranslatePanel(ServerSurface, P));
    }
    private ServerTimer AccessTimer(Inv.Timer InvTimer, Func<Inv.Timer, ServerTimer> BuildFunction)
    {
      if (InvTimer.Node == null)
      {
        var Result = BuildFunction(InvTimer);

        InvTimer.Node = Result;

        return Result;
      }
      else
      {
        return (ServerTimer)InvTimer.Node;
      }
    }
    private ServerSurface AccessSurface(Inv.Surface InvSurface)
    {
      if (InvSurface.Node == null)
      {
        var Result = new ServerSurface(NextSurfaceTag());

        InvSurface.Node = Result;

        SurfaceDictionary.Add(Result.Tag, InvSurface.AsWeakReference());

        ServerContract.NewSurface(Result.Tag);

        return Result;
      }
      else
      {
        return (ServerSurface)InvSurface.Node;
      }
    }

    private ServerPanel TranslatePanel(ServerSurface ServerSurface, Inv.Panel InvPanel)
    {
      if (InvPanel == null)
        return null;
      else
        return RouteArray[InvPanel.PanelType](ServerSurface, InvPanel);
    }
    private void RenderPanel(Inv.Panel InvPanel, ServerPanel ServerElement, Action Action)
    {
      if (InvPanel.Render())
        Action();
    }
    private TElement AccessPanel<TPanel, TElement>(ServerSurface Surface, TPanel InvPanel, Func<TPanel, TElement> BuildFunction)
      where TPanel : Inv.Panel
      where TElement : ServerPanel
    {
      if (InvPanel.Node == null)
      {
        var Result = BuildFunction(InvPanel);
        
        InvPanel.Node = Result;

        PanelDictionary.Add(Result.Tag, InvPanel.AsWeakReference<Inv.Panel>());

        return Result;
      }
      else
      {
        return (TElement)InvPanel.Node;
      }
    }
    private ServerPanel TranslateBrowser(ServerSurface ServerSurface, Inv.Panel InvPanel)
    {
      var InvBrowser = (Inv.Browser)InvPanel;

      var ServerBrowser = AccessPanel(ServerSurface, InvBrowser, P =>
      {
        var Result = new ServerBrowser(NextPanelTag());

        ServerContract.NewBrowser(ServerSurface.Tag, Result.Tag);

        return Result;
      });

      RenderPanel(InvBrowser, ServerBrowser, () =>
      {
        TranslateLayout(InvBrowser, ServerBrowser);

        if ((InvBrowser.Html != null && ServerBrowser.Html != InvBrowser.Html) || (InvBrowser.Uri != null && ServerBrowser.Uri != InvBrowser.Uri))
        {
          ServerBrowser.Html = InvBrowser.Html;
          ServerBrowser.Uri = InvBrowser.Uri;

          ServerContract.LoadBrowser(ServerBrowser.Tag, InvBrowser.Uri, InvBrowser.Html);
        }
      });

      return ServerBrowser;
    }
    private ServerPanel TranslateButton(ServerSurface ServerSurface, Inv.Panel InvPanel)
    {
      var InvButton = (Inv.Button)InvPanel;

      var ServerButton = AccessPanel(ServerSurface, InvButton, P =>
      {
        var Result = new ServerButton(NextPanelTag());

        ServerContract.NewButton(ServerSurface.Tag, Result.Tag);

        return Result;
      });

      RenderPanel(InvButton, ServerButton, () =>
      {
        TranslateLayout(InvButton, ServerButton);

        if (ServerButton.IsEnabled != InvButton.IsEnabled)
        {
          ServerButton.IsEnabled = InvButton.IsEnabled;
          ServerContract.SetButtonIsEnabled(ServerButton.Tag, InvButton.IsEnabled);
        }

        if (ServerButton.IsFocusable != InvButton.IsFocusable)
        {
          ServerButton.IsFocusable = InvButton.IsFocusable;
          ServerContract.SetButtonIsFocusable(ServerButton.Tag, InvButton.IsFocusable);
        }

        if (ServerButton.HasPress != InvButton.HasPress)
        {
          ServerButton.HasPress = InvButton.HasPress;
          ServerContract.SetButtonHasPress(ServerButton.Tag, InvButton.HasPress);
        }

        if (ServerButton.HasRelease != InvButton.HasRelease)
        {
          ServerButton.HasRelease = InvButton.HasRelease;
          ServerContract.SetButtonHasRelease(ServerButton.Tag, InvButton.HasRelease);
        }

        if (InvButton.ContentSingleton.Render())
        {
          var ContentPanel = TranslatePanel(ServerSurface, InvButton.ContentSingleton.Data);
          ServerContract.SetButtonContent(ServerButton.Tag, GetPanelTag(ContentPanel));
        }
      });

      return ServerButton;
    }
    private ServerPanel TranslateBoard(ServerSurface ServerSurface, Inv.Panel InvPanel)
    {
      var InvBoard = (Inv.Board)InvPanel;

      var ServerBoard = AccessPanel(ServerSurface, InvBoard, P =>
      {
        var Result = new ServerBoard(NextPanelTag());

        ServerContract.NewBoard(ServerSurface.Tag, Result.Tag);

        return Result;
      });

      RenderPanel(InvBoard, ServerBoard, () =>
      {
        TranslateLayout(InvBoard, ServerBoard);

        if (InvBoard.PinCollection.Render())
        {
          var PinArray = new ServerBoardPin[InvBoard.PinCollection.Count];
          var PinIndex = 0;
          foreach (var InvPin in InvBoard.PinCollection)
          {
            PinArray[PinIndex++] = new ServerBoardPin()
            {
              Rect = InvPin.Rect,
              PanelTag = TranslatePanelTag(ServerSurface, InvPin.Panel)
            };
          }

          ServerContract.SetBoardCollection(ServerBoard.Tag, PinArray);
        }
      });

      return ServerBoard;
    }
    private ServerPanel TranslateDock(ServerSurface ServerSurface, Inv.Panel InvPanel)
    {
      var InvDock = (Inv.Dock)InvPanel;

      var ServerDock = AccessPanel(ServerSurface, InvDock, P =>
      {
        var Result = new ServerDock(NextPanelTag());

        ServerContract.NewDock(ServerSurface.Tag, Result.Tag, P.Orientation);

        return Result;
      });

      RenderPanel(InvDock, ServerDock, () =>
      {
        TranslateLayout(InvDock, ServerDock);

        if (InvDock.CollectionRender())
        {
          var HeaderArray = new ServerPanelTag[InvDock.HeaderCollection.Count];
          var HeaderIndex = 0;
          foreach (var InvElement in InvDock.HeaderCollection)
            HeaderArray[HeaderIndex++] = TranslatePanelTag(ServerSurface, InvElement);

          var ClientArray = new ServerPanelTag[InvDock.ClientCollection.Count];
          var ClientIndex = 0;
          foreach (var InvElement in InvDock.ClientCollection)
            ClientArray[ClientIndex++] = TranslatePanelTag(ServerSurface, InvElement);

          var FooterArray = new ServerPanelTag[InvDock.FooterCollection.Count];
          var FooterIndex = 0;
          foreach (var InvElement in InvDock.FooterCollection)
            FooterArray[FooterIndex++] = TranslatePanelTag(ServerSurface, InvElement);

          ServerContract.SetDockCollection(ServerDock.Tag, HeaderArray, ClientArray, FooterArray);
        }
      });

      return ServerDock;
    }
    private ServerPanel TranslateEdit(ServerSurface ServerSurface, Inv.Panel InvPanel)
    {
      var InvEdit = (Inv.Edit)InvPanel;

      var ServerEdit = AccessPanel(ServerSurface, InvEdit, P =>
      {
        var Result = new ServerEdit(NextPanelTag());

        ServerContract.NewEdit(ServerSurface.Tag, Result.Tag, P.Input);

        return Result;
      });

      RenderPanel(InvEdit, ServerEdit, () =>
      {
        TranslateLayout(InvEdit, ServerEdit);

        if (ServerEdit.HasChange != InvEdit.HasChange)
        {
          ServerEdit.HasChange = InvEdit.HasChange;
          ServerContract.SetEditHasChange(ServerEdit.Tag, InvEdit.HasChange);
        }

        if (ServerEdit.HasReturn != InvEdit.HasReturn)
        {
          ServerEdit.HasReturn = InvEdit.HasReturn;
          ServerContract.SetEditHasReturn(ServerEdit.Tag, InvEdit.HasReturn);
        }

        if (InvEdit.IsReadOnly != ServerEdit.IsReadOnly)
        {
          ServerEdit.IsReadOnly = InvEdit.IsReadOnly;
          ServerContract.SetEditIsReadOnly(ServerEdit.Tag, InvEdit.IsReadOnly);
        }

        var InvFont = InvEdit.Font;
        if (InvFont.Render())
          ServerContract.SetEditFont(ServerEdit.Tag, InvFont.Name, InvFont.Size, InvFont.Colour, InvFont.Weight);

        if (InvEdit.Text != ServerEdit.Text)
        {
          ServerEdit.Text = InvEdit.Text;
          ServerContract.SetEditText(ServerEdit.Tag, InvEdit.Text);
        }
      });

      return ServerEdit;
    }
    private ServerPanel TranslateFlow(ServerSurface ServerSurface, Inv.Panel InvPanel)
    {
      var InvFlow = (Inv.Flow)InvPanel;

      var ServerFlow = AccessPanel(ServerSurface, InvFlow, P =>
      {
        var Result = new ServerFlow(NextPanelTag());

        ServerContract.NewFlow(ServerSurface.Tag, Result.Tag);

        return Result;
      });

      RenderPanel(InvFlow, ServerFlow, () =>
      {
        TranslateLayout(InvFlow, ServerFlow);

        if (InvFlow.IsRefresh)
          InvFlow.IsRefresh = false;

        if (InvFlow.IsReload || InvFlow.ReloadSectionList.Count > 0 || InvFlow.ReloadItemList.Count > 0)
        {
          InvFlow.IsReload = false;
          InvFlow.ReloadSectionList.Clear(); // not supported.
          InvFlow.ReloadItemList.Clear(); // not supported.

          var SectionArray = new ServerFlowSection[InvFlow.SectionCount];
          var SectionIndex = 0;
          foreach (var Section in InvFlow.GetSections())
          {
            SectionArray[SectionIndex++] = new ServerFlowSection()
            {
              ItemCount = Section.ItemCount,
              HeaderPanelTag = GetPanelTag(TranslatePanel(ServerSurface, Section.Header)),
              FooterPanelTag = GetPanelTag(TranslatePanel(ServerSurface, Section.Footer))
            };
          }

          ServerContract.ReloadFlow(ServerFlow.Tag, SectionArray);
        }
      });

      return ServerFlow;
    }
    private ServerPanel TranslateFrame(ServerSurface ServerSurface, Inv.Panel InvPanel)
    {
      var InvFrame = (Inv.Frame)InvPanel;

      var ServerFrame = AccessPanel(ServerSurface, InvFrame, P =>
      {
        var Result = new ServerFrame(NextPanelTag());

        ServerContract.NewFrame(ServerSurface.Tag, Result.Tag);

        return Result;
      });

      RenderPanel(InvFrame, ServerFrame, () =>
      {
        TranslateLayout(InvFrame, ServerFrame);

        if (InvFrame.ContentSingleton.Render())
        {
          var ContentPanel = TranslatePanel(ServerSurface, InvFrame.ContentSingleton.Data);
          ServerContract.SetFrameContent(ServerFrame.Tag, GetPanelTag(ContentPanel));
        }
      });

      return ServerFrame;
    }
    private ServerPanel TranslateGraphic(ServerSurface ServerSurface, Inv.Panel InvPanel)
    {
      var InvGraphic = (Inv.Graphic)InvPanel;

      var ServerGraphic = AccessPanel(ServerSurface, InvGraphic, P =>
      {
        var Result = new ServerGraphic(NextPanelTag());

        ServerContract.NewGraphic(ServerSurface.Tag, Result.Tag);

        return Result;
      });

      RenderPanel(InvGraphic, ServerGraphic, () =>
      {
        TranslateLayout(InvGraphic, ServerGraphic);

        if (InvGraphic.ImageSingleton.Render())
          ServerContract.SetGraphicImage(ServerGraphic.Tag, TranslateImageTag(InvGraphic.ImageSingleton.Data));
      });

      return ServerGraphic;
    }
    private ServerPanel TranslateLabel(ServerSurface ServerSurface, Inv.Panel InvPanel)
    {
      var InvLabel = (Inv.Label)InvPanel;

      var ServerLabel = AccessPanel(ServerSurface, InvLabel, P =>
      {
        var Result = new ServerLabel(NextPanelTag());

        ServerContract.NewLabel(ServerSurface.Tag, Result.Tag);

        return Result;
      });

      RenderPanel(InvLabel, ServerLabel, () =>
      {
        TranslateLayout(InvLabel, ServerLabel);

        if (InvLabel.LineWrapping != ServerLabel.LineWrapping)
        {
          ServerLabel.LineWrapping = InvLabel.LineWrapping;
          ServerContract.SetLabelLineWrapping(ServerLabel.Tag, InvLabel.LineWrapping);
        }

        if (InvLabel.Justification != ServerLabel.Justification)
        {
          ServerLabel.Justification = InvLabel.Justification;
          ServerContract.SetLabelJustification(ServerLabel.Tag, InvLabel.Justification);
        }

        var InvFont = InvLabel.Font;
        if (InvFont.Render())
          ServerContract.SetLabelFont(ServerLabel.Tag, InvFont.Name, InvFont.Size, InvFont.Colour, InvFont.Weight);

        if (InvLabel.Text != ServerLabel.Text)
        {
          ServerLabel.Text = InvLabel.Text;
          ServerContract.SetLabelText(ServerLabel.Tag, InvLabel.Text);
        }
      });

      return ServerLabel;
    }
    private ServerPanel TranslateMemo(ServerSurface ServerSurface, Inv.Panel InvPanel)
    {
      var InvMemo = (Inv.Memo)InvPanel;

      var ServerMemo = AccessPanel(ServerSurface, InvMemo, P =>
      {
        var Result = new ServerMemo(NextPanelTag());

        ServerContract.NewMemo(ServerSurface.Tag, Result.Tag);

        return Result;
      });

      RenderPanel(InvMemo, ServerMemo, () =>
      {
        TranslateLayout(InvMemo, ServerMemo);

        if (ServerMemo.HasChange != InvMemo.HasChange)
        {
          ServerMemo.HasChange = InvMemo.HasChange;
          ServerContract.SetMemoHasChange(ServerMemo.Tag, InvMemo.HasChange);
        }

        if (InvMemo.IsReadOnly != ServerMemo.IsReadOnly)
        {
          ServerMemo.IsReadOnly = InvMemo.IsReadOnly;
          ServerContract.SetMemoIsReadOnly(ServerMemo.Tag, InvMemo.IsReadOnly);
        }

        var InvFont = InvMemo.Font;
        if (InvFont.Render())
          ServerContract.SetMemoFont(ServerMemo.Tag, InvFont.Name, InvFont.Size, InvFont.Colour, InvFont.Weight);

        if (InvMemo.Text != ServerMemo.Text)
        {
          ServerMemo.Text = InvMemo.Text;
          ServerContract.SetMemoText(ServerMemo.Tag, InvMemo.Text);
        }
      });

      return ServerMemo;
    }
    private ServerPanel TranslateOverlay(ServerSurface ServerSurface, Inv.Panel InvPanel)
    {
      var InvOverlay = (Inv.Overlay)InvPanel;

      var ServerOverlay = AccessPanel(ServerSurface, InvOverlay, P =>
      {
        var Result = new ServerOverlay(NextPanelTag());
        
        ServerContract.NewOverlay(ServerSurface.Tag, Result.Tag);

        return Result;
      });

      RenderPanel(InvOverlay, ServerOverlay, () =>
      {
        TranslateLayout(InvOverlay, ServerOverlay);

        if (InvOverlay.PanelCollection.Render())
        {
          var PanelArray = new ServerPanelTag[InvOverlay.PanelCollection.Count];

          var PanelIndex = 0;
          foreach (var InvElement in InvOverlay.PanelCollection)
            PanelArray[PanelIndex++] = TranslatePanelTag(ServerSurface, InvElement);

          ServerContract.SetOverlayCollection(ServerOverlay.Tag, PanelArray);
        }
      });

      return ServerOverlay;
    }
    private ServerPanel TranslateCanvas(ServerSurface ServerSurface, Inv.Panel InvPanel)
    {
      var InvCanvas = (Inv.Canvas)InvPanel;

      var ServerCanvas = AccessPanel(ServerSurface, InvCanvas, P =>
      {
        var Result = new ServerCanvas(NextPanelTag(), ServerContract, TranslateImageTag);

        ServerContract.NewCanvas(ServerSurface.Tag, Result.Tag);

        return Result;
      });

      RenderPanel(InvCanvas, ServerCanvas, () =>
      {
        TranslateLayout(InvCanvas, ServerCanvas);

        if (InvCanvas.Redrawing)
        {
          ServerCanvas.Begin();

          InvCanvas.DrawInvoke(ServerCanvas);

          var Result = ServerCanvas.End();
          if (Result != null) // is it changed since last time?
            ServerContract.DrawCanvas(ServerCanvas.Tag, Result);
        }
      });

      return ServerCanvas;
    }
    private ServerPanel TranslateScroll(ServerSurface ServerSurface, Inv.Panel InvPanel)
    {
      var InvScroll = (Inv.Scroll)InvPanel;

      var ServerScroll = AccessPanel(ServerSurface, InvScroll, P =>
      {
        var Result = new ServerScroll(NextPanelTag());

        ServerContract.NewScroll(ServerSurface.Tag, Result.Tag, P.Orientation);

        return Result;
      });

      RenderPanel(InvScroll, ServerScroll, () =>
      {
        TranslateLayout(InvScroll, ServerScroll);

        if (InvScroll.ContentSingleton.Render())
        {
          var ContentPanel = TranslatePanel(ServerSurface, InvScroll.ContentSingleton.Data);
          ServerContract.SetScrollContent(ServerScroll.Tag, GetPanelTag(ContentPanel));
        }
      });

      return ServerScroll;
    }
    private ServerPanel TranslateStack(ServerSurface ServerSurface, Inv.Panel InvPanel)
    {
      var InvStack = (Inv.Stack)InvPanel;

      var ServerStack = AccessPanel(ServerSurface, InvStack, P =>
      {
        var Result = new ServerStack(NextPanelTag());

        ServerContract.NewStack(ServerSurface.Tag, Result.Tag, P.Orientation);

        return Result;
      });

      RenderPanel(InvStack, ServerStack, () =>
      {
        TranslateLayout(InvStack, ServerStack);

        if (InvStack.PanelCollection.Render())
        {
          var PanelArray = new ServerPanelTag[InvStack.PanelCollection.Count];
          var PanelIndex = 0;
          foreach (var InvElement in InvStack.PanelCollection)
            PanelArray[PanelIndex++] = TranslatePanelTag(ServerSurface, InvElement);

          ServerContract.SetStackCollection(ServerStack.Tag, PanelArray);
        }
      });

      return ServerStack;
    }
    private ServerPanel TranslateTable(ServerSurface ServerSurface, Inv.Panel InvPanel)
    {
      var InvTable = (Inv.Table)InvPanel;

      var ServerTable = AccessPanel(ServerSurface, InvTable, P =>
      {
        var Result = new ServerTable(NextPanelTag());

        ServerContract.NewTable(ServerSurface.Tag, Result.Tag);

        return Result;
      });

      RenderPanel(InvTable, ServerTable, () =>
      {
        TranslateLayout(InvTable, ServerTable);

        if (InvTable.CollectionRender())
        {
          var RowArray = new ServerTableAxis[InvTable.RowCollection.Count];
          var RowIndex = 0;
          foreach (var Row in InvTable.RowCollection)
          {
            RowArray[RowIndex++] = new ServerTableAxis()
            {
              LengthType = Row.LengthType,
              LengthValue = Row.LengthValue,
              PanelTag = GetPanelTag(TranslatePanel(ServerSurface, Row.Content))
            };
          }

          var ColumnArray = new ServerTableAxis[InvTable.ColumnCollection.Count];
          var ColumnIndex = 0;
          foreach (var Column in InvTable.ColumnCollection)
          {
            ColumnArray[ColumnIndex++] = new ServerTableAxis()
            {
              LengthType = Column.LengthType,
              LengthValue = Column.LengthValue,
              PanelTag = GetPanelTag(TranslatePanel(ServerSurface, Column.Content))
            };
          }

          var CellArray = new ServerTableCell[InvTable.CellCollection.Count];
          var CellIndex = 0;
          foreach (var Cell in InvTable.CellCollection)
          {
            if (Cell.Content != null)
            {
              CellArray[CellIndex++] = new ServerTableCell()
              {
                X = Cell.X,
                Y = Cell.Y,
                PanelTag = TranslatePanelTag(ServerSurface, Cell.Content)
              };
            }
          }

          ServerContract.SetTableCollection(ServerTable.Tag, RowArray, ColumnArray, CellArray);
        }
      });

      return ServerTable;
    }
    private void TranslateLayout(Inv.Panel InvPanel, ServerPanel ServerPanel)
    {
      var InvAlignment = InvPanel.Alignment;
      if (InvAlignment.Render())
        ServerContract.SetPanelAlignment(ServerPanel.Tag, InvAlignment.Get());

      var InvBorder = InvPanel.Border;
      if (InvBorder.Render())
        ServerContract.SetPanelBorder(ServerPanel.Tag, InvBorder.Left, InvBorder.Top, InvBorder.Right, InvBorder.Bottom, InvBorder.Colour);

      var InvCorner = InvPanel.Corner;
      if (InvCorner.Render())
        ServerContract.SetPanelCorner(ServerPanel.Tag, InvCorner.TopLeft, InvCorner.TopRight, InvCorner.BottomRight, InvCorner.BottomLeft);

      var InvElevation = InvPanel.Elevation;
      if (InvElevation.Render())
        ServerContract.SetPanelElevation(ServerPanel.Tag, InvElevation.Get());
      
      var InvMargin = InvPanel.Margin;
      if (InvMargin.Render())
        ServerContract.SetPanelMargin(ServerPanel.Tag, InvMargin.Left, InvMargin.Top, InvMargin.Right, InvMargin.Bottom);

      var InvOpacity = InvPanel.Opacity;
      if (InvOpacity.Render())
        ServerContract.SetPanelOpacity(ServerPanel.Tag, InvOpacity.Get());

      var InvPadding = InvPanel.Padding;
      if (InvPadding.Render())
        ServerContract.SetPanelPadding(ServerPanel.Tag, InvPadding.Left, InvPadding.Top, InvPadding.Right, InvPadding.Bottom);

      var InvSize = InvPanel.Size;
      if (InvSize.Render())
        ServerContract.SetPanelSize(ServerPanel.Tag, InvSize.Width, InvSize.Height, InvSize.MinimumWidth, InvSize.MinimumHeight, InvSize.MaximumWidth, InvSize.MaximumHeight);

      var InvVisibility = InvPanel.Visibility;
      if (InvVisibility.Render())
        ServerContract.SetPanelVisibility(ServerPanel.Tag, InvVisibility.Get());

      var InvBackground = InvPanel.Background;
      if (InvBackground.Render())
        ServerContract.SetPanelBackground(ServerPanel.Tag, InvBackground.Colour);
    }
    private ServerPanelTag GetPanelTag(ServerPanel ServerPanel)
    {
      return ServerPanel != null ? ServerPanel.Tag : ServerPanelTag.Zero;
    }
    private ServerPanelTag TranslatePanelTag(ServerSurface Surface, Inv.Panel InvPanel)
    {
      var Result = TranslatePanel(Surface, InvPanel);
      
      return Result != null ? Result.Tag : ServerPanelTag.Zero;
    }
    private ServerImageTag TranslateImageTag(Inv.Image InvImage)
    {
      if (InvImage == null)
      {
        return ServerImageTag.Zero;
      }
      else
      {
        var ServerImage = InvImage.Node as ServerImage;

        if (ServerImage == null || ServerImage.Engine != ServerEngine)
        {
          ServerImage = ServerEngine.NextImage();
          InvImage.Node = ServerImage;
        }

        if (!ImageDictionary.ContainsKey(ServerImage.Tag))
        {
          ServerContract.NewImage(ServerImage.Tag, InvImage);

          ImageDictionary.Add(ServerImage.Tag, InvImage.AsWeakReference());
        }

        return ServerImage.Tag;
      }
    }
    private ServerSoundTag TranslateSoundTag(Inv.Sound InvSound)
    {
      if (InvSound == null)
      {
        return ServerSoundTag.Zero;
      }
      else
      {
        var ServerSound = InvSound.Node as ServerSound;

        if (ServerSound == null || ServerSound.Engine != ServerEngine)
        {
          ServerSound = ServerEngine.NextSound();
          InvSound.Node = ServerSound;
        }

        if (!SoundDictionary.ContainsKey(ServerSound.Tag))
        {
          ServerContract.NewSound(ServerSound.Tag, InvSound);

          SoundDictionary.Add(ServerSound.Tag, InvSound.AsWeakReference());
        }

        return ServerSound.Tag;
      }
    }
    private ServerClipTag TranslateClipTag(Inv.AudioClip InvClip)
    {
      if (InvClip == null)
      {
        return ServerClipTag.Zero;
      }
      else
      {
        var ServerClip = InvClip.Node as ServerClip;

        if (ServerClip == null || ServerClip.Engine != ServerEngine)
        {
          ServerClip = new ServerClip(ServerEngine, new ServerClipTag(++LastClipID));
          InvClip.Node = ServerClip;

          ClipDictionary.Add(ServerClip.Tag, InvClip);
        }

        return ServerClip.Tag;
      }
    }
    private ServerAnimationTag TranslateAnimationTag(Inv.Animation InvAnimation)
    {
      if (InvAnimation == null)
      {
        return ServerAnimationTag.Zero;
      }
      else
      {
        var ServerAnimation = (ServerAnimation)InvAnimation.Node;

        if (ServerAnimation == null || ServerAnimation.Engine != ServerEngine)
        {
          ServerAnimation = new ServerAnimation(ServerEngine, new ServerAnimationTag(++LastAnimationID));
          InvAnimation.Node = ServerAnimation;

          AnimationDictionary.Add(ServerAnimation.Tag, InvAnimation);
        }

        return ServerAnimation.Tag;
      }
    }

    private void ReceiveSuspendApplication(ServerReader Reader)
    {
      ReceiveProcess(() =>
      {
        InvApplication.SuspendInvoke();
      }, ServerMessage.ConfirmSuspend);
    }
    private void ReceiveResumeApplication(ServerReader Reader)
    {
      ReceiveProcess(() =>
      {
        InvApplication.ResumeInvoke();
      }, ServerMessage.ConfirmResume);
    }
    private void ReceiveExitQueryApplication(ServerReader Reader)
    {
      var Result = false;

      ReceiveProcess(() => 
      {
        Result = InvApplication.ExitQueryInvoke();
      },
      ServerMessage.ConfirmExitQuery, 
      Writer => 
      {
        Writer.WriteBoolean(Result);
      });
    }
    private void ReceiveHandleExceptionApplication(ServerReader Reader)
    {
      var ExceptionReport = Reader.ReadString();

      // TODO: reconstruct client exceptions?

      ReceiveProcess(() =>
      {
        InvApplication.HandleExceptionInvoke(new Exception(ExceptionReport));
      }, ServerMessage.ConfirmHandleException);
    }
    private void ReceiveKeyModifierWindow(ServerReader Reader)
    {
      var UpdateFlags = (KeyModifierFlags)Reader.ReadByte();
      var UpdateModifier = new KeyModifier(UpdateFlags);

      ReceiveProcess(() =>
      {
        InvApplication.Window.CheckModifier(UpdateModifier);
      }, ServerMessage.ConfirmKeyModifierWindow);
    }
    private void ReceiveGestureBackwardSurface(ServerReader Reader)
    {
      var SurfaceTag = Reader.ReadSurfaceTag();
      var Surface = GetSurface(SurfaceTag);

      ReceiveProcess(() =>
      {
        if (Surface != null)
          Surface.GestureBackwardInvoke();
      }, ServerMessage.ConfirmGestureBackwardSurface);
    }
    private void ReceiveGestureForwardSurface(ServerReader Reader)
    {
      var SurfaceTag = Reader.ReadSurfaceTag();
      var Surface = GetSurface(SurfaceTag);

      ReceiveProcess(() =>
      {
        if (Surface != null)
          Surface.GestureForwardInvoke();
      }, ServerMessage.ConfirmGestureForwardSurface);
    }
    private void ReceiveKeystrokeSurface(ServerReader Reader)
    {
      var SurfaceTag = Reader.ReadSurfaceTag();
      var Surface = GetSurface(SurfaceTag);
      
      var Keystroke = new Keystroke();
      Keystroke.Key = (Key)Reader.ReadInt32();
      Keystroke.Modifier = new KeyModifier((KeyModifierFlags)Reader.ReadByte());

      ReceiveProcess(() =>
      {
        if (Surface != null)
          Surface.KeystrokeInvoke(Keystroke);
      }, ServerMessage.ConfirmKeystrokeSurface);
    }
    private void ReceiveArrangeSurface(ServerReader Reader)
    {
      var SurfaceTag = Reader.ReadSurfaceTag();
      var Surface = GetSurface(SurfaceTag);

      InvApplication.Window.Width = Reader.ReadInt32();
      InvApplication.Window.Height = Reader.ReadInt32();

      ReceiveProcess(() =>
      {
        if (Surface != null)
          Surface.ArrangeInvoke();
      }, ServerMessage.ConfirmArrangeSurface);
    }
    private void ReceiveButtonPress(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();

      var Button = GetPanel<Button>(PanelTag);

      ReceiveProcess(() =>
      {
        Button.PressInvoke();
      }, ServerMessage.ConfirmButtonPress);
    }
    private void ReceiveButtonRelease(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();

      var Button = GetPanel<Button>(PanelTag);

      ReceiveProcess(() =>
      {
        Button.ReleaseInvoke();
      }, ServerMessage.ConfirmButtonRelease);
    }
    private void ReceiveButtonSingleTap(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Button = GetPanel<Button>(PanelTag);

      ReceiveProcess(() =>
      {
        Button.SingleTapInvoke();
      }, ServerMessage.ConfirmButtonSingleTap);
    }
    private void ReceiveButtonContextTap(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();

      var Button = GetPanel<Button>(PanelTag);

      ReceiveProcess(() =>
      {
        Button.ContextTapInvoke();
      }, ServerMessage.ConfirmButtonContextTap);
    }
    private void ReceiveEditChange(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Text = Reader.ReadString();

      var Edit = GetPanel<Edit>(PanelTag);

      // the client already knows the text has been updated.
      // we need to fire the change event, but we don't need to call SetEditText back to the client.
      var ServerEdit = (ServerEdit)Edit.Node;
      ServerEdit.Text = Text;

      ReceiveProcess(() =>
      {
        Edit.ChangeText(Text);
      }, ServerMessage.ConfirmEditChange);
    }
    private void ReceiveEditReturn(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();

      var Edit = GetPanel<Edit>(PanelTag);

      ReceiveProcess(() =>
      {
        Edit.Return();
      }, ServerMessage.ConfirmEditReturn);
    }
    private void ReceiveMemoChange(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Text = Reader.ReadString();

      var Memo = GetPanel<Memo>(PanelTag);

      // the client already knows the text has been updated.
      // we need to fire the change event, but we don't need to call SetMemoText back to the client.
      var ServerMemo = (ServerMemo)Memo.Node;
      ServerMemo.Text = Text;

      ReceiveProcess(() =>
      {
        Memo.ChangeText(Text);
      }, ServerMessage.ConfirmMemoChange);
    }
    private void ReceiveCanvasPress(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var PointX = Reader.ReadInt32();
      var PointY = Reader.ReadInt32();

      var Canvas = GetPanel<Canvas>(PanelTag);

      ReceiveProcess(() =>
      {
        Canvas.PressInvoke(new Inv.Point(PointX, PointY));
      }, ServerMessage.ConfirmCanvasPress);
    }
    private void ReceiveCanvasRelease(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var PointX = Reader.ReadInt32();
      var PointY = Reader.ReadInt32();

      var Canvas = GetPanel<Canvas>(PanelTag);
      ReceiveProcess(() =>
      {
        Canvas.ReleaseInvoke(new Inv.Point(PointX, PointY));
      }, ServerMessage.ConfirmCanvasRelease);
    }
    private void ReceiveCanvasSingleTap(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var PointX = Reader.ReadInt32();
      var PointY = Reader.ReadInt32();

      var Canvas = GetPanel<Canvas>(PanelTag);

      ReceiveProcess(() =>
      {
        Canvas.SingleTapInvoke(new Inv.Point(PointX, PointY));
      }, ServerMessage.ConfirmCanvasSingleTap);
    }
    private void ReceiveCanvasDoubleTap(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var PointX = Reader.ReadInt32();
      var PointY = Reader.ReadInt32();

      var Canvas = GetPanel<Canvas>(PanelTag);

      ReceiveProcess(() =>
      {
        Canvas.DoubleTapInvoke(new Inv.Point(PointX, PointY));
      }, ServerMessage.ConfirmCanvasDoubleTap);
    }
    private void ReceiveCanvasContextTap(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var PointX = Reader.ReadInt32();
      var PointY = Reader.ReadInt32();

      var Canvas = GetPanel<Canvas>(PanelTag);

      ReceiveProcess(() =>
      {
        Canvas.ContextTapInvoke(new Inv.Point(PointX, PointY));
      }, ServerMessage.ConfirmCanvasContextTap);
    }
    private void ReceiveCanvasMove(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var PointX = Reader.ReadInt32();
      var PointY = Reader.ReadInt32();

      var Canvas = GetPanel<Canvas>(PanelTag);
      var Point = new Inv.Point(PointX, PointY);

      ReceiveProcess(() =>
      {
        Canvas.MoveInvoke(Point);
      }, ServerMessage.ConfirmCanvasMove);
    }
    private void ReceiveCanvasZoom(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var PointX = Reader.ReadInt32();
      var PointY = Reader.ReadInt32();
      var Delta = Reader.ReadInt32();

      var Canvas = GetPanel<Canvas>(PanelTag);

      ReceiveProcess(() =>
      {
        Canvas.ZoomInvoke(new Inv.Point(PointX, PointY), Delta);
      }, ServerMessage.ConfirmCanvasZoom);
    }
    private void ReceiveCompleteAnimation(ServerReader Reader)
    {
      var AnimationTag = Reader.ReadAnimationTag();

      var Animation = AnimationDictionary[AnimationTag];

      AnimationDictionary.Remove(AnimationTag);

      ReceiveProcess(() =>
      {
        Animation.Complete();
      }, ServerMessage.ConfirmCompleteAnimation);
    }
    private void ReceiveItemQueryFlow(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var SectionIndex = Reader.ReadInt32();
      var Start = Reader.ReadInt32();
      var Count = Reader.ReadInt32();

      var Flow = GetPanel<Flow>(PanelTag);
      var Section = Flow.GetSection(SectionIndex);

      var ResultArray = new ServerPanelTag[Count];

      ReceiveProcess(() =>
      {
        for (var Index = 0; Index < Count; Index++)
          ResultArray[Index] = TranslatePanelTag((ServerSurface)(Flow.Surface.Node), Section.ItemInvoke(Start + Index));
      },
      ServerMessage.ConfirmItemQueryFlow,
      Writer =>
      {
        foreach (var Result in ResultArray)
          Writer.WritePanelTag(Result);
      });
    }
    private void ReceiveSelectCalendarPicker(ServerReader Reader)
    {
      var PickerTag = Reader.ReadPickerTag();
      var Value = Reader.ReadDateTime();

      var CalendarPicker = RemovePicker<CalendarPicker>(PickerTag);

      ReceiveProcess(() =>
      {
        CalendarPicker.Value = Value;
        CalendarPicker.SelectInvoke();
      }, ServerMessage.ConfirmSelectCalendarPicker);
    }
    private void ReceiveCancelCalendarPicker(ServerReader Reader)
    {
      var PickerTag = Reader.ReadPickerTag();
      var Value = Reader.ReadDateTime();

      var CalendarPicker = RemovePicker<CalendarPicker>(PickerTag);

      ReceiveProcess(() =>
      {
        CalendarPicker.Value = Value;
        CalendarPicker.CancelInvoke();
      }, ServerMessage.ConfirmCancelCalendarPicker);
    }
    private void ReceiveSelectDirectoryPicker(ServerReader Reader)
    {
      var PickerTag = Reader.ReadPickerTag();
      var Value = Reader.ReadBinary();

      var DirectoryPicker = RemovePicker<DirectoryFilePicker>(PickerTag);

      ReceiveProcess(() =>
      {
        DirectoryPicker.SelectInvoke(Value);
      }, ServerMessage.ConfirmSelectDirectoryPicker);
    }
    private void ReceiveCancelDirectoryPicker(ServerReader Reader)
    {
      var PickerTag = Reader.ReadPickerTag();

      var DirectoryPicker = RemovePicker<DirectoryFilePicker>(PickerTag);

      ReceiveProcess(() =>
      {
        DirectoryPicker.CancelInvoke();
      }, ServerMessage.ConfirmCancelDirectoryPicker);
    }

    private void ReceiveProcess(Action Action, ServerMessage Message, Action<ServerWriter> Writer = null)
    {
      Action();

      ProcessChanges();

      ServerSender.Return(Message, Writer);
    }
    private ServerSurfaceTag NextSurfaceTag()
    {
      return new ServerSurfaceTag(++LastSurfaceID);
    }
    private ServerPanelTag NextPanelTag()
    {
      return new ServerPanelTag(++LastPanelID);
    }
    private Inv.Surface GetSurface(ServerSurfaceTag SurfaceTag)
    {
      var Reference = SurfaceDictionary.GetValueOrDefault(SurfaceTag);

      if (Reference == null)
        return null;

      Inv.Surface Result;
      if (!Reference.TryGetTarget(out Result))
        return null;

      return Result;
    }
    private T GetPanel<T>(ServerPanelTag PanelTag)
      where T : Inv.Panel
    {
      var Reference = PanelDictionary.GetValueOrDefault(PanelTag);

      if (Reference == null)
        return null;

      Inv.Panel Result;
      if (!Reference.TryGetTarget(out Result))
        return null;

      return (T)Result;
    }
    private T RemovePicker<T>(ServerPickerTag PickerTag)
      where T : Inv.Picker
    {
      var Result = PickerDictionary.GetValueOrDefault(PickerTag);

      if (Result != null)
        PickerDictionary.Remove(PickerTag);

      return (T)Result;
    }
    private void CompactDictionary<TKey, TValue>(Dictionary<TKey, WeakReference<TValue>> Dictionary, Action<TKey> DisposeAction)
      where TKey : struct
      where TValue : class
    {
      TValue Value;

      Inv.DistinctList<TKey> CompactList = null;

      foreach (var Entry in Dictionary)
      {
        if (!Entry.Value.TryGetTarget(out Value))
        {
          if (CompactList == null)
            CompactList = new DistinctList<TKey>();

          CompactList.Add(Entry.Key);
        }
      }

      if (CompactList != null)
      {
        foreach (var Compact in CompactList)
        {
          Dictionary.Remove(Compact);

          DisposeAction(Compact);
        }
      }
    }

    private ServerSender ServerSender;
    private ServerContract ServerContract;
    private ServerSurface ActiveSurface;
    private Inv.EnumArray<ClientMessage, Action<ServerReader>> ClientArray;
    private Inv.EnumArray<Inv.PanelType, Func<ServerSurface, Inv.Panel, ServerPanel>> RouteArray;
    private ushort LastSurfaceID;
    private uint LastPanelID;
    private ushort LastAnimationID;
    private ushort LastPickerID;
    private uint LastClipID;
    private Dictionary<ServerSurfaceTag, WeakReference<Inv.Surface>> SurfaceDictionary;
    private Dictionary<ServerPanelTag, WeakReference<Inv.Panel>> PanelDictionary;
    private Dictionary<ServerImageTag, WeakReference<Inv.Image>> ImageDictionary;
    private Dictionary<ServerSoundTag, WeakReference<Inv.Sound>> SoundDictionary;
    private Dictionary<ServerClipTag, Inv.AudioClip> ClipDictionary;
    private Dictionary<ServerPickerTag, Inv.Picker> PickerDictionary;
    private Dictionary<ServerAnimationTag, Inv.Animation> AnimationDictionary;
    private Inv.DistinctList<Action> PostActionList;
  }

  public sealed class ServerApplication : ServerContract
  {
    public ServerApplication(Inv.Application Base, Guid Identity)
    {
      this.Base = Base;
      this.Identity = Identity;

      this.SurfaceDictionary = new Dictionary<ServerSurfaceTag, Surface>();
      this.PanelDictionary = new Dictionary<ServerPanelTag, Panel>();
      this.ImageDictionary = new Dictionary<ServerImageTag, Image>();
      this.SoundDictionary = new Dictionary<ServerSoundTag, Sound>();
      this.AnimationDictionary = new Dictionary<ServerAnimationTag, Animation>();
      this.ClipDictionary = new Dictionary<ServerClipTag, AudioClip>();
      this.CanvasDrawPacketDictionary = new Dictionary<Canvas, ServerPacket>();

      Base.StartEvent += () => Start();
      Base.StopEvent += () => Stop();
    }

    public Guid Identity { get; private set; }
    public bool IsActive { get; private set; }
    public event Action ExitEvent;

    public void Start()
    {
      if (!IsActive)
      {
        IsActive = true;

        this.WebClient = Base.Web.NewClient(Inv.ServerShell.SocketHost, Inv.ServerShell.SocketPort, null);
        WebClient.Connect();

        this.ServerWriter = new ServerWriter();
        this.ServerQueue = new ServerQueue("Application", new ServerConnection(WebClient.InputStream, WebClient.OutputStream));
        this.ServerReceiver = new ServerReceiver(this);

        ServerQueue.Start();

        SendMessage(ClientMessage.Identification, Writer =>
        {
          Writer.WriteInt64(ServerShell.ProtocolVersion);
          Writer.WriteGuid(Identity);
        });

        // send introductory message.
        SendAndReceive(ClientMessage.StartApplication, Writer =>
        {
          Writer.WriteString(Base.Device.Name);
          Writer.WriteString(Base.Device.Model);
          Writer.WriteString(Base.Device.System);
          Writer.WriteBoolean(Base.Device.Keyboard);
          Writer.WriteBoolean(Base.Device.Mouse);
          Writer.WriteBoolean(Base.Device.Touch);
          Writer.WriteString(Base.Device.ProportionalFontName);
          Writer.WriteString(Base.Device.MonospacedFontName);

          Writer.WriteInt32(Base.Process.Id);

          Writer.WriteInt32(Base.Window.Width);
          Writer.WriteInt32(Base.Window.Height);

          Writer.WriteBoolean(Base.Phone.IsSupported);
          Writer.WriteBoolean(Base.Location.IsSupported);
          Writer.WriteString(Base.Calendar.GetTimeZoneName());
        }, ServerMessage.ConfirmStartApplication, Reader =>
        {
        });

        Base.SuspendEvent += Suspend;
        Base.ResumeEvent += Resume;
        Base.HandleExceptionEvent += HandleException;
        Base.ExitQuery += ExitQuery;
        Base.Window.ProcessEvent += ProcessMessages;
        Base.Window.KeyModifierEvent += KeyModifier;

        this.AttachmentFolder = Base.Directory.NewFolder("Attachments");
      }
    }
    public void Stop()
    {
      if (IsActive)
      {
        this.IsActive = false;

        Base.SuspendEvent -= Suspend;
        Base.ResumeEvent -= Resume;
        Base.HandleExceptionEvent -= HandleException;
        Base.ExitQuery -= ExitQuery;
        Base.Window.ProcessEvent -= ProcessMessages;
        Base.Window.KeyModifierEvent -= KeyModifier;

        //ServerDispatcher.SendConclusion();

        WebClient.Disconnect();

        if (ServerQueue != null)
        {
          ServerQueue.Stop();
          this.ServerQueue = null;
        }
      }
    }

    void ServerContract.MemoryReclamation()
    {
      Base.Process.MemoryReclamation();
    }
    void ServerContract.ExitApplication()
    {
      ExitInvoke();
    }
    void ServerContract.SetWindowBackground(Colour Colour)
    {
      Base.Window.Background.Colour = Colour;
    }
    void ServerContract.NewSurface(ServerSurfaceTag SurfaceTag)
    {
      var Surface = Base.Window.NewSurface();
      Surface.ArrangeEvent += () =>
      {
        SendAndReceive(ClientMessage.ArrangeSurface, Writer =>
        {
          Writer.WriteSurfaceTag(SurfaceTag);
          Writer.WriteInt32(Base.Window.Width);
          Writer.WriteInt32(Base.Window.Height);
        }, ServerMessage.ConfirmArrangeSurface);
      };
      Surface.GestureBackwardEvent += () =>
      {
        SendAndReceive(ClientMessage.GestureBackwardSurface, Writer =>
        {
          Writer.WriteSurfaceTag(SurfaceTag);
        }, ServerMessage.ConfirmGestureBackwardSurface);
      };
      Surface.GestureForwardEvent += () =>
      {
        SendAndReceive(ClientMessage.GestureForwardSurface, Writer =>
        {
          Writer.WriteSurfaceTag(SurfaceTag);
        }, ServerMessage.ConfirmGestureForwardSurface);
      };
      Surface.KeystrokeEvent += (Keystroke) =>
      {
        SendAndReceive(ClientMessage.KeystrokeSurface, Writer =>
        {
          Writer.WriteSurfaceTag(SurfaceTag);
          Writer.WriteInt32((int)Keystroke.Key);
          Writer.WriteByte((byte)Keystroke.Modifier.GetFlags());
        }, ServerMessage.ConfirmKeystrokeSurface);
      };

      SurfaceDictionary.Add(SurfaceTag, Surface);
    }
    void ServerContract.SetSurfaceBackground(ServerSurfaceTag SurfaceTag, Colour Colour)
    {
      var Surface = GetSurface(SurfaceTag);
      Surface.Background.Colour = Colour;
    }
    void ServerContract.TransitionSurface(ServerSurfaceTag SurfaceTag, TransitionAnimation Animation, TimeSpan Duration)
    {
      var Surface = GetSurface(SurfaceTag);

      var InvTransition = Base.Window.Transition(Surface);
      InvTransition.Duration = Duration;
      InvTransition.SetAnimation(Animation);
    }
    void ServerContract.SetSurfaceContent(ServerSurfaceTag SurfaceTag, ServerPanelTag ContentTag)
    {
      var Surface = GetSurface(SurfaceTag);
      Surface.Content = UsePanel(ContentTag);
    }
    void ServerContract.NewBrowser(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag)
    {
      var Surface = GetSurface(SurfaceTag);
      var Browser = Surface.NewBrowser();
      PanelDictionary.Add(PanelTag, Browser);
    }
    void ServerContract.LoadBrowser(ServerPanelTag PanelTag, Uri Uri, string Html)
    {
      var Browser = GetPanel<Browser>(PanelTag);
      if (Uri != null)
        Browser.LoadUri(Uri);
      else if (Html != null)
        Browser.LoadHtml(Html);
    }
    void ServerContract.NewButton(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag)
    {
      var Surface = GetSurface(SurfaceTag);
      var Button = Surface.NewButton();
      PanelDictionary.Add(PanelTag, Button);

      Button.SingleTapEvent += () =>
      {
        SendAndReceive(ClientMessage.ButtonSingleTap, Writer =>
        {
          Writer.WritePanelTag(PanelTag);
        }, ServerMessage.ConfirmButtonSingleTap);
      };
      Button.ContextTapEvent += () =>
      {
        SendAndReceive(ClientMessage.ButtonContextTap, Writer =>
        {
          Writer.WritePanelTag(PanelTag);
        }, ServerMessage.ConfirmButtonContextTap);
      };
    }
    void ServerContract.SetButtonContent(ServerPanelTag PanelTag, ServerPanelTag ContentTag)
    {
      var Button = GetPanel<Button>(PanelTag);
      Button.Content = UsePanel(ContentTag);
    }
    void ServerContract.SetButtonIsEnabled(ServerPanelTag PanelTag, bool IsEnabled)
    {
      var Button = GetPanel<Button>(PanelTag);
      Button.IsEnabled = IsEnabled;
    }
    void ServerContract.SetButtonIsFocusable(ServerPanelTag PanelTag, bool IsFocusable)
    {
      var Button = GetPanel<Button>(PanelTag);
      Button.IsFocusable = IsFocusable;
    }
    void ServerContract.SetButtonHasPress(ServerPanelTag PanelTag, bool HasPress)
    {
      var Button = GetPanel<Button>(PanelTag);

      if (HasPress)
        Button.PressEvent += () => SendAndReceive(ClientMessage.ButtonPress, Writer => Writer.WritePanelTag(PanelTag), ServerMessage.ConfirmButtonPress);
      else
        Button.PressEvent -= () => SendAndReceive(ClientMessage.ButtonPress, Writer => Writer.WritePanelTag(PanelTag), ServerMessage.ConfirmButtonPress);
    }
    void ServerContract.SetButtonHasRelease(ServerPanelTag PanelTag, bool HasRelease)
    {
      var Button = GetPanel<Button>(PanelTag);

      if (HasRelease)
        Button.ReleaseEvent += () => SendAndReceive(ClientMessage.ButtonRelease, Writer => Writer.WritePanelTag(PanelTag), ServerMessage.ConfirmButtonRelease);
      else
        Button.ReleaseEvent -= () => SendAndReceive(ClientMessage.ButtonRelease, Writer => Writer.WritePanelTag(PanelTag), ServerMessage.ConfirmButtonRelease);
    }
    void ServerContract.NewBoard(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag)
    {
      var Surface = GetSurface(SurfaceTag);
      var Board = Surface.NewBoard();
      PanelDictionary.Add(PanelTag, Board);
    }
    void ServerContract.SetBoardCollection(ServerPanelTag PanelTag, ServerBoardPin[] PinArray)
    {
      var Board = GetPanel<Board>(PanelTag);

      Board.RemovePanels();
      foreach (var Pink in PinArray)
        Board.AddPanel(UsePanel(Pink.PanelTag), Pink.Rect);
    }
    void ServerContract.NewDock(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag, DockOrientation Orientation)
    {
      var Surface = GetSurface(SurfaceTag);
      var Dock = Surface.NewDock(Orientation);
      PanelDictionary.Add(PanelTag, Dock);
    }
    void ServerContract.SetDockCollection(ServerPanelTag PanelTag, ServerPanelTag[] HeaderArray, ServerPanelTag[] ClientArray, ServerPanelTag[] FooterArray)
    {
      var Dock = GetPanel<Dock>(PanelTag);

      Dock.RemoveHeaders();
      foreach (var Header in HeaderArray)
        Dock.AddHeader(UsePanel(Header));

      Dock.RemoveClients();
      foreach (var Client in ClientArray)
        Dock.AddClient(UsePanel(Client));

      Dock.RemoveFooters();
      foreach (var Footer in FooterArray)
        Dock.AddFooter(UsePanel(Footer));
    }
    void ServerContract.NewEdit(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag, EditInput Input)
    {
      var Surface = GetSurface(SurfaceTag);
      var Edit = Surface.NewEdit(Input);
      PanelDictionary.Add(PanelTag, Edit);
    }
    void ServerContract.SetEditIsReadOnly(ServerPanelTag PanelTag, bool ReadOnly)
    {
      var Edit = GetPanel<Edit>(PanelTag);
      Edit.IsReadOnly = ReadOnly;
    }
    void ServerContract.SetEditHasChange(ServerPanelTag PanelTag, bool HasChange)
    {
      var Edit = GetPanel<Edit>(PanelTag);

      if (HasChange)
        Edit.ChangeEvent += () => SendAndReceive(ClientMessage.EditChange, Writer =>
        {
          Writer.WritePanelTag(PanelTag);
          Writer.WriteString(Edit.Text);
        }, ServerMessage.ConfirmEditChange);
      else
        Edit.ChangeEvent -= () => SendAndReceive(ClientMessage.EditChange, Writer =>
        {
          Writer.WritePanelTag(PanelTag);
          Writer.WriteString(Edit.Text);
        }, ServerMessage.ConfirmEditChange);
    }
    void ServerContract.SetEditHasReturn(ServerPanelTag PanelTag, bool HasReturn)
    {
      var Edit = GetPanel<Edit>(PanelTag);

      if (HasReturn)
        Edit.ReturnEvent += () => SendAndReceive(ClientMessage.EditReturn, Writer => Writer.WritePanelTag(PanelTag), ServerMessage.ConfirmEditReturn);
      else
        Edit.ReturnEvent -= () => SendAndReceive(ClientMessage.EditReturn, Writer => Writer.WritePanelTag(PanelTag), ServerMessage.ConfirmEditReturn);
    }
    void ServerContract.SetEditFont(ServerPanelTag PanelTag, string Name, int? Size, Colour Colour, FontWeight Weight)
    {
      var Edit = GetPanel<Edit>(PanelTag);
      Edit.Font.Name = Name;
      Edit.Font.Size = Size;
      Edit.Font.Colour = Colour;
      Edit.Font.Weight = Weight;
    }
    void ServerContract.SetEditText(ServerPanelTag PanelTag, string Text)
    {
      var Edit = GetPanel<Edit>(PanelTag);
      Edit.UpdateText(Text);
    }
    void ServerContract.NewFlow(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag)
    {
      var Surface = GetSurface(SurfaceTag);
      var Flow = Surface.NewFlow();

      PanelDictionary.Add(PanelTag, Flow);
    }
    void ServerContract.ReloadFlow(ServerPanelTag PanelTag, ServerFlowSection[] SectionArray)
    {
      var Flow = GetPanel<Flow>(PanelTag);

      Flow.RemoveSections();

      var Index = 0;
      foreach (var Section in SectionArray)
      {
        var SectionIndex = Index;

        var FlowSection = Flow.AddBatchedSection<ServerPanelTag>();
        FlowSection.RequestBatch += (Start, Count, Token, Callback) =>
        {
          // NOTE: this is expected to be in the UI thread.
          SendAndReceive(ClientMessage.ItemQueryFlow, Writer =>
          {
            Writer.WritePanelTag(PanelTag);
            Writer.WriteInt32(SectionIndex);
            Writer.WriteInt32(Start);
            Writer.WriteInt32(Count);
          }, ServerMessage.ConfirmItemQueryFlow, Reader =>
          {
            var ResultArray = new ServerPanelTag[Count];
            for (var ResultIndex = 0; ResultIndex < Count; ResultIndex++)
              ResultArray[ResultIndex] = Reader.ReadPanelTag();

            Callback(ResultArray);
          });
        };
        FlowSection.SetItemCount(Section.ItemCount);
        FlowSection.SetHeader(UsePanel(Section.HeaderPanelTag));
        FlowSection.SetFooter(UsePanel(Section.FooterPanelTag));
        FlowSection.ItemQuery += (ItemTag) => UsePanel(ItemTag);

        Index++;
      }

      Flow.Reload();
    }
    void ServerContract.NewFrame(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag)
    {
      var Surface = GetSurface(SurfaceTag);
      var Frame = Surface.NewFrame();
      PanelDictionary.Add(PanelTag, Frame);
    }
    void ServerContract.SetFrameContent(ServerPanelTag PanelTag, ServerPanelTag ContentTag)
    {
      var Frame = GetPanel<Frame>(PanelTag);
      Frame.Content = UsePanel(ContentTag);
    }
    void ServerContract.NewGraphic(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag)
    {
      var Surface = GetSurface(SurfaceTag);
      var Graphic = Surface.NewGraphic();
      PanelDictionary.Add(PanelTag, Graphic);
    }
    void ServerContract.SetGraphicImage(ServerPanelTag PanelTag, ServerImageTag ImageTag)
    {
      var Graphic = GetPanel<Graphic>(PanelTag);
      Graphic.Image = ImageTag.IsZero ? null : ImageDictionary[ImageTag];
    }
    void ServerContract.NewLabel(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag)
    {
      var Surface = GetSurface(SurfaceTag);
      var Label = Surface.NewLabel();
      PanelDictionary.Add(PanelTag, Label);
    }
    void ServerContract.SetLabelLineWrapping(ServerPanelTag PanelTag, bool LineWrapping)
    {
      var Label = GetPanel<Label>(PanelTag);
      Label.LineWrapping = LineWrapping;
    }
    void ServerContract.SetLabelJustification(ServerPanelTag PanelTag, Justification Justification)
    {
      var Label = GetPanel<Label>(PanelTag);
      Label.Justify(Justification);
    }
    void ServerContract.SetLabelFont(ServerPanelTag PanelTag, string Name, int? Size, Colour Colour, FontWeight Weight)
    {
      var Label = GetPanel<Label>(PanelTag);
      Label.Font.Name = Name;
      Label.Font.Size = Size;
      Label.Font.Colour = Colour;
      Label.Font.Weight = Weight;
    }
    void ServerContract.SetLabelText(ServerPanelTag PanelTag, string Text)
    {
      var Label = GetPanel<Label>(PanelTag);
      Label.Text = Text;
    }
    void ServerContract.NewMemo(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag)
    {
      var Surface = GetSurface(SurfaceTag);
      var Memo = Surface.NewMemo();
      PanelDictionary.Add(PanelTag, Memo);
    }
    void ServerContract.SetMemoIsReadOnly(ServerPanelTag PanelTag, bool ReadOnly)
    {
      var Memo = GetPanel<Memo>(PanelTag);
      Memo.IsReadOnly = ReadOnly;
    }
    void ServerContract.SetMemoHasChange(ServerPanelTag PanelTag, bool HasChange)
    {
      var Memo = GetPanel<Memo>(PanelTag);

      if (HasChange)
        Memo.ChangeEvent += () => SendAndReceive(ClientMessage.MemoChange, Writer =>
        {
          Writer.WritePanelTag(PanelTag);
          Writer.WriteString(Memo.Text);
        }, ServerMessage.ConfirmMemoChange);
      else
        Memo.ChangeEvent -= () => SendAndReceive(ClientMessage.MemoChange, Writer =>
        {
          Writer.WritePanelTag(PanelTag);
          Writer.WriteString(Memo.Text);
        }, ServerMessage.ConfirmMemoChange);
    }
    void ServerContract.SetMemoFont(ServerPanelTag PanelTag, string Name, int? Size, Colour Colour, FontWeight Weight)
    {
      var Memo = GetPanel<Memo>(PanelTag);
      Memo.Font.Name = Name;
      Memo.Font.Size = Size;
      Memo.Font.Colour = Colour;
      Memo.Font.Weight = Weight;
    }
    void ServerContract.SetMemoText(ServerPanelTag PanelTag, string Text)
    {
      var Memo = GetPanel<Memo>(PanelTag);
      Memo.UpdateText(Text);
    }
    void ServerContract.NewOverlay(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag)
    {
      var Surface = GetSurface(SurfaceTag);
      var Overlay = Surface.NewOverlay();
      PanelDictionary.Add(PanelTag, Overlay);
    }
    void ServerContract.SetOverlayCollection(ServerPanelTag PanelTag, ServerPanelTag[] ElementArray)
    {
      var Overlay = GetPanel<Overlay>(PanelTag);

      Overlay.RemovePanels();
      foreach (var Panel in ElementArray)
        Overlay.AddPanel(PanelDictionary[Panel]);
    }
    void ServerContract.NewCanvas(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag)
    {
      var Surface = GetSurface(SurfaceTag);
      var Render = Surface.NewCanvas();
      Render.DrawEvent += (Context) =>
      {
        var Packet = CanvasDrawPacketDictionary[Render];

        if (Packet != null)
        {
          using (var Reader = Packet.ToReader())
          {
            while (!Reader.EndOfPacket)
            {
              var RenderElement = (RenderMessage)Reader.ReadByte();

              switch (RenderElement)
              {
                case Inv.RenderMessage.Text:
                  var Text = Reader.ReadString();
                  var TextFontName = Reader.ReadString();
                  var TextFontSize = Reader.ReadInt16();
                  var TextFontWeight = (FontWeight)Reader.ReadByte();
                  var TextFontColour = Reader.ReadColour();
                  var TextX = Reader.ReadInt16();
                  var TextY = Reader.ReadInt16();
                  var TextHorizontal = (HorizontalPosition)Reader.ReadByte();
                  var TextVertical = (VerticalPosition)Reader.ReadByte();
                  Context.DrawText(Text, TextFontName, TextFontSize, TextFontWeight, TextFontColour, new Point(TextX, TextY), TextHorizontal, TextVertical);
                  break;

                case Inv.RenderMessage.Rectangle:
                  var RectangleFillColour = Reader.ReadColour();
                  var RectangleStrokeThickness = Reader.ReadInt16();
                  var RectangleStrokeColour = RectangleStrokeThickness > 0 ? Reader.ReadColour() : null;
                  var RectangleLeft = Reader.ReadInt16();
                  var RectangleTop = Reader.ReadInt16();
                  var RectangleWidth = Reader.ReadInt16();
                  var RectangleHeight = Reader.ReadInt16();
                  Context.DrawRectangle(RectangleFillColour, RectangleStrokeColour, RectangleStrokeThickness, new Rect(RectangleLeft, RectangleTop, RectangleWidth, RectangleHeight));
                  break;

                case Inv.RenderMessage.StraightRectangle:
                  var StraightRectangleFillColour = Reader.ReadColour();
                  var StraightRectangleStrokeThickness = Reader.ReadInt16();
                  var StraightRectangleStrokeColour = StraightRectangleStrokeThickness > 0 ? Reader.ReadColour() : null;
                  var StraightRectangleLeft = Reader.ReadInt16();
                  var StraightRectangleTop = Reader.ReadInt16();
                  var StraightRectangleSize = Reader.ReadInt16();
                  Context.DrawRectangle(StraightRectangleFillColour, StraightRectangleStrokeColour, StraightRectangleStrokeThickness, new Rect(StraightRectangleLeft, StraightRectangleTop, StraightRectangleSize, StraightRectangleSize));
                  break;

                case Inv.RenderMessage.Ellipse:
                  var EllipseFillColour = Reader.ReadColour();
                  var EllipseStrokeThickness = Reader.ReadInt16();
                  var EllipseStrokeColour = EllipseStrokeThickness > 0 ? Reader.ReadColour() : null;
                  var EllipseCenterX = Reader.ReadInt16();
                  var EllipseCenterY = Reader.ReadInt16();
                  var EllipseRadiusX = Reader.ReadInt16();
                  var EllipseRadiusY = Reader.ReadInt16();
                  Context.DrawEllipse(EllipseFillColour, EllipseStrokeColour, EllipseStrokeThickness, new Point(EllipseCenterX, EllipseCenterY), new Point(EllipseRadiusX, EllipseRadiusY));
                  break;

                case Inv.RenderMessage.Image:
                  var ImageTag = Reader.ReadImageTag();
                  var ImageLeft = Reader.ReadInt16();
                  var ImageTop = Reader.ReadInt16();
                  var ImageWidth = Reader.ReadInt16();
                  var ImageHeight = Reader.ReadInt16();
                  var ImageOpacity = Reader.ReadFloat();
                  var ImageTint = Reader.ReadColour();
                  var ImageMirror = Reader.ReadByte();
                  Context.DrawImage(ImageTag.IsZero ? null : ImageDictionary[ImageTag], new Rect(ImageLeft, ImageTop, ImageWidth, ImageHeight), ImageOpacity, ImageTint, ImageMirror != 255 ? (Mirror)ImageMirror : (Mirror?)null);
                  break;

                case Inv.RenderMessage.StraightImage:
                  var StraightImageTag = Reader.ReadImageTag();
                  var StraightImageLeft = Reader.ReadInt16();
                  var StraightImageTop = Reader.ReadInt16();
                  var StraightImageSize = Reader.ReadInt16();
                  Context.DrawImage(StraightImageTag.IsZero ? null : ImageDictionary[StraightImageTag], new Rect(StraightImageLeft, StraightImageTop, StraightImageSize, StraightImageSize), 1.0F, null, null);
                  break;

                case Inv.RenderMessage.OpacityImage:
                  var OpacityImageTag = Reader.ReadImageTag();
                  var OpacityImageLeft = Reader.ReadInt16();
                  var OpacityImageTop = Reader.ReadInt16();
                  var OpacityImageSize = Reader.ReadInt16();
                  var OpacityImageOpacity = Reader.ReadFloat();
                  Context.DrawImage(OpacityImageTag.IsZero ? null : ImageDictionary[OpacityImageTag], new Rect(OpacityImageLeft, OpacityImageTop, OpacityImageSize, OpacityImageSize), OpacityImageOpacity, null, null);
                  break;

                case Inv.RenderMessage.Line:
                  var LineStrokeThickness = Reader.ReadInt16();
                  var LineStrokeColour = LineStrokeThickness > 0 ? Reader.ReadColour() : null;
                  var LineSourcePointX = Reader.ReadInt16();
                  var LineSourcePointY = Reader.ReadInt16();
                  var LineTargetPointX = Reader.ReadInt16();
                  var LineTargetPointY = Reader.ReadInt16();

                  var LineExtraPointLength = Reader.ReadInt16();
                  var LineExtraPointArray = new Inv.Point[LineExtraPointLength];
                  for (var Index = 0; Index < LineExtraPointLength; Index++)
                  {
                    var LineExtraPointX = Reader.ReadInt16();
                    var LineExtraPointY = Reader.ReadInt16();
                    LineExtraPointArray[Index] = new Inv.Point(LineExtraPointX, LineExtraPointY);
                  }
                  Context.DrawLine(LineStrokeColour, LineStrokeThickness, new Inv.Point(LineSourcePointX, LineSourcePointY), new Inv.Point(LineTargetPointX, LineTargetPointY), LineExtraPointArray);
                  break;

                case Inv.RenderMessage.Arc:
                  var ArcFillColour = Reader.ReadColour();
                  var ArcStrokeThickness = Reader.ReadInt16();
                  var ArcStrokeColour = ArcStrokeThickness > 0 ? Reader.ReadColour() : null;
                  var ArcCenterX = Reader.ReadInt16();
                  var ArcCenterY = Reader.ReadInt16();
                  var ArcRadiusX = Reader.ReadInt16();
                  var ArcRadiusY = Reader.ReadInt16();
                  var ArcStartAngle = Reader.ReadFloat();
                  var ArcSweepAngle = Reader.ReadFloat();
                  Context.DrawArc(ArcFillColour, ArcStrokeColour, ArcStrokeThickness, new Point(ArcCenterX, ArcCenterY), new Point(ArcRadiusX, ArcRadiusY), ArcStartAngle, ArcSweepAngle);
                  break;

                case Inv.RenderMessage.Polygon:
                  var PolygonFillColour = Reader.ReadColour();
                  var PolygonStrokeThickness = Reader.ReadInt16();
                  var PolygonStrokeColour = PolygonStrokeThickness > 0 ? Reader.ReadColour() : null;
                  var PolygonLineJoin = (LineJoin)Reader.ReadByte();
                  var PolygonStartX = Reader.ReadInt16();
                  var PolygonStartY = Reader.ReadInt16();

                  var PolygonPointLength = Reader.ReadInt16();
                  var PolygonPointArray = new Inv.Point[PolygonPointLength];
                  for (var Index = 0; Index < PolygonPointLength; Index++)
                  {
                    var PolygonPointX = Reader.ReadInt16();
                    var PolygonPointY = Reader.ReadInt16();
                    PolygonPointArray[Index] = new Inv.Point(PolygonPointX, PolygonPointY);
                  }

                  Context.DrawPolygon(PolygonFillColour, PolygonStrokeColour, PolygonStrokeThickness, PolygonLineJoin, new Point(PolygonStartX, PolygonStartY), PolygonPointArray);
                  break;


                default:
                  throw new Exception("RenderElement not handled: " + RenderElement);
              }
            }
          }
        }
      };

      Render.SingleTapEvent += (Point) =>
      {
        SendAndReceive(ClientMessage.CanvasSingleTap, Writer =>
        {
          Writer.WritePanelTag(PanelTag);
          Writer.WriteInt32(Point.X);
          Writer.WriteInt32(Point.Y);
        }, ServerMessage.ConfirmCanvasSingleTap);
      };
      Render.DoubleTapEvent += (Point) =>
      {
        SendAndReceive(ClientMessage.CanvasDoubleTap, Writer =>
        {
          Writer.WritePanelTag(PanelTag);
          Writer.WriteInt32(Point.X);
          Writer.WriteInt32(Point.Y);
        }, ServerMessage.ConfirmCanvasDoubleTap);
      };
      Render.ContextTapEvent += (Point) =>
      {
        SendAndReceive(ClientMessage.CanvasContextTap, Writer =>
        {
          Writer.WritePanelTag(PanelTag);
          Writer.WriteInt32(Point.X);
          Writer.WriteInt32(Point.Y);
        }, ServerMessage.ConfirmCanvasContextTap);
      };
      Render.PressEvent += (Point) =>
      {
        SendAndReceive(ClientMessage.CanvasPress, Writer =>
        {
          Writer.WritePanelTag(PanelTag);
          Writer.WriteInt32(Point.X);
          Writer.WriteInt32(Point.Y);
        }, ServerMessage.ConfirmCanvasPress);
      };
      Render.ReleaseEvent += (Point) =>
      {
        SendAndReceive(ClientMessage.CanvasRelease, Writer =>
        {
          Writer.WritePanelTag(PanelTag);
          Writer.WriteInt32(Point.X);
          Writer.WriteInt32(Point.Y);
        }, ServerMessage.ConfirmCanvasRelease);
      };
      Render.MoveEvent += (Point) =>
      {
        SendAndReceive(ClientMessage.CanvasMove, Writer =>
        {
          Writer.WritePanelTag(PanelTag);
          Writer.WriteInt32(Point.X);
          Writer.WriteInt32(Point.Y);
        }, ServerMessage.ConfirmCanvasMove);
      };
      Render.ZoomEvent += (Point, Delta) =>
      {
        SendAndReceive(ClientMessage.CanvasZoom, Writer =>
        {
          Writer.WritePanelTag(PanelTag);
          Writer.WriteInt32(Point.X);
          Writer.WriteInt32(Point.Y);
          Writer.WriteInt32(Delta);
        }, ServerMessage.ConfirmCanvasZoom);
      };

      PanelDictionary.Add(PanelTag, Render);
    }
    void ServerContract.DrawCanvas(ServerPanelTag PanelTag, ServerPacket Packet)
    {
      var Render = GetPanel<Canvas>(PanelTag);

      CanvasDrawPacketDictionary[Render] = Packet;

      Render.Draw();
    }
    void ServerContract.NewScroll(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag, ScrollOrientation Orientation)
    {
      var Surface = GetSurface(SurfaceTag);
      var Scroll = Surface.NewScroll(Orientation);
      PanelDictionary.Add(PanelTag, Scroll);
    }
    void ServerContract.SetScrollContent(ServerPanelTag PanelTag, ServerPanelTag ContentTag)
    {
      var Scroll = GetPanel<Scroll>(PanelTag);

      Scroll.Content = UsePanel(ContentTag);
    }
    void ServerContract.NewStack(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag, StackOrientation Orientation)
    {
      var Surface = GetSurface(SurfaceTag);
      var Stack = Surface.NewStack(Orientation);
      PanelDictionary.Add(PanelTag, Stack);
    }
    void ServerContract.SetStackCollection(ServerPanelTag PanelTag, ServerPanelTag[] ElementArray)
    {
      var Stack = GetPanel<Stack>(PanelTag);

      Stack.RemovePanels();
      foreach (var Panel in ElementArray)
        Stack.AddPanel(UsePanel(Panel));
    }
    void ServerContract.NewTable(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag)
    {
      var Surface = GetSurface(SurfaceTag);
      var Table = Surface.NewTable();
      PanelDictionary.Add(PanelTag, Table);
    }
    void ServerContract.SetTableCollection(ServerPanelTag PanelTag, ServerTableAxis[] RowArray, ServerTableAxis[] ColumnArray, ServerTableCell[] CellArray)
    {
      var Table = GetPanel<Table>(PanelTag);

      Table.RemovePanels();

      foreach (var Row in RowArray)
      {
        var TableRow = Table.AddRow();
        TableRow.Set(Row.LengthType, Row.LengthValue);
        TableRow.Content = UsePanel(Row.PanelTag);
      }

      foreach (var Column in ColumnArray)
      {
        var TableColumn = Table.AddColumn();
        TableColumn.Set(Column.LengthType, Column.LengthValue);
        TableColumn.Content = UsePanel(Column.PanelTag);
      }

      foreach (var Cell in CellArray)
        Table.GetCell(Cell.X, Cell.Y).Content = UsePanel(Cell.PanelTag);
    }
    void ServerContract.SetPanelAlignment(ServerPanelTag PanelTag, Inv.Placement Placement)
    {
      var Panel = PanelDictionary[PanelTag];
      Panel.Alignment.Set(Placement);
    }
    void ServerContract.SetPanelBorder(ServerPanelTag PanelTag, int Left, int Top, int Right, int Bottom, Colour Colour)
    {
      var Panel = PanelDictionary[PanelTag];
      Panel.Border.Set(Left, Top, Right, Bottom);
      Panel.Border.Colour = Colour;
    }
    void ServerContract.SetPanelCorner(ServerPanelTag PanelTag, int TopLeft, int TopRight, int BottomRight, int BottomLeft)
    {
      var Panel = PanelDictionary[PanelTag];
      Panel.Corner.Set(TopLeft, TopRight, BottomRight, BottomLeft);
    }
    void ServerContract.SetPanelElevation(ServerPanelTag PanelTag, int Depth)
    {
      var Panel = PanelDictionary[PanelTag];
      Panel.Elevation.Set(Depth);
    }
    void ServerContract.SetPanelMargin(ServerPanelTag PanelTag, int Left, int Top, int Right, int Bottom)
    {
      var Panel = PanelDictionary[PanelTag];
      Panel.Margin.Set(Left, Top, Right, Bottom);
    }
    void ServerContract.SetPanelOpacity(ServerPanelTag PanelTag, float Opacity)
    {
      var Panel = PanelDictionary[PanelTag];
      Panel.Opacity.Set(Opacity);
    }
    void ServerContract.SetPanelPadding(ServerPanelTag PanelTag, int Left, int Top, int Right, int Bottom)
    {
      var Panel = PanelDictionary[PanelTag];
      Panel.Padding.Set(Left, Top, Right, Bottom);
    }
    void ServerContract.SetPanelSize(ServerPanelTag PanelTag, int? Width, int? Height, int? MinimumWidth, int? MinimumHeight, int? MaximumWidth, int? MaximumHeight)
    {
      var Panel = PanelDictionary[PanelTag];
      Panel.Size.Set(Width, Height, MinimumWidth, MinimumHeight, MaximumWidth, MaximumHeight);
    }
    void ServerContract.SetPanelVisibility(ServerPanelTag PanelTag, bool Visibility)
    {
      var Panel = PanelDictionary[PanelTag];
      Panel.Visibility.Set(Visibility);
    }
    void ServerContract.SetPanelBackground(ServerPanelTag PanelTag, Colour BackgroundColour)
    {
      var Panel = PanelDictionary[PanelTag];
      Panel.Background.Colour = BackgroundColour;
    }
    void ServerContract.NewImage(ServerImageTag ImageTag, Inv.Image ImageSource)
    {
      ImageDictionary.Add(ImageTag, ImageSource);
    }
    void ServerContract.NewSound(ServerSoundTag SoundTag, Inv.Sound SoundSource)
    {
      SoundDictionary.Add(SoundTag, SoundSource);
    }
    void ServerContract.PlaySound(ServerSoundTag SoundTag, float SoundVolume, float SoundRate)
    {
      Base.Audio.Play(LookupSound(SoundTag), SoundVolume, SoundRate);
    }
    void ServerContract.PlayClip(ServerClipTag ClipTag, ServerSoundTag SoundTag, float SoundVolume, float SoundRate, bool SoundLoop)
    {
      var Clip = Base.Audio.NewClip(LookupSound(SoundTag), SoundVolume, SoundRate, SoundLoop);

      ClipDictionary.Add(ClipTag, Clip);

      Clip.Play();
    }
    void ServerContract.StopClip(ServerClipTag ClipTag)
    {
      var Clip = ClipDictionary.GetValueOrDefault(ClipTag);

      if (Clip != null)
      {
        ClipDictionary.Remove(ClipTag);

        Clip.Stop();
      }      
    }
    void ServerContract.StartAnimation(ServerSurfaceTag SurfaceTag, ServerAnimationTag AnimationTag, ServerAnimationTarget[] TargetArray)
    {
      var Surface = GetSurface(SurfaceTag);
      var Animation = Surface.NewAnimation();
      Animation.CompleteEvent += () =>
      {
        SendAndReceive(ClientMessage.CompleteAnimation, Writer =>
        {
          Writer.WriteAnimationTag(AnimationTag);
        }, ServerMessage.ConfirmCompleteAnimation);
      };

      foreach (var Target in TargetArray)
      {
        var Panel = GetPanel<Inv.Panel>(Target.PanelTag);

        var AnimationTarget = Animation.AddTarget(Panel);

        foreach (var Command in Target.CommandArray)
        {
          switch (Command.Type)
          {
            case AnimationType.Opacity:
              AnimationTarget.FadeOpacity(Command.OpacityFrom, Command.OpacityTo, Command.OpacityDuration, Command.OpacityOffset);
              break;

            default:
              throw new Exception("AnimationType not handled: " + Command.Type);
          }
        }
      }
      AnimationDictionary.Add(AnimationTag, Animation);

      Animation.Start();
    }
    void ServerContract.StopAnimation(ServerAnimationTag AnimationTag)
    {
      var Animation = AnimationDictionary.GetValueOrDefault(AnimationTag);

      if (Animation != null)
      {
        AnimationDictionary.Remove(AnimationTag);

        Animation.Stop();
      }
    }
    void ServerContract.BrowseMarket(string AppleiTunesID, string GooglePlayID, string WindowsStoreID)
    {
      Base.Market.Browse(AppleiTunesID, GooglePlayID, WindowsStoreID);
    }
    void ServerContract.LaunchWebUri(Uri Uri)
    {
      Base.Web.Launch(Uri);
    }
    void ServerContract.ShowCalendarPicker(ServerPickerTag PickerTag, bool SetDate, bool SetTime, DateTime Value)
    {
      var CalendarPicker = Base.Calendar.NewPicker(SetDate, SetTime);
      CalendarPicker.Value = Value;
      CalendarPicker.SelectEvent += () =>
      {
        SendAndReceive(ClientMessage.SelectCalendarPicker, Writer =>
        {
          Writer.WritePickerTag(PickerTag);
          Writer.WriteDateTime(CalendarPicker.Value);
        }, ServerMessage.ConfirmSelectCalendarPicker);
      };
      CalendarPicker.CancelEvent += () =>
      {
        SendAndReceive(ClientMessage.CancelCalendarPicker, Writer =>
        {
          Writer.WritePickerTag(PickerTag);
          Writer.WriteDateTime(CalendarPicker.Value);
        }, ServerMessage.ConfirmCancelCalendarPicker);
      };
      CalendarPicker.Show();
    }
    void ServerContract.ShowDirectoryPicker(ServerPickerTag PickerTag, string Title, FileType FileType)
    {
      var FilePicker = Base.Directory.NewFilePicker(FileType);
      FilePicker.SelectEvent += (Binary) =>
      {
        SendAndReceive(ClientMessage.SelectDirectoryPicker, Writer =>
        {
          Writer.WritePickerTag(PickerTag);
          Writer.WriteBinary(Binary);
        }, ServerMessage.ConfirmSelectDirectoryPicker);
      };
      FilePicker.CancelEvent += () =>
      {
        SendAndReceive(ClientMessage.CancelDirectoryPicker, Writer =>
        {
          Writer.WritePickerTag(PickerTag);
        }, ServerMessage.ConfirmCancelDirectoryPicker);
      };
      FilePicker.Show();
    }
    bool ServerContract.SendEmailMessage(string Subject, string Body, ServerEmailTo[] ToArray, ServerEmailAttachment[] AttachmentArray)
    {
      var EmailMessage = Base.Email.NewMessage();
      EmailMessage.Subject = Subject;
      EmailMessage.Body = Body;

      foreach (var To in ToArray)
        EmailMessage.To(To.Name, To.Address);

      foreach (var Attachment in AttachmentArray)
      {
        var File = AttachmentFolder.NewFile(Attachment.Name);
        File.WriteAllBytes(Attachment.Content);

        EmailMessage.Attach(Attachment.Name, File);
      }

      var Result = EmailMessage.Send();

      SendMessage(ClientMessage.ConfirmSendEmailMessage, Writer =>
      {
        ServerWriter.WriteBoolean(Result);
      });

      return Result;
    }
    void ServerContract.DialPhone(string PhoneNumber)
    {
      Base.Phone.Dial(PhoneNumber);

      SendMessage(ClientMessage.ConfirmDialPhone);
    }
    void ServerContract.SMSPhone(string PhoneNumber)
    {
      Base.Phone.SMS(PhoneNumber);

      SendMessage(ClientMessage.ConfirmSMSPhone);
    }
    void ServerContract.DisposeSurface(ServerSurfaceTag SurfaceTag)
    {
      SurfaceDictionary.Remove(SurfaceTag);
    }
    void ServerContract.DisposePanel(ServerPanelTag PanelTag)
    {
      PanelDictionary.Remove(PanelTag);
    }
    void ServerContract.DisposeImage(ServerImageTag ImageTag)
    {
      ImageDictionary.Remove(ImageTag);
    }
    void ServerContract.DisposeSound(ServerSoundTag SoundTag)
    {
      SoundDictionary.Remove(SoundTag);
    }

    private void ExitInvoke()
    {
      if (ExitEvent != null)
        ExitEvent();
      else
        Base.Exit();
    }
    private void KeyModifier()
    {
      SendAndReceive(ClientMessage.KeyModifierWindow, Writer =>
      {
        Writer.WriteByte((byte)Base.Window.KeyModifier.GetFlags());
      }, ServerMessage.ConfirmKeyModifierWindow);
    }
    private bool ExitQuery()
    {
      // synchronously receive the response (true or false).
      var Result = false;

      SendAndReceive(ClientMessage.ExitQueryApplication, null, ServerMessage.ConfirmExitQuery, Reader =>
      {
        Result = Reader.ReadBoolean();
      });

      return Result;
    }
    private void HandleException(Exception Exception)
    {
      SendAndReceive(ClientMessage.HandleExceptionApplication, Writer =>
      {
        Writer.WriteString(Exception.AsReport());
      }, ServerMessage.ConfirmHandleException);
    }
    private void Resume()
    {
      SendAndReceive(ClientMessage.ResumeApplication, ServerMessage.ConfirmResume);
    }
    private void Suspend()
    {
      SendAndReceive(ClientMessage.SuspendApplication, ServerMessage.ConfirmSuspend);
    }
    private void SendAndReceive(ClientMessage ClientMessage, ServerMessage ServerMessage)
    {
      SendAndReceive(ClientMessage, null, ServerMessage, null);
    }
    private void SendAndReceive(ClientMessage ClientMessage, Action<ServerWriter> SendAction, ServerMessage ServerMessage, Action<ServerReader> ReceiveAction = null)
    {
      ProcessMessages();

      SendMessage(ClientMessage, SendAction);
      ReceiveMessage(ServerMessage, ReceiveAction);
    }
    private void SendMessage(ClientMessage Message, Action<ServerWriter> Action = null)
    {
      ServerWriter.WriteMessage(Message);

      if (Action != null)
        Action(ServerWriter);

      if (IsActive)
        ServerQueue.SendPacket(ServerWriter.ToPacket());

      ServerWriter.Reset();
    }
    private void ReceiveMessage(ServerMessage Message, Action<ServerReader> Action = null)
    {
      while (IsActive)
      {
        var Packet = ServerQueue.ReceivePacket();

        if (Packet == null)
        {
          // disconnected.
          // TODO: reconnect or display message on exit?

          ExitInvoke();
          return;
        }

        using (var Reader = Packet.ToReader())
        {
          var NextMessage = (ServerMessage)Reader.ReadByte();

          while (IsActive && NextMessage != Message)
          {
            ProcessMessage(NextMessage, Reader);

            if (Reader.EndOfPacket)
              break;

            NextMessage = (ServerMessage)Reader.ReadByte();
          }

          if (IsActive && NextMessage == Message)
          {
            if (Action != null)
              Action(Reader);

            if (!Reader.EndOfPacket)
            {
#if DEBUG
              throw new Exception("End of packet was not found: " + NextMessage);
#endif
            }

            break;
          }
        }
      }
    }
    private Surface GetSurface(ServerSurfaceTag SurfaceTag)
    {
      return SurfaceTag.IsZero ? null : SurfaceDictionary[SurfaceTag];
    }
    private Inv.Panel UsePanel(ServerPanelTag PanelTag)
    {
      var Result = PanelTag.IsZero ? null : PanelDictionary[PanelTag];

      if (Result == null)
        return null;

      if (Result.Parent != null)
        Result.Unparent();

      return Result;
    }
    private T GetPanel<T>(ServerPanelTag PanelTag)
      where T : Inv.Panel
    {
      var Result = PanelDictionary.GetValueOrDefault(PanelTag);

      if (Result == null)
        return null;

      return (T)Result;
    }
    private void ProcessMessages()
    {
      Base.CheckThreadAffinity();

      if (IsActive)
      {
        var Packet = ServerQueue.TryReceivePacket();
        while (Packet != null)
        {
          ServerReceiver.ReceiveAll(Packet);

          if (IsActive)
            Packet = ServerQueue.TryReceivePacket();
          else
            Packet = null;
        }
      }
    }
    private void ProcessMessage(ServerMessage Message, ServerReader Reader)
    {
      Base.CheckThreadAffinity();

      ServerReceiver.ReceiveOne(Message, Reader);
    }
    private Inv.Sound LookupSound(ServerSoundTag SoundTag)
    {
      return SoundTag.IsZero ? null : SoundDictionary.GetValueOrDefault(SoundTag);
    }
    private Inv.Image LookupImage(ServerImageTag ImageTag)
    {
      return ImageTag.IsZero ? null : ImageDictionary.GetValueOrDefault(ImageTag);
    }

    public static implicit operator Inv.Application(ServerApplication Self)
    {
      return Self != null ? Self.Base : null;
    }

    private Application Base;
    private WebClient WebClient;
    private ServerReceiver ServerReceiver;
    private ServerQueue ServerQueue;
    private ServerWriter ServerWriter;
    private Dictionary<ServerSurfaceTag, Inv.Surface> SurfaceDictionary;
    private Dictionary<ServerPanelTag, Panel> PanelDictionary;
    private Dictionary<ServerImageTag, Inv.Image> ImageDictionary;
    private Dictionary<ServerSoundTag, Inv.Sound> SoundDictionary;
    private Dictionary<ServerClipTag, Inv.AudioClip> ClipDictionary;
    private Dictionary<ServerAnimationTag, Inv.Animation> AnimationDictionary;
    private Dictionary<Canvas, ServerPacket> CanvasDrawPacketDictionary;
    private Inv.Folder AttachmentFolder;
  }

  internal sealed class ServerPlatform : Inv.Platform
  {
    public ServerPlatform(ServerTenant Tenant)
    {
      this.Tenant = Tenant;
      this.HostPlatform = Tenant.ServerEngine.HostApplication.Platform;
    }

    int Platform.ThreadAffinity()
    {
      return HostPlatform.ThreadAffinity();
    }
    string Platform.CalendarTimeZoneName()
    {
      return Tenant.CalendarTimeZoneName;
    }
    void Platform.CalendarShowPicker(CalendarPicker CalendarPicker)
    {
      Tenant.CalendarShowPicker(CalendarPicker);
    }
    bool Platform.EmailSendMessage(EmailMessage EmailMessage)
    {
      return Tenant.EmailSendMessage(EmailMessage);
    }
    bool Platform.PhoneIsSupported
    {
      get { return Tenant.PhoneIsSupported; }
    }
    void Platform.PhoneDial(string PhoneNumber)
    {
      Tenant.PhoneDial(PhoneNumber);
    }
    void Platform.PhoneSMS(string PhoneNumber)
    {
      Tenant.PhoneSMS(PhoneNumber);
    }
    long Platform.DirectoryGetLengthFile(File File)
    {
      return HostPlatform.DirectoryGetLengthFile(File);
    }
    DateTime Platform.DirectoryGetLastWriteTimeUtcFile(File File)
    {
      return HostPlatform.DirectoryGetLastWriteTimeUtcFile(File);
    }
    void Platform.DirectorySetLastWriteTimeUtcFile(File File, DateTime Timestamp)
    {
      HostPlatform.DirectorySetLastWriteTimeUtcFile(File, Timestamp);
    }
    Stream Platform.DirectoryCreateFile(File File)
    {
      return HostPlatform.DirectoryCreateFile(File);
    }
    Stream Platform.DirectoryAppendFile(File File)
    {
      return HostPlatform.DirectoryAppendFile(File);
    }
    Stream Platform.DirectoryOpenFile(File File)
    {
      return HostPlatform.DirectoryOpenFile(File);
    }
    bool Platform.DirectoryExistsFile(File File)
    {
      return HostPlatform.DirectoryExistsFile(File);
    }
    void Platform.DirectoryDeleteFile(File File)
    {
      HostPlatform.DirectoryDeleteFile(File);
    }
    void Platform.DirectoryCopyFile(File SourceFile, File TargetFile)
    {
      HostPlatform.DirectoryCopyFile(SourceFile, TargetFile);
    }
    void Platform.DirectoryMoveFile(File SourceFile, File TargetFile)
    {
      HostPlatform.DirectoryMoveFile(SourceFile, TargetFile);
    }
    IEnumerable<File> Platform.DirectoryGetFolderFiles(Folder Folder, string FileMask)
    {
      return HostPlatform.DirectoryGetFolderFiles(Folder, FileMask);
    }
    Stream Platform.DirectoryOpenAsset(Asset Asset)
    {
      return HostPlatform.DirectoryOpenAsset(Asset);
    }
    bool Platform.DirectoryExistsAsset(Asset Asset)
    {
      return HostPlatform.DirectoryExistsAsset(Asset);
    }
    void Platform.DirectoryShowFilePicker(DirectoryFilePicker FilePicker)
    {
      Tenant.DirectoryShowPicker(FilePicker);
    }
    bool Platform.LocationIsSupported
    {
      get { return Tenant.LocationIsSupported; }
    }
    void Platform.LocationLookup(LocationLookup LocationLookup)
    {
      Tenant.LocationLookup(LocationLookup);
    }
    void Platform.AudioPlaySound(Sound Sound, float Volume, float Rate)
    {
      Tenant.PlaySound(Sound, Volume, Rate);
    }
    void Platform.AudioPlayClip(AudioClip Clip)
    {
      Tenant.PlayClip(Clip);
    }
    void Platform.AudioStopClip(AudioClip Clip)
    {
      Tenant.StopClip(Clip);
    }
    void Platform.WindowBrowse(File File)
    {
      Tenant.WindowBrowse(File);
    }
    void Platform.WindowPost(Action Action)
    {
      Tenant.Post(Action);
    }
    void Platform.WindowCall(Action Action)
    {
      Tenant.Call(Action);
    }
    long Platform.ProcessMemoryUsedBytes()
    {
      return HostPlatform.ProcessMemoryUsedBytes();
    }
    void Platform.ProcessMemoryReclamation()
    {
      Tenant.Reclamation();
    }
    void Platform.WebClientConnect(WebClient WebClient)
    {
      HostPlatform.WebClientConnect(WebClient);
    }
    void Platform.WebClientDisconnect(WebClient WebClient)
    {
      HostPlatform.WebClientDisconnect(WebClient);
    }
    void Platform.WebServerConnect(WebServer WebServer)
    {
      HostPlatform.WebServerConnect(WebServer);
    }
    void Platform.WebServerDisconnect(WebServer WebServer)
    {
      HostPlatform.WebServerDisconnect(WebServer);
    }
    void Platform.WebLaunchUri(Uri Uri)
    {
      Tenant.WebLaunchUri(Uri);
    }
    void Platform.MarketBrowse(string AppleiTunesID, string GooglePlayID, string WindowsStoreID)
    {
      Tenant.MarketBrowse(AppleiTunesID, GooglePlayID, WindowsStoreID);
    }
    void Platform.VaultLoadSecret(Secret Secret)
    {
      HostPlatform.VaultLoadSecret(Secret);
    }
    void Platform.VaultSaveSecret(Secret Secret)
    {
      HostPlatform.VaultSaveSecret(Secret);
    }
    void Platform.VaultDeleteSecret(Secret Secret)
    {
      HostPlatform.VaultDeleteSecret(Secret);
    }

    private ServerTenant Tenant;
    private Platform HostPlatform;
  }

  internal struct ServerAnimationTarget
  {
    public ServerPanelTag PanelTag;
    public ServerAnimationCommand[] CommandArray;
  }

  internal struct ServerAnimationCommand
  {
    public AnimationType Type;
    // logical union.
    public TimeSpan? OpacityOffset;
    public TimeSpan OpacityDuration;
    public float OpacityFrom;
    public float OpacityTo;
  }

  internal struct ServerBoardPin
  {
    public Rect Rect;
    public ServerPanelTag PanelTag;
  }

  internal struct ServerTableAxis
  {
    public TableAxisLength LengthType;
    public int LengthValue;
    public ServerPanelTag PanelTag;
  }

  internal struct ServerTableCell
  {
    public int X;
    public int Y;
    public ServerPanelTag PanelTag;
  }

  internal struct ServerFlowSection
  {
    public int ItemCount;
    public ServerPanelTag HeaderPanelTag;
    public ServerPanelTag FooterPanelTag;
  }

  internal struct ServerEmailTo
  {
    public string Name;
    public string Address;
  }

  internal struct ServerEmailAttachment
  {
    public string Name;
    public byte[] Content;
  }

  internal interface ServerContract
  {
    void MemoryReclamation();
    void ExitApplication();
    void SetWindowBackground(Colour Colour);
    void NewSurface(ServerSurfaceTag SurfaceTag);
    void DisposeSurface(ServerSurfaceTag SurfaceTag);
    void DisposePanel(ServerPanelTag PanelTag);
    void SetSurfaceBackground(ServerSurfaceTag SurfaceTag, Colour Colour);
    void SetSurfaceContent(ServerSurfaceTag SurfaceTag, ServerPanelTag ContentTag);
    void TransitionSurface(ServerSurfaceTag SurfaceTag, TransitionAnimation Animation, TimeSpan Duration);
    void NewBrowser(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag);
    void LoadBrowser(ServerPanelTag PanelTag, Uri Uri, string Html);
    void NewButton(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag);
    void SetButtonContent(ServerPanelTag PanelTag, ServerPanelTag ContentTag);
    void SetButtonIsEnabled(ServerPanelTag PanelTag, bool IsEnabled);
    void SetButtonIsFocusable(ServerPanelTag PanelTag, bool IsFocusable);
    void SetButtonHasPress(ServerPanelTag PanelTag, bool HasPress);
    void SetButtonHasRelease(ServerPanelTag PanelTag, bool HasRelease);
    void NewBoard(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag);
    void SetBoardCollection(ServerPanelTag PanelTag, ServerBoardPin[] PinArray);
    void NewDock(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag, DockOrientation Orientation);
    void SetDockCollection(ServerPanelTag PanelTag, ServerPanelTag[] HeaderArray, ServerPanelTag[] ClientArray, ServerPanelTag[] FooterArray);
    void NewEdit(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag, EditInput Input);
    void SetEditFont(ServerPanelTag PanelTag, string Name, int? Size, Colour Colour, FontWeight Weight);
    void SetEditText(ServerPanelTag PanelTag, string Text);
    void SetEditIsReadOnly(ServerPanelTag PanelTag, bool ReadOnly);
    void SetEditHasChange(ServerPanelTag PanelTag, bool HasChange);
    void SetEditHasReturn(ServerPanelTag PanelTag, bool HasReturn);
    void NewFlow(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag);
    void ReloadFlow(ServerPanelTag PanelTag, ServerFlowSection[] SectionArray);
    void NewFrame(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag);
    void SetFrameContent(ServerPanelTag PanelTag, ServerPanelTag ContentTag);
    void NewGraphic(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag);
    void SetGraphicImage(ServerPanelTag PanelTag, ServerImageTag ImageTag);
    void NewLabel(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag);
    void SetLabelLineWrapping(ServerPanelTag PanelTag, bool LineWrapping);
    void SetLabelJustification(ServerPanelTag PanelTag, Justification Justification);
    void SetLabelFont(ServerPanelTag PanelTag, string Name, int? Size, Colour Colour, FontWeight Weight);
    void SetLabelText(ServerPanelTag PanelTag, string Text);
    void NewMemo(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag);
    void SetMemoFont(ServerPanelTag PanelTag, string Name, int? Size, Colour Colour, FontWeight Weight);
    void SetMemoText(ServerPanelTag PanelTag, string Text);
    void SetMemoIsReadOnly(ServerPanelTag PanelTag, bool ReadOnly);
    void SetMemoHasChange(ServerPanelTag PanelTag, bool HasChange);
    void NewOverlay(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag);
    void SetOverlayCollection(ServerPanelTag PanelTag, ServerPanelTag[] ElementArray);
    void NewCanvas(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag);
    void DrawCanvas(ServerPanelTag PanelTag, ServerPacket Packet);
    void NewScroll(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag, ScrollOrientation Orientation);
    void SetScrollContent(ServerPanelTag PanelTag, ServerPanelTag ContentTag);
    void NewStack(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag, StackOrientation Orientation);
    void SetStackCollection(ServerPanelTag PanelTag, ServerPanelTag[] ElementArray);
    void NewTable(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag);
    void SetTableCollection(ServerPanelTag PanelTag, ServerTableAxis[] RowArray, ServerTableAxis[] ColumnArray, ServerTableCell[] CellArray);

    void SetPanelAlignment(ServerPanelTag PanelTag, Inv.Placement Placement);
    void SetPanelBorder(ServerPanelTag PanelTag, int Left, int Top, int Right, int Bottom, Colour Colour);
    void SetPanelCorner(ServerPanelTag PanelTag, int TopLeft, int TopRight, int BottomRight, int BottomLeft);
    void SetPanelElevation(ServerPanelTag PanelTag, int Depth);
    void SetPanelMargin(ServerPanelTag PanelTag, int Left, int Top, int Right, int Bottom);
    void SetPanelOpacity(ServerPanelTag PanelTag, float Opacity);
    void SetPanelPadding(ServerPanelTag PanelTag, int Left, int Top, int Right, int Bottom);
    void SetPanelSize(ServerPanelTag PanelTag, int? Width, int? Height, int? MinimumWidth, int? MinimumHeight, int? MaximumWidth, int? MaximumHeight);
    void SetPanelVisibility(ServerPanelTag PanelTag, bool Visibility);
    void SetPanelBackground(ServerPanelTag PanelTag, Colour BackgroundColour);

    void NewImage(ServerImageTag ImageTag, Inv.Image Image);
    void DisposeImage(ServerImageTag ImageTag);
    void NewSound(ServerSoundTag SoundTag, Inv.Sound Sound);
    void DisposeSound(ServerSoundTag SoundTag);
    void PlaySound(ServerSoundTag SoundTag, float SoundVolume, float SoundRate);
    void PlayClip(ServerClipTag ClipTag, ServerSoundTag SoundTag, float SoundVolume, float SoundRate, bool SoundLoop);
    void StopClip(ServerClipTag ClipTag);

    void StartAnimation(ServerSurfaceTag SurfaceTag, ServerAnimationTag AnimationTag, ServerAnimationTarget[] TargetArray);
    void StopAnimation(ServerAnimationTag AnimationTag);

    void BrowseMarket(string AppleiTunesID, string GooglePlayID, string WindowsStoreID);
    void LaunchWebUri(Uri Uri);
    void ShowCalendarPicker(ServerPickerTag PickerTag, bool SetDate, bool SetTime, DateTime Value);
    void ShowDirectoryPicker(ServerPickerTag PickerTag, string Title, FileType FileType);

    bool SendEmailMessage(string Subject, string Body, ServerEmailTo[] ToArray, ServerEmailAttachment[] AttachmentArray);
    void DialPhone(string PhoneNumber);
    void SMSPhone(string PhoneNumber);
  }

  internal enum ServerMessage
  {
    Invalid,
    ConfirmStartApplication,
    ConfirmSuspend,
    ConfirmResume,
    ConfirmHandleException,
    ConfirmExitQuery,
    ConfirmKeyModifierWindow,
    ConfirmArrangeSurface, 
    ConfirmGestureBackwardSurface, 
    ConfirmGestureForwardSurface, 
    ConfirmKeystrokeSurface,
    ConfirmButtonPress,
    ConfirmButtonRelease,
    ConfirmButtonSingleTap,
    ConfirmButtonContextTap,
    ConfirmEditChange,
    ConfirmEditReturn,
    ConfirmItemQueryFlow,
    ConfirmMemoChange,
    ConfirmCanvasSingleTap,
    ConfirmCanvasDoubleTap,
    ConfirmCanvasContextTap,
    ConfirmCanvasPress,
    ConfirmCanvasRelease,
    ConfirmCanvasMove,
    ConfirmCanvasZoom,
    ConfirmCompleteAnimation,
    ConfirmSelectCalendarPicker,
    ConfirmCancelCalendarPicker,
    ConfirmSelectDirectoryPicker,
    ConfirmCancelDirectoryPicker,
    MemoryReclamation,
    ExitApplication,
    SetWindowBackground,
    NewSurface,
    SetSurfaceBackground,
    SetSurfaceContent,
    TransitionSurface,
    NewBrowser,
    LoadBrowser,
    NewButton,
    SetButtonContent,
    SetButtonIsEnabled,
    SetButtonIsFocusable,
    SetButtonHasPress,
    SetButtonHasRelease,
    NewBoard,
    SetBoardCollection,
    NewHorizontalDock,
    NewVerticalDock,
    NewEdit,
    SetEditText,
    SetEditIsReadOnly,
    SetEditHasChange,
    SetEditHasReturn,
    SetEditFont,
    NewFlow,
    ReloadFlow,
    NewFrame,
    SetFrameContent,
    NewGraphic,
    SetGraphicImage,
    SetDockCollection,
    NewLabel,
    SetLabelText,
    SetLabelJustification,
    SetLabelLineWrapping,
    SetLabelFont,
    NewMemo,
    SetMemoText,
    SetMemoIsReadOnly,
    SetMemoHasChange,
    SetMemoFont,
    NewOverlay,
    SetOverlayCollection,
    NewCanvas,
    DrawCanvas,
    NewHorizontalScroll,
    NewVerticalScroll,
    SetScrollContent,
    NewHorizontalStack,
    NewVerticalStack,
    SetStackCollection,
    NewTable,
    SetTableCollection,
    SetPanelAlignment,
    SetPanelBackground,
    SetPanelSize,
    SetPanelMargin,
    SetPanelPadding,
    SetPanelBorder,
    SetPanelOpacity,
    SetPanelElevation,
    SetPanelCorner,
    SetPanelVisibility,
    NewImage,
    NewSound,
    PlaySound,
    PlayClip,
    StopClip,
    StartAnimation,
    StopAnimation,
    BrowseMarket,
    LaunchWebUri,
    ShowCalendarPicker,
    ShowDirectoryPicker,
    SendEmailMessage,
    DialPhone,
    SMSPhone,
    DisposeSurface,
    DisposePanel,
    DisposeImage,
    DisposeSound
  }

  internal enum ClientMessage
  {
    Invalid,
    ConfirmSendEmailMessage,
    ConfirmDialPhone,
    ConfirmSMSPhone,
    Identification,
    StartApplication,
    SuspendApplication,
    ResumeApplication,
    HandleExceptionApplication,
    ExitQueryApplication,
    KeyModifierWindow,
    ArrangeSurface,
    GestureBackwardSurface,
    GestureForwardSurface,
    KeystrokeSurface,
    ButtonPress,
    ButtonRelease,
    ButtonSingleTap,
    ButtonContextTap,
    EditChange,
    EditReturn,
    ItemQueryFlow,
    MemoChange,
    CanvasSingleTap,
    CanvasDoubleTap,
    CanvasContextTap,
    CanvasPress,
    CanvasRelease,
    CanvasMove,
    CanvasZoom,
    CompleteAnimation,
    SelectCalendarPicker,
    CancelCalendarPicker,
    SelectDirectoryPicker,
    CancelDirectoryPicker
  }

  internal static class ServerFoundation
  {
    public static void WriteSurfaceTag(this ServerWriter Writer, ServerSurfaceTag SurfaceTag)
    {
      Writer.WriteUInt16(SurfaceTag.ID);
    }
    public static ServerSurfaceTag ReadSurfaceTag(this ServerReader Reader)
    {
      return new ServerSurfaceTag(Reader.ReadUInt16());
    }
    public static ServerPanelTag ReadPanelTag(this ServerReader Reader)
    {
      return new ServerPanelTag(Reader.ReadUInt32());
    }
    public static void WritePanelTag(this ServerWriter Writer, ServerPanelTag PanelTag)
    {
      Writer.WriteUInt32(PanelTag.ID);
    }
    public static void WriteMessage(this ServerWriter Writer, ServerMessage Message)
    {
      Writer.WriteByte((byte)Message);
    }
    public static void WriteMessage(this ServerWriter Writer, ClientMessage Message)
    {
      Writer.WriteByte((byte)Message);
    }
    public static void WriteAnimationTag(this ServerWriter Writer, ServerAnimationTag AnimationTag)
    {
      Writer.WriteUInt16(AnimationTag.ID);
    }
    public static ServerAnimationTag ReadAnimationTag(this ServerReader Reader)
    {
      return new ServerAnimationTag(Reader.ReadUInt16());
    }
    public static void WriteImageTag(this ServerWriter Writer, ServerImageTag ImageTag)
    {
      Writer.WriteUInt16(ImageTag.ID);
    }
    public static ServerImageTag ReadImageTag(this ServerReader Reader)
    {
      return new ServerImageTag(Reader.ReadUInt16());
    }
    public static void WriteSoundTag(this ServerWriter Writer, ServerSoundTag SoundTag)
    {
      Writer.WriteUInt16(SoundTag.ID);
    }
    public static ServerSoundTag ReadSoundTag(this ServerReader Reader)
    {
      return new ServerSoundTag(Reader.ReadUInt16());
    }
    public static void WriteClipTag(this ServerWriter Writer, ServerClipTag ClipTag)
    {
      Writer.WriteUInt32(ClipTag.ID);
    }
    public static ServerClipTag ReadClipTag(this ServerReader Reader)
    {
      return new ServerClipTag(Reader.ReadUInt32());
    }
    public static void WritePickerTag(this ServerWriter Writer, ServerPickerTag PickerTag)
    {
      Writer.WriteUInt16(PickerTag.ID);
    }
    public static ServerPickerTag ReadPickerTag(this ServerReader Reader)
    {
      return new ServerPickerTag(Reader.ReadUInt16());
    }
    public static WeakReference<T> AsWeakReference<T>(this T Target)
      where T : class
    {
      return new WeakReference<T>(Target);
    }
  }

  internal sealed class ServerSender : ServerContract
  {
    public ServerSender(ServerTenant Tenant)
    {
      this.Tenant = Tenant;
      this.Writer = new ServerWriter();
    }

    public void Return(ServerMessage Message, Action<ServerWriter> Action = null)
    {
      Writer.WriteByte((byte)Message);

      if (Action != null)
        Action(Writer);

      Send();
    }
    public void Send()
    {
      if (Writer.Size > 0)
      {
        Tenant.ServerQueue.SendPacket(Writer.ToPacket());
        Writer.Reset();
      }
    }

    void ServerContract.MemoryReclamation()
    {
      Writer.WriteMessage(ServerMessage.MemoryReclamation);
    }
    void ServerContract.ExitApplication()
    {
      Writer.WriteMessage(ServerMessage.ExitApplication);
    }
    void ServerContract.SetWindowBackground(Colour Colour)
    {
      Writer.WriteMessage(ServerMessage.SetWindowBackground);
      Writer.WriteColour(Colour);
    }
    void ServerContract.NewSurface(ServerSurfaceTag SurfaceTag)
    {
      Writer.WriteMessage(ServerMessage.NewSurface);
      Writer.WriteSurfaceTag(SurfaceTag);
    }
    void ServerContract.SetSurfaceBackground(ServerSurfaceTag SurfaceTag, Colour Colour)
    {
      Writer.WriteMessage(ServerMessage.SetSurfaceBackground);
      Writer.WriteSurfaceTag(SurfaceTag);
      Writer.WriteColour(Colour);
    }
    void ServerContract.SetSurfaceContent(ServerSurfaceTag SurfaceTag, ServerPanelTag ContentTag)
    {
      Writer.WriteMessage(ServerMessage.SetSurfaceContent);
      Writer.WriteSurfaceTag(SurfaceTag);
      Writer.WritePanelTag(ContentTag);
    }
    void ServerContract.TransitionSurface(ServerSurfaceTag SurfaceTag, TransitionAnimation Animation, TimeSpan Duration)
    {
      Writer.WriteMessage(ServerMessage.TransitionSurface);
      Writer.WriteSurfaceTag(SurfaceTag);
      Writer.WriteByte((byte)Animation);
      Writer.WriteTimeSpan(Duration);
    }
    void ServerContract.NewBrowser(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag)
    {
      Writer.WriteMessage(ServerMessage.NewBrowser);
      Writer.WriteSurfaceTag(SurfaceTag);
      Writer.WritePanelTag(PanelTag);
    }
    void ServerContract.LoadBrowser(ServerPanelTag PanelTag, Uri Uri, string Html)
    {
      Writer.WriteMessage(ServerMessage.LoadBrowser);
      Writer.WritePanelTag(PanelTag);
      Writer.WriteUri(Uri);
      Writer.WriteString(Html);
    }
    void ServerContract.NewButton(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag)
    {
      Writer.WriteMessage(ServerMessage.NewButton);
      Writer.WriteSurfaceTag(SurfaceTag);
      Writer.WritePanelTag(PanelTag);
    }
    void ServerContract.SetButtonContent(ServerPanelTag PanelTag, ServerPanelTag ContentTag)
    {
      Writer.WriteMessage(ServerMessage.SetButtonContent);
      Writer.WritePanelTag(PanelTag);
      Writer.WritePanelTag(ContentTag);
    }
    void ServerContract.SetButtonIsEnabled(ServerPanelTag PanelTag, bool IsEnabled)
    {
      Writer.WriteMessage(ServerMessage.SetButtonIsEnabled);
      Writer.WritePanelTag(PanelTag);
      Writer.WriteBoolean(IsEnabled);
    }
    void ServerContract.SetButtonIsFocusable(ServerPanelTag PanelTag, bool IsFocusable)
    {
      Writer.WriteMessage(ServerMessage.SetButtonIsFocusable);
      Writer.WritePanelTag(PanelTag);
      Writer.WriteBoolean(IsFocusable);
    }
    void ServerContract.SetButtonHasPress(ServerPanelTag PanelTag, bool HasPress)
    {
      Writer.WriteMessage(ServerMessage.SetButtonHasPress);
      Writer.WritePanelTag(PanelTag);
      Writer.WriteBoolean(HasPress);
    }
    void ServerContract.SetButtonHasRelease(ServerPanelTag PanelTag, bool HasRelease)
    {
      Writer.WriteMessage(ServerMessage.SetButtonHasRelease);
      Writer.WritePanelTag(PanelTag);
      Writer.WriteBoolean(HasRelease);
    }
    void ServerContract.NewBoard(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag)
    {
      Writer.WriteMessage(ServerMessage.NewBoard);
      Writer.WriteSurfaceTag(SurfaceTag);
      Writer.WritePanelTag(PanelTag);
    }
    void ServerContract.SetBoardCollection(ServerPanelTag PanelTag, ServerBoardPin[] PinArray)
    {
      Writer.WriteMessage(ServerMessage.SetBoardCollection);
      Writer.WritePanelTag(PanelTag);
      Writer.WriteInt32(PinArray.Length);
      foreach (var Pin in PinArray)
      {
        Writer.WritePanelTag(Pin.PanelTag);
        Writer.WriteInt32(Pin.Rect.Left);
        Writer.WriteInt32(Pin.Rect.Top);
        Writer.WriteInt32(Pin.Rect.Width);
        Writer.WriteInt32(Pin.Rect.Height);
      }
    }
    void ServerContract.NewDock(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag, DockOrientation Orientation)
    {
      Writer.WriteMessage(Orientation == DockOrientation.Horizontal ? ServerMessage.NewHorizontalDock : ServerMessage.NewVerticalDock);
      Writer.WriteSurfaceTag(SurfaceTag);
      Writer.WritePanelTag(PanelTag);
    }
    void ServerContract.NewEdit(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag, EditInput Input)
    {
      Writer.WriteMessage(ServerMessage.NewEdit);
      Writer.WriteSurfaceTag(SurfaceTag);
      Writer.WritePanelTag(PanelTag);
      Writer.WriteByte((byte)Input);
    }
    void ServerContract.SetEditIsReadOnly(ServerPanelTag PanelTag, bool ReadOnly)
    {
      Writer.WriteMessage(ServerMessage.SetEditIsReadOnly);
      Writer.WritePanelTag(PanelTag);
      Writer.WriteBoolean(ReadOnly);
    }
    void ServerContract.SetEditHasChange(ServerPanelTag PanelTag, bool HasChange)
    {
      Writer.WriteMessage(ServerMessage.SetEditHasChange);
      Writer.WritePanelTag(PanelTag);
      Writer.WriteBoolean(HasChange);
    }
    void ServerContract.SetEditHasReturn(ServerPanelTag PanelTag, bool HasReturn)
    {
      Writer.WriteMessage(ServerMessage.SetEditHasReturn);
      Writer.WritePanelTag(PanelTag);
      Writer.WriteBoolean(HasReturn);
    }
    void ServerContract.SetEditText(ServerPanelTag PanelTag, string Text)
    {
      Writer.WriteMessage(ServerMessage.SetEditText);
      Writer.WritePanelTag(PanelTag);
      Writer.WriteString(Text);
    }
    void ServerContract.SetEditFont(ServerPanelTag PanelTag, string Name, int? Size, Colour Colour, FontWeight Weight)
    {
      Writer.WriteMessage(ServerMessage.SetEditFont);
      Writer.WritePanelTag(PanelTag);
      Writer.WriteString(Name);
      Writer.WriteInt32(Size ?? 0);
      Writer.WriteColour(Colour);
      Writer.WriteByte((byte)Weight);
    }
    void ServerContract.NewFlow(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag)
    {
      Writer.WriteMessage(ServerMessage.NewFlow);
      Writer.WriteSurfaceTag(SurfaceTag);
      Writer.WritePanelTag(PanelTag);
    }
    void ServerContract.ReloadFlow(ServerPanelTag PanelTag, ServerFlowSection[] SectionArray)
    {
      Writer.WriteMessage(ServerMessage.ReloadFlow);
      Writer.WritePanelTag(PanelTag);
      Writer.WriteInt32(SectionArray.Length);
      foreach (var Section in SectionArray)
      {
        Writer.WriteInt32(Section.ItemCount);
        Writer.WritePanelTag(Section.HeaderPanelTag);
        Writer.WritePanelTag(Section.FooterPanelTag);
      }
    }
    void ServerContract.NewFrame(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag)
    {
      Writer.WriteMessage(ServerMessage.NewFrame);
      Writer.WriteSurfaceTag(SurfaceTag);
      Writer.WritePanelTag(PanelTag);
    }
    void ServerContract.NewGraphic(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag)
    {
      Writer.WriteMessage(ServerMessage.NewGraphic);
      Writer.WriteSurfaceTag(SurfaceTag);
      Writer.WritePanelTag(PanelTag);
    }
    void ServerContract.SetGraphicImage(ServerPanelTag PanelTag, ServerImageTag ImageTag)
    {
      Writer.WriteMessage(ServerMessage.SetGraphicImage);
      Writer.WritePanelTag(PanelTag);
      Writer.WriteImageTag(ImageTag);
    }
    void ServerContract.NewLabel(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag)
    {
      Writer.WriteMessage(ServerMessage.NewLabel);
      Writer.WriteSurfaceTag(SurfaceTag);
      Writer.WritePanelTag(PanelTag);
    }
    void ServerContract.SetLabelLineWrapping(ServerPanelTag PanelTag, bool LineWrapping)
    {
      Writer.WriteMessage(ServerMessage.SetLabelLineWrapping);
      Writer.WritePanelTag(PanelTag);
      Writer.WriteBoolean(LineWrapping);
    }
    void ServerContract.SetLabelJustification(ServerPanelTag PanelTag, Justification Justification)
    {
      Writer.WriteMessage(ServerMessage.SetLabelJustification);
      Writer.WritePanelTag(PanelTag);
      Writer.WriteByte((byte)Justification);
    }
    void ServerContract.SetLabelText(ServerPanelTag PanelTag, string Text)
    {
      Writer.WriteMessage(ServerMessage.SetLabelText);
      Writer.WritePanelTag(PanelTag);
      Writer.WriteString(Text);
    }
    void ServerContract.SetLabelFont(ServerPanelTag PanelTag, string Name, int? Size, Colour Colour, FontWeight Weight)
    {
      Writer.WriteMessage(ServerMessage.SetLabelFont);
      Writer.WritePanelTag(PanelTag);
      Writer.WriteString(Name);
      Writer.WriteInt32(Size ?? 0);
      Writer.WriteColour(Colour);
      Writer.WriteByte((byte)Weight);
    }
    void ServerContract.NewMemo(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag)
    {
      Writer.WriteMessage(ServerMessage.NewMemo);
      Writer.WriteSurfaceTag(SurfaceTag);
      Writer.WritePanelTag(PanelTag);
    }
    void ServerContract.SetMemoIsReadOnly(ServerPanelTag PanelTag, bool ReadOnly)
    {
      Writer.WriteMessage(ServerMessage.SetMemoIsReadOnly);
      Writer.WritePanelTag(PanelTag);
      Writer.WriteBoolean(ReadOnly);
    }
    void ServerContract.SetMemoHasChange(ServerPanelTag PanelTag, bool HasChange)
    {
      Writer.WriteMessage(ServerMessage.SetMemoHasChange);
      Writer.WritePanelTag(PanelTag);
      Writer.WriteBoolean(HasChange);
    }
    void ServerContract.SetMemoText(ServerPanelTag PanelTag, string Text)
    {
      Writer.WriteMessage(ServerMessage.SetMemoText);
      Writer.WritePanelTag(PanelTag);
      Writer.WriteString(Text);
    }
    void ServerContract.SetMemoFont(ServerPanelTag PanelTag, string Name, int? Size, Colour Colour, FontWeight Weight)
    {
      Writer.WriteMessage(ServerMessage.SetMemoFont);
      Writer.WritePanelTag(PanelTag);
      Writer.WriteString(Name);
      Writer.WriteInt32(Size ?? 0);
      Writer.WriteColour(Colour);
      Writer.WriteByte((byte)Weight);
    }
    void ServerContract.NewOverlay(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag)
    {
      Writer.WriteMessage(ServerMessage.NewOverlay);
      Writer.WriteSurfaceTag(SurfaceTag);
      Writer.WritePanelTag(PanelTag);
    }
    void ServerContract.NewCanvas(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag)
    {
      Writer.WriteMessage(ServerMessage.NewCanvas);
      Writer.WriteSurfaceTag(SurfaceTag);
      Writer.WritePanelTag(PanelTag);
    }
    void ServerContract.DrawCanvas(ServerPanelTag PanelTag, ServerPacket Packet)
    {
      Writer.WriteMessage(ServerMessage.DrawCanvas);
      Writer.WritePanelTag(PanelTag);
      Writer.WriteByteArray(Packet.Buffer);
    }
    void ServerContract.NewScroll(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag, ScrollOrientation Orientation)
    {
      Writer.WriteMessage(Orientation == ScrollOrientation.Horizontal ? ServerMessage.NewHorizontalScroll : ServerMessage.NewVerticalScroll);
      Writer.WriteSurfaceTag(SurfaceTag);
      Writer.WritePanelTag(PanelTag);
    }
    void ServerContract.NewStack(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag, StackOrientation Orientation)
    {
      Writer.WriteMessage(Orientation == StackOrientation.Horizontal ? ServerMessage.NewHorizontalStack : ServerMessage.NewVerticalStack);
      Writer.WriteSurfaceTag(SurfaceTag);
      Writer.WritePanelTag(PanelTag);
    }
    void ServerContract.NewTable(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag)
    {
      Writer.WriteMessage(ServerMessage.NewTable);
      Writer.WriteSurfaceTag(SurfaceTag);
      Writer.WritePanelTag(PanelTag);
    }
    void ServerContract.SetTableCollection(ServerPanelTag PanelTag, ServerTableAxis[] RowArray, ServerTableAxis[] ColumnArray, ServerTableCell[] CellArray)
    {
      Writer.WriteMessage(ServerMessage.SetTableCollection);
      Writer.WritePanelTag(PanelTag);

      Writer.WriteInt32(RowArray.Length);
      foreach (var Row in RowArray)
      {
        Writer.WriteByte((byte)Row.LengthType);
        Writer.WriteInt32(Row.LengthValue);
        Writer.WritePanelTag(Row.PanelTag);
      }

      Writer.WriteInt32(ColumnArray.Length);
      foreach (var Column in ColumnArray)
      {
        Writer.WriteByte((byte)Column.LengthType);
        Writer.WriteInt32(Column.LengthValue);
        Writer.WritePanelTag(Column.PanelTag);
      }

      Writer.WriteInt32(CellArray.Length);
      foreach (var Cell in CellArray)
      {
        Writer.WriteInt32(Cell.X);
        Writer.WriteInt32(Cell.Y);
        Writer.WritePanelTag(Cell.PanelTag);
      }
    }
    void ServerContract.SetPanelAlignment(ServerPanelTag PanelTag, Inv.Placement Placement)
    {
      Writer.WriteMessage(ServerMessage.SetPanelAlignment);
      Writer.WritePanelTag(PanelTag);
      Writer.WriteByte((byte)Placement);
    }
    void ServerContract.SetPanelBorder(ServerPanelTag PanelTag, int Left, int Top, int Right, int Bottom, Colour Colour)
    {
      Writer.WriteMessage(ServerMessage.SetPanelBorder);
      Writer.WritePanelTag(PanelTag);
      Writer.WriteInt32(Left);
      Writer.WriteInt32(Top);
      Writer.WriteInt32(Right);
      Writer.WriteInt32(Bottom);
      Writer.WriteColour(Colour);
    }
    void ServerContract.SetPanelCorner(ServerPanelTag PanelTag, int TopLeft, int TopRight, int BottomRight, int BottomLeft)
    {
      Writer.WriteMessage(ServerMessage.SetPanelCorner);
      Writer.WritePanelTag(PanelTag);
      Writer.WriteInt32(TopLeft);
      Writer.WriteInt32(TopRight);
      Writer.WriteInt32(BottomRight);
      Writer.WriteInt32(BottomLeft);
    }
    void ServerContract.SetPanelElevation(ServerPanelTag PanelTag, int Depth)
    {
      Writer.WriteMessage(ServerMessage.SetPanelElevation);
      Writer.WritePanelTag(PanelTag);
      Writer.WriteInt32(Depth);
    }
    void ServerContract.SetPanelMargin(ServerPanelTag PanelTag, int Left, int Top, int Right, int Bottom)
    {
      Writer.WriteMessage(ServerMessage.SetPanelMargin);
      Writer.WritePanelTag(PanelTag);
      Writer.WriteInt32(Left);
      Writer.WriteInt32(Top);
      Writer.WriteInt32(Right);
      Writer.WriteInt32(Bottom);
    }
    void ServerContract.SetPanelOpacity(ServerPanelTag PanelTag, float Opacity)
    {
      Writer.WriteMessage(ServerMessage.SetPanelOpacity);
      Writer.WritePanelTag(PanelTag);
      Writer.WriteFloat(Opacity);
    }
    void ServerContract.SetPanelPadding(ServerPanelTag PanelTag, int Left, int Top, int Right, int Bottom)
    {
      Writer.WriteMessage(ServerMessage.SetPanelPadding);
      Writer.WritePanelTag(PanelTag);
      Writer.WriteInt32(Left);
      Writer.WriteInt32(Top);
      Writer.WriteInt32(Right);
      Writer.WriteInt32(Bottom);
    }
    void ServerContract.SetPanelSize(ServerPanelTag PanelTag, int? Width, int? Height, int? MinimumWidth, int? MinimumHeight, int? MaximumWidth, int? MaximumHeight)
    {
      Writer.WriteMessage(ServerMessage.SetPanelSize);
      Writer.WritePanelTag(PanelTag);
      Writer.WriteInt32(Width ?? -1);
      Writer.WriteInt32(Height ?? -1);
      Writer.WriteInt32(MinimumWidth ?? -1);
      Writer.WriteInt32(MinimumHeight ?? -1);
      Writer.WriteInt32(MaximumWidth ?? -1);
      Writer.WriteInt32(MaximumHeight ?? -1);
    }
    void ServerContract.SetPanelVisibility(ServerPanelTag PanelTag, bool Visibility)
    {
      Writer.WriteMessage(ServerMessage.SetPanelVisibility);
      Writer.WritePanelTag(PanelTag);
      Writer.WriteBoolean(Visibility);
    }
    void ServerContract.SetPanelBackground(ServerPanelTag PanelTag, Colour BackgroundColour)
    {
      Writer.WriteMessage(ServerMessage.SetPanelBackground);
      Writer.WritePanelTag(PanelTag);
      Writer.WriteColour(BackgroundColour);
    }
    void ServerContract.SetDockCollection(ServerPanelTag PanelTag, ServerPanelTag[] HeaderArray, ServerPanelTag[] ClientArray, ServerPanelTag[] FooterArray)
    {
      Writer.WriteMessage(ServerMessage.SetDockCollection);
      Writer.WritePanelTag(PanelTag);

      Writer.WriteInt32(HeaderArray.Length);
      foreach (var Panel in HeaderArray)
        Writer.WritePanelTag(Panel);

      Writer.WriteInt32(ClientArray.Length);
      foreach (var Panel in ClientArray)
        Writer.WritePanelTag(Panel);

      Writer.WriteInt32(FooterArray.Length);
      foreach (var Panel in FooterArray)
        Writer.WritePanelTag(Panel);
    }
    void ServerContract.SetFrameContent(ServerPanelTag PanelTag, ServerPanelTag ContentTag)
    {
      Writer.WriteMessage(ServerMessage.SetFrameContent);
      Writer.WritePanelTag(PanelTag);
      Writer.WritePanelTag(ContentTag);
    }
    void ServerContract.SetOverlayCollection(ServerPanelTag PanelTag, ServerPanelTag[] ElementArray)
    {
      Writer.WriteMessage(ServerMessage.SetOverlayCollection);
      Writer.WritePanelTag(PanelTag);

      Writer.WriteInt32(ElementArray.Length);
      foreach (var Panel in ElementArray)
        Writer.WritePanelTag(Panel);
    }
    void ServerContract.SetScrollContent(ServerPanelTag PanelTag, ServerPanelTag ContentTag)
    {
      Writer.WriteMessage(ServerMessage.SetScrollContent);
      Writer.WritePanelTag(PanelTag);
      Writer.WritePanelTag(ContentTag);
    }
    void ServerContract.SetStackCollection(ServerPanelTag PanelTag, ServerPanelTag[] ElementArray)
    {
      Writer.WriteMessage(ServerMessage.SetStackCollection);
      Writer.WritePanelTag(PanelTag);

      Writer.WriteInt32(ElementArray.Length);
      foreach (var Panel in ElementArray)
        Writer.WritePanelTag(Panel);
    }
    void ServerContract.NewImage(ServerImageTag ImageTag, Inv.Image ImageSource)
    {
      Writer.WriteMessage(ServerMessage.NewImage);
      Writer.WriteImageTag(ImageTag);
      Writer.WriteImage(ImageSource);
    }
    void ServerContract.NewSound(ServerSoundTag SoundTag, Inv.Sound SoundSource)
    {
      Writer.WriteMessage(ServerMessage.NewSound);
      Writer.WriteSoundTag(SoundTag);
      Writer.WriteSound(SoundSource);
    }
    void ServerContract.PlaySound(ServerSoundTag SoundTag, float SoundVolume, float SoundRate)
    {
      Writer.WriteMessage(ServerMessage.PlaySound);
      Writer.WriteSoundTag(SoundTag);
      Writer.WriteFloat(SoundVolume);
      Writer.WriteFloat(SoundRate);
    }
    void ServerContract.PlayClip(ServerClipTag ClipTag, ServerSoundTag SoundTag, float Volume, float Rate, bool Loop)
    {
      Writer.WriteMessage(ServerMessage.PlayClip);
      Writer.WriteClipTag(ClipTag);
      Writer.WriteSoundTag(SoundTag);
      Writer.WriteFloat(Volume);
      Writer.WriteFloat(Rate);
      Writer.WriteBoolean(Loop);
    }
    void ServerContract.StopClip(ServerClipTag ClipTag)
    {
      Writer.WriteMessage(ServerMessage.StopClip);
      Writer.WriteClipTag(ClipTag);
    }
    void ServerContract.StartAnimation(ServerSurfaceTag SurfaceTag, ServerAnimationTag AnimationTag, ServerAnimationTarget[] TargetArray)
    {
      Writer.WriteMessage(ServerMessage.StartAnimation);
      Writer.WriteSurfaceTag(SurfaceTag);
      Writer.WriteAnimationTag(AnimationTag);
      Writer.WriteInt32(TargetArray.Length);
      foreach (var Target in TargetArray)
      {
        Writer.WritePanelTag(Target.PanelTag);
        Writer.WriteInt32(Target.CommandArray.Length);
        foreach (var Command in Target.CommandArray)
        {
          Writer.WriteByte((byte)Command.Type);

          switch (Command.Type)
          {
            case AnimationType.Opacity:
              Writer.WriteTimeSpan(Command.OpacityOffset ?? TimeSpan.Zero);
              Writer.WriteTimeSpan(Command.OpacityDuration);
              Writer.WriteFloat(Command.OpacityFrom);
              Writer.WriteFloat(Command.OpacityTo);
              break;

            default:
              throw new Exception("AnimationType not handled: " + Command.Type);
          }
        }
      }
    }
    void ServerContract.StopAnimation(ServerAnimationTag AnimationTag)
    {
      Writer.WriteMessage(ServerMessage.StopAnimation);
      Writer.WriteAnimationTag(AnimationTag);
    }
    void ServerContract.BrowseMarket(string AppleiTunesID, string GooglePlayID, string WindowsStoreID)
    {
      Writer.WriteMessage(ServerMessage.BrowseMarket);
      Writer.WriteString(AppleiTunesID);
      Writer.WriteString(GooglePlayID);
      Writer.WriteString(WindowsStoreID);
    }
    void ServerContract.LaunchWebUri(Uri Uri)
    {
      Writer.WriteMessage(ServerMessage.LaunchWebUri);
      Writer.WriteUri(Uri);
    }
    void ServerContract.ShowCalendarPicker(ServerPickerTag PickerTag, bool SetDate, bool SetTime, DateTime Value)
    {
      Writer.WriteMessage(ServerMessage.ShowCalendarPicker);
      Writer.WritePickerTag(PickerTag);
      Writer.WriteBoolean(SetDate);
      Writer.WriteBoolean(SetTime);
      Writer.WriteDateTime(Value);
    }
    void ServerContract.ShowDirectoryPicker(ServerPickerTag PickerTag, string Title, FileType FileType)
    {
      Writer.WriteMessage(ServerMessage.ShowDirectoryPicker);
      Writer.WritePickerTag(PickerTag);
      Writer.WriteString(Title);
      Writer.WriteByte((byte)FileType);
    }
    bool ServerContract.SendEmailMessage(string Subject, string Body, ServerEmailTo[] ToArray, ServerEmailAttachment[] AttachmentArray)
    {
      Tenant.ProcessMessages();

      Writer.WriteMessage(ServerMessage.SendEmailMessage);
      Writer.WriteString(Subject);
      Writer.WriteString(Body);
      
      Writer.WriteInt32(ToArray.Length);
      foreach (var To in ToArray)
      {
        Writer.WriteString(To.Name);
        Writer.WriteString(To.Address);
      }

      Writer.WriteInt32(AttachmentArray.Length);
      foreach (var Attachment in AttachmentArray)
      {
        Writer.WriteString(Attachment.Name);
        Writer.WriteByteArray(Attachment.Content);
      }

      Send();

      var Result = false;

      ReceiveMessage(ClientMessage.ConfirmSendEmailMessage, Reader =>
      {
        Result = Reader.ReadBoolean();
      });

      return Result;
    }
    void ServerContract.DialPhone(string PhoneNumber)
    {
      Tenant.ProcessMessages();

      Writer.WriteMessage(ServerMessage.DialPhone);
      Writer.WriteString(PhoneNumber);

      Send();

      ReceiveMessage(ClientMessage.ConfirmDialPhone);
    }
    void ServerContract.SMSPhone(string PhoneNumber)
    {
      Tenant.ProcessMessages();

      Writer.WriteMessage(ServerMessage.SMSPhone);
      Writer.WriteString(PhoneNumber);

      Send();

      ReceiveMessage(ClientMessage.ConfirmSMSPhone);
    }
    void ServerContract.DisposeSurface(ServerSurfaceTag SurfaceTag)
    {
      Writer.WriteMessage(ServerMessage.DisposeSurface);
      Writer.WriteSurfaceTag(SurfaceTag);
    }
    void ServerContract.DisposePanel(ServerPanelTag PanelTag)
    {
      Writer.WriteMessage(ServerMessage.DisposePanel);
      Writer.WritePanelTag(PanelTag);
    }
    void ServerContract.DisposeImage(ServerImageTag ImageTag)
    {
      Writer.WriteMessage(ServerMessage.DisposeImage);
      Writer.WriteImageTag(ImageTag);
    }
    void ServerContract.DisposeSound(ServerSoundTag SoundTag)
    {
      Writer.WriteMessage(ServerMessage.DisposeSound);
      Writer.WriteSoundTag(SoundTag);
    }

    private void ReceiveMessage(ClientMessage Message, Action<ServerReader> Action = null)
    {
      do
      {
        var Packet = Tenant.ServerQueue.ReceivePacket();

        using (var Reader = Packet.ToReader())
        {
          var NextMessage = (ClientMessage)Reader.ReadByte();

          while (NextMessage != Message)
          {
            Tenant.ProcessMessage(NextMessage, Reader);

            if (Reader.EndOfPacket)
              break;

            NextMessage = (ClientMessage)Reader.ReadByte();
          }

          if (NextMessage == Message)
          {
            if (Action != null)
              Action(Reader);

            if (!Reader.EndOfPacket)
              throw new Exception("End of packet was not found: " + NextMessage);

            break;
          }
        }
      }
      while (true);
    }

    private ServerTenant Tenant;
    private ServerWriter Writer;
  }

  internal sealed class ServerReceiver
  {
    public ServerReceiver(ServerContract Contract)
    {
      this.Contract = Contract;
      this.MessageArray = new EnumArray<ServerMessage, Action<ServerReader>>();

      var OwnerType = GetType().GetReflectionInfo();
      var MethodInfoArray = OwnerType.GetReflectionMethods().Where(M => !M.IsPublic && !M.IsStatic).ToArray();

      foreach (var MethodInfo in MethodInfoArray)
      {
        var ParameterInfoArray = MethodInfo.GetParameters();

        if (ParameterInfoArray.Length == 1 && ParameterInfoArray[0].ParameterType == typeof(ServerReader))
        {
          var Message = Inv.Support.EnumHelper.Parse<ServerMessage>(MethodInfo.Name);

          MessageArray[Message] = (Action<ServerReader>)MethodInfo.CreateDelegate(typeof(Action<>).MakeGenericType(typeof(ServerReader)), this);
        }
      }
    }

    public void ReceiveAll(ServerPacket Packet)
    {
      using (var Reader = Packet.ToReader())
      {
        while (!Reader.EndOfPacket)
        {
          var Message = (ServerMessage)Reader.ReadByte();
          ReceiveOne(Message, Reader);
        }
      }
    }
    public void ReceiveOne(ServerMessage Message, ServerReader Reader)
    {
      var ReceiveAction = MessageArray[Message];

      if (ReceiveAction == null)
        throw new Exception("ServerMessage not handled: " + Message);

      ReceiveAction(Reader);
    }

    private void MemoryReclamation(ServerReader Reader)
    {
      Contract.MemoryReclamation();
    }
    private void ExitApplication(ServerReader Reader)
    {
      Contract.ExitApplication();
    }
    private void SetWindowBackground(ServerReader Reader)
    {
      var Colour = Reader.ReadColour();
      Contract.SetWindowBackground(Colour);
    }
    private void NewSurface(ServerReader Reader)
    {
      var SurfaceTag = Reader.ReadSurfaceTag();
      Contract.NewSurface(SurfaceTag);
    }
    private void SetSurfaceBackground(ServerReader Reader)
    {
      var SurfaceTag = Reader.ReadSurfaceTag();
      var Colour = Reader.ReadColour();
      Contract.SetSurfaceBackground(SurfaceTag, Colour);
    }
    private void SetSurfaceContent(ServerReader Reader)
    {
      var SurfaceTag = Reader.ReadSurfaceTag();
      var ContentTag = Reader.ReadPanelTag();
      Contract.SetSurfaceContent(SurfaceTag, ContentTag);
    }
    private void TransitionSurface(ServerReader Reader)
    {
      var SurfaceTag = Reader.ReadSurfaceTag();
      var Animation = (TransitionAnimation)Reader.ReadByte();
      var Duration = Reader.ReadTimeSpan();

      Contract.TransitionSurface(SurfaceTag, Animation, Duration);
    }
    private void NewBrowser(ServerReader Reader)
    {
      var SurfaceTag = Reader.ReadSurfaceTag();
      var PanelTag = Reader.ReadPanelTag();
      Contract.NewBrowser(SurfaceTag, PanelTag);
    }
    private void LoadBrowser(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Uri = Reader.ReadUri();
      var Html = Reader.ReadString();
      Contract.LoadBrowser(PanelTag, Uri, Html);
    }
    private void NewButton(ServerReader Reader)
    {
      var SurfaceTag = Reader.ReadSurfaceTag();
      var PanelTag = Reader.ReadPanelTag();
      Contract.NewButton(SurfaceTag, PanelTag);
    }
    private void SetButtonContent(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var ContentTag = Reader.ReadPanelTag();
      Contract.SetButtonContent(PanelTag, ContentTag);
    }
    private void SetButtonIsEnabled(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var IsEnabled = Reader.ReadBoolean();
      Contract.SetButtonIsEnabled(PanelTag, IsEnabled);
    }
    private void SetButtonIsFocusable(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var IsFocusable = Reader.ReadBoolean();
      Contract.SetButtonIsFocusable(PanelTag, IsFocusable);
    }
    private void SetButtonHasPress(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var HasPress = Reader.ReadBoolean();
      Contract.SetButtonHasPress(PanelTag, HasPress);
    }
    private void SetButtonHasRelease(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var HasRelease = Reader.ReadBoolean();
      Contract.SetButtonHasRelease(PanelTag, HasRelease);
    }
    private void NewBoard(ServerReader Reader)
    {
      var SurfaceTag = Reader.ReadSurfaceTag();
      var PanelTag = Reader.ReadPanelTag();
      Contract.NewBoard(SurfaceTag, PanelTag);
    }
    private void SetBoardCollection(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();

      var Count = Reader.ReadInt32();
      var PinArray = new ServerBoardPin[Count];
      for (var i = 0; i < Count; i++)
      {
        var PinTag = Reader.ReadPanelTag();
        var PanelLeft = Reader.ReadInt32();
        var PanelTop = Reader.ReadInt32();
        var PanelWidth = Reader.ReadInt32();
        var PanelHeight = Reader.ReadInt32();

        PinArray[i] = new ServerBoardPin()
        {
          PanelTag = PinTag,
          Rect = new Rect(PanelLeft, PanelTop, PanelWidth, PanelHeight)
        };
      }

      Contract.SetBoardCollection(PanelTag, PinArray);
    }
    private void NewHorizontalDock(ServerReader Reader)
    {
      var SurfaceTag = Reader.ReadSurfaceTag();
      var PanelTag = Reader.ReadPanelTag();
      Contract.NewDock(SurfaceTag, PanelTag, DockOrientation.Horizontal);
    }
    private void NewVerticalDock(ServerReader Reader)
    {
      var SurfaceTag = Reader.ReadSurfaceTag();
      var PanelTag = Reader.ReadPanelTag();
      Contract.NewDock(SurfaceTag, PanelTag, DockOrientation.Vertical);
    }
    private void SetDockCollection(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();

      var HeaderCount = Reader.ReadInt32();
      var HeaderArray = new ServerPanelTag[HeaderCount];
      for (var i = 0; i < HeaderCount; i++)
        HeaderArray[i] = Reader.ReadPanelTag();

      var ClientCount = Reader.ReadInt32();
      var ClientArray = new ServerPanelTag[ClientCount];
      for (var i = 0; i < ClientCount; i++)
        ClientArray[i] = Reader.ReadPanelTag();

      var FooterCount = Reader.ReadInt32();
      var FooterArray = new ServerPanelTag[FooterCount];
      for (var i = 0; i < FooterCount; i++)
        FooterArray[i] = Reader.ReadPanelTag();

      Contract.SetDockCollection(PanelTag, HeaderArray, ClientArray, FooterArray);
    }
    private void NewEdit(ServerReader Reader)
    {
      var SurfaceTag = Reader.ReadSurfaceTag();
      var PanelTag = Reader.ReadPanelTag();
      var Input = (EditInput)Reader.ReadByte();
      Contract.NewEdit(SurfaceTag, PanelTag, Input);
    }
    private void SetEditText(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Text = Reader.ReadString();
      Contract.SetEditText(PanelTag, Text);
    }
    private void SetEditIsReadOnly(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var ReadOnly = Reader.ReadBoolean();
      Contract.SetEditIsReadOnly(PanelTag, ReadOnly);
    }
    private void SetEditHasChange(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var HasChange = Reader.ReadBoolean();
      Contract.SetEditHasChange(PanelTag, HasChange);
    }
    private void SetEditHasReturn(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var HasReturn = Reader.ReadBoolean();
      Contract.SetEditHasReturn(PanelTag, HasReturn);
    }
    private void SetEditFont(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Name = Reader.ReadString();
      var Size = Reader.ReadInt32();
      var Colour = Reader.ReadColour();
      var Weight = (FontWeight)Reader.ReadByte();
      Contract.SetEditFont(PanelTag, Name, Size > 0 ? Size : (int?)null, Colour, Weight);
    }
    private void NewFlow(ServerReader Reader)
    {
      var SurfaceTag = Reader.ReadSurfaceTag();
      var PanelTag = Reader.ReadPanelTag();
      Contract.NewFlow(SurfaceTag, PanelTag);
    }
    private void ReloadFlow(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var SectionCount = Reader.ReadInt32();

      var SectionArray = new ServerFlowSection[SectionCount]; 
      for (var i = 0; i < SectionCount; i++)
      {
        var ItemCount = Reader.ReadInt32();
        var HeaderPanelTag = Reader.ReadPanelTag();
        var FooterPanelTag = Reader.ReadPanelTag();

        SectionArray[i] = new ServerFlowSection()
        {
          ItemCount = ItemCount,
          HeaderPanelTag = HeaderPanelTag,
          FooterPanelTag = FooterPanelTag
        };
      }

      Contract.ReloadFlow(PanelTag, SectionArray);
    }
    private void NewFrame(ServerReader Reader)
    {
      var SurfaceTag = Reader.ReadSurfaceTag();
      var PanelTag = Reader.ReadPanelTag();
      Contract.NewFrame(SurfaceTag, PanelTag);
    }
    private void NewGraphic(ServerReader Reader)
    {
      var SurfaceTag = Reader.ReadSurfaceTag();
      var PanelTag = Reader.ReadPanelTag();
      Contract.NewGraphic(SurfaceTag, PanelTag);
    }
    private void SetGraphicImage(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var ImageTag = Reader.ReadImageTag();
      Contract.SetGraphicImage(PanelTag, ImageTag);
    }
    private void SetFrameContent(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var ContentTag = Reader.ReadPanelTag();
      Contract.SetFrameContent(PanelTag, ContentTag);
    }
    private void NewLabel(ServerReader Reader)
    {
      var SurfaceTag = Reader.ReadSurfaceTag();
      var PanelTag = Reader.ReadPanelTag();
      Contract.NewLabel(SurfaceTag, PanelTag);
    }
    private void SetLabelText(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Text = Reader.ReadString();
      Contract.SetLabelText(PanelTag, Text);
    }
    private void SetLabelLineWrapping(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var LineWrapping = Reader.ReadBoolean();
      Contract.SetLabelLineWrapping(PanelTag, LineWrapping);
    }
    private void SetLabelJustification(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Justification = Reader.ReadByte();

      // NOTE: the 255 comparison is backwards compatability from when it was a nullable field.
      Contract.SetLabelJustification(PanelTag, Justification != 255 ? (Inv.Justification)Justification : Inv.Justification.Left);
    }
    private void SetLabelFont(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Name = Reader.ReadString();
      var Size = Reader.ReadInt32();
      var Colour = Reader.ReadColour();
      var Weight = (FontWeight)Reader.ReadByte();
      Contract.SetLabelFont(PanelTag, Name, Size > 0 ? Size : (int?)null, Colour, Weight);
    }
    private void NewMemo(ServerReader Reader)
    {
      var SurfaceTag = Reader.ReadSurfaceTag();
      var PanelTag = Reader.ReadPanelTag();
      Contract.NewMemo(SurfaceTag, PanelTag);
    }
    private void SetMemoText(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Text = Reader.ReadString();
      Contract.SetMemoText(PanelTag, Text);
    }
    private void SetMemoIsReadOnly(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var ReadOnly = Reader.ReadBoolean();
      Contract.SetMemoIsReadOnly(PanelTag, ReadOnly);
    }
    private void SetMemoHasChange(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var HasChange = Reader.ReadBoolean();
      Contract.SetMemoHasChange(PanelTag, HasChange);
    }
    private void SetMemoFont(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Name = Reader.ReadString();
      var Size = Reader.ReadInt32();
      var Colour = Reader.ReadColour();
      var Weight = (FontWeight)Reader.ReadByte();
      Contract.SetMemoFont(PanelTag, Name, Size > 0 ? Size : (int?)null, Colour, Weight);
    }
    private void NewOverlay(ServerReader Reader)
    {
      var SurfaceTag = Reader.ReadSurfaceTag();
      var PanelTag = Reader.ReadPanelTag();
      Contract.NewOverlay(SurfaceTag, PanelTag);
    }
    private void SetOverlayCollection(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Count = Reader.ReadInt32();
      var PanelArray = new ServerPanelTag[Count];
      for (var i = 0; i < Count; i++)
        PanelArray[i] = Reader.ReadPanelTag();

      Contract.SetOverlayCollection(PanelTag, PanelArray);
    }
    private void NewCanvas(ServerReader Reader)
    {
      var SurfaceTag = Reader.ReadSurfaceTag();
      var PanelTag = Reader.ReadPanelTag();
      Contract.NewCanvas(SurfaceTag, PanelTag);
    }
    private void DrawCanvas(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Packet = new ServerPacket(Reader.ReadByteArray());

      Contract.DrawCanvas(PanelTag, Packet);
    }
    private void NewHorizontalScroll(ServerReader Reader)
    {
      var SurfaceTag = Reader.ReadSurfaceTag();
      var PanelTag = Reader.ReadPanelTag();
      Contract.NewScroll(SurfaceTag, PanelTag, ScrollOrientation.Horizontal);
    }
    private void NewVerticalScroll(ServerReader Reader)
    {
      var SurfaceTag = Reader.ReadSurfaceTag();
      var PanelTag = Reader.ReadPanelTag();
      Contract.NewScroll(SurfaceTag, PanelTag, ScrollOrientation.Vertical);
    }
    private void SetScrollContent(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var ContentTag = Reader.ReadPanelTag();
      Contract.SetScrollContent(PanelTag, ContentTag);
    }
    private void NewHorizontalStack(ServerReader Reader)
    {
      var SurfaceTag = Reader.ReadSurfaceTag();
      var PanelTag = Reader.ReadPanelTag();
      Contract.NewStack(SurfaceTag, PanelTag, StackOrientation.Horizontal);
    }
    private void NewVerticalStack(ServerReader Reader)
    {
      var SurfaceTag = Reader.ReadSurfaceTag();
      var PanelTag = Reader.ReadPanelTag();
      Contract.NewStack(SurfaceTag, PanelTag, StackOrientation.Vertical);
    }
    private void SetStackCollection(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();

      var Count = Reader.ReadInt32();
      var PanelArray = new ServerPanelTag[Count];
      for (var i = 0; i < Count; i++)
        PanelArray[i] = Reader.ReadPanelTag();

      Contract.SetStackCollection(PanelTag, PanelArray);
    }
    private void NewTable(ServerReader Reader)
    {
      var SurfaceTag = Reader.ReadSurfaceTag();
      var PanelTag = Reader.ReadPanelTag();
      Contract.NewTable(SurfaceTag, PanelTag);
    }
    private void SetTableCollection(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();

      var RowCount = Reader.ReadInt32();
      var RowArray = new ServerTableAxis[RowCount];
      for (var i = 0; i < RowCount; i++)
      {
        var RowLengthType = (TableAxisLength)Reader.ReadByte();
        var RowLengthValue = Reader.ReadInt32();
        var RowPanelTag = Reader.ReadPanelTag();

        RowArray[i] = new ServerTableAxis()
        {
          PanelTag = RowPanelTag,
          LengthType = RowLengthType,
          LengthValue = RowLengthValue
        };
      }

      var ColumnCount = Reader.ReadInt32();
      var ColumnArray = new ServerTableAxis[ColumnCount];
      for (var i = 0; i < ColumnCount; i++)
      {
        var ColumnLengthType = (TableAxisLength)Reader.ReadByte();
        var ColumnLengthValue = Reader.ReadInt32();
        var ColumnPanelTag = Reader.ReadPanelTag(); 

        ColumnArray[i] = new ServerTableAxis()
        {
          PanelTag = ColumnPanelTag,
          LengthType = ColumnLengthType,
          LengthValue = ColumnLengthValue
        };
      }

      var CellCount = Reader.ReadInt32();
      var CellArray = new ServerTableCell[CellCount];
      for (var i = 0; i < CellCount; i++)
      {
        var CellX = Reader.ReadInt32();
        var CellY = Reader.ReadInt32();
        var CellPanelTag = Reader.ReadPanelTag();

        CellArray[i] = new ServerTableCell()
        {
          X = CellX,
          Y = CellY,
          PanelTag = CellPanelTag,
        };
      }

      Contract.SetTableCollection(PanelTag, RowArray, ColumnArray, CellArray);
    }
    private void SetPanelBackground(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Colour = Reader.ReadColour();
      Contract.SetPanelBackground(PanelTag, Colour);
    }
    private void SetPanelAlignment(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Placement = (Placement)Reader.ReadByte();
      Contract.SetPanelAlignment(PanelTag, Placement);
    }
    private void SetPanelCorner(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var TopLeft = Reader.ReadInt32();
      var TopRight = Reader.ReadInt32();
      var BottomRight = Reader.ReadInt32();
      var BottomLeft = Reader.ReadInt32();
      Contract.SetPanelCorner(PanelTag, TopLeft, TopRight, BottomRight, BottomLeft);
    }
    private void SetPanelVisibility(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Visibility = Reader.ReadBoolean();
      Contract.SetPanelVisibility(PanelTag, Visibility);
    }
    private void SetPanelMargin(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Left = Reader.ReadInt32();
      var Top = Reader.ReadInt32();
      var Right = Reader.ReadInt32();
      var Bottom = Reader.ReadInt32();
      Contract.SetPanelMargin(PanelTag, Left, Top, Right, Bottom);
    }
    private void SetPanelPadding(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Left = Reader.ReadInt32();
      var Top = Reader.ReadInt32();
      var Right = Reader.ReadInt32();
      var Bottom = Reader.ReadInt32();
      Contract.SetPanelPadding(PanelTag, Left, Top, Right, Bottom);
    }
    private void SetPanelBorder(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Left = Reader.ReadInt32();
      var Top = Reader.ReadInt32();
      var Right = Reader.ReadInt32();
      var Bottom = Reader.ReadInt32();
      var Colour = Reader.ReadColour();
      Contract.SetPanelBorder(PanelTag, Left, Top, Right, Bottom, Colour);
    }
    private void SetPanelElevation(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Depth = Reader.ReadInt32();
      Contract.SetPanelElevation(PanelTag, Depth);
    }
    private void SetPanelOpacity(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Opacity = Reader.ReadFloat();
      Contract.SetPanelOpacity(PanelTag, Opacity);
    }
    private void SetPanelSize(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Width = Reader.ReadInt32();
      var Height = Reader.ReadInt32();
      var MinimumWidth = Reader.ReadInt32();
      var MinimumHeight = Reader.ReadInt32();
      var MaximumWidth = Reader.ReadInt32();
      var MaximumHeight = Reader.ReadInt32();
      Contract.SetPanelSize(PanelTag, 
        Width >= 0 ? Width : (int?)null, 
        Height >= 0 ? Height : (int?)null, 
        MinimumWidth >= 0 ? MinimumWidth : (int?)null, 
        MinimumHeight >= 0 ? MinimumHeight : (int?)null, 
        MaximumWidth >= 0 ? MaximumWidth : (int?)null, 
        MaximumHeight >= 0 ? MaximumHeight : (int?)null);
    }
    private void NewImage(ServerReader Reader)
    {
      var ImageTag = Reader.ReadImageTag();
      var ImageSource = Reader.ReadImage();
      Contract.NewImage(ImageTag, ImageSource);
    }
    private void NewSound(ServerReader Reader)
    {
      var SoundTag = Reader.ReadSoundTag();
      var SoundSource = Reader.ReadSound();
      Contract.NewSound(SoundTag, SoundSource);
    }
    private void PlaySound(ServerReader Reader)
    {
      var SoundTag = Reader.ReadSoundTag();
      var SoundVolume = Reader.ReadFloat();
      var SoundRate = Reader.ReadFloat();
      Contract.PlaySound(SoundTag, SoundVolume, SoundRate);
    }
    private void PlayClip(ServerReader Reader)
    {
      var ClipTag = Reader.ReadClipTag();
      var SoundTag = Reader.ReadSoundTag();
      var Volume = Reader.ReadFloat();
      var Rate = Reader.ReadFloat();
      var Loop = Reader.ReadBoolean();

      Contract.PlayClip(ClipTag, SoundTag, Volume, Rate, Loop);
    }
    private void StopClip(ServerReader Reader)
    {
      var ClipTag = Reader.ReadClipTag();
      Contract.StopClip(ClipTag);
    }
    private void StartAnimation(ServerReader Reader)
    {
      var SurfaceTag = Reader.ReadSurfaceTag();
      var AnimationTag = Reader.ReadAnimationTag();
      var TargetLength = Reader.ReadInt32();
      var TargetArray = new ServerAnimationTarget[TargetLength];
      for (var TargetIndex = 0; TargetIndex < TargetLength; TargetIndex++)
      {
        var Target = new ServerAnimationTarget();
        Target.PanelTag = Reader.ReadPanelTag();

        var CommandLength = Reader.ReadInt32();
        var CommandArray = new ServerAnimationCommand[CommandLength];
        Target.CommandArray = CommandArray;

        for (var CommandIndex = 0; CommandIndex < CommandLength; CommandIndex++)
        {
          var Command = new ServerAnimationCommand();
          Command.Type = (AnimationType)Reader.ReadByte();

          switch (Command.Type)
          {
            case AnimationType.Opacity:
              Command.OpacityOffset = Reader.ReadTimeSpan();
              if (Command.OpacityOffset == TimeSpan.Zero)
                Command.OpacityOffset = null;
              Command.OpacityDuration = Reader.ReadTimeSpan();
              Command.OpacityFrom = Reader.ReadFloat();
              Command.OpacityTo = Reader.ReadFloat();
              break;

            default:
              throw new Exception("AnimationType not handled: " + Command.Type);
          }

          CommandArray[CommandIndex] = Command;
        }

        TargetArray[TargetIndex] = Target;
      }

      Contract.StartAnimation(SurfaceTag, AnimationTag, TargetArray);
    }
    private void StopAnimation(ServerReader Reader)
    {
      var AnimationTag = Reader.ReadAnimationTag();
      Contract.StopAnimation(AnimationTag);
    }
    private void BrowseMarket(ServerReader Reader)
    {
      var AppleiTunesID = Reader.ReadString();
      var GooglePlayID = Reader.ReadString();
      var WindowsStoreID = Reader.ReadString();

      Contract.BrowseMarket(AppleiTunesID, GooglePlayID, WindowsStoreID);
    }
    private void LaunchWebUri(ServerReader Reader)
    {
      var Uri = Reader.ReadUri();

      Contract.LaunchWebUri(Uri);
    }
    private void ShowCalendarPicker(ServerReader Reader)
    {
      var PickerTag = Reader.ReadPickerTag();
      var SetDate = Reader.ReadBoolean();
      var SetTime = Reader.ReadBoolean();
      var Value = Reader.ReadDateTime();

      Contract.ShowCalendarPicker(PickerTag, SetDate, SetTime, Value);
    }
    private void ShowDirectoryPicker(ServerReader Reader)
    {
      var PickerTag = Reader.ReadPickerTag();
      var Title = Reader.ReadString();
      var FileType = (FileType)Reader.ReadByte();

      Contract.ShowDirectoryPicker(PickerTag, Title, FileType);
    }
    private void SendEmailMessage(ServerReader Reader)
    {
      var Subject = Reader.ReadString();
      var Body = Reader.ReadString();

      var ToCount = Reader.ReadInt32();
      var ToArray = new ServerEmailTo[ToCount];
      for (var i = 0; i < ToCount; i++)
      {
        var Name = Reader.ReadString();
        var Address = Reader.ReadString();

        ToArray[i] = new ServerEmailTo()
        {
          Name = Name,
          Address = Address
        };
      }

      var AttachmentCount = Reader.ReadInt32();
      var AttachmentArray = new ServerEmailAttachment[AttachmentCount];
      for (var i = 0; i < AttachmentCount; i++)
      {
        var Name = Reader.ReadString();
        var Content = Reader.ReadByteArray();

        AttachmentArray[i] = new ServerEmailAttachment()
        {
          Name = Name,
          Content = Content
        };
      }

      var Result = Contract.SendEmailMessage(Subject, Body, ToArray, AttachmentArray);
    }
    private void DialPhone(ServerReader Reader)
    {
      var PhoneNumber = Reader.ReadString();

      Contract.DialPhone(PhoneNumber);
    }
    private void SMSPhone(ServerReader Reader)
    {
      var PhoneNumber = Reader.ReadString();

      Contract.SMSPhone(PhoneNumber);
    }
    private void DisposeSurface(ServerReader Reader)
    {
      var SurfaceTag = Reader.ReadSurfaceTag();

      Contract.DisposeSurface(SurfaceTag);
    }
    private void DisposePanel(ServerReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();

      Contract.DisposePanel(PanelTag);
    }
    private void DisposeImage(ServerReader Reader)
    {
      var ImageTag = Reader.ReadImageTag();

      Contract.DisposeImage(ImageTag);
    }
    private void DisposeSound(ServerReader Reader)
    {
      var SoundTag = Reader.ReadSoundTag();

      Contract.DisposeSound(SoundTag);
    }

    private Inv.EnumArray<ServerMessage, Action<ServerReader>> MessageArray;
    private ServerContract Contract;
  }

  internal sealed class ServerLog : ServerContract
  {
    public ServerLog(ServerContract Adapter)
    {
      this.Adapter = Adapter;
    }

    void ServerContract.NewSurface(ServerSurfaceTag SurfaceTag)
    {
      WriteLine("Surface[{0}].New();", SurfaceTag);

      Adapter.NewSurface(SurfaceTag);
    }
    void ServerContract.SetSurfaceContent(ServerSurfaceTag SurfaceTag, ServerPanelTag ContentTag)
    {
      WriteLine("Surface[{0}].Content = {1};", SurfaceTag, FormatPanelTag(ContentTag));

      Adapter.SetSurfaceContent(SurfaceTag, ContentTag);
    }
    void ServerContract.NewFrame(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag)
    {
      WriteLine("Panel[{1}] = Surface[{0}].NewFrame();", SurfaceTag, PanelTag);

      Adapter.NewFrame(SurfaceTag, PanelTag);
    }
    void ServerContract.NewDock(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag, DockOrientation Orientation)
    {
      WriteLine("Panel[{1}] = Surface[{0}].New{2}Dock();", SurfaceTag, PanelTag, Orientation);

      Adapter.NewDock(SurfaceTag, PanelTag, Orientation);
    }
    void ServerContract.SetPanelAlignment(ServerPanelTag PanelTag, Inv.Placement Placement)
    {
      WriteLine("Panel[{0}].Alignment.Set({1});", PanelTag, Placement);

      Adapter.SetPanelAlignment(PanelTag, Placement);
    }
    void ServerContract.SetPanelBorder(ServerPanelTag PanelTag, int Left, int Top, int Right, int Bottom, Colour Colour)
    {
      WriteLine("Panel[{0}].Border.Set({1}, {2});", PanelTag, FormatEdge(Left, Top, Right, Bottom), FormatColour(Colour));

      Adapter.SetPanelBorder(PanelTag, Left, Top, Right, Bottom, Colour);
    }
    void ServerContract.SetPanelCorner(ServerPanelTag PanelTag, int TopLeft, int TopRight, int BottomRight, int BottomLeft)
    {
      WriteLine("Panel[{0}].Corner.Set({1});", PanelTag, FormatCorner(TopLeft, TopRight, BottomRight, BottomLeft));

      Adapter.SetPanelCorner(PanelTag, TopLeft, TopRight, BottomRight, BottomLeft);
    }
    void ServerContract.SetPanelElevation(ServerPanelTag PanelTag, int Depth)
    {
      WriteLine("Panel[{0}].Elevation.Set({1});", PanelTag, Depth);

      Adapter.SetPanelElevation(PanelTag, Depth);
    }
    void ServerContract.SetPanelMargin(ServerPanelTag PanelTag, int Left, int Top, int Right, int Bottom)
    {
      WriteLine("Panel[{0}].Margin.Set({1});", PanelTag, FormatEdge(Left, Top, Right, Bottom));

      Adapter.SetPanelMargin(PanelTag, Left, Top, Right, Bottom);
    }
    void ServerContract.SetPanelOpacity(ServerPanelTag PanelTag, float Opacity)
    {
      WriteLine("Panel[{0}].Opacity.Set({1});", PanelTag, Opacity);

      Adapter.SetPanelOpacity(PanelTag, Opacity);
    }
    void ServerContract.SetPanelPadding(ServerPanelTag PanelTag, int Left, int Top, int Right, int Bottom)
    {
      WriteLine("Panel[{0}].Padding.Set({1});", PanelTag, FormatEdge(Left, Top, Right, Bottom));

      Adapter.SetPanelPadding(PanelTag, Left, Top, Right, Bottom);
    }
    void ServerContract.SetPanelSize(ServerPanelTag PanelTag, int? Width, int? Height, int? MinimumWidth, int? MaximumWidth, int? MinimumHeight, int? MaximumHeight)
    {
      WriteLine("Panel[{0}].Size.Set({1}x{2}, {3}x{4}, {5}x{6});", PanelTag, Width, Height, MinimumWidth, MaximumWidth, MinimumHeight, MaximumHeight);

      Adapter.SetPanelSize(PanelTag, Width, Height, MinimumWidth, MaximumWidth, MinimumHeight, MaximumHeight);
    }
    void ServerContract.SetPanelVisibility(ServerPanelTag PanelTag, bool Visibility)
    {
      WriteLine("Panel[{0}].Visibility.Set({1});", PanelTag, FormatBoolean(Visibility));

      Adapter.SetPanelVisibility(PanelTag, Visibility);
    }
    void ServerContract.SetPanelBackground(ServerPanelTag PanelTag, Colour BackgroundColour)
    {
      WriteLine("Panel[{0}].Background.Colour = {1};", PanelTag, FormatColour(BackgroundColour));

      Adapter.SetPanelBackground(PanelTag, BackgroundColour);
    }
    void ServerContract.SetButtonContent(ServerPanelTag PanelTag, ServerPanelTag ContentTag)
    {
      WriteLine("Button[{0}].Content = {1};", PanelTag, FormatPanelTag(ContentTag));

      Adapter.SetButtonContent(PanelTag, ContentTag);
    }
    void ServerContract.SetButtonIsEnabled(ServerPanelTag PanelTag, bool IsEnabled)
    {
      WriteLine("Button[{0}].IsEnabled = {1};", PanelTag, FormatBoolean(IsEnabled));

      Adapter.SetButtonIsEnabled(PanelTag, IsEnabled);
    }
    void ServerContract.SetButtonIsFocusable(ServerPanelTag PanelTag, bool IsFocusable)
    {
      WriteLine("Button[{0}].IsFocusable = {1};", PanelTag, FormatBoolean(IsFocusable));

      Adapter.SetButtonIsFocusable(PanelTag, IsFocusable);
    }
    void ServerContract.SetButtonHasPress(ServerPanelTag PanelTag, bool HasPress)
    {
      WriteLine("Button[{0}].HasPress = {1};", PanelTag, FormatBoolean(HasPress));

      Adapter.SetButtonHasPress(PanelTag, HasPress);
    }
    void ServerContract.SetButtonHasRelease(ServerPanelTag PanelTag, bool HasRelease)
    {
      WriteLine("Button[{0}].HasRelease = {1};", PanelTag, FormatBoolean(HasRelease));

      Adapter.SetButtonHasRelease(PanelTag, HasRelease);
    }
    void ServerContract.SetDockCollection(ServerPanelTag PanelTag, ServerPanelTag[] HeaderArray, ServerPanelTag[] ClientArray, ServerPanelTag[] FooterArray)
    {
      WriteLine("Dock[{0}].Collection({1}, {2}, {3});", PanelTag, FormatPanelTagArray(HeaderArray), FormatPanelTagArray(ClientArray), FormatPanelTagArray(FooterArray));

      Adapter.SetDockCollection(PanelTag, HeaderArray, ClientArray, FooterArray);
    }
    void ServerContract.SetFrameContent(ServerPanelTag PanelTag, ServerPanelTag ContentTag)
    {
      WriteLine("Frame[{0}].Content = {1};", PanelTag, FormatPanelTag(ContentTag));

      Adapter.SetFrameContent(PanelTag, ContentTag);
    }
    void ServerContract.SetLabelLineWrapping(ServerPanelTag PanelTag, bool LineWrapping)
    {
      WriteLine("Label[{0}].LineWrapping = {1};", PanelTag, FormatBoolean(LineWrapping));

      Adapter.SetLabelLineWrapping(PanelTag, LineWrapping);
    }
    void ServerContract.SetLabelJustification(ServerPanelTag PanelTag, Justification Justification)
    {
      WriteLine("Label[{0}].Justify{1}();", PanelTag, Justification.ToString());

      Adapter.SetLabelJustification(PanelTag, Justification);
    }
    void ServerContract.SetLabelText(ServerPanelTag PanelTag, string Text)
    {
      WriteLine("Label[{0}].Text = {1};", PanelTag, FormatString(Text));

      Adapter.SetLabelText(PanelTag, Text);
    }
    void ServerContract.SetOverlayCollection(ServerPanelTag PanelTag, ServerPanelTag[] ElementArray)
    {
      WriteLine("Overlay[{0}].Collection({1});", PanelTag, FormatPanelTagArray(ElementArray));

      Adapter.SetOverlayCollection(PanelTag, ElementArray);
    }
    void ServerContract.SetScrollContent(ServerPanelTag PanelTag, ServerPanelTag ContentTag)
    {
      WriteLine("Scroll[{0}].Content = {1};", PanelTag, FormatPanelTag(ContentTag));

      Adapter.SetScrollContent(PanelTag, ContentTag);
    }
    void ServerContract.SetStackCollection(ServerPanelTag PanelTag, ServerPanelTag[] ElementArray)
    {
      WriteLine("Stack[{0}].Collection({1});", PanelTag, FormatPanelTagArray(ElementArray));

      Adapter.SetStackCollection(PanelTag, ElementArray);
    }
    void ServerContract.MemoryReclamation()
    {
      WriteLine("Process.MemoryReclamation();");

      Adapter.MemoryReclamation();
    }
    void ServerContract.ExitApplication()
    {
      WriteLine("Application.Exit();");

      Adapter.ExitApplication();
    }
    void ServerContract.SetWindowBackground(Colour Colour)
    {
      WriteLine("Window.Bacgkround = {0};", FormatColour(Colour));

      Adapter.SetWindowBackground(Colour);
    }
    void ServerContract.SetSurfaceBackground(ServerSurfaceTag SurfaceTag, Colour Colour)
    {
      WriteLine("Surface[{0}].Background = {1};", SurfaceTag, FormatColour(Colour));

      Adapter.SetSurfaceBackground(SurfaceTag, Colour);
    }
    void ServerContract.TransitionSurface(ServerSurfaceTag SurfaceTag, TransitionAnimation Animation, TimeSpan Duration)
    {
      WriteLine("Surface[{0}].Transition().{1}({2});", SurfaceTag, Animation, Duration);

      Adapter.TransitionSurface(SurfaceTag, Animation, Duration);
    }
    void ServerContract.NewBrowser(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag)
    {
      WriteLine("Panel[{1}] = Surface[{0}].NewBrowser();", SurfaceTag, PanelTag);

      Adapter.NewBrowser(SurfaceTag, PanelTag);
    }
    void ServerContract.LoadBrowser(ServerPanelTag PanelTag, Uri Uri, string Html)
    {
      WriteLine("Browser[{0}].Load{1}({2});", PanelTag, Uri != null ? "Uri" : "Html", Uri != null ? Uri.OriginalString : Html);

      Adapter.LoadBrowser(PanelTag, Uri, Html);
    }
    void ServerContract.NewButton(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag)
    {
      WriteLine("Panel[{1}] = Surface[{0}].NewButton();", SurfaceTag, PanelTag);

      Adapter.NewButton(SurfaceTag, PanelTag);
    }
    void ServerContract.NewBoard(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag)
    {
      WriteLine("Panel[{1}] = Surface[{0}].NewBoard();", SurfaceTag, PanelTag);

      Adapter.NewBoard(SurfaceTag, PanelTag);
    }
    void ServerContract.SetBoardCollection(ServerPanelTag PanelTag, ServerBoardPin[] PinArray)
    {
      WriteLine("Stack[{0}].Collection({1});", PanelTag, FormatBoardPin(PinArray));

      Adapter.SetBoardCollection(PanelTag, PinArray);
    }
    void ServerContract.NewEdit(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag, EditInput Input)
    {
      WriteLine("Panel[{1}] = Surface[{0}].New{2}Edit();", SurfaceTag, PanelTag, Input);

      Adapter.NewEdit(SurfaceTag, PanelTag, Input);
    }
    void ServerContract.SetEditFont(ServerPanelTag PanelTag, string Name, int? Size, Colour Colour, FontWeight Weight)
    {
      WriteLine("Edit[{0}].SetFont({1}, {2}, {3}, {4});", PanelTag, FormatString(Name), Size, FormatColour(Colour), Weight);

      Adapter.SetEditFont(PanelTag, Name, Size, Colour, Weight);
    }
    void ServerContract.SetEditText(ServerPanelTag PanelTag, string Text)
    {
      WriteLine("Edit[{0}].Text = {1};", PanelTag, FormatString(Text));

      Adapter.SetEditText(PanelTag, Text);
    }
    void ServerContract.SetEditIsReadOnly(ServerPanelTag PanelTag, bool ReadOnly)
    {
      WriteLine("Edit[{0}].IsReadOnly = {1};", PanelTag, FormatBoolean(ReadOnly));

      Adapter.SetEditIsReadOnly(PanelTag, ReadOnly);
    }
    void ServerContract.SetEditHasChange(ServerPanelTag PanelTag, bool HasChange)
    {
      WriteLine("Edit[{0}].HasChange = {1};", PanelTag, FormatBoolean(HasChange));

      Adapter.SetEditHasChange(PanelTag, HasChange);
    }
    void ServerContract.SetEditHasReturn(ServerPanelTag PanelTag, bool HasReturn)
    {
      WriteLine("Edit[{0}].HasReturn = {1};", PanelTag, FormatBoolean(HasReturn));

      Adapter.SetEditHasReturn(PanelTag, HasReturn);
    }
    void ServerContract.NewFlow(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag)
    {
      WriteLine("Panel[{1}] = Surface[{0}].NewFlow();", SurfaceTag, PanelTag);

      Adapter.NewFlow(SurfaceTag, PanelTag);
    }
    void ServerContract.ReloadFlow(ServerPanelTag PanelTag, ServerFlowSection[] SectionArray)
    {
      WriteLine("Flow[{0}].Reload()", PanelTag); // TODO: sections.

      Adapter.ReloadFlow(PanelTag, SectionArray);
    }
    void ServerContract.NewGraphic(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag)
    {
      WriteLine("Panel[{1}] = Surface[{0}].NewGraphic();", SurfaceTag, PanelTag);

      Adapter.NewGraphic(SurfaceTag, PanelTag);
    }
    void ServerContract.SetGraphicImage(ServerPanelTag PanelTag, ServerImageTag ImageTag)
    {
      WriteLine("Graphic[{0}].Image = Image[{1}];", PanelTag, ImageTag);

      Adapter.SetGraphicImage(PanelTag, ImageTag);
    }
    void ServerContract.NewLabel(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag)
    {
      WriteLine("Panel[{1}] = Surface[{0}].NewLabel();", SurfaceTag, PanelTag);

      Adapter.NewLabel(SurfaceTag, PanelTag);
    }
    void ServerContract.SetLabelFont(ServerPanelTag PanelTag, string Name, int? Size, Colour Colour, FontWeight Weight)
    {
      WriteLine("Label[{0}].SetFont({1}, {2}, {3}, {4});", PanelTag, FormatString(Name), Size, FormatColour(Colour), Weight);

      Adapter.SetLabelFont(PanelTag, Name, Size, Colour, Weight);
    }
    void ServerContract.NewMemo(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag)
    {
      WriteLine("Panel[{1}] = Surface[{0}].NewMemo();", SurfaceTag, PanelTag);

      Adapter.NewMemo(SurfaceTag, PanelTag);
    }
    void ServerContract.SetMemoFont(ServerPanelTag PanelTag, string Name, int? Size, Colour Colour, FontWeight Weight)
    {
      WriteLine("Memo[{0}].SetFont({1}, {2}, {3}, {4});", PanelTag, FormatString(Name), Size, FormatColour(Colour), Weight);

      Adapter.SetMemoFont(PanelTag, Name, Size, Colour, Weight);
    }
    void ServerContract.SetMemoText(ServerPanelTag PanelTag, string Text)
    {
      WriteLine("Memo[{0}].Text = {1};", PanelTag, FormatString(Text));

      Adapter.SetMemoText(PanelTag, Text);
    }
    void ServerContract.SetMemoIsReadOnly(ServerPanelTag PanelTag, bool ReadOnly)
    {
      WriteLine("Memo[{0}].IsReadOnly = {1};", PanelTag, FormatBoolean(ReadOnly));

      Adapter.SetMemoIsReadOnly(PanelTag, ReadOnly);
    }
    void ServerContract.SetMemoHasChange(ServerPanelTag PanelTag, bool HasChange)
    {
      WriteLine("Memo[{0}].HasChange = {1};", PanelTag, FormatBoolean(HasChange));

      Adapter.SetMemoHasChange(PanelTag, HasChange);
    }
    void ServerContract.NewOverlay(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag)
    {
      WriteLine("Panel[{1}] = Surface[{0}].NewOverlay();", SurfaceTag, PanelTag);

      Adapter.NewOverlay(SurfaceTag, PanelTag);
    }
    void ServerContract.NewCanvas(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag)
    {
      WriteLine("Panel[{1}] = Surface[{0}].NewCanvas();", SurfaceTag, PanelTag);

      Adapter.NewCanvas(SurfaceTag, PanelTag);
    }
    void ServerContract.DrawCanvas(ServerPanelTag PanelTag, ServerPacket Packet)
    {
      WriteLine("Canvas[{0}].Draw({1});", PanelTag, Packet.Buffer.Length);

      Adapter.DrawCanvas(PanelTag, Packet);
    }
    void ServerContract.NewScroll(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag, ScrollOrientation Orientation)
    {
      WriteLine("Panel[{1}] = Surface[{0}].New{2}Scroll();", SurfaceTag, PanelTag, Orientation);

      Adapter.NewScroll(SurfaceTag, PanelTag, Orientation);
    }
    void ServerContract.NewStack(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag, StackOrientation Orientation)
    {
      WriteLine("Panel[{1}] = Surface[{0}].New{2}Stack();", SurfaceTag, PanelTag, Orientation);

      Adapter.NewStack(SurfaceTag, PanelTag, Orientation);
    }
    void ServerContract.NewTable(ServerSurfaceTag SurfaceTag, ServerPanelTag PanelTag)
    {
      WriteLine("Panel[{1}] = Surface[{0}].NewTable();", SurfaceTag, PanelTag);

      Adapter.NewTable(SurfaceTag, PanelTag);
    }
    void ServerContract.SetTableCollection(ServerPanelTag PanelTag, ServerTableAxis[] RowArray, ServerTableAxis[] ColumnArray, ServerTableCell[] CellArray)
    {
      WriteLine("Table[{0}].Collection", PanelTag);

      Adapter.SetTableCollection(PanelTag, RowArray, ColumnArray, CellArray);
    }
    void ServerContract.NewImage(ServerImageTag ImageTag, Image Image)
    {
      WriteLine("Image[{0}] = NewImage();", ImageTag);

      Adapter.NewImage(ImageTag, Image);
    }
    void ServerContract.NewSound(ServerSoundTag SoundTag, Sound Sound)
    {
      WriteLine("Sound[{0}] = NewSound();", SoundTag);

      Adapter.NewSound(SoundTag, Sound);
    }
    void ServerContract.PlaySound(ServerSoundTag SoundTag, float SoundVolume, float SoundRate)
    {
      WriteLine("Sound[{0}].Play({1}, {2});", SoundTag, SoundVolume, SoundRate);

      Adapter.PlaySound(SoundTag, SoundVolume, SoundRate);
    }
    void ServerContract.StartAnimation(ServerSurfaceTag SurfaceTag, ServerAnimationTag AnimationTag, ServerAnimationTarget[] TargetArray)
    {
      WriteLine("Animation[{0}] = Surface[{1}].StartAnimation({2});", AnimationTag, SurfaceTag, FormatAnimationTarget(TargetArray));

      Adapter.StartAnimation(SurfaceTag, AnimationTag, TargetArray);
    }
    void ServerContract.StopAnimation(ServerAnimationTag AnimationTag)
    {
      WriteLine("Animation[{0}].Stop();", AnimationTag);

      Adapter.StopAnimation(AnimationTag);
    }
    void ServerContract.PlayClip(ServerClipTag ClipTag, ServerSoundTag SoundTag, float SoundVolume, float SoundRate, bool SoundLoop)
    {
      WriteLine("Clip[{0}] = Application.Audio.NewClip({1}, {2:F2}, {2:F2}, {3}).Play();", ClipTag, SoundTag, SoundVolume, SoundRate, FormatBoolean(SoundLoop));

      Adapter.PlayClip(ClipTag, SoundTag, SoundVolume, SoundRate, SoundLoop);
    }
    void ServerContract.StopClip(ServerClipTag ClipTag)
    {
      WriteLine("Clip[{0}].Stop();", ClipTag);

      Adapter.StopClip(ClipTag);
    }
    void ServerContract.BrowseMarket(string AppleiTunesID, string GooglePlayID, string WindowsStoreID)
    {
      WriteLine("Application.Market.Browse({0}, {1}, {2});", FormatString(AppleiTunesID), FormatString(GooglePlayID), FormatString(WindowsStoreID));

      Adapter.BrowseMarket(AppleiTunesID, GooglePlayID, WindowsStoreID);
    }
    void ServerContract.LaunchWebUri(Uri Uri)
    {
      WriteLine("Application.Web.Launch({0});", Uri);

      Adapter.LaunchWebUri(Uri);
    }
    void ServerContract.ShowCalendarPicker(ServerPickerTag PickerTag, bool SetDate, bool SetTime, DateTime Value)
    {
      WriteLine("Application.Calendar.Show{0}{1}Picker({2}, {3});", SetDate ? "Date" : "", SetTime ? "Time" : "", PickerTag, Value);

      Adapter.ShowCalendarPicker(PickerTag, SetDate, SetTime, Value);
    }
    void ServerContract.ShowDirectoryPicker(ServerPickerTag PickerTag, string Title, FileType FileType)
    {
      WriteLine("Application.Directory.Show{0}FilePicker({1}, {2});", FileType, PickerTag, Title.ConvertToCSharpString());

      Adapter.ShowDirectoryPicker(PickerTag, Title, FileType);
    }
    bool ServerContract.SendEmailMessage(string Subject, string Body, ServerEmailTo[] ToArray, ServerEmailAttachment[] AttachmentArray)
    {
      WriteLine("Application.Email.SendMessage({0}, {1}, {2}, {3});", Subject, Body, "{ " + ToArray.Select(T => T.Name + "[" + T.Address + "]").AsSeparatedText(",") + " }", "{ " + AttachmentArray.Select(A => A.Name + "[" + Inv.DataSize.FromBytes(A.Content.Length).ToBriefString() + "]").AsSeparatedText(",") + " }");

      return Adapter.SendEmailMessage(Subject, Body, ToArray, AttachmentArray);
    }
    void ServerContract.DialPhone(string PhoneNumber)
    {
      WriteLine("Application.Phone.Dial({0});", PhoneNumber);

      Adapter.DialPhone(PhoneNumber);
    }
    void ServerContract.SMSPhone(string PhoneNumber)
    {
      WriteLine("Application.Phone.SMS({0});", PhoneNumber);

      Adapter.SMSPhone(PhoneNumber);
    }
    void ServerContract.DisposeSurface(ServerSurfaceTag SurfaceTag)
    {
      WriteLine("Surface[{0}].Dispose();", SurfaceTag);
    }
    void ServerContract.DisposePanel(ServerPanelTag PanelTag)
    {
      WriteLine("Panel[{0}].Dispose();", PanelTag);
    }
    void ServerContract.DisposeImage(ServerImageTag ImageTag)
    {
      WriteLine("Image[{0}].Dispose();", ImageTag);
    }
    void ServerContract.DisposeSound(ServerSoundTag SoundTag)
    {
      WriteLine("Sound[{0}].Dispose();", SoundTag);
    }

    private void WriteLine(string Format, params object[] FieldArray)
    {
      //LogWriter.WriteLine(Format, FieldArray);
    }
    private string FormatString(string Value)
    {
      if (Value == null)
        return "null";
      else
        return Value.ConvertToCSharpString();
    }
    private string FormatBoolean(bool Value)
    {
      return Value.ToString().ToLower();
    }
    private string FormatAnimationTarget(ServerAnimationTarget[] TargetArray)
    {
      // TODO: all fields.

      return TargetArray.Select(T => T.PanelTag + ":" + T.CommandArray.Select(C => C.Type.ToString()).AsSeparatedText(",")).AsSeparatedText(" | ");
    }
    private string FormatPanelTag(ServerPanelTag PanelTag)
    {
      return PanelTag.IsZero ? "null" : "Panel[" + PanelTag + "]";
    }
    private string FormatPanelTagArray(ServerPanelTag[] PanelTagArray)
    {
      return "{" + PanelTagArray.Select(P => P.ToString()).AsSeparatedText(",") + "}";
    }
    private string FormatBoardPin(ServerBoardPin[] PinArray)
    {
      return "{" + PinArray.Select(P => P.Rect.ToString() + ":" + P.PanelTag.ToString()).AsSeparatedText(",") + "}";
    }
    private string FormatColour(Colour Colour)
    {
      return Colour != null ? Colour.ToString() : "null";
    }
    private string FormatEdge(int Left, int Top, int Right, int Bottom)
    {
      if (Left == Top && Top == Right && Right == Bottom)
        return Left.ToString();
      else
        return string.Format("{0}, {1}, {2}, {3}", Left, Top, Right, Bottom);
    }
    private string FormatCorner(int TopLeft, int TopRight, int BottomRight, int BottomLeft)
    {
      if (TopLeft == TopRight && TopRight == BottomRight && BottomRight == BottomLeft)
        return TopLeft.ToString();
      else
        return string.Format("{0}, {1}, {2}, {3}", TopLeft, TopRight, BottomRight, BottomLeft);
    }

    private ServerContract Adapter;
  }
}