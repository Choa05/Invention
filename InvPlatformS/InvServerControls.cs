﻿/*! 2 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;
using System.Threading;
using System.Diagnostics;

namespace Inv
{
  internal struct ServerSurfaceTag
  {
    public ServerSurfaceTag(ushort IDValue)
    {
      ID = IDValue;
    }

    public readonly ushort ID;
    public bool IsZero
    {
      get { return ID == 0; }
    }

    public override bool Equals(object obj)
    {
      return base.Equals(obj);
    }
    public override int GetHashCode()
    {
      return ID.GetHashCode();
    }
    public override string ToString()
    {
      return ID.ToString();
    }

    public static readonly ServerSurfaceTag Zero = new ServerSurfaceTag(0);
  }

  internal struct ServerPanelTag
  {
    public ServerPanelTag(uint IDValue)
    {
      ID = IDValue;
    }

    public readonly uint ID;
    public bool IsZero
    {
      get { return ID == 0; }
    }

    public override bool Equals(object obj)
    {
      return base.Equals(obj);
    }
    public override int GetHashCode()
    {
      return ID.GetHashCode();
    }
    public override string ToString()
    {
      return ID.ToString();
    }

    public static readonly ServerPanelTag Zero = new ServerPanelTag(0);
  }

  internal struct ServerImageTag
  {
    public ServerImageTag(ushort IDValue)
    {
      ID = IDValue;
    }

    public readonly ushort ID;
    public bool IsZero
    {
      get { return ID == 0; }
    }

    public override bool Equals(object obj)
    {
      return base.Equals(obj);
    }
    public override int GetHashCode()
    {
      return ID.GetHashCode();
    }
    public override string ToString()
    {
      return ID.ToString();
    }

    public static readonly ServerImageTag Zero = new ServerImageTag(0);
  }

  internal struct ServerSoundTag
  {
    public ServerSoundTag(ushort IDValue)
    {
      ID = IDValue;
    }

    public readonly ushort ID;
    public bool IsZero
    {
      get { return ID == 0; }
    }

    public override bool Equals(object obj)
    {
      return base.Equals(obj);
    }
    public override int GetHashCode()
    {
      return ID.GetHashCode();
    }
    public override string ToString()
    {
      return ID.ToString();
    }

    public static readonly ServerSoundTag Zero = new ServerSoundTag(0);
  }

  internal struct ServerClipTag
  {
    public ServerClipTag(uint IDValue)
    {
      ID = IDValue;
    }

    public readonly uint ID;
    public bool IsZero
    {
      get { return ID == 0; }
    }

    public override bool Equals(object obj)
    {
      return base.Equals(obj);
    }
    public override int GetHashCode()
    {
      return ID.GetHashCode();
    }
    public override string ToString()
    {
      return ID.ToString();
    }

    public static readonly ServerClipTag Zero = new ServerClipTag(0);
  }

  internal struct ServerAnimationTag
  {
    public ServerAnimationTag(ushort IDValue)
    {
      ID = IDValue;
    }

    public readonly ushort ID;
    public bool IsZero
    {
      get { return ID == 0; }
    }

    public override bool Equals(object obj)
    {
      return base.Equals(obj);
    }
    public override int GetHashCode()
    {
      return ID.GetHashCode();
    }
    public override string ToString()
    {
      return ID.ToString();
    }

    public static readonly ServerAnimationTag Zero = new ServerAnimationTag(0);
  }

  internal struct ServerPickerTag
  {
    public ServerPickerTag(ushort IDValue)
    {
      ID = IDValue;
    }

    public readonly ushort ID;
    public bool IsZero
    {
      get { return ID == 0; }
    }

    public override bool Equals(object obj)
    {
      return base.Equals(obj);
    }
    public override int GetHashCode()
    {
      return ID.GetHashCode();
    }
    public override string ToString()
    {
      return ID.ToString();
    }
  }

  internal sealed class ServerSurface
  {
    public ServerSurface(ServerSurfaceTag Tag)
    {
      this.Tag = Tag;
    }

    public ServerSurfaceTag Tag { get; private set; }
  }

  internal sealed class ServerImage
  {
    public ServerImage(ServerEngine Engine, ServerImageTag Tag)
    {
      this.Engine = Engine;
      this.Tag = Tag;
    }

    public ServerEngine Engine { get; private set; }
    public ServerImageTag Tag { get; private set; }
  }

  internal sealed class ServerSound
  {
    public ServerSound(ServerEngine Engine, ServerSoundTag Tag)
    {
      this.Engine = Engine;
      this.Tag = Tag;
    }

    public ServerEngine Engine { get; private set; }
    public ServerSoundTag Tag { get; private set; }
  }

  internal sealed class ServerClip
  {
    public ServerClip(ServerEngine Engine, ServerClipTag Tag)
    {
      this.Engine = Engine;
      this.Tag = Tag;
    }

    public ServerEngine Engine { get; private set; }
    public ServerClipTag Tag { get; private set; }
  }

  internal sealed class ServerAnimation
  {
    public ServerAnimation(ServerEngine Engine, ServerAnimationTag Tag)
    {
      this.Engine = Engine;
      this.Tag = Tag;
    }

    public ServerEngine Engine { get; private set; }
    public ServerAnimationTag Tag { get; private set; }
  }

  internal sealed class ServerPicker
  {
    public ServerPicker(ServerPickerTag Tag)
    {
      this.Tag = Tag;
    }

    public ServerPickerTag Tag { get; private set; }
  }

  internal abstract class ServerPanel
  {
    public ServerPanel(ServerPanelTag Tag)
    {
      this.Tag = Tag;
    }

    public ServerPanelTag Tag { get; private set; }
  }

  internal sealed class ServerBrowser : ServerPanel
  {
    public ServerBrowser(ServerPanelTag Tag)
      : base(Tag)
    {
    }

    public string Html { get; set; }
    public Uri Uri { get; set; }
  }

  internal sealed class ServerButton : ServerPanel
  {
    public ServerButton(ServerPanelTag Tag)
      : base(Tag)
    {
      this.IsEnabled = true;
      this.IsFocusable = false;
    }

    public bool IsEnabled { get; set; }
    public bool IsFocusable { get; set; }
    public bool HasPress { get; set; }
    public bool HasRelease { get; set; }
  }

  internal sealed class ServerBoard : ServerPanel
  {
    public ServerBoard(ServerPanelTag Tag)
      : base(Tag)
    {
    }
  }

  internal sealed class ServerDock : ServerPanel
  {
    public ServerDock(ServerPanelTag Tag)
      : base(Tag)
    {
    }
  }

  internal sealed class ServerEdit : ServerPanel
  {
    public ServerEdit(ServerPanelTag Tag)
      : base(Tag)
    {
    }

    public bool HasChange { get; set; }
    public bool HasReturn { get; set; }
    public bool IsReadOnly { get; set; }
    public string Text { get; set; }
  }

  internal sealed class ServerFlow : ServerPanel
  {
    public ServerFlow(ServerPanelTag Tag)
      : base(Tag)
    {
    }
  }

  internal sealed class ServerFrame : ServerPanel
  {
    public ServerFrame(ServerPanelTag Tag)
      : base(Tag)
    {
    }
  }

  internal sealed class ServerGraphic : ServerPanel
  {
    public ServerGraphic(ServerPanelTag Tag)
      : base(Tag)
    {
    }
  }

  internal sealed class ServerLabel : ServerPanel
  {
    public ServerLabel(ServerPanelTag Tag)
      : base(Tag)
    {
    }

    public string Text { get; set; }
    public Justification? Justification { get; set; }
    public bool LineWrapping { get; set; }
  }

  internal sealed class ServerMemo : ServerPanel
  {
    public ServerMemo(ServerPanelTag Tag)
      : base(Tag)
    {
    }

    public bool IsReadOnly { get; set; }
    public string Text { get; set; }
    public bool HasChange { get; set; }
  }

  internal sealed class ServerOverlay : ServerPanel
  {
    public ServerOverlay(ServerPanelTag Tag)
      : base(Tag)
    {
    }
  }

  public enum RenderMessage
  {
    Invalid,
    Text,
    Image,
    StraightImage,
    OpacityImage,
    Arc,
    Line,
    Rectangle,
    StraightRectangle,
    Ellipse,
    Polygon
  }

  internal sealed class ServerCanvas : ServerPanel, DrawContract
  {
    public ServerCanvas(ServerPanelTag Tag, ServerContract Contract, Func<Inv.Image, ServerImageTag> TranslateImageTag)
      : base(Tag)
    {
      this.Contract = Contract;
      this.TranslateImageTag = TranslateImageTag;
      this.Writer = new ServerWriter();
      this.LastPacket = new ServerPacket(new byte[] { });
    }

    public void Begin()
    {
      Writer.Reset();
    }
    public ServerPacket End()
    {
      var Result = Writer.ToPacket();

      // TODO: could we optimise this further by checking bytes as we go?
      if (Result.Buffer.Length == LastPacket.Buffer.Length && Result.Buffer.ShallowEqualTo(LastPacket.Buffer))
        return null;

      LastPacket = Result;

      return Result;
    }

    int DrawContract.Width
    {
      get { return 1024; }
    }
    int DrawContract.Height
    {
      get { return 768; }
    }
    void DrawContract.DrawText(string TextFragment, string TextFontName, int TextFontSize, FontWeight TextFontWeight, Colour TextFontColour, Point TextPoint, HorizontalPosition TextHorizontal, VerticalPosition TextVertical)
    {
      Writer.WriteByte((byte)RenderMessage.Text);
      Writer.WriteString(TextFragment);
      Writer.WriteString(TextFontName);
      Writer.WriteInt16((short)TextFontSize);
      Writer.WriteByte((byte)TextFontWeight);
      Writer.WriteColour(TextFontColour);
      Writer.WriteInt16((short)TextPoint.X);
      Writer.WriteInt16((short)TextPoint.Y);
      Writer.WriteByte((byte)TextHorizontal);
      Writer.WriteByte((byte)TextVertical);
    }
    void DrawContract.DrawLine(Colour LineStrokeColour, int LineStrokeThickness, Point LineSourcePoint, Point LineTargetPoint, params Point[] LineExtraPointArray)
    {
      Writer.WriteByte((byte)RenderMessage.Line);
      Writer.WriteInt16((short)LineStrokeThickness);
      if (LineStrokeThickness > 0)
        Writer.WriteColour(LineStrokeColour);
      Writer.WriteInt16((short)LineSourcePoint.X);
      Writer.WriteInt16((short)LineSourcePoint.Y);
      Writer.WriteInt16((short)LineTargetPoint.X);
      Writer.WriteInt16((short)LineTargetPoint.Y);
      Writer.WriteInt16((short)LineExtraPointArray.Length);
      foreach (var LineExtraPoint in LineExtraPointArray)
      {
        Writer.WriteInt16((short)LineExtraPoint.X);
        Writer.WriteInt16((short)LineExtraPoint.Y);
      }
    }
    void DrawContract.DrawRectangle(Colour RectangleFillColour, Colour RectangleStrokeColour, int RectangleStrokeThickness, Rect RectangleRect)
    {
      if (RectangleRect.Width == RectangleRect.Height)
      {
        Writer.WriteByte((byte)RenderMessage.StraightRectangle);
        Writer.WriteColour(RectangleFillColour);
        Writer.WriteInt16((short)RectangleStrokeThickness);
        if (RectangleStrokeThickness > 0)
          Writer.WriteColour(RectangleStrokeColour);
        Writer.WriteInt16((short)RectangleRect.Left);
        Writer.WriteInt16((short)RectangleRect.Top);
        Writer.WriteInt16((short)RectangleRect.Width);
      }
      else
      {
        Writer.WriteByte((byte)RenderMessage.Rectangle);
        Writer.WriteColour(RectangleFillColour);
        Writer.WriteInt16((short)RectangleStrokeThickness);
        if (RectangleStrokeThickness > 0)
          Writer.WriteColour(RectangleStrokeColour);
        Writer.WriteInt16((short)RectangleRect.Left);
        Writer.WriteInt16((short)RectangleRect.Top);
        Writer.WriteInt16((short)RectangleRect.Width);
        Writer.WriteInt16((short)RectangleRect.Height);
      }
    }
    void DrawContract.DrawArc(Colour ArcFillColour, Colour ArcStrokeColour, int ArcStrokeThickness, Point ArcCenter, Point ArcRadius, float ArcStartAngle, float ArcSweepAngle)
    {
      Writer.WriteByte((byte)RenderMessage.Arc);
      Writer.WriteColour(ArcFillColour);
      Writer.WriteInt16((short)ArcStrokeThickness);
      if (ArcStrokeThickness > 0)
        Writer.WriteColour(ArcStrokeColour);
      Writer.WriteInt16((short)ArcCenter.X);
      Writer.WriteInt16((short)ArcCenter.Y);
      Writer.WriteInt16((short)ArcRadius.X);
      Writer.WriteInt16((short)ArcRadius.Y);
      Writer.WriteFloat(ArcStartAngle);
      Writer.WriteFloat(ArcSweepAngle);
    }
    void DrawContract.DrawEllipse(Colour EllipseFillColour, Colour EllipseStrokeColour, int EllipseStrokeThickness, Point EllipseCenter, Point EllipseRadius)
    {
      Writer.WriteByte((byte)RenderMessage.Ellipse);
      Writer.WriteColour(EllipseFillColour);
      Writer.WriteInt16((short)EllipseStrokeThickness);
      if (EllipseStrokeThickness > 0)
        Writer.WriteColour(EllipseStrokeColour);
      Writer.WriteInt16((short)EllipseCenter.X);
      Writer.WriteInt16((short)EllipseCenter.Y);
      Writer.WriteInt16((short)EllipseRadius.X);
      Writer.WriteInt16((short)EllipseRadius.Y);
    }
    void DrawContract.DrawImage(Image ImageSource, Rect ImageRect, float ImageOpacity, Colour ImageTint, Mirror? ImageMirror)
    {
      if (ImageRect.Width == ImageRect.Height && ImageTint == null && ImageMirror == null)
      {
        if (ImageOpacity == 1.0F)
        {
          Writer.WriteByte((byte)RenderMessage.StraightImage);
          Writer.WriteImageTag(TranslateImageTag(ImageSource));
          Writer.WriteInt16((short)ImageRect.Left);
          Writer.WriteInt16((short)ImageRect.Top);
          Writer.WriteInt16((short)ImageRect.Width);
        }
        else
        {
          Writer.WriteByte((byte)RenderMessage.OpacityImage);
          Writer.WriteImageTag(TranslateImageTag(ImageSource));
          Writer.WriteInt16((short)ImageRect.Left);
          Writer.WriteInt16((short)ImageRect.Top);
          Writer.WriteInt16((short)ImageRect.Width);
          Writer.WriteFloat(ImageOpacity);
        }
      }
      else
      {
        Writer.WriteByte((byte)RenderMessage.Image);
        Writer.WriteImageTag(TranslateImageTag(ImageSource));
        Writer.WriteInt16((short)ImageRect.Left);
        Writer.WriteInt16((short)ImageRect.Top);
        Writer.WriteInt16((short)ImageRect.Width);
        Writer.WriteInt16((short)ImageRect.Height);
        Writer.WriteFloat(ImageOpacity);
        Writer.WriteColour(ImageTint);
        Writer.WriteByte(ImageMirror != null ? (byte)ImageMirror.Value : (byte)255);
      }
    }
    void DrawContract.DrawPolygon(Colour PolygonFillColour, Colour PolygonStrokeColour, int PolygonStrokeThickness, LineJoin PolygonLineJoin, Point PolygonStartPoint, params Point[] PolygonPointArray)
    {
      Writer.WriteByte((byte)RenderMessage.Polygon);
      Writer.WriteColour(PolygonFillColour);
      Writer.WriteInt16((short)PolygonStrokeThickness);
      if (PolygonStrokeThickness > 0)
        Writer.WriteColour(PolygonStrokeColour);
      Writer.WriteByte((byte)PolygonLineJoin);
      Writer.WriteInt16((short)PolygonStartPoint.X);
      Writer.WriteInt16((short)PolygonStartPoint.Y);
      Writer.WriteInt16((short)PolygonPointArray.Length);
      foreach (var PolygonPoint in PolygonPointArray)
      {
        Writer.WriteInt16((short)PolygonPoint.X);
        Writer.WriteInt16((short)PolygonPoint.Y);
      }
    }

    private ServerContract Contract;
    private ServerWriter Writer;
    private Func<Image, ServerImageTag> TranslateImageTag;
    private ServerPacket LastPacket;
  }

  internal sealed class ServerScroll : ServerPanel
  {
    public ServerScroll(ServerPanelTag Tag)
      : base(Tag)
    {
    }
  }

  internal sealed class ServerStack : ServerPanel
  {
    public ServerStack(ServerPanelTag Tag)
      : base(Tag)
    {
    }
  }

  internal sealed class ServerTable : ServerPanel
  {
    public ServerTable(ServerPanelTag Tag)
      : base(Tag)
    {
    }
  }

  internal sealed class ServerTimer
  {
    public ServerTimer()
    {
    }

    public event Action IntervalEvent;
    public bool IsEnabled
    {
      get { return IsEnabledField; }
      set
      {
        if (value)
          Stop();
        else
          Start();
      }
    }
    public TimeSpan IntervalTime
    {
      get { return IntervalTimeField; }
      set { this.IntervalTimeField = value; }
    }

    public void Start()
    {
      if (!IsEnabledField)
      {
        IsEnabledField = true;

        this.CancelSource = new CancellationTokenSource();

        this.Task = Task.Run(() =>
        {
          while (IsEnabled)
          {
            try
            {
              Task.Delay(IntervalTime, CancelSource.Token);

              if (IsEnabled && IntervalEvent != null)
                IntervalEvent();
            }
            catch (Exception Exception)
            {
              // suppress exceptions.
              if (Debugger.IsAttached)
                Debugger.Break();

              Debug.WriteLine(Exception.Message);
            }
          }
        }, CancelSource.Token);
      }
    }
    public void Stop()
    {
      if (IsEnabledField)
      {
        IsEnabledField = false;

        if (CancelSource != null)
        {
          CancelSource.Cancel();
          this.CancelSource = null;
        }

        if (Task != null)
        {
          Task.Wait();
          this.Task = null;
        }
      }
    }

    private TimeSpan IntervalTimeField;
    private bool IsEnabledField;
    private Task Task;
    private CancellationTokenSource CancelSource;
  }
}