rem TODO: clean Inv solution?


@echo off
set /p CurrentBuild=<InvNugetBuild.txt
set /a NewBuild=%CurrentBuild%+1
attrib -r InvNugetBuild.txt
echo %NewBuild% >InvNugetBuild.txt
set Version=1.0.%NewBuild%
@echo.
@echo BUILDING VERSION %Version%
@echo.
@echo LIBRARY
@echo -------
c:\hole\nuget.exe pack c:\development\forge\inv\InvLibrary\InvLibrary.csproj -OutputDirectory C:\Deployment\Inv\ -Build -Properties InventionVersion=%Version%;Configuration=Release;Platform=AnyCPU
if %errorlevel% neq 0 exit /b %errorlevel%
@echo.
@echo PLATFORM
@echo --------
c:\hole\nuget.exe pack c:\development\forge\inv\InvPlatform\InvPlatform.csproj -OutputDirectory C:\Deployment\Inv\ -Build -Properties InventionVersion=%Version%;Configuration=Release;Platform=AnyCPU
if %errorlevel% neq 0 exit /b %errorlevel%
@echo.
@echo PLATFORM W - Windows Desktop (WPF)
@echo --------
c:\hole\nuget.exe pack c:\development\forge\inv\InvPlatformW\InvPlatformW.csproj -OutputDirectory C:\Deployment\Inv\ -Build -Properties InventionVersion=%Version%;Configuration=Release;Platform=AnyCPU
if %errorlevel% neq 0 exit /b %errorlevel%
rem @echo.
rem @echo PLATFORM A - Android
rem @echo --------
rem c:\hole\nuget.exe pack c:\development\forge\inv\InvPlatformA\InvPlatformA.csproj -OutputDirectory C:\Deployment\Inv\ -Build -Properties InventionVersion=%Version%;Configuration=Release;Platform=AnyCPU
rem if %errorlevel% neq 0 exit /b %errorlevel%
rem @echo.
rem @echo PLATFORM I - iOS
rem @echo --------
rem c:\hole\nuget.exe pack c:\development\forge\inv\InvPlatformI\InvPlatformI.csproj -OutputDirectory C:\Deployment\Inv\ -Build -Properties InventionVersion=%Version%;Configuration=Release;Platform=AnyCPU;ServerAddress=10.250.22.65;ServerUser=callanh;ServerPassword=
rem if %errorlevel% neq 0 exit /b %errorlevel%
rem @echo.
rem @echo PLATFORM U - Universal Windows (WinRT)
rem @echo --------
rem c:\hole\nuget.exe pack c:\development\forge\inv\InvPlatformU\InvPlatformU.csproj -OutputDirectory C:\Deployment\Inv\ -Build -Properties InventionVersion=%Version%;Configuration=Release;Platform=AnyCPU
rem if %errorlevel% neq 0 exit /b %errorlevel%
rem @echo.
rem @echo PLATFORM S - Server
rem @echo --------
rem c:\hole\nuget.exe pack c:\development\forge\inv\InvPlatformS\InvPlatformS.csproj -OutputDirectory C:\Deployment\Inv\ -Build -Properties InventionVersion=%Version%;Configuration=Release;Platform=AnyCPU
rem if %errorlevel% neq 0 exit /b %errorlevel%
@echo.
@echo PUBLISHING TO NUGET?
@echo.
pause
c:\hole\nuget push C:\Deployment\Inv\Invention.Library.%Version%.nupkg -Source https://www.nuget.org/api/v2/package
if %errorlevel% neq 0 exit /b %errorlevel%

c:\hole\nuget push C:\Deployment\Inv\Invention.Platform.%Version%.nupkg -Source https://www.nuget.org/api/v2/package
if %errorlevel% neq 0 exit /b %errorlevel%

c:\hole\nuget push C:\Deployment\Inv\Invention.Platform.W.%Version%.nupkg -Source https://www.nuget.org/api/v2/package
if %errorlevel% neq 0 exit /b %errorlevel%

rem c:\hole\nuget push C:\Deployment\Inv\Invention.Platform.A.%Version%.nupkg -Source https://www.nuget.org/api/v2/package
rem if %errorlevel% neq 0 exit /b %errorlevel%

rem c:\hole\nuget push C:\Deployment\Inv\Invention.Platform.I.%Version%.nupkg -Source https://www.nuget.org/api/v2/package
rem if %errorlevel% neq 0 exit /b %errorlevel%

rem c:\hole\nuget push C:\Deployment\Inv\Invention.Platform.U.%Version%.nupkg -Source https://www.nuget.org/api/v2/package
rem if %errorlevel% neq 0 exit /b %errorlevel%

rem c:\hole\nuget push C:\Deployment\Inv\Invention.Platform.S.%Version%.nupkg -Source https://www.nuget.org/api/v2/package
rem if %errorlevel% neq 0 exit /b %errorlevel%

@echo on