﻿/*! 3 !*/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv
{
  internal static class UwaStyles
  {
    static UwaStyles()
    {
      var StyleDictionary = new Windows.UI.Xaml.ResourceDictionary();
      StyleDictionary.Source = new Uri("ms-appx:///Inv.PlatformU/InvUwaStyles.xaml", UriKind.RelativeOrAbsolute);

      ButtonStyle = GetStyle(StyleDictionary, "InvUwaButton");
      MemoStyle = GetStyle(StyleDictionary, "InvUwaMemo");
    }

    public static readonly Windows.UI.Xaml.Style ButtonStyle;
    public static readonly Windows.UI.Xaml.Style MemoStyle;

    private static Windows.UI.Xaml.Style GetStyle(Windows.UI.Xaml.ResourceDictionary Dictionary, string Name)
    {
      return (Windows.UI.Xaml.Style)Dictionary[Name];
    }
  }

  internal interface UwaOverrideFocusContract
  {
    void OverrideFocus();
  }

  internal abstract class UwaPanel : Windows.UI.Xaml.Controls.ContentControl
  {
    public UwaPanel()
    {
      this.Border = new Windows.UI.Xaml.Controls.Border(); // border is a sealed class.
      Content = Border;

      HorizontalContentAlignment = Windows.UI.Xaml.HorizontalAlignment.Stretch;
      VerticalContentAlignment = Windows.UI.Xaml.VerticalAlignment.Stretch;
    }

    public Windows.UI.Xaml.Controls.Border Border { get; private set; }
  }

  [Windows.UI.Xaml.TemplatePart(Name = "PART_Border", Type = typeof(Windows.UI.Xaml.Controls.Border))]
  internal sealed class UwaButton : Windows.UI.Xaml.Controls.Button
  {
    public UwaButton()
    {
      this.Style = UwaStyles.ButtonStyle;
      this.Border = new Windows.UI.Xaml.Controls.Border(); // need an extra border because PartBorder is only available after the template is applied.
      this.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Stretch;
      this.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Stretch;
    }

    public Windows.UI.Xaml.Controls.Border Border { get; private set; }

    protected override void OnApplyTemplate()
    {
      base.OnApplyTemplate();

      this.PartBorder = this.GetTemplateChild("PART_Border") as Windows.UI.Xaml.Controls.Border;

      if (PartBorder != null)
        PartBorder.Child = Border;
    }

    private Windows.UI.Xaml.Controls.Border PartBorder;
  }

  internal sealed class UwaBoard : UwaPanel
  {
    public UwaBoard()
    {
      this.Canvas = new Windows.UI.Xaml.Controls.Canvas();
      Border.Child = Canvas;
    }

    public Windows.UI.Xaml.Controls.Canvas Canvas { get; private set; }
  }

  internal sealed class UwaBrowser : UwaPanel
  {
    public UwaBrowser()
    {
      this.Inner = new Windows.UI.Xaml.Controls.WebView();
      Border.Child = Inner;
      Inner.NavigationCompleted += (Sender, Event) =>
      {
        Debug.WriteLine("NavigationCompleted");
      };
      Inner.UnsafeContentWarningDisplaying += (Sender, Event) =>
      {
        Debug.WriteLine("UnsafeContentWarning");
      };
      Inner.UnviewableContentIdentified += (Sender, Event) =>
      {
        Debug.WriteLine("UnviewableContentIdentified");
      };
    }

    public void Navigate(Uri Uri, string Html)
    {
      if (Html != null)
        Inner.NavigateToString(Html);
      else if (Uri != null)
        Inner.Navigate(Uri);
      else
        Inner.Navigate(new Uri("about:blank"));
    }

    private Windows.UI.Xaml.Controls.WebView Inner;
  }

  internal sealed class UwaFlow : UwaPanel
  {
    public UwaFlow()
    {
      this.ScrollViewer = new Windows.UI.Xaml.Controls.ScrollViewer();
      Border.Child = ScrollViewer;
      ScrollViewer.VerticalScrollBarVisibility = Windows.UI.Xaml.Controls.ScrollBarVisibility.Auto;
      ScrollViewer.HorizontalScrollBarVisibility = Windows.UI.Xaml.Controls.ScrollBarVisibility.Disabled;

      this.StackPanel = new Windows.UI.Xaml.Controls.StackPanel();
      ScrollViewer.Content = StackPanel;
      StackPanel.Orientation = Windows.UI.Xaml.Controls.Orientation.Vertical;
    }

    public Func<int, Windows.UI.Xaml.FrameworkElement> HeaderContentQuery;
    public Func<int, Windows.UI.Xaml.FrameworkElement> FooterContentQuery;
    public Func<int> SectionCountQuery;
    public Func<int, int> ItemCountQuery;
    public Func<int, int, Windows.UI.Xaml.FrameworkElement> ItemContentQuery;

    public void Reload()
    {
      StackPanel.Children.Clear();

      var SectionCount = SectionCountQuery();

      for (var SectionIndex = 0; SectionIndex < SectionCount; SectionIndex++)
      {
        var HeaderContent = HeaderContentQuery(SectionIndex);
        if (HeaderContent != null)
          StackPanel.SafeAddChild(HeaderContent);

        var ItemCount = ItemCountQuery(SectionIndex);

        for (var ItemIndex = 0; ItemIndex < ItemCount; ItemIndex++)
        {
          var ItemContent = ItemContentQuery(SectionIndex, ItemIndex);

          if (ItemContent != null)
            StackPanel.SafeAddChild(ItemContent);
        }

        var FooterContent = FooterContentQuery(SectionIndex);
        if (FooterContent != null)
          StackPanel.SafeAddChild(FooterContent);
      }
    }
    public void ScrollTo(int Section, int Index)
    {
      // NOTE: idle async is required if the control has not yet been layed out in the visual tree.
      Dispatcher.RunIdleAsync(e =>
      {
        var ItemContent = ItemContentQuery(Section, Index);
        var visual = ItemContent.TransformToVisual(ScrollViewer);
        var point = visual.TransformPoint(new Windows.Foundation.Point(0, 0));

        ScrollViewer.ChangeView(null, point.Y, null);
      }).AsTask().Forget();
    }

    private int GetAbsoluteIndex(int Section, int Index)
    {
      var Position = 0;

      for (var SectionIndex = 0; SectionIndex <= Section; SectionIndex++)
      {
        if (HeaderContentQuery(SectionIndex) != null)
          Position++;

        if (SectionIndex == Section)
        {
          Position += Index;
          break;
        }

        Position += ItemCountQuery(SectionIndex);

        if (FooterContentQuery(SectionIndex) != null)
          Position++;
      }

      return Position;
    }

    private Windows.UI.Xaml.Controls.ScrollViewer ScrollViewer;
    private Windows.UI.Xaml.Controls.StackPanel StackPanel;
  }

  internal sealed class UwaDock : UwaPanel
  {
    public UwaDock()
    {
      this.Grid = new Windows.UI.Xaml.Controls.Grid();
      Border.Child = Grid;
    }

    public Windows.UI.Xaml.Controls.Grid Grid { get; private set; }

    public void AddChild(Windows.UI.Xaml.FrameworkElement UwaPanel)
    {
      Grid.SafeAddChild(UwaPanel);
    }
  }

  internal sealed class UwaEdit : UwaPanel, UwaOverrideFocusContract
  {
    public UwaEdit(bool PasswordMask)
    {
      if (PasswordMask)
      {
        this.PasswordBox = new Windows.UI.Xaml.Controls.PasswordBox()
        {
          Background = null,
          BorderBrush = null,
          BorderThickness = new Windows.UI.Xaml.Thickness(0),
          IsPasswordRevealButtonEnabled = true
        };
        Border.Child = PasswordBox;
      }
      else
      {
        this.TextBox = new Windows.UI.Xaml.Controls.TextBox()
        {
          AcceptsReturn = false,
          Background = null,
          BorderBrush = null,
          TextWrapping = Windows.UI.Xaml.TextWrapping.Wrap,
          BorderThickness = new Windows.UI.Xaml.Thickness(0)
        };
        Border.Child = TextBox;
      }
    }

    public Windows.UI.Xaml.Controls.TextBox TextBox { get; private set; }
    public Windows.UI.Xaml.Controls.PasswordBox PasswordBox { get; private set; }

    void UwaOverrideFocusContract.OverrideFocus()
    {
      if (TextBox != null)
        TextBox.Focus(Windows.UI.Xaml.FocusState.Programmatic);
      else
        PasswordBox.Focus(Windows.UI.Xaml.FocusState.Programmatic);
    }
  }

  internal sealed class UwaGraphic : UwaPanel
  {
    public UwaGraphic()
    {
      this.Image = new Windows.UI.Xaml.Controls.Image()
      {
        Stretch = Windows.UI.Xaml.Media.Stretch.Uniform
      };
      Image.CacheMode = new Windows.UI.Xaml.Media.BitmapCache();
      //RenderOptions.SetBitmapScalingMode(Inner, BitmapScalingMode.Fant);

      Border.Child = Image;
    }

    public Windows.UI.Xaml.Controls.Image Image { get; private set; }
  }

  internal sealed class UwaLabel : UwaPanel
  {
    public UwaLabel()
    {
      this.TextBlock = new Windows.UI.Xaml.Controls.TextBlock()
      {
        VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Center,
        TextTrimming = Windows.UI.Xaml.TextTrimming.CharacterEllipsis
      };
      Border.Child = TextBlock;
    }

    public Windows.UI.Xaml.Controls.TextBlock TextBlock { get; private set; }
  }

  internal sealed class UwaMemo : UwaPanel, UwaOverrideFocusContract
  {
    public UwaMemo()
    {
      this.TextBox = new Windows.UI.Xaml.Controls.TextBox()
      {
        AcceptsReturn = true,
        //AcceptsTabs = true, // Not available in WinRT?
        TextWrapping = Windows.UI.Xaml.TextWrapping.Wrap,
        Background = null,
        BorderBrush = null,
        BorderThickness = new Windows.UI.Xaml.Thickness(0),
        Style = UwaStyles.MemoStyle
      };
      Border.Child = TextBox;
    }

    public Windows.UI.Xaml.Controls.TextBox TextBox { get; private set; }
    public string Text
    {
      get { return TextBox.Text; }
      set { TextBox.Text = value; }
      /*
      get
      {
        string Result;
        TextBox.Document.GetText(Windows.UI.Text.TextGetOptions.None, out Result);
        return Result;
      }
      set { TextBox.Document.SetText(Windows.UI.Text.TextSetOptions.None, value); }*/
    }

    void UwaOverrideFocusContract.OverrideFocus()
    {
      TextBox.Focus(Windows.UI.Xaml.FocusState.Programmatic);
    }
  }

  internal sealed class UwaOverlay : UwaPanel
  {
    public UwaOverlay()
    {
      this.Grid = new Windows.UI.Xaml.Controls.Grid();
      Border.Child = Grid;
    }

    public Windows.UI.Xaml.Controls.Grid Grid { get; private set; }

    public void AddChild(Windows.UI.Xaml.FrameworkElement UwaPanel)
    {
      Grid.SafeAddChild(UwaPanel);
    }
  }

  internal sealed class UwaCanvas : UwaPanel, Inv.DrawContract
  {
    public UwaCanvas(UwaEngine UwaEngine)
    {
      this.UwaEngine = UwaEngine;
      this.Canvas = new Microsoft.Graphics.Canvas.UI.Xaml.CanvasControl();
      Border.Child = Canvas;
      Canvas.CreateResources += (Sender, Event) =>
      {
        if (ResetEvent != null)
          ResetEvent();
      };
      Canvas.Draw += (Sender, Event) =>
      {
        this.UwaSession = Event.DrawingSession;
        DrawAction();
      };

      this.ImageSourceRect = new Windows.Foundation.Rect();
      this.ImageDestinationRect = new Windows.Foundation.Rect();

      this.UwaStrokeStyle = new Microsoft.Graphics.Canvas.Geometry.CanvasStrokeStyle()
      {
        EndCap = Microsoft.Graphics.Canvas.Geometry.CanvasCapStyle.Round
      };
    }

    public event Action ResetEvent;
    public Action DrawAction;

    public Microsoft.Graphics.Canvas.UI.Xaml.CanvasControl Canvas { get; private set; }

    int DrawContract.Width
    {
      get { return (int)ActualWidth; }
    }
    int DrawContract.Height
    {
      get { return (int)ActualHeight; }
    }
    void DrawContract.DrawText(string TextFragment, string TextFontName, int TextFontSize, FontWeight TextFontWeight, Colour TextFontColour, Point TextPoint, HorizontalPosition TextHorizontal, VerticalPosition TextVertical)
    {
      var TextFormat = new Microsoft.Graphics.Canvas.Text.CanvasTextFormat()
      {
        FontSize = TextFontSize,
        WordWrapping = Microsoft.Graphics.Canvas.Text.CanvasWordWrapping.NoWrap,
        FontWeight = UwaEngine.TranslateFontWeight(TextFontWeight)
      };

      var TextLayout = new Microsoft.Graphics.Canvas.Text.CanvasTextLayout(UwaSession, TextFragment, TextFormat, 0.0f, 0.0f);

      var TextX = TextPoint.X - (int)TextLayout.DrawBounds.X;
      var TextY = TextPoint.Y - (int)TextLayout.DrawBounds.Y;

      if (TextHorizontal != HorizontalPosition.Left)
      {
        var TextWidth = (int)TextLayout.DrawBounds.Width;

        if (TextHorizontal == HorizontalPosition.Right)
          TextX -= TextWidth;
        else if (TextHorizontal == HorizontalPosition.Center)
          TextX -= TextWidth / 2;
      }

      if (TextVertical != VerticalPosition.Top)
      {
        var TextHeight = (int)TextLayout.DrawBounds.Height;

        if (TextVertical == VerticalPosition.Bottom)
          TextY -= TextHeight;
        else if (TextVertical == VerticalPosition.Center)
          TextY -= TextHeight / 2;
      }

      UwaSession.DrawTextLayout(TextLayout, TextX, TextY, UwaEngine.TranslateMediaColour(TextFontColour));
    }
    void DrawContract.DrawLine(Inv.Colour LineStrokeColour, int LineStrokeThickness, Inv.Point LineSourcePoint, Inv.Point LineTargetPoint, params Inv.Point[] LineExtraPointArray)
    {
      if (LineStrokeThickness <= 0 || LineStrokeColour == Inv.Colour.Transparent)
        return;

      var LineStrokeBrush = UwaEngine.TranslateCanvasBrush(UwaSession, LineStrokeColour);

      UwaSession.DrawLine(LineSourcePoint.X, LineSourcePoint.Y, LineTargetPoint.X, LineTargetPoint.Y, LineStrokeBrush, LineStrokeThickness, UwaStrokeStyle);

      var CurrentPoint = LineTargetPoint;
      for (var Index = 0; Index < LineExtraPointArray.Length; Index++)
      {
        var TargetPoint = LineExtraPointArray[Index];
        UwaSession.DrawLine(CurrentPoint.X, CurrentPoint.Y, TargetPoint.X, TargetPoint.Y, LineStrokeBrush, LineStrokeThickness, UwaStrokeStyle);

        CurrentPoint = TargetPoint;
      }
    }
    void DrawContract.DrawRectangle(Colour RectangleFillColour, Colour RectangleStrokeColour, int RectangleStrokeThickness, Rect RectangleRect)
    {
      var RectangleX = RectangleRect.Left;
      var RectangleY = RectangleRect.Top;
      var RectangleWidth = RectangleRect.Width;
      var RectangleHeight = RectangleRect.Height;

      if (RectangleFillColour != null)
        UwaSession.FillRectangle(RectangleX, RectangleY, RectangleWidth, RectangleHeight, UwaEngine.TranslateCanvasBrush(UwaSession, RectangleFillColour));

      if (RectangleStrokeColour != null && RectangleStrokeThickness > 0)
      {
        RectangleX += RectangleStrokeThickness / 2;
        RectangleY += RectangleStrokeThickness / 2;

        if (RectangleWidth >= RectangleStrokeThickness)
          RectangleWidth -= RectangleStrokeThickness;

        if (RectangleHeight >= RectangleStrokeThickness)
          RectangleHeight -= RectangleStrokeThickness;

        UwaSession.DrawRectangle(RectangleX, RectangleY, RectangleWidth, RectangleHeight, UwaEngine.TranslateCanvasBrush(UwaSession, RectangleStrokeColour), RectangleStrokeThickness);
      }
    }
    void DrawContract.DrawEllipse(Colour EllipseFillColour, Colour EllipseStrokeColour, int EllipseStrokeThickness, Point EllipseCenter, Point EllipseRadius)
    {
      var EllipseX = EllipseCenter.X;
      var EllipseY = EllipseCenter.Y;
      var EllipseRadiusX = EllipseRadius.X;
      var EllipseRadiusY = EllipseRadius.Y;

      if (EllipseFillColour != null)
        UwaSession.FillEllipse(EllipseX, EllipseY, EllipseRadiusX, EllipseRadiusY, UwaEngine.TranslateCanvasBrush(UwaSession, EllipseFillColour));

      if (EllipseStrokeColour != null && EllipseStrokeThickness > 0)
        UwaSession.DrawEllipse(EllipseX, EllipseY, EllipseRadiusX, EllipseRadiusY, UwaEngine.TranslateCanvasBrush(UwaSession, EllipseStrokeColour), EllipseStrokeThickness);
    }
    void DrawContract.DrawArc(Inv.Colour ArcFillColour, Inv.Colour ArcStrokeColour, int ArcStrokeThickness, Inv.Point ArcCenter, Inv.Point ArcRadius, float StartAngle, float SweepAngle)
    {
      var MaxWidth = Math.Max(0.0, (ArcRadius.X * 2) - ArcStrokeThickness);
      var MaxHeight = Math.Max(0.0, (ArcRadius.Y * 2) - ArcStrokeThickness);

      var StartX = ArcRadius.X * (float)Math.Cos(StartAngle * Math.PI / 180.0);
      var StartY = ArcRadius.Y * (float)Math.Sin(StartAngle * Math.PI / 180.0);

      var EndX = ArcRadius.X * (float)Math.Cos(SweepAngle * Math.PI / 180.0);
      var EndY = ArcRadius.Y * (float)Math.Sin(SweepAngle * Math.PI / 180.0);

      var WpfBuilder = new Microsoft.Graphics.Canvas.Geometry.CanvasPathBuilder(Canvas);

      WpfBuilder.BeginFigure(new Microsoft.Graphics.Canvas.Numerics.Vector2() { X = ArcCenter.X + StartX, Y = ArcCenter.Y - StartY });
      WpfBuilder.AddArc(
        endPoint: new Microsoft.Graphics.Canvas.Numerics.Vector2() { X = ArcCenter.X + EndX, Y = ArcCenter.Y - EndY }, 
        radiusX: ArcRadius.X, 
        radiusY: ArcRadius.Y,
        rotationAngle: 0.0F, 
        sweepDirection: Microsoft.Graphics.Canvas.Geometry.CanvasSweepDirection.CounterClockwise, 
        arcSize: (SweepAngle - StartAngle) > 180 ? Microsoft.Graphics.Canvas.Geometry.CanvasArcSize.Large : Microsoft.Graphics.Canvas.Geometry.CanvasArcSize.Small
      );
      WpfBuilder.AddLine(ArcCenter.X, ArcCenter.Y);

      WpfBuilder.EndFigure(Microsoft.Graphics.Canvas.Geometry.CanvasFigureLoop.Closed);

      var WpfGeometry = Microsoft.Graphics.Canvas.Geometry.CanvasGeometry.CreatePath(WpfBuilder);

      if (ArcFillColour != null)
        UwaSession.FillGeometry(WpfGeometry, UwaEngine.TranslateCanvasBrush(UwaSession, ArcFillColour));

      if (ArcStrokeColour != null && ArcStrokeThickness > 0)
        UwaSession.DrawGeometry(WpfGeometry, UwaEngine.TranslateCanvasBrush(UwaSession, ArcStrokeColour), ArcStrokeThickness);
    }
    void DrawContract.DrawImage(Image ImageSource, Rect ImageRect, float ImageOpacity, Colour ImageTint, Mirror? ImageMirror)
    {
      var ImageBitmap = UwaEngine.TranslateCanvasBitmap(UwaSession, ImageSource);

      ImageSourceRect.Width = ImageBitmap.Size.Width;
      ImageSourceRect.Height = ImageBitmap.Size.Height;

      ImageDestinationRect.X = ImageRect.Left;
      ImageDestinationRect.Y = ImageRect.Top;
      ImageDestinationRect.Width = ImageRect.Width;
      ImageDestinationRect.Height = ImageRect.Height;

      var CanvasImage = (Microsoft.Graphics.Canvas.ICanvasImage)ImageBitmap;

      if (ImageTint != null)
      {
        var TintTarget = new Microsoft.Graphics.Canvas.CanvasRenderTarget(UwaSession, (int)ImageSourceRect.Width, (int)ImageSourceRect.Height);
        using (var TintSession = TintTarget.CreateDrawingSession())
        {
          TintSession.Clear(Windows.UI.Colors.Transparent);
          TintSession.DrawImage(ImageBitmap, ImageSourceRect);
          TintSession.FillRectangle(ImageSourceRect, new Microsoft.Graphics.Canvas.Brushes.CanvasSolidColorBrush(TintSession, UwaEngine.TranslateMediaColour(ImageTint)), new Microsoft.Graphics.Canvas.Brushes.CanvasImageBrush(TintSession, ImageBitmap));
        }

        CanvasImage = TintTarget;
      }

      if (ImageMirror != null)
      {
        CanvasImage = new Microsoft.Graphics.Canvas.Effects.Transform2DEffect()
        {
          Source = CanvasImage,
          TransformMatrix = System.Numerics.Matrix3x2.CreateScale(ImageMirror.Value == Mirror.Horizontal ? -1 : +1, ImageMirror.Value == Mirror.Vertical ? -1 : +1, new System.Numerics.Vector2((float)ImageSourceRect.Width / 2.0F, (float)ImageSourceRect.Height / 2.0F))
        };
      }

      UwaSession.DrawImage(CanvasImage, ImageDestinationRect, ImageSourceRect, ImageOpacity);
    }

    void DrawContract.DrawPolygon(Colour FillColour, Colour StrokeColour, int StrokeThickness, LineJoin LineJoin, Point StartPoint, params Point[] PointArray)
    {
      throw new NotImplementedException();
    }

    private UwaEngine UwaEngine;
    private Microsoft.Graphics.Canvas.CanvasDrawingSession UwaSession;
    private Windows.Foundation.Rect ImageSourceRect;
    private Windows.Foundation.Rect ImageDestinationRect;
    private Microsoft.Graphics.Canvas.Geometry.CanvasStrokeStyle UwaStrokeStyle;
  }

  internal sealed class UwaScroll : UwaPanel
  {
    public UwaScroll(bool IsVertical, bool IsHorizontal)
    {
      this.ScrollViewer = new Windows.UI.Xaml.Controls.ScrollViewer();
      Border.Child = ScrollViewer;

      if (!IsVertical && IsHorizontal)
      {
        ScrollViewer.VerticalScrollBarVisibility = Windows.UI.Xaml.Controls.ScrollBarVisibility.Disabled;
        ScrollViewer.HorizontalScrollBarVisibility = Windows.UI.Xaml.Controls.ScrollBarVisibility.Auto;
        ScrollViewer.HorizontalScrollMode = Windows.UI.Xaml.Controls.ScrollMode.Auto;
        ScrollViewer.VerticalScrollMode = Windows.UI.Xaml.Controls.ScrollMode.Disabled;
      }
      else if (IsVertical && !IsHorizontal)
      {
        ScrollViewer.VerticalScrollBarVisibility = Windows.UI.Xaml.Controls.ScrollBarVisibility.Auto;
        ScrollViewer.HorizontalScrollBarVisibility = Windows.UI.Xaml.Controls.ScrollBarVisibility.Disabled;
        ScrollViewer.HorizontalScrollMode = Windows.UI.Xaml.Controls.ScrollMode.Disabled;
        ScrollViewer.VerticalScrollMode = Windows.UI.Xaml.Controls.ScrollMode.Auto;
      }
      else
      {
        ScrollViewer.VerticalScrollBarVisibility = Windows.UI.Xaml.Controls.ScrollBarVisibility.Auto;
        ScrollViewer.HorizontalScrollBarVisibility = Windows.UI.Xaml.Controls.ScrollBarVisibility.Auto;
        ScrollViewer.HorizontalScrollMode = Windows.UI.Xaml.Controls.ScrollMode.Auto;
        ScrollViewer.VerticalScrollMode = Windows.UI.Xaml.Controls.ScrollMode.Auto;
      }
    }

    public Windows.UI.Xaml.Controls.ScrollViewer ScrollViewer { get; private set; }
  }

  internal sealed class UwaStack : UwaPanel
  {
    public UwaStack()
    {
      this.StackPanel = new Windows.UI.Xaml.Controls.StackPanel();
      Border.Child = StackPanel;
    }

    public Windows.UI.Xaml.Controls.StackPanel StackPanel { get; private set; }

    public void Compose(IEnumerable<Windows.UI.Xaml.FrameworkElement> UwaElements)
    {
      StackPanel.Children.Clear();

      foreach (var UwaElement in UwaElements)
        StackPanel.SafeAddChild(UwaElement);
    }
  }

  internal sealed class UwaSurface : UwaPanel
  {
    public UwaSurface()
    {
      this.Loaded += (Sender, Event) =>
      {
        this.IsLoaded = true;
      };
      this.Unloaded += (Sender, Event) =>
      {
        this.IsLoaded = false;
      };
    }

    public bool IsLoaded { get; private set; }
  }

  internal sealed class UwaTable : UwaPanel
  {
    public UwaTable()
    {
      this.Grid = new Windows.UI.Xaml.Controls.Grid();
      Border.Child = Grid;
    }

    public Windows.UI.Xaml.Controls.Grid Grid { get; private set; }

    public void AddChild(Windows.UI.Xaml.FrameworkElement UwaCell)
    {
      Grid.SafeAddChild(UwaCell);
    }
  }

  internal static class UwaFoundation
  {
    public static void SafeAddChild(this Windows.UI.Xaml.Controls.Panel Parent, Windows.UI.Xaml.FrameworkElement Child)
    {
      var Owner = Windows.UI.Xaml.Media.VisualTreeHelper.GetParent(Child);

      if (Owner == null)
      {
        Parent.Children.Add(Child);
      }
      else if (Owner != Parent)
      {
        var OwnerAsPanel = Owner as Windows.UI.Xaml.Controls.Panel;
        if (OwnerAsPanel != null)
        {
          OwnerAsPanel.Children.Remove(Child);
        }
        else
        {
          var OwnerAsContentControl = Owner as Windows.UI.Xaml.Controls.ContentControl;
          if (OwnerAsContentControl != null)
          {
            OwnerAsContentControl.Content = null;
          }
          else
          {
            var OwnerAsBorder = Owner as Windows.UI.Xaml.Controls.Border;
            if (OwnerAsBorder != null)
            {
              OwnerAsBorder.Child = null;
            }
            else
            {
              Debug.Assert(false, "Unhandled owner.");
            }
          }
        }

        Parent.Children.Add(Child);
      }
    }
  }
}
