﻿/*! 2 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.Devices.Enumeration.Pnp;
using Windows.Networking.Connectivity;
using Windows.System;

namespace Inv
{
  internal static class UwaSystem
  {
    // https://github.com/Q42/Q42.WinRT/blob/master/Q42.WinRT/Util.cs

    public static string GetAppVersion()
    {
      var package = Package.Current;
      var packageId = package.Id;
      var version = packageId.Version;

      return string.Format("{0}.{1}.{2}.{3}", version.Major, version.Minor, version.Build, version.Revision);
    }
    public static async Task<string> GetOsVersionAsync()
    {
      var userAgent = await GetUserAgentAsync();

      var result = string.Empty;

      //Parse user agent
      var startIndex = userAgent.ToLower().IndexOf("windows");
      if (startIndex > 0)
      {
        int endIndex = userAgent.IndexOf(";", startIndex);

        if (endIndex > startIndex)
          result = userAgent.Substring(startIndex, endIndex - startIndex);
      }

      return result;
    }
    public static string GetOsVersion()
    {
      var Task = GetOsVersionAsync();
      Task.Wait();
      return Task.Result;
    }
    public static string GetMachineName()
    {
      try
      {
        var list = NetworkInformation.GetHostNames().ToArray();
        string name = null;
        if (list.Length > 0)
        {
          for (var i = 0; i < list.Length; i++)
          {
            var entry = list[i];
            if (entry.Type == Windows.Networking.HostNameType.DomainName)
            {
              var s = entry.CanonicalName;
              if (!string.IsNullOrEmpty(s))
              {
                // Domain-joined. Requires at least a one-
                // character name.
                var j = s.IndexOf('.');

                if (j > 0)
                {
                  name = s.Substring(0, j);
                  break;
                }
                else
                {
                  // Typical home machine.
                  name = s;
                }
              }
            }
          }
        }

        if (string.IsNullOrEmpty(name))
          name = "Unknown Windows ?";  // TODO: Localize?

        return name;
      }
      catch
      {
        return "Windows ?";
      }
    }
    public static bool HasTouchInput()
    {
      //var MAXTOUCHES_INDEX = 0x95;
      //return GetSystemMetrics(MAXTOUCHES_INDEX) > 0;
      return true;
    }

    /// <summary>
    /// Uses WebView control to get the user agent string
    /// </summary>
    /// <returns></returns>
    private static Task<string> GetUserAgentAsync()
    {
      var tcs = new TaskCompletionSource<string>();

      var webView = new Windows.UI.Xaml.Controls.WebView();

      string htmlFragment =
        @"<html>
                    <head>
                        <script type='text/javascript'>
                            function GetUserAgent() 
                            {
                                return navigator.userAgent;
                            }
                        </script>
                    </head>
                </html>";

      webView.NavigationCompleted += async (sender, e) =>
      {
        try
        {
          //Invoke the javascript when the html load is complete
          var result = await webView.InvokeScriptAsync("GetUserAgent", null);

          //Set the task result
          tcs.TrySetResult(result);
        }
        catch (Exception ex)
        {
          tcs.TrySetException(ex);
        }
      };

      //Load Html
      webView.NavigateToString(htmlFragment);

      return tcs.Task;
    }

    // https://gist.github.com/HamGuy/8294463
    public static async Task<ProcessorArchitecture> GetProcessorArchitectureAsync()
    {
      var halDevice = await GetHalDevice(ItemNameKey);
      if (halDevice != null && halDevice.Properties[ItemNameKey] != null)
      {
        var halName = halDevice.Properties[ItemNameKey].ToString();
        if (halName.Contains("x64")) return ProcessorArchitecture.X64;
        if (halName.Contains("ARM")) return ProcessorArchitecture.Arm;
        return ProcessorArchitecture.X86;
      }
      return ProcessorArchitecture.Unknown;
    }
    public static Task<string> GetDeviceManufacturerAsync()
    {
      return GetRootDeviceInfoAsync(ManufacturerKey);
    }
    public static string GetDeviceModel()
    {
      try
      {
        var Task = GetDeviceModelAsync();
        if (Task.Wait(1000))
          return Task.Result;
        else
          return "Timeout";
      }
      catch
      {
        return "Unknown";
      }
    }
    public static Task<string> GetDeviceModelAsync()
    {
      return GetRootDeviceInfoAsync(ModelNameKey);
    }
    public static Task<string> GetDeviceCategoryAsync()
    {
      return GetRootDeviceInfoAsync(PrimaryCategoryKey);
    }
    public static string GetWindowsVersion()
    {
      var Task = GetWindowsVersionAsync();
      Task.Wait();
      return Task.Result;
    }
    public static async Task<string> GetWindowsVersionAsync()
    {
      // There is no good place to get this so we're going to use the most popular
      // Microsoft driver version number from the device tree.
      var requestedProperties = new[] { DeviceDriverVersionKey, DeviceDriverProviderKey };

      var microsoftVersionedDevices = (await PnpObject.FindAllAsync(PnpObjectType.Device, requestedProperties, RootContainerQuery))
          .Select(d => new
          {
            Provider = (string)d.Properties.GetValueOrDefault(DeviceDriverProviderKey),
            Version = (string)d.Properties.GetValueOrDefault(DeviceDriverVersionKey)
          })
          .Where(d => d.Provider == "Microsoft" && d.Version != null)
          .ToList();

      var versionNumbers = microsoftVersionedDevices
          .GroupBy(d => d.Version.Substring(0, d.Version.IndexOf('.', d.Version.IndexOf('.') + 1)))
          .OrderByDescending(d => d.Count())
          .ToList();

      var confidence = (versionNumbers[0].Count() * 100 / microsoftVersionedDevices.Count);
      return versionNumbers.Count > 0 ? versionNumbers[0].Key : "";
    }

    private static async Task<string> GetRootDeviceInfoAsync(string propertyKey)
    {
      var pnp = await PnpObject.CreateFromIdAsync(PnpObjectType.DeviceContainer, RootContainer, new[] { propertyKey });
      return (string)pnp.Properties[propertyKey];
    }
    private static async Task<PnpObject> GetHalDevice(params string[] properties)
    {
      var actualProperties = properties.Concat(new[] { DeviceClassKey });
      var rootDevices = await PnpObject.FindAllAsync(PnpObjectType.Device,
          actualProperties, RootContainerQuery);

      foreach (var rootDevice in rootDevices.Where(d => d.Properties != null && d.Properties.Any()))
      {
        var lastProperty = rootDevice.Properties.Last();
        if (lastProperty.Value != null)
          if (lastProperty.Value.ToString().Equals(HalDeviceClass))
            return rootDevice;
      }
      return null;
    }
    private static object GetValueOrDefault<TKey, TValue>(this IReadOnlyDictionary<TKey, TValue> Dictionary, TKey Key, TValue Default = default(TValue))
    {
      TValue Result;
      if (!Dictionary.TryGetValue(Key, out Result))
        Result = Default;
      return Result;
    }

    // Error Found: API GetSystemMetrics in user32.dll is not supported for this application type.
    //[System.Runtime.InteropServices.DllImport("user32.dll")]
    //private static extern int GetSystemMetrics(int nIndex);

    private const string ItemNameKey = "System.ItemNameDisplay";
    private const string ModelNameKey = "System.Devices.ModelName";
    private const string ManufacturerKey = "System.Devices.Manufacturer";
    private const string DeviceClassKey = "{A45C254E-DF1C-4EFD-8020-67D146A850E0},10";
    private const string PrimaryCategoryKey = "{78C34FC8-104A-4ACA-9EA4-524D52996E57},97";
    private const string DeviceDriverKey = "{A8B865DD-2E3D-4094-AD97-E593A70C75D6}";
    private const string DeviceDriverVersionKey = DeviceDriverKey + ",3";
    private const string DeviceDriverProviderKey = DeviceDriverKey + ",9";
    private const string HalDeviceClass = "4d36e966-e325-11ce-bfc1-08002be10318";
    private const string RootContainer = "{00000000-0000-0000-FFFF-FFFFFFFFFFFF}";
    private const string RootContainerQuery = "System.Devices.ContainerId:=\"" + RootContainer + "\"";
  }
}
