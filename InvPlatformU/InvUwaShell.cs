﻿/*! 11 !*/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv
{
  public static class UwaShell
  {
    static UwaShell()
    {
    }

    public static void Attach(Windows.UI.Xaml.Controls.Page UwaPage, Action<Inv.Application> ApplicationAction)
    {
      try
      {
        var InvApplication = new Inv.Application();

        var UwaEngine = new UwaEngine(UwaPage, InvApplication);

        if (ApplicationAction != null)
          ApplicationAction(InvApplication);

        UwaEngine.Start();

#if DEBUG
        //throw new Exception("I'm a fail");
#endif
      }
      catch (Exception Exception)
      {
        var Grid = new Windows.UI.Xaml.Controls.Grid();
        UwaPage.Content = Grid;
        Grid.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Stretch;
        Grid.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Stretch;

        var Scroll = new Windows.UI.Xaml.Controls.ScrollViewer();
        Grid.Children.Add(Scroll);
        Scroll.Margin = new Windows.UI.Xaml.Thickness(0, 100, 0, 0);
        Scroll.VerticalScrollBarVisibility = Windows.UI.Xaml.Controls.ScrollBarVisibility.Auto;
        Scroll.HorizontalScrollBarVisibility = Windows.UI.Xaml.Controls.ScrollBarVisibility.Auto;

        var TextBox = new Windows.UI.Xaml.Controls.TextBox();
        Scroll.Content = TextBox;
        TextBox.IsReadOnly = true;
        TextBox.TextWrapping = Windows.UI.Xaml.TextWrapping.Wrap;

        string MessageVersion;
        try
        {
          var PackageVersion = Windows.ApplicationModel.Package.Current.Id.Version;
          MessageVersion = (PackageVersion.Major + "." + PackageVersion.Minor + "." + PackageVersion.Build + "." + PackageVersion.Revision);
        }
        catch
        {
          MessageVersion = "unknown";
        }

        var Message = MessageVersion + Environment.NewLine + Environment.NewLine + Exception.AsReport();
        TextBox.Text = Message;

        var Button = new Windows.UI.Xaml.Controls.Button();
        Grid.Children.Add(Button);
        Button.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Center;
        Button.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Top;
        Button.Margin = new Windows.UI.Xaml.Thickness(0, 30, 0, 0);
        Button.Content = "Send this start up fault to the author";
        Button.Click += (Sender, Event) => MailTo("hodgskin.callan@gmail.com", "Start Up Fault", Message);
      }
    }

    internal static void MailTo(string To, string Subject, string Body)
    {
      var MailUri = new Uri(string.Format("mailto:{0}?subject={1}&body={2}", To, Uri.EscapeDataString(Subject), Uri.EscapeDataString(Body)));

      // TODO: attachments not supported by mailto protocol.

      var MailTask = Windows.System.Launcher.LaunchUriAsync(MailUri).AsTask();
      MailTask.Wait();
    }
  }

  internal sealed class UwaPlatform : Inv.Platform
  {
    public UwaPlatform(UwaEngine Engine)
    {
      this.Engine = Engine;
      this.Vault = new UwaVault();
      this.LocalFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
    }

    int Platform.ThreadAffinity()
    {
      return Environment.CurrentManagedThreadId;
    }
    string Platform.CalendarTimeZoneName()
    {
      return TimeZoneInfo.Local.DisplayName;
    }
    void Platform.CalendarShowPicker(CalendarPicker CalendarPicker)
    {
      Engine.ShowCalendarPicker(CalendarPicker);
    }
    bool Platform.EmailSendMessage(EmailMessage EmailMessage)
    {
      // TODO: Windows.ApplicationModel.Email.EmailMessage in Windows 10 only?
      //var em = new Windows.ApplicationModel.Email.EmailMessage() ;   
      //em.To.Add(new EmailRecipient("yourname@yourdomain.com"));    
      //em.Subject = "Your Subject...";    
      //em.Body = "Your email body...";    
      //em.Attachments.Add(new EmailAttachment(...); 
      //EmailManager.ShowComposeNewEmailAsync(em).AsTask().Wait();

      UwaShell.MailTo(EmailMessage.GetTos().Select(T => T.Address).AsSeparatedText(","), EmailMessage.Subject ?? "", EmailMessage.Body ?? "");

      return true;
    }
    bool Platform.PhoneIsSupported
    {
      get { return false; }
    }
    void Platform.PhoneDial(string PhoneNumber)
    {
    }
    void Platform.PhoneSMS(string PhoneNumber)
    {
    }
    long Platform.DirectoryGetLengthFile(File File)
    {
      var StorageFolder = SelectStorageFolder(File.Folder);

      var FileTask = StorageFolder.GetBasicPropertiesAsync().AsTask();
      FileTask.Wait();

      return (long)FileTask.Result.Size;
    }
    DateTime Platform.DirectoryGetLastWriteTimeUtcFile(File File)
    {
      var StorageFolder = SelectStorageFolder(File.Folder);

      var FileTask = StorageFolder.GetFileAsync(File.Name).AsTask();
      FileTask.Wait();

      var PropertiesTask = FileTask.Result.GetBasicPropertiesAsync().AsTask();
      PropertiesTask.Wait();

      return PropertiesTask.Result.DateModified.UtcDateTime;
    }
    void Platform.DirectorySetLastWriteTimeUtcFile(File File, DateTime Timestamp)
    {
      var StorageFolder = SelectStorageFolder(File.Folder);

      var FileTask = StorageFolder.GetFileAsync(File.Name).AsTask();
      FileTask.Wait();

      /*
      var PropertiesTask = FileTask.Result.Properties.SavePropertiesAsync(new List<System.Collections.Generic.KeyValuePair<string, object>> 
      { 
        //new System.Collections.Generic.KeyValuePair<string, object>("{B725F130-47EF-101A-A5F1-02608C9EEBAC}", Timestamp.ToString("dd/MM/yyyy hh:mm:ss tt +00:00")) 
        new System.Collections.Generic.KeyValuePair<string, object>("{B725F130-47EF-101A-A5F1-02608C9EEBAC}", new DateTimeOffset(Timestamp, TimeSpan.Zero)) 
      }).AsTask();
      PropertiesTask.Wait();
      */

      // TODO: WinRT equivalent of System.IO.File.SetLastWriteTimeUtc(SelectFilePath(File), Timestamp);
    }
    System.IO.Stream Platform.DirectoryCreateFile(File File)
    {
      var StorageFolder = SelectStorageFolder(File.Folder);

      var WriteTask = System.IO.WindowsRuntimeStorageExtensions.OpenStreamForWriteAsync(StorageFolder, File.Name, Windows.Storage.CreationCollisionOption.ReplaceExisting);
      WriteTask.Wait();

      return WriteTask.Result;
    }
    System.IO.Stream Platform.DirectoryAppendFile(File File)
    {
      var StorageFolder = SelectStorageFolder(File.Folder);

      var WriteTask = System.IO.WindowsRuntimeStorageExtensions.OpenStreamForWriteAsync(StorageFolder, File.Name, Windows.Storage.CreationCollisionOption.OpenIfExists);
      WriteTask.Wait();

      WriteTask.Result.Seek(0, System.IO.SeekOrigin.End);

      return WriteTask.Result;
    }
    System.IO.Stream Platform.DirectoryOpenFile(File File)
    {
      var StorageFolder = SelectStorageFolder(File.Folder);

      var ReadTask = System.IO.WindowsRuntimeStorageExtensions.OpenStreamForReadAsync(StorageFolder, File.Name); 
      ReadTask.Wait();

      return ReadTask.Result;
    }
    bool Platform.DirectoryExistsFile(File File)
    {
      var StorageFolder = SelectStorageFolder(File.Folder);

      var FileTask = StorageFolder.TryGetItemAsync(File.Name).AsTask();
      FileTask.Wait();

      return FileTask.Result != null;
    }
    void Platform.DirectoryDeleteFile(File File)
    {
      var StorageFolder = SelectStorageFolder(File.Folder);

      var FileTask = StorageFolder.GetFileAsync(File.Name).AsTask();
      FileTask.Wait();

      var DeleteTask = FileTask.Result.DeleteAsync().AsTask();
      DeleteTask.Wait();
    }
    void Platform.DirectoryCopyFile(File SourceFile, File TargetFile)
    {
      var SourceFolder = SelectStorageFolder(SourceFile.Folder);
      var TargetFolder = SelectStorageFolder(TargetFile.Folder);

      var SourceFileTask = SourceFolder.GetFileAsync(SourceFile.Name).AsTask();
      SourceFileTask.Wait();

      var CopyTask = SourceFileTask.Result.CopyAsync(TargetFolder, TargetFile.Name).AsTask();
      CopyTask.Wait();
    }
    void Platform.DirectoryMoveFile(File SourceFile, File TargetFile)
    {
      var SourceFolder = SelectStorageFolder(SourceFile.Folder);
      var TargetFolder = SelectStorageFolder(TargetFile.Folder);

      var SourceFileTask = SourceFolder.GetFileAsync(SourceFile.Name).AsTask();
      SourceFileTask.Wait();

      var MoveTask = SourceFileTask.Result.MoveAsync(TargetFolder, TargetFile.Name).AsTask();
      MoveTask.Wait();
    }
    IEnumerable<File> Platform.DirectoryGetFolderFiles(Folder Folder, string FileMask)
    {
      var StorageFolder = SelectStorageFolder(Folder);

      var FilesTask = StorageFolder.GetFilesAsync(Windows.Storage.Search.CommonFileQuery.OrderByName).AsTask();
      FilesTask.Wait();

      var Regex = new System.Text.RegularExpressions.Regex("^" + System.Text.RegularExpressions.Regex.Escape(FileMask).Replace(@"\*", ".*").Replace(@"\?", ".") + "$");

      foreach (var StorageFile in FilesTask.Result)
      {
        if (Regex.IsMatch(StorageFile.Name))
          yield return new File(Folder, StorageFile.Name);
      }
    }
    System.IO.Stream Platform.DirectoryOpenAsset(Asset Asset)
    {
      var InstallFolder = Windows.ApplicationModel.Package.Current.InstalledLocation;

      var FileTask = InstallFolder.GetFileAsync(System.IO.Path.Combine("Assets", Asset.Name)).AsTask();
      FileTask.Wait();

      var StreamTask = FileTask.Result.OpenStreamForReadAsync();
      StreamTask.Wait();

      return StreamTask.Result;
    }
    bool Platform.DirectoryExistsAsset(Asset Asset)
    {
      var InstallFolder = Windows.ApplicationModel.Package.Current.InstalledLocation;

      var FileTask = InstallFolder.TryGetItemAsync(System.IO.Path.Combine("Assets", Asset.Name)).AsTask();
      FileTask.Wait();

      return FileTask.Result != null;
    }
    void Platform.DirectoryShowFilePicker(DirectoryFilePicker FilePicker)
    {
      var FilePick = new Windows.Storage.Pickers.FileOpenPicker();
      FilePick.ViewMode = Windows.Storage.Pickers.PickerViewMode.Thumbnail;

      switch (FilePicker.Type)
      {
        case FileType.Image:
          FilePick.SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.PicturesLibrary;
          FilePick.FileTypeFilter.Add(".bmp");
          FilePick.FileTypeFilter.Add(".cmp");
          FilePick.FileTypeFilter.Add(".jpg");
          FilePick.FileTypeFilter.Add(".png");
          FilePick.FileTypeFilter.Add(".tif");
          break;

        default:
          throw new Exception("FileType not handled: " + FilePicker.Type);
      }

      // NOTE: can't be blocking here because it will break the Windows FileDialog.
      //       the good news is this dialog is old-style modal and prevents input to the window anyway.
      var FileTask = FilePick.PickSingleFileAsync().AsTask();
      FileTask.ContinueWith(Task =>
      {
        var Result = Task.Result;

        if (Result == null)
        {
          Engine.Post(() => FilePicker.CancelInvoke());
        }
        else
        {
          var ReadTask = Result.OpenReadAsync().AsTask();
          ReadTask.Wait();

          using (var FileStream = ReadTask.Result.AsStreamForRead())
          using (var MemoryStream = new MemoryStream())
          {
            FileStream.CopyTo(MemoryStream);

            MemoryStream.Flush();

            Engine.Post(() => FilePicker.SelectInvoke(new Inv.Binary(MemoryStream.ToArray(), System.IO.Path.GetExtension(Result.Name))));            
          }
        }
      });
    }
    bool Platform.LocationIsSupported
    {
      get { return false; }
    }
    void Platform.LocationLookup(LocationLookup LocationLookup)
    {
      // TODO: implement windows 8 app location services.
      LocationLookup.SetPlacemarks(new Placemark[] { });
    }
    void Platform.AudioPlaySound(Sound Sound, float Volume, float Rate)
    {
      UwaSoundPlayer.Play(Sound, Volume, Rate, false);
    }
    void Platform.AudioPlayClip(AudioClip Clip)
    {
      Clip.Node = UwaSoundPlayer.Play(Clip.Sound, Clip.Volume, Clip.Rate, Clip.Loop);
    }
    void Platform.AudioStopClip(AudioClip Clip)
    {
      var UwaMediaElement = Clip.Node as Windows.UI.Xaml.Controls.MediaElement;

      if (UwaMediaElement != null)
      {
        Clip.Node = null;
        UwaMediaElement.Stop();
      }
    }
    void Platform.WindowBrowse(Inv.File File)
    {
      var StorageFolder = SelectStorageFolder(File.Folder);

      var StorageFileTask = StorageFolder.GetFileAsync(File.Name).AsTask();
      StorageFileTask.Wait();

      var LaunchFileTask = Windows.System.Launcher.LaunchFileAsync(StorageFileTask.Result).AsTask();
      LaunchFileTask.Wait();
    }
    void Platform.WindowPost(Action Action)
    {
      Engine.Post(() => Action());
    }
    void Platform.WindowCall(Action Action)
    {
      Engine.Call(() => Action());
    }
    long Platform.ProcessMemoryUsedBytes()
    {
      return GC.GetTotalMemory(false);
    }
    void Platform.ProcessMemoryReclamation()
    {
      Engine.Reclamation();
    }
    void Platform.WebClientConnect(WebClient WebClient)
    {
      var TcpClient = new Inv.Tcp.Client(WebClient.Host, WebClient.Port, WebClient.CertHash);
      TcpClient.Connect();
      
      WebClient.Node = TcpClient;
      WebClient.SetStreams(TcpClient.InputStream, TcpClient.OutputStream);
    }
    void Platform.WebClientDisconnect(WebClient WebClient)
    {
      var TcpClient = (Inv.Tcp.Client)WebClient.Node;
      if (TcpClient != null)
      {
        TcpClient.Disconnect();

        WebClient.Node = null;
        WebClient.SetStreams(null, null);
      }
    }
    void Platform.WebServerConnect(WebServer WebServer)
    {
      var TcpServer = new Inv.Tcp.Server(WebServer.Host, WebServer.Port, WebServer.CertHash);
      TcpServer.AcceptEvent += (TcpChannel) => WebServer.AcceptChannel(TcpChannel, TcpChannel.InputStream, TcpChannel.OutputStream);
      TcpServer.RejectEvent += (TcpChannel) => WebServer.RejectChannel(TcpChannel);

      WebServer.Node = TcpServer;
      WebServer.DropDelegate = (Node) => ((Inv.Tcp.Channel)Node).Drop();

      TcpServer.Connect();
    }
    void Platform.WebServerDisconnect(WebServer WebServer)
    {
      var TcpServer = (Inv.Tcp.Server)WebServer.Node;
      if (TcpServer != null)
      {
        TcpServer.Disconnect();

        WebServer.Node = null;
        WebServer.DropDelegate = null;
      }
    }
    void Platform.WebLaunchUri(Uri Uri)
    {
      Windows.System.Launcher.LaunchUriAsync(Uri).AsTask().Wait();
    }
    void Platform.MarketBrowse(string AppleiTunesID, string GooglePlayID, string WindowsStoreID)
    {
      Windows.System.Launcher.LaunchUriAsync(new Uri("ms-windows-store:REVIEW?PFN=" + WindowsStoreID)).AsTask().Wait();
    }
    void Platform.VaultLoadSecret(Secret Secret)
    {
      Vault.Load(Secret);
    }
    void Platform.VaultSaveSecret(Secret Secret)
    {
      Vault.Save(Secret);
    }
    void Platform.VaultDeleteSecret(Secret Secret)
    {
      Vault.Delete(Secret);
    }

    private Windows.Storage.StorageFolder SelectStorageFolder(Folder Folder)
    {
      if (Folder.Name == null)
        return LocalFolder;

      var FolderTask = LocalFolder.TryGetItemAsync(Folder.Name).AsTask();
      FolderTask.Wait();

      if (FolderTask.Result != null)
        return FolderTask.Result as Windows.Storage.StorageFolder;

      var CreateTask = LocalFolder.CreateFolderAsync(Folder.Name).AsTask();
      CreateTask.Wait();
      return CreateTask.Result;
    }

    private UwaEngine Engine;
    private UwaVault Vault;
    private Windows.Storage.StorageFolder LocalFolder;
  }

  internal static class UwaSoundPlayer
  {
    static UwaSoundPlayer()
    {
      SoundList = new Inv.DistinctList<Inv.Sound>(1024);
    }

    public static Windows.UI.Xaml.Controls.MediaElement Play(Inv.Sound InvSound, float VolumeScale, float RateScale, bool Looped)
    {
      if (InvSound == null)
        return null;

      Windows.UI.Xaml.Controls.MediaElement MediaElement;

      if (VolumeScale == 1.0F && RateScale == 1.0F && !Looped)
      {
        MediaElement = InvSound.Node as Windows.UI.Xaml.Controls.MediaElement;

        if (MediaElement == null)
        {
          MediaElement = ProduceMediaElement(InvSound, VolumeScale, RateScale, Looped);

          if (InvSound.Node == null || !SoundList.Contains(InvSound))
            SoundList.Add(InvSound);

          InvSound.Node = MediaElement;
        }

        // can only overlap the same sound effect by having two media elements.
        if (MediaElement.CurrentState == Windows.UI.Xaml.Media.MediaElementState.Playing)
          MediaElement = ProduceMediaElement(InvSound, VolumeScale, RateScale, Looped);
      }
      else
      {
        MediaElement = ProduceMediaElement(InvSound, VolumeScale, RateScale, Looped);
      }

      MediaElement.Play();

      return MediaElement;
    }
    public static void Reclamation()
    {
      foreach (var Sound in SoundList)
        Sound.Node = null;
      SoundList.Clear();
    }

    private static Windows.UI.Xaml.Controls.MediaElement ProduceMediaElement(Inv.Sound Sound, float Volume, float Rate, bool Looped)
    {
      var Result = new Windows.UI.Xaml.Controls.MediaElement();

      var MemoryStream = new Windows.Storage.Streams.InMemoryRandomAccessStream();

      using (var DataWriter = new Windows.Storage.Streams.DataWriter(MemoryStream))
      {
        // Write the bytes to the stream
        DataWriter.WriteBytes(Sound.GetBuffer());

        // Store the bytes to the MemoryStream
        DataWriter.StoreAsync().AsTask().Wait();

        // Not necessary, but do it anyway
        DataWriter.FlushAsync().AsTask().Wait();

        // Detach from the Memory stream so we don't close it
        DataWriter.DetachStream();
      }

      Result.Volume = Volume;
      Result.PlaybackRate = Rate;
      Result.IsLooping = Looped;

      Result.SetSource(MemoryStream, "mp3");

      Result.MediaEnded += (Sender, Event) => Result.Stop();

      return Result;
    }

    private static Inv.DistinctList<Inv.Sound> SoundList;
  }

  internal sealed class UwaEngine
  {
    public UwaEngine(Windows.UI.Xaml.Controls.Page UwaPage, Inv.Application Application)
    {
      this.UwaPage = UwaPage;
      this.InvApplication = Application;

      this.UwaMaster = new Windows.UI.Xaml.Controls.Grid();
      UwaPage.Content = UwaMaster;

      this.ImageList = new DistinctList<Inv.Image>();
      this.MediaBrushDictionary = new Dictionary<Colour, Windows.UI.Xaml.Media.Brush>();
      this.MediaImageDictionary = new Dictionary<Inv.Image, Windows.UI.Xaml.Media.Imaging.BitmapSource>();

      #region Key mapping.
      this.KeyDictionary = new Dictionary<Windows.System.VirtualKey, Inv.Key>()
      {
        { Windows.System.VirtualKey.Number0, Key.n0 },
        { Windows.System.VirtualKey.Number1, Key.n1 },
        { Windows.System.VirtualKey.Number2, Key.n2 },
        { Windows.System.VirtualKey.Number3, Key.n3 },
        { Windows.System.VirtualKey.Number4, Key.n4 },
        { Windows.System.VirtualKey.Number5, Key.n5 },
        { Windows.System.VirtualKey.Number6, Key.n6 },
        { Windows.System.VirtualKey.Number7, Key.n7 },
        { Windows.System.VirtualKey.Number8, Key.n8 },
        { Windows.System.VirtualKey.Number9, Key.n9 },
        { Windows.System.VirtualKey.A, Key.A },
        { Windows.System.VirtualKey.B, Key.B },
        { Windows.System.VirtualKey.C, Key.C },
        { Windows.System.VirtualKey.D, Key.D },
        { Windows.System.VirtualKey.E, Key.E },
        { Windows.System.VirtualKey.F, Key.F },
        { Windows.System.VirtualKey.G, Key.G },
        { Windows.System.VirtualKey.H, Key.H },
        { Windows.System.VirtualKey.I, Key.I },
        { Windows.System.VirtualKey.J, Key.J },
        { Windows.System.VirtualKey.K, Key.K },
        { Windows.System.VirtualKey.L, Key.L },
        { Windows.System.VirtualKey.M, Key.M },
        { Windows.System.VirtualKey.N, Key.N },
        { Windows.System.VirtualKey.O, Key.O },
        { Windows.System.VirtualKey.P, Key.P },
        { Windows.System.VirtualKey.Q, Key.Q },
        { Windows.System.VirtualKey.R, Key.R },
        { Windows.System.VirtualKey.S, Key.S },
        { Windows.System.VirtualKey.T, Key.T },
        { Windows.System.VirtualKey.U, Key.U },
        { Windows.System.VirtualKey.V, Key.V },
        { Windows.System.VirtualKey.W, Key.W },
        { Windows.System.VirtualKey.X, Key.X },
        { Windows.System.VirtualKey.Y, Key.Y },
        { Windows.System.VirtualKey.Z, Key.Z },
        { Windows.System.VirtualKey.F1, Key.F1 },
        { Windows.System.VirtualKey.F2, Key.F2 },
        { Windows.System.VirtualKey.F3, Key.F3 },
        { Windows.System.VirtualKey.F4, Key.F4 },
        { Windows.System.VirtualKey.F5, Key.F5 },
        { Windows.System.VirtualKey.F6, Key.F6 },
        { Windows.System.VirtualKey.F7, Key.F7 },
        { Windows.System.VirtualKey.F8, Key.F8 },
        { Windows.System.VirtualKey.F9, Key.F9 },
        { Windows.System.VirtualKey.F10, Key.F10 },
        { Windows.System.VirtualKey.F11, Key.F11 },
        { Windows.System.VirtualKey.F12, Key.F12 },
        { Windows.System.VirtualKey.Escape, Key.Escape },
        { Windows.System.VirtualKey.Enter, Key.Enter },
        { Windows.System.VirtualKey.Tab, Key.Tab },
        { Windows.System.VirtualKey.Space, Key.Space },
        { Windows.System.VirtualKey.Decimal, Key.Period },
        { (Windows.System.VirtualKey)188, Key.Comma },
        { (Windows.System.VirtualKey)192, Key.BackQuote }, 
        { Windows.System.VirtualKey.Add, Key.Plus },
        { Windows.System.VirtualKey.Subtract, Key.Minus },
        { Windows.System.VirtualKey.Up, Key.Up },
        { Windows.System.VirtualKey.Down, Key.Down },
        { Windows.System.VirtualKey.Left, Key.Left },
        { Windows.System.VirtualKey.Right, Key.Right },
        { Windows.System.VirtualKey.Home, Key.Home },
        { Windows.System.VirtualKey.End, Key.End },
        { Windows.System.VirtualKey.PageUp, Key.PageUp },
        { Windows.System.VirtualKey.PageDown, Key.PageDown },
        { Windows.System.VirtualKey.Clear, Key.Clear },
        { Windows.System.VirtualKey.Insert, Key.Insert },
        { Windows.System.VirtualKey.Delete, Key.Delete },
        { Windows.System.VirtualKey.Divide, Key.Slash },
        { Windows.System.VirtualKey.Multiply, Key.Asterisk },
        { Windows.System.VirtualKey.Back, Key.Backspace },
        { Windows.System.VirtualKey.LeftShift, Key.LeftShift },
        { Windows.System.VirtualKey.RightShift, Key.RightShift },
        { Windows.System.VirtualKey.LeftControl, Key.LeftCtrl },
        { Windows.System.VirtualKey.RightControl, Key.RightCtrl },
        { Windows.System.VirtualKey.LeftMenu, Key.LeftAlt },
        { Windows.System.VirtualKey.RightMenu, Key.RightAlt },
        { (Windows.System.VirtualKey)190, Key.Period },
        { (Windows.System.VirtualKey)191, Key.Slash },
        { (Windows.System.VirtualKey)220, Key.Backslash },
        //{ (Windows.System.VirtualKey)255,  }, // Happens when F10 key is spammed?
      };
      #endregion

      this.RouteArray = new Inv.EnumArray<Inv.PanelType, Func<Inv.Panel, Windows.UI.Xaml.FrameworkElement>>()
      {
        { Inv.PanelType.Browser, TranslateBrowser },
        { Inv.PanelType.Button, TranslateButton },
        { Inv.PanelType.Board, TranslateBoard },
        { Inv.PanelType.Dock, TranslateDock },
        { Inv.PanelType.Edit, TranslateEdit },
        { Inv.PanelType.Flow, TranslateFlow },
        { Inv.PanelType.Graphic, TranslateGraphic },
        { Inv.PanelType.Label, TranslateLabel },
        { Inv.PanelType.Memo, TranslateMemo },
        { Inv.PanelType.Overlay, TranslateOverlay },
        { Inv.PanelType.Canvas, TranslateCanvas },
        { Inv.PanelType.Scroll, TranslateScroll },
        { Inv.PanelType.Frame, TranslateFrame },
        { Inv.PanelType.Stack, TranslateStack },
        { Inv.PanelType.Table, TranslateTable },
      };

      InvApplication.SetPlatform(new UwaPlatform(this));

      Resize();

      // TODO: unique hardware identifier: Windows.System.Profile.HardwareIdentification.GetPackageSpecificToken(null);

      InvApplication.Directory.Installation = Windows.ApplicationModel.Package.Current.Id.Name;

      InvApplication.Device.Name = ""; // TODO: is this hanging? UwaSystem.GetMachineName();
      InvApplication.Device.Model = ""; // TODO: is this hanging? UwaSystem.GetDeviceModel();
      InvApplication.Device.System = "";// TODO: this is hanging? UwaSystem.GetWindowsVersion();

      InvApplication.Device.Keyboard = true; // TODO: System.Windows.Input.Keyboard;
      InvApplication.Device.Mouse = true; // TODO: System.Windows.Input.Mouse;
      InvApplication.Device.Touch = UwaSystem.HasTouchInput();
      InvApplication.Device.ProportionalFontName = "Segoe UI";
      InvApplication.Device.MonospacedFontName = "Consolas";

      InvApplication.Process.Id = 0; // TODO: how do you get process id for WinRT app?
    }

    private void VisibilityChanged(Windows.UI.Core.CoreWindow Sender, Windows.UI.Core.VisibilityChangedEventArgs Event)
    {
      if (!Event.Visible)
      {
        Debug.WriteLine("Window hidden.");
        // TODO: this can be used to trap 'shutdown' but will also be fired on minimise and alt+tab.
        //InvApplication.StopInvoke();
      }
    }
    private void KeyDown(Windows.UI.Core.CoreWindow Sender, Windows.UI.Core.KeyEventArgs Event)
    {
      var Modifier = GetModifier();
      InvApplication.Window.CheckModifier(Modifier);

      // ignore keystrokes while transitioning.
      if (UwaMaster.Children.Count == 1)
        KeyPress(Event.VirtualKey, Modifier);

      Event.Handled = true; 
    }
    private void KeyUp(Windows.UI.Core.CoreWindow Sender, Windows.UI.Core.KeyEventArgs Event)
    {
      var Modifier = GetModifier();
      InvApplication.Window.CheckModifier(Modifier);

      Event.Handled = true;
    }
    private void AcceleratorKeyActivated(Windows.UI.Core.CoreDispatcher Sender, Windows.UI.Core.AcceleratorKeyEventArgs Event)
    {
      var Modifier = GetModifier();
      InvApplication.Window.CheckModifier(Modifier);

      var EventType = Event.EventType;
      var VirtualKey = Event.VirtualKey;
      var KeyStatus = Event.KeyStatus;

      var IsSystemKey = (EventType == Windows.UI.Core.CoreAcceleratorKeyEventType.KeyDown || EventType == Windows.UI.Core.CoreAcceleratorKeyEventType.SystemKeyDown) && 
        (VirtualKey == Windows.System.VirtualKey.Tab || VirtualKey == Windows.System.VirtualKey.Menu || VirtualKey == Windows.System.VirtualKey.LeftMenu || VirtualKey == Windows.System.VirtualKey.RightMenu);

      var IsFunctionKey = !KeyStatus.IsMenuKeyDown && VirtualKey >= Windows.System.VirtualKey.F1 && VirtualKey <= Windows.System.VirtualKey.F12 && EventType == Windows.UI.Core.CoreAcceleratorKeyEventType.SystemKeyDown;

      if (IsSystemKey || IsFunctionKey)
      {
        // ignore keystrokes while transitioning.
        if (UwaMaster.Children.Count == 1)
          KeyPress(VirtualKey, Modifier);

        Event.Handled = true;
      }
      else if (KeyStatus.IsMenuKeyDown && EventType == Windows.UI.Core.CoreAcceleratorKeyEventType.SystemKeyDown && VirtualKey >= Windows.System.VirtualKey.F4)
      {
        // we can prevent the closing of the app from Alt+F4?
        Event.Handled = !InvApplication.ExitQueryInvoke();
      }

      Debug.WriteLine(VirtualKey);
    }
    private void KeyPress(Windows.System.VirtualKey VirtualKey, Inv.KeyModifier Modifier)
    {
      var InvSurface = InvApplication.Window.ActiveSurface;

      if (InvSurface != null)
      {
        var InvKey = TranslateKey(VirtualKey);
        if (InvKey != null)
        {
          var Keystroke = new Keystroke()
          {
            Key = InvKey.Value,
            Modifier = Modifier
          };
          InvSurface.KeystrokeInvoke(Keystroke);

          Process();
        }
      }
    }

    public void Start()
    {
      InvApplication.StartInvoke();

      Process();

      Windows.ApplicationModel.Core.CoreApplication.UnhandledErrorDetected += UnhandledErrorDetected;
      Windows.ApplicationModel.Core.CoreApplication.Exiting += Exiting;
      Windows.ApplicationModel.Core.CoreApplication.Suspending += Suspending;
      Windows.ApplicationModel.Core.CoreApplication.Resuming += Resuming;

      Windows.UI.Xaml.Media.CompositionTarget.Rendering += Rendering;

      this.CoreWindow = Windows.UI.Core.CoreWindow.GetForCurrentThread();
      CoreWindow.KeyDown += KeyDown;
      CoreWindow.KeyUp += KeyUp;
      CoreWindow.Closed += Closed;
      CoreWindow.Dispatcher.AcceleratorKeyActivated += AcceleratorKeyActivated;
      CoreWindow.SizeChanged += SizeChanged;
      CoreWindow.PointerPressed += PointerPressed;
      CoreWindow.PointerReleased += PointerReleased;
      CoreWindow.PointerMoved += PointerMoved;
      CoreWindow.VisibilityChanged += VisibilityChanged;
    }

    public void Stop()
    {
      // NOTE: InvApplication.StopInvoke() is called by Exiting.

      if (CoreWindow != null)
      {
        CoreWindow.Dispatcher.AcceleratorKeyActivated -= AcceleratorKeyActivated;
        CoreWindow.Closed -= Closed;
        CoreWindow.KeyDown -= KeyDown;
        CoreWindow.SizeChanged -= SizeChanged;
        CoreWindow.PointerPressed -= PointerPressed;
        CoreWindow.PointerReleased -= PointerReleased;
        CoreWindow.PointerMoved -= PointerMoved;
        CoreWindow.VisibilityChanged -= VisibilityChanged;
      }

      Windows.UI.Xaml.Media.CompositionTarget.Rendering -= Rendering;

      Windows.ApplicationModel.Core.CoreApplication.Suspending -= Suspending;
      Windows.ApplicationModel.Core.CoreApplication.Resuming -= Resuming;
      Windows.ApplicationModel.Core.CoreApplication.Exiting -= Exiting;
      Windows.ApplicationModel.Core.CoreApplication.UnhandledErrorDetected -= UnhandledErrorDetected;
    }

    internal Windows.UI.Core.CoreWindow CoreWindow { get; private set; }

    internal KeyModifier GetModifier()
    {
      return new KeyModifier()
      {
        IsLeftShift = (CoreWindow.GetKeyState(Windows.System.VirtualKey.LeftShift) & Windows.UI.Core.CoreVirtualKeyStates.Down) == Windows.UI.Core.CoreVirtualKeyStates.Down,
        IsRightShift = (CoreWindow.GetKeyState(Windows.System.VirtualKey.RightShift) & Windows.UI.Core.CoreVirtualKeyStates.Down) == Windows.UI.Core.CoreVirtualKeyStates.Down,
        IsLeftAlt = (CoreWindow.GetKeyState(Windows.System.VirtualKey.LeftMenu) & Windows.UI.Core.CoreVirtualKeyStates.Down) == Windows.UI.Core.CoreVirtualKeyStates.Down,
        IsRightAlt = (CoreWindow.GetKeyState(Windows.System.VirtualKey.RightMenu) & Windows.UI.Core.CoreVirtualKeyStates.Down) == Windows.UI.Core.CoreVirtualKeyStates.Down,
        IsLeftCtrl = (CoreWindow.GetKeyState(Windows.System.VirtualKey.LeftControl) & Windows.UI.Core.CoreVirtualKeyStates.Down) == Windows.UI.Core.CoreVirtualKeyStates.Down,
        IsRightCtrl = (CoreWindow.GetKeyState(Windows.System.VirtualKey.RightControl) & Windows.UI.Core.CoreVirtualKeyStates.Down) == Windows.UI.Core.CoreVirtualKeyStates.Down
      };
    }
    internal void Reclamation()
    {
      foreach (var Image in ImageList)
      {
        var UwaImage = Image.Node as Microsoft.Graphics.Canvas.CanvasBitmap;

        if (UwaImage != null)
          UwaImage.Dispose();

        Image.Node = null;
      }
      ImageList.Clear();

      MediaImageDictionary.Clear();
      MediaBrushDictionary.Clear();

      UwaSoundPlayer.Reclamation();
    }
    internal void ShowCalendarPicker(CalendarPicker CalendarPicker)
    {
      // TODO: download WinRT Xaml Toolkit? install from nuget or from https://github.com/xyzzer/WinRTXamlToolkit

      throw new NotImplementedException("CalendarPicker is not yet implemented.");
    }
    internal void Post(Action Action)
    {
      CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () => Action()).AsTask().Forget();
    }
    internal void Call(Action Action)
    {
      CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () => Action()).AsTask().Wait();
    }
    internal Microsoft.Graphics.Canvas.Brushes.ICanvasBrush TranslateCanvasBrush(Microsoft.Graphics.Canvas.CanvasDrawingSession DS, Inv.Colour InvColour)
    {
      if (InvColour == null)
      {
        return null;
      }
      else if (InvColour.Node == null)
      {
        var Result = new Microsoft.Graphics.Canvas.Brushes.CanvasSolidColorBrush(DS, TranslateMediaColour(InvColour));

        InvColour.Node = Result;

        return Result;
      }
      else
      {
        return (Microsoft.Graphics.Canvas.Brushes.CanvasSolidColorBrush)InvColour.Node;
      }
    }
    internal Windows.UI.Color TranslateMediaColour(Inv.Colour Colour)
    {
      var ArgbArray = BitConverter.GetBytes(Colour.RawValue);

      return Windows.UI.Color.FromArgb(ArgbArray[3], ArgbArray[2], ArgbArray[1], ArgbArray[0]);
    }
    internal Windows.UI.Text.FontWeight TranslateFontWeight(FontWeight InvFontWeight)
    {
      switch (InvFontWeight)
      {
        case FontWeight.Thin:
          return Windows.UI.Text.FontWeights.Thin;

        case FontWeight.Light:
          return Windows.UI.Text.FontWeights.Light;

        case FontWeight.Regular:
          return Windows.UI.Text.FontWeights.Normal;

        case FontWeight.Medium:
          return Windows.UI.Text.FontWeights.Medium;

        case FontWeight.Bold:
          return Windows.UI.Text.FontWeights.Bold;

        case FontWeight.Heavy:
          return Windows.UI.Text.FontWeights.Black;

        default:
          throw new Exception("FontWeight not handled: " + InvFontWeight);
      }
    }
    internal Microsoft.Graphics.Canvas.CanvasBitmap TranslateCanvasBitmap(Microsoft.Graphics.Canvas.CanvasDrawingSession DS, Inv.Image InvImage)
    {
      if (InvImage == null)
      {
        return null;
      }
      else 
      {
        var Result = InvImage.Node as Microsoft.Graphics.Canvas.CanvasBitmap;

        if (Result == null)
        {
          var ImageTask = LoadCanvasBitmap(DS, InvImage);
          ImageTask.Wait();

          Result = ImageTask.Result;

		      if (InvImage.Node == null || !ImageList.Contains(InvImage))
            ImageList.Add(InvImage);
		  
          InvImage.Node = Result;
        }

        return Result;
      }
    }
    internal void Guard(Action Action)
    {
      try
      {
        Action();
      }
      catch (Exception Exception)
      {
        InvApplication.HandleExceptionInvoke(Exception);
      }
    }

    private void Resize()
    {
      var Display = Windows.Graphics.Display.DisplayInformation.GetForCurrentView();

      var ResolutionWidthPixels = (int)Windows.UI.Xaml.Window.Current.Bounds.Width;
      var ResolutionHeightPixels = (int)Windows.UI.Xaml.Window.Current.Bounds.Height;

      InvApplication.Window.Width = ResolutionWidthPixels;
      InvApplication.Window.Height = ResolutionHeightPixels;
    }
    private void Process()
    {
      if (InvApplication.IsExit)
      {
        Windows.ApplicationModel.Core.CoreApplication.Exit();
        Stop();
      }
      else
      {
        var InvWindow = InvApplication.Window;

        InvWindow.ProcessInvoke();

        if (InvWindow.ActiveTimerSet.Count > 0)
        {
          foreach (var InvTimer in InvWindow.ActiveTimerSet)
          {
            var UwaTimer = AccessTimer(InvTimer, S =>
            {
              var Result = new Windows.UI.Xaml.DispatcherTimer();
              Result.Tick += (Sender, Event) => InvTimer.IntervalInvoke();
              return Result;
            });

            if (InvTimer.IsRestarting)
            {
              InvTimer.IsRestarting = false;
              UwaTimer.Stop();
            }

            if (UwaTimer.Interval != InvTimer.IntervalTime)
              UwaTimer.Interval = InvTimer.IntervalTime;

            if (InvTimer.IsEnabled && !UwaTimer.IsEnabled)
              UwaTimer.Start();
            else if (!InvTimer.IsEnabled && UwaTimer.IsEnabled)
              UwaTimer.Stop();
          }

          InvWindow.ActiveTimerSet.RemoveWhere(T => !T.IsEnabled);
        }

        var InvSurface = InvWindow.ActiveSurface;

        if (InvSurface != null)
        {
          var UwaSurface = AccessSurface(InvSurface, S =>
          {
            var Result = new UwaSurface();
            return Result;
          });

          if (!UwaMaster.Children.Contains(UwaSurface))
            InvSurface.ArrangeInvoke();

          ProcessTransition(UwaSurface);

          InvSurface.ComposeInvoke();

          UpdateSurface(InvSurface, UwaSurface);

          // NOTE: setting focus doesn't completely work on startup (but if you alt+tab away and then back to the window, it will have the focus in the expected control).
          if (InvSurface.Focus != null && UwaSurface.IsLoaded)
          {
            var UwaOverrideFocus = InvSurface.Focus.Node as UwaOverrideFocusContract;

            if (UwaOverrideFocus != null)
            {
              UwaOverrideFocus.OverrideFocus();

              // NOTE: these don't seem to overcome the startup issue described above.
              //Task.Factory.StartNew(() => UwaSurface.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Low, () => UwaOverrideFocus.OverrideFocus()));
              //Asynchronise(() => UwaOverrideFocus.OverrideFocus());
            }
            else
            {
              var UwaControl = InvSurface.Focus.Node as Windows.UI.Xaml.Controls.Control;

              if (UwaControl != null)
                UwaControl.Focus(Windows.UI.Xaml.FocusState.Programmatic);
            }

            InvSurface.Focus = null;
          }
        }
        else
        {
          UwaPage.Content = null;
        }

        if (InvWindow.Render())
        {
          //UwaWindow.Title = InvApplication.Title ?? "";

          //UwaMaster.Background = TranslateBrush(InvWindow.Background.Colour);
        }

        if (InvSurface != null)
          ProcessAnimation(InvSurface);

        InvWindow.DisplayRate.Calculate();
      }
    }
    private void UpdateSurface(Surface InvSurface, UwaSurface UwaSurface)
    {
      if (InvSurface.Render())
      {
        UwaSurface.Border.Background = TranslateMediaBrush(InvSurface.Background.Colour);
        UwaSurface.Border.Child = TranslatePanel(InvSurface.Content);
      }

      InvSurface.ProcessChanges(P => TranslatePanel(P));
    }
    private void ProcessTransition(UwaSurface UwaSurface)
    {
      var InvWindow = InvApplication.Window;

      var InvTransition = InvWindow.ActiveTransition;
      if (InvTransition == null)
      {
        Debug.Assert(UwaMaster.Children.Contains(UwaSurface));
      }
      else if (UwaMaster.Children.Count <= 1) // don't transition while animating a previous transition.
      {
        var UwaFromSurface = UwaMaster.Children.Count == 0 ? null : (UwaSurface)UwaMaster.Children[0];
        var UwaToSurface = UwaSurface;

        if (UwaFromSurface == UwaToSurface)
        {
          Debug.WriteLine("Transition to self");
        }
        else
        {
          if (InvTransition.FromSurface != null && UwaFromSurface != null)
            UpdateSurface(InvTransition.FromSurface, UwaFromSurface);

          switch (InvTransition.Animation)
          {
            case TransitionAnimation.None:
              UwaMaster.Children.Clear();
              UwaMaster.Children.Add(UwaToSurface);
              break;

            case TransitionAnimation.Fade:
              UwaMaster.Children.Add(UwaToSurface);

              UwaToSurface.IsHitTestVisible = false;
              UwaToSurface.Opacity = 0.0;

              var UwaFadeDuration = new Windows.UI.Xaml.Duration(TimeSpan.FromMilliseconds(InvTransition.Duration.TotalMilliseconds / 2));

              var UwaFadeOutStoryboard = new Windows.UI.Xaml.Media.Animation.Storyboard();
              var UwaFadeInStoryboard = new Windows.UI.Xaml.Media.Animation.Storyboard();

              var UwaFadeInAnimation = new Windows.UI.Xaml.Media.Animation.DoubleAnimation()
              {
                FillBehavior = Windows.UI.Xaml.Media.Animation.FillBehavior.HoldEnd,
                From = 0.0,
                To = 1.0,
                Duration = UwaFadeDuration
              };
              Windows.UI.Xaml.Media.Animation.Storyboard.SetTarget(UwaFadeInAnimation, UwaToSurface);
              Windows.UI.Xaml.Media.Animation.Storyboard.SetTargetProperty(UwaFadeInAnimation, "Opacity");
              UwaFadeInStoryboard.Children.Add(UwaFadeInAnimation);
              UwaFadeInAnimation.Completed += (Sender, Event) =>
              {
                UwaToSurface.IsHitTestVisible = true;
                UwaToSurface.Opacity = 1.0;
              };

              if (UwaFromSurface == null)
              {
                UwaFadeInStoryboard.Begin();
              }
              else
              {
                UwaFromSurface.IsHitTestVisible = false;

                var UwaFadeOutAnimation = new Windows.UI.Xaml.Media.Animation.DoubleAnimation()
                {
                  FillBehavior = Windows.UI.Xaml.Media.Animation.FillBehavior.HoldEnd,
                  From = 1.0,
                  To = 0.0,
                  Duration = UwaFadeDuration                  
                };
                Windows.UI.Xaml.Media.Animation.Storyboard.SetTarget(UwaFadeOutAnimation, UwaFromSurface);
                Windows.UI.Xaml.Media.Animation.Storyboard.SetTargetProperty(UwaFadeOutAnimation, "Opacity");
                UwaFadeOutStoryboard.Children.Add(UwaFadeOutAnimation);
                UwaFadeOutAnimation.Completed += (Sender, Event) =>
                {
                  UwaFromSurface.IsHitTestVisible = true;
                  UwaFromSurface.Opacity = 1.0;
                  UwaMaster.Children.Remove(UwaFromSurface);

                  UwaFadeInStoryboard.Begin();
                };

                UwaFadeOutStoryboard.Begin();
              }
              break;

            case TransitionAnimation.CarouselBack:
            case TransitionAnimation.CarouselNext:
              var CarouselForward = InvTransition.Animation == TransitionAnimation.CarouselNext;

              var UwaCarouselDuration = new Windows.UI.Xaml.Duration(InvTransition.Duration);

              var UwaCarouselStoryboard = new Windows.UI.Xaml.Media.Animation.Storyboard();

              if (UwaFromSurface != null)
              {
                var FromCarouselTransform = new Windows.UI.Xaml.Media.TranslateTransform() { X = 0, Y = 0 };
                UwaFromSurface.RenderTransform = FromCarouselTransform;
                UwaFromSurface.IsHitTestVisible = false;

                var UwaFromAnimation = new Windows.UI.Xaml.Media.Animation.DoubleAnimation()
                {
                  //AccelerationRatio = 0.5,
                  //DecelerationRatio = 0.5,
                  Duration = UwaCarouselDuration,
                  From = 0,
                  To = CarouselForward ? -UwaMaster.ActualWidth : UwaMaster.ActualWidth
                };
                Windows.UI.Xaml.Media.Animation.Storyboard.SetTarget(UwaFromAnimation, FromCarouselTransform);
                Windows.UI.Xaml.Media.Animation.Storyboard.SetTargetProperty(UwaFromAnimation, "X");
                UwaCarouselStoryboard.Children.Add(UwaFromAnimation);
              }

              if (UwaToSurface != null)
              {
                var ToCarouselTransform = new Windows.UI.Xaml.Media.TranslateTransform() { X = 0, Y = 0 };
                UwaToSurface.RenderTransform = ToCarouselTransform;
                UwaToSurface.IsHitTestVisible = false;

                UwaMaster.Children.Add(UwaToSurface);

                var UwaToAnimation = new Windows.UI.Xaml.Media.Animation.DoubleAnimation()
                {
                  //AccelerationRatio = 0.5,
                  //DecelerationRatio = 0.5,
                  Duration = UwaCarouselDuration,
                  From = CarouselForward ? UwaMaster.ActualWidth : -UwaMaster.ActualWidth,
                  To = 0
                };
                Windows.UI.Xaml.Media.Animation.Storyboard.SetTarget(UwaToAnimation, ToCarouselTransform);
                Windows.UI.Xaml.Media.Animation.Storyboard.SetTargetProperty(UwaToAnimation, "X");
                UwaCarouselStoryboard.Children.Add(UwaToAnimation);
              }

              UwaCarouselStoryboard.Completed += (Sender, Event) =>
              {
                if (UwaFromSurface != null)
                {
                  UwaFromSurface.IsHitTestVisible = true;
                  UwaMaster.Children.Remove(UwaFromSurface);
                  UwaFromSurface.RenderTransform = null;
                }

                if (UwaToSurface != null)
                {
                  UwaToSurface.IsHitTestVisible = true;
                  UwaToSurface.RenderTransform = null;
                }
              };

              UwaCarouselStoryboard.Begin();
              break;

            default:
              throw new Exception("TransitionAnimation not handled: " + InvTransition.Animation);
          }
        }

        InvWindow.ActiveTransition = null;
      }
    }
    private void ProcessAnimation(Inv.Surface InvSurfaceActive)
    {
      if (InvSurfaceActive.StopAnimationSet.Count > 0)
      {
        foreach (var InvAnimation in InvSurfaceActive.StopAnimationSet)
        {
          if (InvAnimation.Node != null)
          {
            var WpfStoryboard = (Windows.UI.Xaml.Media.Animation.Storyboard)InvAnimation.Node;

            foreach (var InvTarget in InvAnimation.GetTargets())
            {
              var WpfPanel = TranslatePanel(InvTarget.Panel);
              if (WpfPanel != null)
              {
                WpfPanel.Opacity = (float)WpfPanel.Opacity;

                InvTarget.Panel.Opacity.BypassSet((float)WpfPanel.Opacity);
              }
            }

            WpfStoryboard.Stop();
          }
        }
        InvSurfaceActive.StopAnimationSet.Clear();
      }
      else if (InvSurfaceActive.StartAnimationSet.Count > 0)
      {
        foreach (var InvAnimation in InvSurfaceActive.StartAnimationSet)
        {
          var WpfStoryboard = new Windows.UI.Xaml.Media.Animation.Storyboard();
          InvAnimation.Node = WpfStoryboard;
          WpfStoryboard.Completed += (Sender, Event) =>
          {
            InvAnimation.Complete();
            InvAnimation.Node = null;
          };

          foreach (var InvTarget in InvAnimation.GetTargets())
          {
            var InvPanel = InvTarget.Panel;
            var WpfPanel = TranslatePanel(InvPanel);
            foreach (var InvCommand in InvTarget.GetCommands())
              WpfStoryboard.Children.Add(TranslateAnimationCommand(InvPanel, WpfPanel, InvCommand));
          }

          WpfStoryboard.Begin();
        }
        InvSurfaceActive.StartAnimationSet.Clear();
      }
    }
    private Windows.UI.Xaml.Media.Animation.Timeline TranslateAnimationCommand(Inv.Panel InvPanel, Windows.UI.Xaml.FrameworkElement WpfPanel, AnimationCommand InvCommand)
    {
      // TODO: more types of animation commands.
      var Result = TranslateAnimationOpacityCommand(InvPanel, WpfPanel, InvCommand);
      Result.Completed += (Sender, Event) => InvCommand.Complete();
      return Result;
    }
    private Windows.UI.Xaml.Media.Animation.Timeline TranslateAnimationOpacityCommand(Inv.Panel InvPanel, Windows.UI.Xaml.FrameworkElement WpfPanel, AnimationCommand InvCommand)
    {
      var InvOpacityCommand = (Inv.AnimationOpacityCommand)InvCommand;

      var Result = new Windows.UI.Xaml.Media.Animation.DoubleAnimation()
      {
        From = InvOpacityCommand.From,
        To = InvOpacityCommand.To,
        Duration = InvOpacityCommand.Duration,
        FillBehavior = Windows.UI.Xaml.Media.Animation.FillBehavior.HoldEnd
      };

      // NOTE: if you set BeginTime to null, the animation will not run!
      if (InvOpacityCommand.Offset != null)
        Result.BeginTime = InvOpacityCommand.Offset;

      Windows.UI.Xaml.Media.Animation.Storyboard.SetTarget(Result, WpfPanel);
      Windows.UI.Xaml.Media.Animation.Storyboard.SetTargetProperty(Result, "Opacity");

      WpfPanel.Opacity = InvOpacityCommand.From;
      InvPanel.Opacity.BypassSet(InvOpacityCommand.To);

      Result.Completed += (Sender, Event) =>
      {
        WpfPanel.Opacity = InvPanel.Opacity.Get();
      };

      return Result;
    }
    private Windows.UI.Xaml.FrameworkElement TranslateBoard(Inv.Panel InvPanel)
    {
      var InvBoard = (Inv.Board)InvPanel;

      var UwaBoard = AccessPanel(InvBoard, P =>
      {
        return new UwaBoard();
      });

      RenderPanel(InvBoard, UwaBoard, () =>
      {
        TranslateLayout(InvBoard, UwaBoard.Border, UwaBoard.Border);

        if (InvBoard.PinCollection.Render())
        {
          UwaBoard.Canvas.Children.Clear();
          foreach (var InvPin in InvBoard.PinCollection)
          {
            var UwaElement = new Windows.UI.Xaml.Controls.Border();
            UwaBoard.Canvas.Children.Add(UwaElement);
            Windows.UI.Xaml.Controls.Canvas.SetLeft(UwaElement, InvPin.Rect.Left);
            Windows.UI.Xaml.Controls.Canvas.SetTop(UwaElement, InvPin.Rect.Top);
            UwaElement.Width = InvPin.Rect.Width;
            UwaElement.Height = InvPin.Rect.Height;

            var UwaPanel = TranslatePanel(InvPin.Panel);
            UwaElement.Child = UwaPanel;
          }
        }
      });

      return UwaBoard;
    }
    private Windows.UI.Xaml.FrameworkElement TranslateBrowser(Inv.Panel InvPanel)
    {
      var InvBrowser = (Inv.Browser)InvPanel;

      var UwaBrowser = AccessPanel(InvBrowser, P =>
      {
        var Result = new UwaBrowser();
        return Result;
      });

      RenderPanel(InvBrowser, UwaBrowser, () =>
      {
        TranslateLayout(InvBrowser, UwaBrowser, UwaBrowser.Border);

        var Navigate = InvBrowser.UriSingleton.Render();
        if (InvBrowser.HtmlSingleton.Render())
          Navigate = true;

        if (Navigate)
          UwaBrowser.Navigate(InvBrowser.UriSingleton.Data, InvBrowser.HtmlSingleton.Data);
      });

      return UwaBrowser;
    }
    private Windows.UI.Xaml.FrameworkElement TranslateButton(Inv.Panel InvPanel)
    {
      var InvButton = (Inv.Button)InvPanel;

      var UwaButton = AccessPanel(InvButton, P =>
      {
        var Result = new UwaButton();
        Result.PointerEntered += (Sender, Event) =>
        {
          if (InvApplication.Window.IsActiveSurface(P.Surface) && (ExclusiveButton == null || ExclusiveButton == Result))
            Result.Border.Background = TranslateMediaBrush(P.Background.Colour.Lighten(0.25F));
        };
        Result.PointerExited += (Sender, Event) =>
        {
          if (InvApplication.Window.IsActiveSurface(P.Surface) && (ExclusiveButton == null || ExclusiveButton == Result))
            Result.Border.Background = TranslateMediaBrush(P.Background.Colour);
        };
        Result.PointerCanceled += (Sender, Event) =>
        {
          ReleaseButtonPointer(P, Result);
        };
        Result.PointerCaptureLost += (Sender, Event) =>
        {
          ReleaseButtonPointer(P, Result);
        };
/*#if DEBUG
        Result.PointerPressed += (Sender, Event) =>
        {
          // NOTE: .PointerPressed += doesn't fire for buttons?
          if (Debugger.IsAttached)
            Debugger.Break();
        };
        Result.PointerReleased += (Sender, Event) =>
        {
          // NOTE: .PointerReleased += doesn't fire for buttons?
          if (Debugger.IsAttached)
            Debugger.Break();
        };
#endif*/
        Result.AddHandler(Windows.UI.Xaml.Controls.Primitives.ButtonBase.PointerPressedEvent, new Windows.UI.Xaml.Input.PointerEventHandler((S, E) =>
        {
          if (ExclusiveButton == null)
          {
            this.ExclusiveButton = Result;

            if (InvApplication.Window.IsActiveSurface(P.Surface))
            {
              Result.Border.Background = TranslateMediaBrush(P.Background.Colour.Darken(0.25F));

              P.PressInvoke();
            }
          }
        }), true);
        Result.AddHandler(Windows.UI.Xaml.Controls.Primitives.ButtonBase.PointerReleasedEvent, new Windows.UI.Xaml.Input.PointerEventHandler((S, E) =>
        {
          ReleaseButtonPointer(P, Result);
        }), true);

        Result.Tapped += (Sender, Event) =>
        {
          if (InvApplication.Window.IsActiveSurface(P.Surface) && ActivatedButton == Result)
          {
            this.ActivatedButton = null;
            P.SingleTapInvoke();
          }

          Event.Handled = true;
        };
        Result.RightTapped += (Sender, Event) =>
        {
          if (InvApplication.Window.IsActiveSurface(P.Surface) && ActivatedButton == Result)
          {
            this.ActivatedButton = null;
            P.ContextTapInvoke();
          }

          Event.Handled = true;
        };
        return Result;
      });

      RenderPanel(InvButton, UwaButton, () =>
      {
        TranslateLayout(InvButton, UwaButton, UwaButton.Border);

        UwaButton.IsEnabled = InvButton.IsEnabled;
        //UwaButton.Button.Focusable = InvButton.IsFocused;

        if (InvButton.ContentSingleton.Render())
        {
          UwaButton.Border.Child = null; // detach previous content in case it has moved.
          UwaButton.Border.Child = TranslatePanel(InvButton.ContentSingleton.Data);
        }
      });

      return UwaButton;
    }

    private Windows.UI.Xaml.FrameworkElement TranslateDock(Inv.Panel InvPanel)
    {
      var InvDock = (Inv.Dock)InvPanel;

      var IsHorizontal = InvDock.Orientation == DockOrientation.Horizontal;

      var UwaDock = AccessPanel(InvDock, P =>
      {
        var Result = new UwaDock();
        return Result;
      });

      RenderPanel(InvDock, UwaDock, () =>
      {
        TranslateLayout(InvDock, UwaDock.Border, UwaDock.Border);

        if (InvDock.CollectionRender())
        {
          UwaDock.Grid.Children.Clear();
          UwaDock.Grid.RowDefinitions.Clear();
          UwaDock.Grid.ColumnDefinitions.Clear();

          var Position = 0;

          foreach (var InvHeader in InvDock.HeaderCollection)
          {
            var UwaPanel = TranslatePanel(InvHeader);
            UwaDock.AddChild(UwaPanel);

            if (IsHorizontal)
            {
              UwaDock.Grid.ColumnDefinitions.Add(new Windows.UI.Xaml.Controls.ColumnDefinition() { Width = Windows.UI.Xaml.GridLength.Auto });
              Windows.UI.Xaml.Controls.Grid.SetColumn(UwaPanel, Position);
            }
            else
            {
              UwaDock.Grid.RowDefinitions.Add(new Windows.UI.Xaml.Controls.RowDefinition() { Height = Windows.UI.Xaml.GridLength.Auto });
              Windows.UI.Xaml.Controls.Grid.SetRow(UwaPanel, Position);
            }

            Position++;
          }

          if (InvDock.HasClients())
          {
            foreach (var InvClient in InvDock.ClientCollection)
            {
              var UwaPanel = TranslatePanel(InvClient);
              UwaDock.AddChild(UwaPanel);

              if (IsHorizontal)
              {
                UwaDock.Grid.ColumnDefinitions.Add(new Windows.UI.Xaml.Controls.ColumnDefinition() { Width = new Windows.UI.Xaml.GridLength(1.0, Windows.UI.Xaml.GridUnitType.Star) });
                Windows.UI.Xaml.Controls.Grid.SetColumn(UwaPanel, Position);
              }
              else
              {
                UwaDock.Grid.RowDefinitions.Add(new Windows.UI.Xaml.Controls.RowDefinition() { Height = new Windows.UI.Xaml.GridLength(1.0, Windows.UI.Xaml.GridUnitType.Star) });
                Windows.UI.Xaml.Controls.Grid.SetRow(UwaPanel, Position);
              }

              Position++;
            }
          }
          else
          {
            if (IsHorizontal)
              UwaDock.Grid.ColumnDefinitions.Add(new Windows.UI.Xaml.Controls.ColumnDefinition() { Width = new Windows.UI.Xaml.GridLength(1.0, Windows.UI.Xaml.GridUnitType.Star) });
            else
              UwaDock.Grid.RowDefinitions.Add(new Windows.UI.Xaml.Controls.RowDefinition() { Height = new Windows.UI.Xaml.GridLength(1.0, Windows.UI.Xaml.GridUnitType.Star) });

            Position++;
          }

          foreach (var InvFooter in InvDock.FooterCollection)
          {
            var UwaPanel = TranslatePanel(InvFooter);
            UwaDock.AddChild(UwaPanel);

            if (IsHorizontal)
            {
              UwaDock.Grid.ColumnDefinitions.Add(new Windows.UI.Xaml.Controls.ColumnDefinition() { Width = Windows.UI.Xaml.GridLength.Auto });
              Windows.UI.Xaml.Controls.Grid.SetColumn(UwaPanel, Position);
            }
            else
            {
              UwaDock.Grid.RowDefinitions.Add(new Windows.UI.Xaml.Controls.RowDefinition() { Height = Windows.UI.Xaml.GridLength.Auto });
              Windows.UI.Xaml.Controls.Grid.SetRow(UwaPanel, Position);
            }

            Position++;
          }
        }
      });

      return UwaDock;
    }
    private Windows.UI.Xaml.FrameworkElement TranslateEdit(Inv.Panel InvPanel)
    {
      var InvEdit = (Inv.Edit)InvPanel;

      var IsPassword = InvEdit.Input == EditInput.Password;

      var UwaEdit = AccessPanel(InvEdit, P =>
      {
        var Result = new UwaEdit(IsPassword);

        // TODO: Uwa allowed input.
        switch (InvEdit.Input)
        {
          case EditInput.Decimal:
            break;

          case EditInput.Email:
            break;

          case EditInput.Integer:
            break;

          case EditInput.Name:
            break;

          case EditInput.Number:
            break;

          case EditInput.Password:
            break;

          case EditInput.Phone:
            break;

          case EditInput.Search:
            break;

          case EditInput.Text:
            break;

          case EditInput.Uri:
            break;

          default:
            throw new Exception("EditInput not handled: " + InvEdit.Input);
        }

        if (IsPassword)
        {
          Result.PasswordBox.PasswordChanged += (Sender, Event) => P.ChangeText(Result.PasswordBox.Password);
          Result.PasswordBox.KeyDown += (Sender, Event) =>
          {
            if (Event.Key == Windows.System.VirtualKey.Enter)
              P.Return();
          };
        }
        else
        {
          Result.TextBox.TextChanged += (Sender, Event) => P.ChangeText(Result.TextBox.Text);
          Result.TextBox.KeyDown += (Sender, Event) =>
          {
            if (Event.Key == Windows.System.VirtualKey.Enter)
              P.Return();
          };
        }

        return Result;
      });

      RenderPanel(InvEdit, UwaEdit, () =>
      {
        TranslateLayout(InvEdit, UwaEdit.Border, UwaEdit.Border);

        if (IsPassword)
        {
          TranslateFont(InvEdit.Font, UwaEdit.PasswordBox);

          UwaEdit.PasswordBox.IsEnabled = !InvEdit.IsReadOnly;
          UwaEdit.PasswordBox.Password = InvEdit.Text ?? "";
        }
        else
        {
          TranslateFont(InvEdit.Font, UwaEdit.TextBox);

          UwaEdit.TextBox.IsReadOnly = InvEdit.IsReadOnly;
          UwaEdit.TextBox.Text = InvEdit.Text ?? "";
        }
      });

      return UwaEdit;
    }
    private Windows.UI.Xaml.FrameworkElement TranslateFlow(Inv.Panel InvPanel)
    {
      var InvFlow = (Inv.Flow)InvPanel;

      var UwaFlow = AccessPanel(InvFlow, P =>
      {
        var Result = new UwaFlow();
        Result.SectionCountQuery = () => InvFlow.SectionList.Count;
        Result.HeaderContentQuery = (Section) => TranslatePanel(InvFlow.SectionList[Section].Header);
        Result.FooterContentQuery = (Section) => TranslatePanel(InvFlow.SectionList[Section].Footer);
        Result.ItemCountQuery = (Section) => InvFlow.SectionList[Section].ItemCount;
        Result.ItemContentQuery = (Section, Item) => TranslatePanel(InvFlow.SectionList[Section].ItemInvoke(Item));
        return Result;
      });

      RenderPanel(InvFlow, UwaFlow, () =>
      {
        TranslateLayout(InvFlow, UwaFlow, UwaFlow.Border);

        if (InvFlow.IsRefresh)
        {
          InvFlow.IsRefresh = false;

          // TODO: animate refresh.
        }

        if (InvFlow.IsReload || InvFlow.ReloadSectionList.Count > 0 || InvFlow.ReloadItemList.Count > 0)
        {
          InvFlow.IsReload = false;
          InvFlow.ReloadSectionList.Clear(); // not supported.
          InvFlow.ReloadItemList.Clear(); // not supported.

          UwaFlow.Reload();
        }

        if (InvFlow.ScrollSection != null || InvFlow.ScrollIndex != null)
        {
          if (InvFlow.ScrollSection != null && InvFlow.ScrollIndex != null)
            UwaFlow.ScrollTo(InvFlow.ScrollSection.Value, InvFlow.ScrollIndex.Value);

          InvFlow.ScrollSection = null;
          InvFlow.ScrollIndex = null;
        }
      });

      return UwaFlow;
    }
    private Windows.UI.Xaml.FrameworkElement TranslateGraphic(Inv.Panel InvPanel)
    {
      var InvGraphic = (Inv.Graphic)InvPanel;

      var UwaGraphic = AccessPanel(InvGraphic, P =>
      {
        var Result = new UwaGraphic();
        return Result;
      });

      RenderPanel(InvGraphic, UwaGraphic, () =>
      {
        var SizeChanged = InvGraphic.Size.IsChanged;

        TranslateLayout(InvGraphic, UwaGraphic.Border, UwaGraphic.Border);

        if (InvGraphic.ImageSingleton.Render() || SizeChanged)
          UwaGraphic.Image.Source = TranslateMediaImage(InvGraphic.Image);
      });

      return UwaGraphic;
    }
    private Windows.UI.Xaml.FrameworkElement TranslateLabel(Inv.Panel InvPanel)
    {
      var InvLabel = (Inv.Label)InvPanel;

      var UwaLabel = AccessPanel(InvLabel, P =>
      {
        var Result = new UwaLabel();
        return Result;
      });

      RenderPanel(InvLabel, UwaLabel.Border, () =>
      {
        TranslateLayout(InvLabel, UwaLabel.Border, UwaLabel.Border);
        TranslateFont(InvLabel.Font, UwaLabel.TextBlock);

        UwaLabel.TextBlock.TextWrapping = InvLabel.LineWrapping ? Windows.UI.Xaml.TextWrapping.Wrap : Windows.UI.Xaml.TextWrapping.NoWrap;
        UwaLabel.TextBlock.TextAlignment = InvLabel.Justification == Justification.Left ? Windows.UI.Xaml.TextAlignment.Left : InvLabel.Justification == Justification.Right ? Windows.UI.Xaml.TextAlignment.Right : Windows.UI.Xaml.TextAlignment.Center;
        UwaLabel.TextBlock.Text = InvLabel.Text ?? "";
      });

      return UwaLabel;
    }
    private Windows.UI.Xaml.FrameworkElement TranslateMemo(Inv.Panel InvPanel)
    {
      var InvMemo = (Inv.Memo)InvPanel;

      var UwaMemo = AccessPanel(InvMemo, P =>
      {
        var Result = new UwaMemo();
        Result.TextBox.TextChanged += (Sender, Event) => P.ChangeText(Result.Text);
        return Result;
      });

      RenderPanel(InvMemo, UwaMemo, () =>
      {
        var IsAligning = InvMemo.Alignment.IsChanged;

        TranslateLayout(InvMemo, UwaMemo.Border, UwaMemo.Border);

        if (IsAligning)
        {
          // NOTE: this is a workaround to get the textbox to autosize properly.

          UwaMemo.TextBox.HorizontalAlignment = UwaMemo.Border.HorizontalAlignment;
          UwaMemo.TextBox.VerticalAlignment = UwaMemo.Border.VerticalAlignment;
        }

        TranslateFont(InvMemo.Font, UwaMemo.TextBox);

        UwaMemo.TextBox.IsReadOnly = InvMemo.IsReadOnly;
        UwaMemo.Text = InvMemo.Text ?? "";
      });

      return UwaMemo;
    }
    private Windows.UI.Xaml.FrameworkElement TranslateOverlay(Inv.Panel InvPanel)
    {
      var InvOverlay = (Inv.Overlay)InvPanel;

      var UwaOverlay = AccessPanel(InvOverlay, P =>
      {
        var Result = new UwaOverlay();
        return Result;
      });

      RenderPanel(InvOverlay, UwaOverlay, () =>
      {
        TranslateLayout(InvOverlay, UwaOverlay.Border, UwaOverlay.Border);

        if (InvOverlay.PanelCollection.Render())
        {
          UwaOverlay.Grid.Children.Clear();
          foreach (var InvElement in InvOverlay.GetPanels())
          {
            var UwaPanel = TranslatePanel(InvElement);
            UwaOverlay.AddChild(UwaPanel);
          }
        }
      });

      return UwaOverlay;
    }
    private Windows.UI.Xaml.FrameworkElement TranslateCanvas(Inv.Panel InvPanel)
    {
      var InvCanvas = (Inv.Canvas)InvPanel;

      var UwaCanvas = AccessPanel(InvCanvas, P =>
      {
        var Result = new UwaCanvas(this);

        var IsLeftPressed = false;
        var PressedTimestamp = DateTime.Now;

        Result.ManipulationMode = Windows.UI.Xaml.Input.ManipulationModes.Scale;
        Result.PointerMoved += (Sender, Event) =>
        {
          if (IsLeftPressed)
          {
            var MovedPoint = Event.GetCurrentPoint(Result);
            P.MoveInvoke(TranslatePoint(MovedPoint.Position));
          }
        };
        Result.PointerPressed += (Sender, Event) =>
        {
          Result.CapturePointer(Event.Pointer);

          var PressedPoint = Event.GetCurrentPoint(Result);

          if (PressedPoint.PointerDevice.PointerDeviceType != Windows.Devices.Input.PointerDeviceType.Mouse || PressedPoint.Properties.IsLeftButtonPressed)
          {
            IsLeftPressed = true;

            PressedTimestamp = DateTime.Now;

            P.PressInvoke(TranslatePoint(PressedPoint.Position));
          }
        };
        Result.PointerReleased += (Sender, Event) =>
        {
          Result.ReleasePointerCapture(Event.Pointer);

          var ReleasedPoint = Event.GetCurrentPoint(Result);
          var ReleasePoint = TranslatePoint(ReleasedPoint.Position);

          if (IsLeftPressed)
          {
            IsLeftPressed = false;
            P.ReleaseInvoke(ReleasePoint);

            if (DateTime.Now - PressedTimestamp <= TimeSpan.FromMilliseconds(250))
              P.SingleTapInvoke(ReleasePoint);
          }
        };
        Result.RightTapped += (Sender, Event) =>
        {
          var RightTappedPoint = Event.GetPosition(Result);

          P.ContextTapInvoke(TranslatePoint(RightTappedPoint));

          IsLeftPressed = false;

          Event.Handled = true;
        };
        Result.DoubleTapped += (Sender, Event) =>
        {
          var DoubleTappedPoint = TranslatePoint(Event.GetPosition(Result));

          P.ReleaseInvoke(DoubleTappedPoint);

          P.DoubleTapInvoke(DoubleTappedPoint);

          IsLeftPressed = false;

          Event.Handled = true;
        };
        Result.PointerWheelChanged += (Sender, Event) =>
        {
          var WheelChangedPoint = Event.GetCurrentPoint(Result);

          P.ZoomInvoke(TranslatePoint(WheelChangedPoint.Position), WheelChangedPoint.Properties.MouseWheelDelta > 0 ? +1 : -1);
        };
        Result.ManipulationDelta += (Sender, Event) =>
        {
          var ZoomedPoint = TranslatePoint(Event.Position);

          P.ZoomInvoke(ZoomedPoint, Event.Delta.Scale > 1.0 ? +1 : -1);
        };
        Result.ResetEvent += () =>
        {
          Reclamation();
        };
        Result.DrawAction = () => Guard(() => InvCanvas.DrawInvoke(Result));
  
        return Result;
      });

      RenderPanel(InvCanvas, UwaCanvas, () =>
      {
        TranslateLayout(InvCanvas, UwaCanvas.Border, UwaCanvas.Border);

        if (InvCanvas.Redrawing)
          UwaCanvas.Canvas.Invalidate();
      });

      return UwaCanvas;
    }
    private Windows.UI.Xaml.FrameworkElement TranslateScroll(Inv.Panel InvPanel)
    {
      var InvScroll = (Inv.Scroll)InvPanel;

      var UwaScroll = AccessPanel(InvScroll, P =>
      {
        var Result = new UwaScroll(P.Orientation == ScrollOrientation.Vertical, P.Orientation == ScrollOrientation.Horizontal);
        return Result;
      });

      RenderPanel(InvScroll, UwaScroll, () =>
      {
        TranslateLayout(InvScroll, UwaScroll.Border, UwaScroll.Border);

        if (InvScroll.ContentSingleton.Render())
          UwaScroll.ScrollViewer.Content = TranslatePanel(InvScroll.ContentSingleton.Data);
      });

      return UwaScroll;
    }
    private Windows.UI.Xaml.FrameworkElement TranslateFrame(Inv.Panel InvPanel)
    {
      var InvFrame = (Inv.Frame)InvPanel;

      var UwaFrame = AccessPanel(InvFrame, P =>
      {
        var Result = new Windows.UI.Xaml.Controls.Border();
        return Result;
      });

      RenderPanel(InvFrame, UwaFrame, () =>
      {
        TranslateLayout(InvFrame, UwaFrame, UwaFrame);

        if (InvFrame.ContentSingleton.Render())
        {
          UwaFrame.Child = null; // detach previous content in case it has moved.
          UwaFrame.Child = TranslatePanel(InvFrame.ContentSingleton.Data);
        }
      });

      return UwaFrame;
    }
    private Windows.UI.Xaml.FrameworkElement TranslateStack(Inv.Panel InvPanel)
    {
      var InvStack = (Inv.Stack)InvPanel;

      var UwaStack = AccessPanel(InvStack, P =>
      {
        var Result = new UwaStack();
        Result.StackPanel.Orientation = P.Orientation == Inv.StackOrientation.Horizontal ? Windows.UI.Xaml.Controls.Orientation.Horizontal : Windows.UI.Xaml.Controls.Orientation.Vertical;
        return Result;
      });

      RenderPanel(InvStack, UwaStack, () =>
      {
        TranslateLayout(InvStack, UwaStack.Border, UwaStack.Border);

        if (InvStack.PanelCollection.Render())
          UwaStack.Compose(InvStack.GetPanels().Select(P => TranslatePanel(P)));
      });

      return UwaStack;
    }
    private Windows.UI.Xaml.FrameworkElement TranslateTable(Inv.Panel InvPanel)
    {
      var InvTable = (Inv.Table)InvPanel;

      var UwaTable = AccessPanel(InvTable, P =>
      {
        var Result = new UwaTable();
        return Result;
      });

      RenderPanel(InvTable, UwaTable, () =>
      {
        TranslateLayout(InvTable, UwaTable.Border, UwaTable.Border);

        if (InvTable.CollectionRender())
        {
          UwaTable.Grid.Children.Clear();
          UwaTable.Grid.RowDefinitions.Clear();
          UwaTable.Grid.ColumnDefinitions.Clear();

          foreach (var TableColumn in InvTable.ColumnCollection)
          {
            UwaTable.Grid.ColumnDefinitions.Add(new Windows.UI.Xaml.Controls.ColumnDefinition() { Width = TranslateTableLength(TableColumn, true) });

            var UwaColumn = TranslatePanel(TableColumn.Content);

            if (UwaColumn != null)
            {
              UwaTable.AddChild(UwaColumn);

              Windows.UI.Xaml.Controls.Grid.SetColumn(UwaColumn, TableColumn.Index);
              Windows.UI.Xaml.Controls.Grid.SetRow(UwaColumn, 0);
              Windows.UI.Xaml.Controls.Grid.SetRowSpan(UwaColumn, InvTable.ColumnCollection.Count);
            }
          }

          foreach (var TableRow in InvTable.RowCollection)
          {
            UwaTable.Grid.RowDefinitions.Add(new Windows.UI.Xaml.Controls.RowDefinition() { Height = TranslateTableLength(TableRow, false) });

            var UwaRow = TranslatePanel(TableRow.Content);

            if (UwaRow != null)
            {
              UwaTable.AddChild(UwaRow);

              Windows.UI.Xaml.Controls.Grid.SetRow(UwaRow, TableRow.Index);
              Windows.UI.Xaml.Controls.Grid.SetColumn(UwaRow, 0);
              Windows.UI.Xaml.Controls.Grid.SetColumnSpan(UwaRow, InvTable.ColumnCollection.Count);
            }
          }

          foreach (var TableCell in InvTable.CellCollection)
          {
            var UwaCell = TranslatePanel(TableCell.Content);

            if (UwaCell != null)
            {
              UwaTable.AddChild(UwaCell);

              Windows.UI.Xaml.Controls.Grid.SetColumn(UwaCell, TableCell.Column.Index);
              Windows.UI.Xaml.Controls.Grid.SetRow(UwaCell, TableCell.Row.Index);
            }
          }
        }
      });

      return UwaTable;
    }

    private UwaSurface AccessSurface(Inv.Surface InvSurface, Func<Inv.Surface, UwaSurface> BuildFunction)
    {
      if (InvSurface.Node == null)
      {
        var Result = BuildFunction(InvSurface);

        InvSurface.Node = Result;

        return Result;
      }
      else
      {
        return (UwaSurface)InvSurface.Node;
      }
    }
    private Windows.UI.Xaml.FrameworkElement TranslatePanel(Inv.Panel InvPanel)
    {
      if (InvPanel == null)
        return null;
      else
        return RouteArray[InvPanel.PanelType](InvPanel);
    }
    private void RenderPanel(Inv.Panel InvPanel, Windows.UI.Xaml.FrameworkElement UwaPanel, Action Action)
    {
      if (InvPanel.Render())
        Action();
    }
    private TElement AccessPanel<TPanel, TElement>(TPanel InvPanel, Func<TPanel, TElement> BuildFunction)
      where TPanel : Inv.Panel
      where TElement : Windows.UI.Xaml.FrameworkElement
    {
      if (InvPanel.Node == null)
      {
        var Result = BuildFunction(InvPanel);

        InvPanel.Node = Result;

        return Result;
      }
      else
      {
        return (TElement)InvPanel.Node;
      }
    }
    private Windows.UI.Xaml.DispatcherTimer AccessTimer(Inv.Timer InvTimer, Func<Inv.Timer, Windows.UI.Xaml.DispatcherTimer> BuildFunction)
    {
      if (InvTimer.Node == null)
      {
        var Result = BuildFunction(InvTimer);

        InvTimer.Node = Result;

        return Result;
      }
      else
      {
        return (Windows.UI.Xaml.DispatcherTimer)InvTimer.Node;
      }
    }
    private void ReleaseButtonPointer(Button InvButton, UwaButton UwaButton)
    {
      if (ExclusiveButton == UwaButton)
      {
        this.ActivatedButton = UwaButton;
        this.ExclusiveButton = null;

        if (InvApplication.Window.IsActiveSurface(InvButton.Surface))
        {
          UwaButton.Border.Background = TranslateMediaBrush(InvButton.Background.Colour);

          InvButton.ReleaseInvoke();
        }
      }
    }
    private void TranslateLayout(Inv.Panel InvPanel, Windows.UI.Xaml.FrameworkElement UwaControl, Windows.UI.Xaml.Controls.Border UwaBorder)
    {
      if (InvPanel.Background.Render())
        UwaBorder.Background = TranslateMediaBrush(InvPanel.Background.Colour);

      if (InvPanel.Corner.Render())
        UwaBorder.CornerRadius = TranslateCorner(InvPanel.Corner);

      var InvBorder = InvPanel.Border;
      if (InvBorder.Render())
      {
        UwaBorder.BorderBrush = TranslateMediaBrush(InvBorder.Colour);
        UwaBorder.BorderThickness = TranslateEdge(InvBorder);
      }

      var InvMargin = InvPanel.Margin;
      if (InvMargin.Render())
        UwaControl.Margin = TranslateEdge(InvMargin); // this has to be the layout control, otherwise the margin will on the inside of the button clickable area.

      var InvPadding = InvPanel.Padding;
      if (InvPadding.Render())
        UwaBorder.Padding = TranslateEdge(InvPadding);

      //var InvElevation = InvPanel.Elevation;
      //if (InvElevation.Render())
      //{
      //  if (InvElevation.Depth == 0)
      //  {
      //    UwaControl.Effect = null;
      //  }
      //  else
      //  {
      //    UwaControl.Effect = new System.Windows.Media.Effects.DropShadowEffect()
      //    {
      //      Direction = 315,
      //      Color = TranslateColour(Inv.Colour.DimGray),
      //      ShadowDepth = VerticalPtToPx(InvElevation.Depth),
      //      Opacity = 0.5F
      //    };
      //  }
      //}
      
      var InvOpacity = InvPanel.Opacity;
      if (InvOpacity.Render())
        UwaControl.Opacity = InvOpacity.Get();

      var InvVisibility = InvPanel.Visibility;
      if (InvVisibility.Render())
        UwaControl.Visibility = InvVisibility.Get() ? Windows.UI.Xaml.Visibility.Visible : Windows.UI.Xaml.Visibility.Collapsed;

      TranslateSize(InvPanel.Size, UwaControl);

      TranslateAlignment(InvPanel.Alignment, UwaControl);
    }
    private Windows.UI.Xaml.CornerRadius TranslateCorner(Inv.Corner InvCorner)
    {
      return new Windows.UI.Xaml.CornerRadius(InvCorner.TopLeft, InvCorner.TopRight, InvCorner.BottomRight, InvCorner.BottomLeft);
    }
    private void TranslateSize(Inv.Size InvSize, Windows.UI.Xaml.FrameworkElement UwaElement)
    {
      if (InvSize.Render())
      {
        if (InvSize.Width != null)
          UwaElement.Width = InvSize.Width.Value;
        else
          UwaElement.ClearValue(Windows.UI.Xaml.FrameworkElement.WidthProperty);

        if (InvSize.Height != null)
          UwaElement.Height = InvSize.Height.Value;
        else
          UwaElement.ClearValue(Windows.UI.Xaml.FrameworkElement.HeightProperty);

        if (InvSize.MinimumWidth != null)
          UwaElement.MinWidth = InvSize.MinimumWidth.Value;
        else
          UwaElement.ClearValue(Windows.UI.Xaml.FrameworkElement.MinWidthProperty);

        if (InvSize.MinimumHeight != null)
          UwaElement.MinHeight = InvSize.MinimumHeight.Value;
        else
          UwaElement.ClearValue(Windows.UI.Xaml.FrameworkElement.MinHeightProperty);

        if (InvSize.MaximumWidth != null)
          UwaElement.MaxWidth = InvSize.MaximumWidth.Value;
        else
          UwaElement.ClearValue(Windows.UI.Xaml.FrameworkElement.MaxWidthProperty);

        if (InvSize.MaximumHeight != null)
          UwaElement.MaxHeight = InvSize.MaximumHeight.Value;
        else
          UwaElement.ClearValue(Windows.UI.Xaml.FrameworkElement.MaxHeightProperty);
      }
    }
    private void TranslateAlignment(Inv.Alignment InvAlignment, Windows.UI.Xaml.FrameworkElement UwaElement)
    {
      if (InvAlignment.Render())
      {
        switch (InvAlignment.Get())
        {
          case Inv.Placement.Stretch:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Stretch;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Stretch;
            break;

          case Inv.Placement.StretchLeft:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Left;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Stretch;
            break;

          case Inv.Placement.StretchCenter:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Center;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Stretch;
            break;

          case Inv.Placement.StretchRight:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Right;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Stretch;
            break;

          case Inv.Placement.TopStretch:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Stretch;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Top;
            break;

          case Inv.Placement.TopLeft:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Left;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Top;
            break;

          case Inv.Placement.TopCenter:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Center;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Top;
            break;

          case Inv.Placement.TopRight:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Right;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Top;
            break;

          case Inv.Placement.CenterStretch:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Stretch;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Center;
            break;

          case Inv.Placement.CenterLeft:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Left;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Center;
            break;

          case Inv.Placement.Center:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Center;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Center;
            break;

          case Inv.Placement.CenterRight:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Right;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Center;
            break;

          case Inv.Placement.BottomStretch:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Stretch;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Bottom;
            break;

          case Inv.Placement.BottomLeft:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Left;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Bottom;
            break;

          case Inv.Placement.BottomCenter:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Center;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Bottom;
            break;

          case Inv.Placement.BottomRight:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Right;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Bottom;
            break;

          default:
            throw new Exception("Inv.Placement not handled: " + InvAlignment.Get());
        }
      }
    }
    private Windows.UI.Xaml.Media.Brush TranslateMediaBrush(Inv.Colour InvColour)
    {
      if (InvColour == null)
        return null;

      return MediaBrushDictionary.GetOrAdd(InvColour, C =>
      {
        var Result = new Windows.UI.Xaml.Media.SolidColorBrush(TranslateMediaColour(C));
        return Result;
      });
    }
    private Windows.Foundation.Rect TranslateRect(Inv.Rect InvRect)
    {
      return new Windows.Foundation.Rect(InvRect.Left, InvRect.Top, InvRect.Width, InvRect.Height);
    }
    private Windows.Foundation.Point TranslatePoint(Inv.Point InvPoint)
    {
      return new Windows.Foundation.Point(InvPoint.X, InvPoint.Y);
    }
    private Inv.Point TranslatePoint(Windows.Foundation.Point UwaPoint)
    {
      return new Inv.Point((int)UwaPoint.X, (int)UwaPoint.Y);
    }
    private Windows.UI.Xaml.Thickness TranslateEdge(Inv.Edge InvEdge)
    {
      return new Windows.UI.Xaml.Thickness(InvEdge.Left, InvEdge.Top, InvEdge.Right, InvEdge.Bottom);
    }
    private void TranslateFont(Inv.Font InvFont, Windows.UI.Xaml.Controls.TextBlock UwaElement)
    {
      if (InvFont.Render())
      {
        if (InvFont.Name != null)
          UwaElement.FontFamily = new Windows.UI.Xaml.Media.FontFamily(InvFont.Name);
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Controls.TextBlock.FontFamilyProperty);

        if (InvFont.Size != null)
          UwaElement.FontSize = InvFont.Size.Value;
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Controls.TextBlock.FontSizeProperty);

        if (InvFont.Colour != null)
          UwaElement.Foreground = TranslateMediaBrush(InvFont.Colour);
        else
          UwaElement.Foreground = new Windows.UI.Xaml.Media.SolidColorBrush(Windows.UI.Colors.Black);

        UwaElement.FontWeight = TranslateFontWeight(InvFont.Weight);
      }
    }
    private void TranslateFont(Inv.Font InvFont, Windows.UI.Xaml.Controls.TextBox UwaElement)
    {
      if (InvFont.Render())
      {
        if (InvFont.Name != null)
          UwaElement.FontFamily = new Windows.UI.Xaml.Media.FontFamily(InvFont.Name);
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Controls.TextBox.FontFamilyProperty);

        if (InvFont.Size != null)
          UwaElement.FontSize = InvFont.Size.Value;
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Controls.TextBox.FontSizeProperty);

        var UwaBrush = TranslateMediaBrush(InvFont.Colour);

        if (UwaBrush != null)
          UwaElement.Foreground = UwaBrush;
        else
          UwaElement.Foreground = new Windows.UI.Xaml.Media.SolidColorBrush(Windows.UI.Colors.Black);

        UwaElement.FontWeight = TranslateFontWeight(InvFont.Weight);
      }
    }
    private void TranslateFont(Inv.Font InvFont, Windows.UI.Xaml.Controls.PasswordBox UwaElement)
    {
      if (InvFont.Render())
      {
        if (InvFont.Name != null)
          UwaElement.FontFamily = new Windows.UI.Xaml.Media.FontFamily(InvFont.Name);
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Controls.TextBox.FontFamilyProperty);

        if (InvFont.Size != null)
          UwaElement.FontSize = InvFont.Size.Value;
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Controls.TextBox.FontSizeProperty);

        var UwaBrush = TranslateMediaBrush(InvFont.Colour);

        if (UwaBrush != null)
          UwaElement.Foreground = UwaBrush;
        else
          UwaElement.Foreground = new Windows.UI.Xaml.Media.SolidColorBrush(Windows.UI.Colors.Black);

        UwaElement.FontWeight = TranslateFontWeight(InvFont.Weight);
      }
    }
    private void TranslateFont(Inv.Font InvFont, Windows.UI.Xaml.Controls.RichEditBox UwaElement)
    {
      if (InvFont.Render())
      {
        if (InvFont.Name != null)
          UwaElement.FontFamily = new Windows.UI.Xaml.Media.FontFamily(InvFont.Name);
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Controls.TextBox.FontFamilyProperty);

        if (InvFont.Size != null)
          UwaElement.FontSize = InvFont.Size.Value;
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Controls.TextBox.FontSizeProperty);

        var UwaBrush = TranslateMediaBrush(InvFont.Colour);

        if (UwaBrush != null)
          UwaElement.Foreground = UwaBrush;
        else
          UwaElement.Foreground = new Windows.UI.Xaml.Media.SolidColorBrush(Windows.UI.Colors.Black);

        UwaElement.FontWeight = TranslateFontWeight(InvFont.Weight);
      }
    }
    private Windows.UI.Xaml.GridLength TranslateTableLength(TableAxis InvTableLength, bool Horizontal)
    {
      switch (InvTableLength.LengthType)
      {
        case TableAxisLength.Auto:
          return Windows.UI.Xaml.GridLength.Auto;

        case TableAxisLength.Fixed:
          return new Windows.UI.Xaml.GridLength(Horizontal ? InvTableLength.LengthValue : InvTableLength.LengthValue, Windows.UI.Xaml.GridUnitType.Pixel);

        case TableAxisLength.Star:
          return new Windows.UI.Xaml.GridLength(InvTableLength.LengthValue, Windows.UI.Xaml.GridUnitType.Star);

        default:
          throw new Exception("Inv.TableLength not handled: " + InvTableLength.LengthType);
      }
    }
    private Microsoft.Graphics.Canvas.Effects.Matrix5x4 MakeColorMatrix(Windows.UI.Color color)
    {
      return new Microsoft.Graphics.Canvas.Effects.Matrix5x4()
      {
        M11 = color.R, M12 = 0,       M13 = 0,       M14 = 0,
        M21 = 0,       M22 = color.G, M23 = 0,       M24 = 0,
        M31 = 0,       M32 = 0,       M33 = color.B, M34 = 0,
        M41 = 0,       M42 = 0,       M43 = 0,       M44 = color.A,
        M51 = 0,       M52 = 0,       M53 = 0,       M54 = 0,
      };
    }
    private Inv.Key? TranslateKey(Windows.System.VirtualKey UwaKey)
    {
      Inv.Key Result;
      if (KeyDictionary.TryGetValue(UwaKey, out Result))
        return Result;
      else
        return null;
    }
    private Windows.UI.Xaml.Media.Imaging.BitmapSource TranslateMediaImage(Inv.Image InvImage)
    {
      if (InvImage == null)
      {
        return null;
      }
      else
      {
        return MediaImageDictionary.GetOrAdd(InvImage, K =>
        {
          var Task = LoadMediaImage(K);
          Task.Wait();
          return Task.Result;
        });
      }
    }
    private async Task<Windows.UI.Xaml.Media.Imaging.BitmapImage> LoadMediaImage(Inv.Image InvImage)
    {
      using (var MemoryStream = new Windows.Storage.Streams.InMemoryRandomAccessStream())
      {
        using (var DataWriter = new Windows.Storage.Streams.DataWriter(MemoryStream))
        {
          // Write the bytes to the stream
          DataWriter.WriteBytes(InvImage.GetBuffer());

          // Store the bytes to the MemoryStream
          await DataWriter.StoreAsync();

          // Not necessary, but do it anyway
          await DataWriter.FlushAsync();

          // Detach from the Memory stream so we don't close it
          DataWriter.DetachStream();
        }

        MemoryStream.Seek(0);

        var Result = new Windows.UI.Xaml.Media.Imaging.BitmapImage();

        Result.SetSource(MemoryStream);

        return Result;
      }
    }
    private async Task<Microsoft.Graphics.Canvas.CanvasBitmap> LoadCanvasBitmap(Microsoft.Graphics.Canvas.CanvasDrawingSession DS, Inv.Image InvImage)
    {
      using (var MemoryStream = new Windows.Storage.Streams.InMemoryRandomAccessStream())
      {
        using (var DataWriter = new Windows.Storage.Streams.DataWriter(MemoryStream))
        {
          // Write the bytes to the stream
          DataWriter.WriteBytes(InvImage.GetBuffer());

          // Store the bytes to the MemoryStream
          await DataWriter.StoreAsync();

          // Not necessary, but do it anyway
          await DataWriter.FlushAsync();

          // Detach from the Memory stream so we don't close it
          DataWriter.DetachStream();
        }

        MemoryStream.Seek(0);

        //return await Microsoft.Graphics.Canvas.CanvasBitmap.LoadAsync(DS, MemoryStream, 96);

        var LoadTask = Microsoft.Graphics.Canvas.CanvasBitmap.LoadAsync(DS, MemoryStream, 96).AsTask();
        LoadTask.Wait();
        return LoadTask.Result;
      }
    }
    private void Rendering(object Sender, object Event)
    {
      Process();
    }
    private void Suspending(object Sender, object Event)
    {
      InvApplication.SuspendInvoke();
    }
    private void Resuming(object Sender, object Event)
    {
      InvApplication.ResumeInvoke();
    }
    private void Exiting(object Sender, object Event)
    {
      // TODO: this is not running in the UI thread... so we have to wait for it to run before exiting -- NOTE the call to Wait() at the end.
      CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () => InvApplication.StopInvoke()).AsTask().Wait();
    }
    private void Closed(Windows.UI.Core.CoreWindow Sender, Windows.UI.Core.CoreWindowEventArgs Event)
    {
      // TODO: doesn't seem to get called.
      Debug.WriteLine("closed");
    }
    private void SizeChanged(Windows.UI.Core.CoreWindow Sender, Windows.UI.Core.WindowSizeChangedEventArgs Event)
    {
      Resize();

      var InvSurface = InvApplication.Window.ActiveSurface;
      if (InvSurface != null)
        InvSurface.ArrangeInvoke();
    }
    private void PointerPressed(Windows.UI.Core.CoreWindow Sender, Windows.UI.Core.PointerEventArgs Event)
    {
      if (Event.CurrentPoint.Properties.IsXButton1Pressed)
      {
        var InvSurface = InvApplication.Window.ActiveSurface;

        if (InvSurface != null)
          InvSurface.GestureBackwardInvoke();
      }
      else if (Event.CurrentPoint.Properties.IsXButton2Pressed)
      {
        var InvSurface = InvApplication.Window.ActiveSurface;

        if (InvSurface != null)
          InvSurface.GestureForwardInvoke();
      }
      else
      {
        var PointerPosition = Event.CurrentPoint.Position;

        LeftEdgeSwipe = PointerPosition.X <= 10;
        RightEdgeSwipe = PointerPosition.X >= UwaPage.ActualWidth - 10;
      }
    }
    private void PointerReleased(Windows.UI.Core.CoreWindow Sender, Windows.UI.Core.PointerEventArgs Event)
    {
      LeftEdgeSwipe = false;
      RightEdgeSwipe = false;
    }
    private void PointerMoved(Windows.UI.Core.CoreWindow Sender, Windows.UI.Core.PointerEventArgs Event)
    {
      if (LeftEdgeSwipe || RightEdgeSwipe)
      {
        var InvSurface = InvApplication.Window.ActiveSurface;

        if (InvSurface != null)
        {
          var PointerPosition = Event.CurrentPoint.Position;

          if (LeftEdgeSwipe && PointerPosition.X >= 0)
          {
            if (PointerPosition.X >= 20)
            {
              LeftEdgeSwipe = false;
              InvSurface.GestureBackwardInvoke();
            }
          }
          else if (RightEdgeSwipe && PointerPosition.X <= UwaPage.ActualWidth)
          {
            if (PointerPosition.X <= UwaPage.ActualWidth - 20)
            {
              RightEdgeSwipe = false;
              InvSurface.GestureForwardInvoke();
            }
          }
          else
          {
            LeftEdgeSwipe = false;
            RightEdgeSwipe = false;
          }
        }
        else
        {
          LeftEdgeSwipe = false;
          RightEdgeSwipe = false;
        }

        Event.Handled = true;
      }
    }
    private void UnhandledErrorDetected(object Sender, Windows.ApplicationModel.Core.UnhandledErrorDetectedEventArgs Event)
    {
      // Unhandled errors result in the app being terminated once this event propagates to the Windows Runtime system.
      if (!Event.UnhandledError.Handled)
      {
        try
        {
          Event.UnhandledError.Propagate();
        }
        catch (Exception Exception)
        {
          InvApplication.HandleExceptionInvoke(Exception);
        }
      }
    }

    private Application InvApplication;
    private Windows.UI.Xaml.Controls.Page UwaPage;
    private Windows.UI.Xaml.Controls.Grid UwaMaster;
    private bool LeftEdgeSwipe;
    private bool RightEdgeSwipe;
    private Inv.EnumArray<Inv.PanelType, Func<Inv.Panel, Windows.UI.Xaml.FrameworkElement>> RouteArray;
    private Dictionary<Windows.System.VirtualKey, Key> KeyDictionary;
    private Inv.DistinctList<Inv.Image> ImageList;
    private Dictionary<Inv.Image, Windows.UI.Xaml.Media.Imaging.BitmapSource> MediaImageDictionary;
    private Dictionary<Inv.Colour, Windows.UI.Xaml.Media.Brush> MediaBrushDictionary;
    private UwaButton ExclusiveButton;
    private UwaButton ActivatedButton;
  }
}