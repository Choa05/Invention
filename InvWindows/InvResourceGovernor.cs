﻿/*! 1 !*/

#if(DEBUG)
  #define RESIZE_DEBUG
#endif

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Inv.Support;
using System.Diagnostics;

namespace Inv
{
  public static class ResourceGovernor
  {
    static ResourceGovernor()
    {
      StandardImageSizeList = new Inv.DistinctList<ResourceImageSize>();

      StandardImageSize16x16 = NewStandardImageSize(16, 16);
      StandardImageSize20x20 = NewStandardImageSize(20, 20);
      StandardImageSize24x24 = NewStandardImageSize(24, 24);
      StandardImageSize32x32 = NewStandardImageSize(32, 32);
      StandardImageSize48x48 = NewStandardImageSize(48, 48);
      StandardImageSize64x64 = NewStandardImageSize(64, 64);
      StandardImageSize96x96 = NewStandardImageSize(96, 96);
      StandardImageSize128x128 = NewStandardImageSize(128, 128);
      StandardImageSize256x256 = NewStandardImageSize(256, 256);
    }

    public static ResourceImageSize StandardImageSize16x16 { get; private set; }
    public static ResourceImageSize StandardImageSize20x20 { get; private set; }
    public static ResourceImageSize StandardImageSize24x24 { get; private set; }
    public static ResourceImageSize StandardImageSize32x32 { get; private set; }
    public static ResourceImageSize StandardImageSize48x48 { get; private set; }
    public static ResourceImageSize StandardImageSize64x64 { get; private set; }
    public static ResourceImageSize StandardImageSize96x96 { get; private set; }
    public static ResourceImageSize StandardImageSize128x128 { get; private set; }
    public static ResourceImageSize StandardImageSize256x256 { get; private set; }

    public static ResourceCrate NewCrate(Assembly Assembly, string Name, string DefaultNamespace = null)
    {
      return new ResourceCrate(Assembly.ExtractResourceManager(Name, DefaultNamespace), Assembly);
    }

    public static bool HasStandardImageSize(int Width, int Height)
    {
      return StandardImageSizeList.Exists(Index => Index.Width == Width && Index.Height == Height);
    }
    public static ResourceImageSize CustomImageSize(int Width, int Height)
    {
      return new ResourceImageSize(Width, Height);
    }

    private static ResourceImageSize NewStandardImageSize(int Width, int Height)
    {
      var ImageSize = new ResourceImageSize(Width, Height);

      StandardImageSizeList.Add(ImageSize);

      return ImageSize;
    }

    private static Inv.DistinctList<ResourceImageSize> StandardImageSizeList;
  }

  public sealed class ResourceCrate
  {
    internal ResourceCrate(System.Resources.ResourceManager ResourceManager, Assembly OwningAssembly)
    {
      this.ResourceManager = ResourceManager;
      this.OwningAssembly = OwningAssembly;
      
      this.CacheImageDictionary = new ConcurrentDictionary<string, BitmapImage>();
      this.VectorImageDictionary = new ConcurrentDictionary<string, DrawingImage>();
      this.VectorPathDictionaries = new ConcurrentDictionary<string,ResourceDictionary>();
    }

    public ResourceCompartment NewCompartment()
    {
      return new ResourceCompartment(this);
    }

    internal BitmapImage AccessBitmapImage(ResourceImageConcept Concept, ResourceImageSize Size)
    {
      return CacheImageDictionary.GetOrAdd(Concept.Name + Size.Name, Handle => ResourceManager.ExtractResourceImage(Handle));
    }
    internal ImageSource AccessVectorSource(string VectorName, string VectorPath, Brush FillBrush, Pen StrokePen)
    {
      Debug.Assert(FillBrush != null, "FillBrush must be supplied.");
      Debug.Assert(StrokePen != null, "StrokePen must be supplied.");

      return VectorImageDictionary.GetOrAdd(VectorName + FillBrush + StrokePen, Handle =>
      {
        var pathDictionaryName = VectorPath.Split(new char[] { '.' }).First();
        var pathDictionary = VectorPathDictionaries.GetOrAdd(System.Windows.Threading.Dispatcher.CurrentDispatcher.GetHashCode().ToString() + VectorPath.Split(new char[] { '.' }).First(), PathDictionaryName =>
        {
          var baseName = "pack://application:,,,/" + OwningAssembly.GetName().Name + ";component/";

          ResourceDictionary dict = null;

          // Set up the list of paths to try (the path will vary depending on how you compiled the assembly) then try them
          // until one succeeds or we run out.
          var namesToTry = new string[] 
          {
            baseName + "resources/" + pathDictionaryName + ".xaml",
            baseName + pathDictionaryName + ".xaml",
            baseName + "metro/resources/" + pathDictionaryName + ".xaml"
          }.Select(x => new Uri(x, UriKind.Absolute));

          // Marshal the actual load to the UI thread
          System.Windows.Threading.Dispatcher.CurrentDispatcher.Invoke(new Action(() => 
          {
            dict = new ResourceDictionary();

            foreach (var nameUri in namesToTry)
            {
              if (dict.TrySetSource(nameUri))
                break;
            }              
          }));

          return dict;
        });

        if (!pathDictionary.Contains(VectorName))
          return null;

        var path = pathDictionary[VectorName] as System.Windows.Shapes.Path;
        if (path == null)
          return null;

        Geometry pathData = null;
        
        // Make sure the data access occurs on the thread it's supposed to.
        path.Dispatcher.Invoke(new Action(() => { pathData = (Geometry)path.Data.GetAsFrozen();  }));

        // Transform the specified path into an icon by centering it and adding a border.
        var geom = new PathGeometry();

        geom.AddGeometry(pathData);

        var largestDimension = Math.Max(pathData.Bounds.Width, pathData.Bounds.Height);
          
        var paddingFactor = 0.4;

        // Translate the path into the centre of what's going to be the final image
        geom.Transform = new TranslateTransform(-pathData.Bounds.TopLeft.X + largestDimension / (1 / paddingFactor) + Math.Abs(pathData.Bounds.Width - largestDimension) / 2.0,
                                                -pathData.Bounds.TopLeft.Y + largestDimension / (1 / paddingFactor) + Math.Abs(pathData.Bounds.Height - largestDimension) / 2.0);
        
        var di = new DrawingImage();

        var group = new GeometryGroup();

        // Double rectangle is to force the geometry to have the size we want and an empty fill (drawing something twice makes it invisible due to the odd-even rule
        // used when filling polygons)
        group.Children.Add(new RectangleGeometry(new Rect(0, 0, largestDimension * (1 + 2 * paddingFactor), largestDimension * (1 + 2 * paddingFactor))));
        group.Children.Add(new RectangleGeometry(new Rect(0, 0, largestDimension * (1 + 2 * paddingFactor), largestDimension * (1 + 2 * paddingFactor))));

        var icon = new GeometryDrawing(FillBrush, StrokePen, geom);

        var surround = new GeometryDrawing(FillBrush, new Pen() { Thickness = 1.0 }.FreezeIt(), group);

        var drawingGroup = new DrawingGroup();
        drawingGroup.Children.Add(icon);
        drawingGroup.Children.Add(surround);

        // Freeze it and return it.
        di.Drawing = drawingGroup;
        di.Freeze();

        return di;
      });
    }
    internal string AccessText(ResourceTextConcept Concept)
    {
      return ResourceManager.ExtractResourceString(Concept.Name);
    }
    internal Inv.Sound AccessSound(ResourceSoundConcept Concept)
    {
      return new Inv.Sound(ResourceManager.ExtractResourceBinary(Concept.Name), Concept.Extension);
    }
    internal Inv.Binary AccessBinary(ResourceBinaryConcept Concept)
    {
      return new Inv.Binary(ResourceManager.ExtractResourceBinary(Concept.Name), Concept.Extension);
    }

    private System.Resources.ResourceManager ResourceManager;
    private Assembly OwningAssembly;
    private ConcurrentDictionary<string, System.Windows.ResourceDictionary> VectorPathDictionaries;
    private ConcurrentDictionary<string, BitmapImage> CacheImageDictionary;
    private ConcurrentDictionary<string, DrawingImage> VectorImageDictionary;
    // TODO: text/binary caching?
  }

  public sealed class ResourceCompartment
  {
    internal ResourceCompartment(ResourceCrate Context)
    {
      this.Context = Context;
      this.ImageConceptDictionary = new Dictionary<string, ResourceImageConcept>();
    }

    public IEnumerable<ResourceImageConcept> GetImageConcepts()
    {
      return ImageConceptDictionary.Values;
    }
    public ResourceImageConcept GetImageConcept(string Name)
    {
      return ImageConceptDictionary[Name];
    }
    public ResourceImageConcept NewImageConcept(string Name, string Extension, params ResourceImageSize[] SizeArray)
    {
      var Result = new ResourceImageConcept(Context, Name, Extension, SizeArray);

      ImageConceptDictionary.Add(Name, Result);

      return Result;
    }
    public ResourceImageConcept NewImageConcept(string Name, string Extension, string VectorName, params ResourceImageSize[] SizeArray)
    {
      var Result = new ResourceImageConcept(Context, Name, Extension, VectorName, SizeArray);

      ImageConceptDictionary.Add(Name, Result);

      return Result;
    }
    public ResourceTextConcept NewTextConcept(string Name, string Extension)
    {
      return new ResourceTextConcept(Context, Name, Extension);
    }
    public ResourceBinaryConcept NewBinaryConcept(string Name, string Extension)
    {
      return new ResourceBinaryConcept(Context, Name, Extension);
    }
    public ResourceSoundConcept NewSoundConcept(string Name, string Extension)
    {
      return new ResourceSoundConcept(Context, Name, Extension);
    }

    private ResourceCrate Context;
    private Dictionary<string, ResourceImageConcept> ImageConceptDictionary;
  }

  public sealed class ResourceImageSize
  {
    internal ResourceImageSize(int Width, int Height)
    {
      this.Width = Width;
      this.Height = Height;
      this.Name = Width + "x" + Height;
    }

    public string Name { get; private set; }
    public int Width { get; private set; }
    public int Height { get; private set; }

    public override int GetHashCode()
    {
      return Width.GetHashCode() ^ Height.GetHashCode();
    }
    public override bool Equals(object Source)
    {
      var SizeSource = Source as ResourceImageSize;

      return SizeSource != null && Width == SizeSource.Width && Height == SizeSource.Height;
    }
  }

  public sealed class ResourceImageConcept : ImageConcept
  {
    internal ResourceImageConcept(ResourceCrate Context, string Name, string Extension, string VectorName, params ResourceImageSize[] SizeArray)
    {
      this.Crate = Context;
      this.Name = Name;
      this.Extension = Extension;
      this.VectorName = VectorName;

      // Order the size array by total pixel count. This will break down a little if, for some reason, adjacent images
      // have vastly different aspect ratios.
      this.SizeArray = SizeArray.OrderBy(Size => Size.Width * Size.Height).ToArray();

      this.ResizedImageCache = new ConcurrentDictionary<ResourceImageSize, ImageSource>();
    }

    internal ResourceImageConcept(ResourceCrate Context, string Name, string Extension, params ResourceImageSize[] SizeArray)
      : this(Context, Name, Extension, null, SizeArray) { }

    public string Name { get; private set; }
    public string Extension { get; private set; }
    public string VectorName { get; private set; }

    public ImageSource Access(int Width, int Height)
    {
      var RequestedSize = new ResourceImageSize(Width, Height);

      // we prefer an exact match.
      var ExactMatch = SizeArray.Where(Size => Size.Width == Width && Size.Height == Height).Select(Size => Crate.AccessBitmapImage(this, Size)).FirstOrDefault();

      return ExactMatch ?? ResizedImageCache.GetOrAdd(RequestedSize, size =>
      {
        if (this.VectorName != null)
          return Crate.AccessVectorSource(VectorName, Name, Brushes.White, new Pen() { Brush = Brushes.White, Thickness = 1.0 }.FreezeIt());

        // Grab either the first size larger than the specified size, or the largest size available.
        var CandidateSize = SizeArray.Where(Size => Size.Width >= size.Width && Size.Height >= size.Height)
                                     .Concat(SizeArray.LastOrDefault().ToEnumerable())
                                     .FirstOrDefault();
        
        if (CandidateSize != null)
        {
#if RESIZE_DEBUG
          var Timer = System.Diagnostics.Stopwatch.StartNew();
          long load;
          long resample;
#endif

          var NativeImage = Crate.AccessBitmapImage(this, CandidateSize);

#if RESIZE_DEBUG
          load = Timer.ElapsedMilliseconds;
#endif
          var Renderable = Inv.Rendering.RenderableFactory.RenderableFromImage(NativeImage);
          Renderable.ResampleIntoBoundingBox(size.Width, size.Height);

#if RESIZE_DEBUG
          resample = Timer.ElapsedMilliseconds - load;
#endif
          var ResizedImage = Renderable.Render();

          if (!ResizedImage.IsFrozen)
            ResizedImage.Freeze();

#if RESIZE_DEBUG
          var render = Timer.ElapsedMilliseconds - resample - load;
          Timer.Stop();

          // Debugging message just because. This could probably be logged so that we can add explicit replacement icons for commonly used sizes.
          //Console.WriteLine("resized {0} from {1}x{2} to {3}x{4} in {5}ms ({6}ms load, {7}ms resample, {8}ms render)", this.Name, CandidateSize.Width, CandidateSize.Height, size.Width, size.Height, Timer.ElapsedMilliseconds, load, resample, render);
#endif
          return ResizedImage;
        }

        return null;
      });
    }
    public Inv.VectorImageConcept AsVector(Brush FillBrush, Brush StrokeBrush)
    {
      return AsVector(FillBrush, StrokeBrush != null ? new Pen(StrokeBrush, 1).FreezeIt() : null);
    }
    public Inv.VectorImageConcept AsVector(Brush FillBrush = null, Pen StrokePen = null)
    {
      Debug.Assert(VectorName != null, "Resource image must contain a vector.");

      return new Inv.VectorImageConcept(Crate, VectorName, Name, FillBrush, StrokePen);
    }

    ImageSource ImageConcept.Access(int Width, int Height)
    {
      return Access(Width, Height);
    }

    private ConcurrentDictionary<ResourceImageSize, ImageSource> ResizedImageCache;
    private ResourceImageSize[] SizeArray;
    private ResourceCrate Crate;
  }

  public sealed class ResourceTextConcept
  {
    internal ResourceTextConcept(ResourceCrate Repository, string Name, string Extension)
    {
      this.Repository = Repository;
      this.Name = Name;
      this.Extension = Extension;
    }

    public string Name { get; private set; }
    public string Extension { get; private set; }

    public string Access()
    {
      return Repository.AccessText(this);
    }

    public static implicit operator string(ResourceTextConcept Concept)
    {
      return Concept != null ? Concept.Access() : null;
    }

    private ResourceCrate Repository;
  }

  public sealed class ResourceSoundConcept
  {
    internal ResourceSoundConcept(ResourceCrate Repository, string Name, string Extension)
    {
      this.Repository = Repository;
      this.Name = Name;
      this.Extension = Extension;
    }

    public string Name { get; private set; }
    public string Extension { get; private set; }

    public Inv.Sound Access()
    {
      return Repository.AccessSound(this);
    }

    public static implicit operator Inv.Sound(ResourceSoundConcept Concept)
    {
      return Concept.Access();
    }

    private ResourceCrate Repository;
  }

  public sealed class ResourceBinaryConcept
  {
    internal ResourceBinaryConcept(ResourceCrate Repository, string Name, string Extension)
    {
      this.Repository = Repository;
      this.Name = Name;
      this.Extension = Extension;
    }

    public string Name { get; private set; }
    public string Extension { get; private set; }

    public Inv.Binary Access()
    {
      return Repository.AccessBinary(this);
    }

    public static implicit operator Inv.Binary(ResourceBinaryConcept Concept)
    {
      return Concept.Access();
    }

    private ResourceCrate Repository;
  }
}
