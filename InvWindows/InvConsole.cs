﻿/*! 1 !*/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace Inv
{
  [SuppressUnmanagedCodeSecurity]
  public static class Console
  {
    public static bool IsVisible
    {
      get { return Win32.Kernel32.GetConsoleWindow() != IntPtr.Zero; }
    }
    public static int WindowHeight
    {
      get { return System.Console.WindowHeight; }
    }

    public static void Show()
    {
      if (!IsVisible)
      {
        if (!Win32.Kernel32.AttachConsole(Win32.Kernel32.ATTACH_PARENT_PROCESS))
        {
          Win32.Kernel32.AllocConsole();

          var ConsoleType = typeof(System.Console);
          var _out = ConsoleType.GetField("_out", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic);
          var _error = ConsoleType.GetField("_error", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic);
          var _InitializeStdOutError = ConsoleType.GetMethod("InitializeStdOutError", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic);

          Debug.Assert(_out != null);
          Debug.Assert(_error != null);
          Debug.Assert(_InitializeStdOutError != null);

          _out.SetValue(null, null);
          _error.SetValue(null, null);
          _InitializeStdOutError.Invoke(null, new object[] { true });
        }
      }
    }
    public static void Hide()
    {
      if (IsVisible)
      {
        System.Console.SetOut(TextWriter.Null);
        System.Console.SetError(TextWriter.Null);

        Win32.Kernel32.FreeConsole();
      }
    }
    public static void WriteLine()
    {
      System.Console.WriteLine();
    }
    public static void Write(string Field)
    {
      System.Console.Write(Field);
    }
    public static void WriteLine(string Line)
    {
      System.Console.WriteLine(Line);
    }
    public static string ReadLine()
    {
      return System.Console.ReadLine();
    }
    public static int Read()
    {
      return System.Console.Read();
    }
  }
}