﻿/*! 1 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inv
{
  /// <summary>
  /// Represents the amount of memory in the system.
  /// </summary>
  public sealed class SystemMemory
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="SystemMemory"/> class with current system memory values.
    /// </summary>
    public SystemMemory()
    {
      Refresh();
    }

    /// <summary>
    /// The total number of bytes of virtual memory in the system.
    /// </summary>
    public Inv.DataSize TotalVirtualMemory { get; private set; }

    /// <summary>
    /// The available number of bytes of virtual memory in the system.
    /// </summary>
    public Inv.DataSize AvailableVirtualMemory { get; private set; }

    /// <summary>
    /// The total number of bytes of physical memory in the system.
    /// </summary>
    public Inv.DataSize TotalPhysicalMemory { get; private set; }

    /// <summary>
    /// The available number of bytes of physical memory in the system.
    /// </summary>
    public Inv.DataSize AvailablePhysicalMemory { get; private set; }

    /// <summary>
    /// Attempt to refresh memory values from the system.
    /// </summary>
    /// <remarks>
    /// Attempts to fetch total and available memory values from Windows.
    /// If this fails, all values will be set to zero.
    /// </remarks>
    public void Refresh()
    {
      var MemoryStatus = new Win32.Kernel32.MEMORYSTATUSEX();

      if (Win32.Kernel32.GlobalMemoryStatusEx(MemoryStatus))
      {
        this.TotalVirtualMemory = Inv.DataSize.FromBytes((long)MemoryStatus.ullTotalVirtual);
        this.AvailableVirtualMemory = Inv.DataSize.FromBytes((long)MemoryStatus.ullAvailVirtual);
        this.TotalPhysicalMemory = Inv.DataSize.FromBytes((long)MemoryStatus.ullTotalPhys);
        this.AvailablePhysicalMemory = Inv.DataSize.FromBytes((long)MemoryStatus.ullAvailPhys);
      }
      else
      {
        this.TotalVirtualMemory = Inv.DataSize.Zero;
        this.AvailableVirtualMemory = Inv.DataSize.Zero;
        this.TotalPhysicalMemory = Inv.DataSize.Zero;
        this.AvailablePhysicalMemory = Inv.DataSize.Zero;
      }
    }
  }
}
