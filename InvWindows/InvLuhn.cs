﻿/*! 1 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inv
{
  public static class Luhn
  {
    public static bool IsValid(string CardNumber)
    {
      var DigitArray = CardNumber.Select(c => c - '0').ToArray();

      if (DigitArray.Any(D => D < 0 || D > 9))
        return false;

      var Index = 0;
      var LengthMod = DigitArray.Length % 2;

      var Result = DigitArray.Sum(d => Index++ % 2 == LengthMod ? results[d] : d) % 10;

      return Result == 0;
    }

    private static readonly int[] results = { 0, 2, 4, 6, 8, 1, 3, 5, 7, 9 };
  }
}
