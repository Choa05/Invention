﻿/*! 1 !*/
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inv
{
  public sealed class VisualStudioControl
  {
    public VisualStudioControl()
    {
      var ProgIDArray = new string[] { "VisualStudio.DTE.14.0", "VisualStudio.DTE.13.0", "VisualStudio.DTE.12.0", "VisualStudio.DTE.11.0", "VisualStudio.DTE.10.0" };

      using (var ClassesRootKey = RegistryKey.OpenBaseKey(RegistryHive.ClassesRoot, RegistryView.Default))
      {
        for (var Index = 0; Index < ProgIDArray.Length && VisualStudioProgID == null; Index++)
        {
          var DTEKey = ClassesRootKey.OpenSubKey(ProgIDArray[Index]);
          if (DTEKey != null)
          {
            VisualStudioProgID = ProgIDArray[Index];
            DTEKey.Close();
          }
        }
      }

      if (VisualStudioProgID != null)
      {
        var Version = VisualStudioProgID.Substring(17);

        var PathArray = new[] 
        { 
          @"C:\Program Files\Microsoft Visual Studio " + Version + @"\Common7\IDE\devenv.exe", 
          @"C:\Program Files (x86)\Microsoft Visual Studio " + Version + @"\Common7\IDE\devenv.exe"
        };

        foreach (var Path in PathArray)
        {
          if (File.Exists(Path))
          {
            this.VisualStudioPath = Path;
            break;
          }
        }
      }
    }

    public bool IsInstalled
    {
      get { return VisualStudioPath != null && VisualStudioProgID != null; }
    }

    public void OpenFile(string FilePath, long? LineNumber = null)
    {
      // NOTE: the dynamic is to remove the hard dependency which is only available when Visual Studio is installed (EnvDTE80.DTE2)
      var DTE = GetVisualStudioDTE();

      if (DTE == null)
      {
        var StartInfo = new ProcessStartInfo();

        if (VisualStudioPath != null)
        {
          StartInfo.Verb = "Open";
          StartInfo.FileName = "\"" + VisualStudioPath + "\"";
          StartInfo.Arguments = "/edit \"" + FilePath + "\"";

          if (LineNumber.HasValue)
          {
            StartInfo.Arguments += " /command \"edit.goto " + LineNumber.Value.ToString() + "\"";
            StartInfo.UseShellExecute = true;
          }
        }
        else
        {
          StartInfo.Verb = "Open";
          StartInfo.FileName = FilePath;
        }

        Process.Start(StartInfo);
      }
      else
      {
        DTE.ExecuteCommand("File.OpenFile", FilePath);
        if (LineNumber.HasValue)
          DTE.ExecuteCommand("Edit.Goto", LineNumber.Value.ToString());
        DTE.MainWindow.Activate();
      }
    }
    public void AttachDebugger(int ProcessID)
    {
      try
      {
        var DTE = GetVisualStudioDTE();

        if (DTE != null)
        {
          foreach (var LocalProcess in DTE.Debugger.LocalProcesses)
          {
            if (LocalProcess.ProcessID == ProcessID)
            {
              LocalProcess.Attach();
              return;
            }
          }
        }
      }
      catch (Exception Exception)
      {
        Debug.WriteLine(Exception.Message);

        if (Debugger.IsAttached)
          Debugger.Break();
      }

      // Otherwise prompt the user.
      Debugger.Launch();
    }

    private dynamic GetVisualStudioDTE()
    {
      try
      {
        return System.Runtime.InteropServices.Marshal.GetActiveObject(VisualStudioProgID);
      }
      catch
      {
        return null;
      }
    }

    private string VisualStudioProgID;
    private string VisualStudioPath;
  }
}
