﻿/*! 1 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Win32;

namespace Inv
{
  public sealed class Icon : Inv.ImageConcept
  {
    public Icon(string FilePath)
    {
      this.FilePath = FilePath;
    }

    public string FilePath { get; private set; }

    public System.Windows.Media.Imaging.BitmapSource AccessDefault()
    {
      if (Environment.OSVersion.Platform == PlatformID.Win32NT &&
          Environment.OSVersion.Version.Major < 6)
      {
        using (var FileIcon = System.Drawing.Icon.ExtractAssociatedIcon(FilePath))
        {
          var Result = System.Windows.Interop.Imaging.CreateBitmapSourceFromHIcon(FileIcon.Handle, System.Windows.Int32Rect.Empty, System.Windows.Media.Imaging.BitmapSizeOptions.FromEmptyOptions());

          Result.Freeze();

          return Result;
        }
      }
      else
      {
        IShellItem ShellItem;
        IntPtr HBitmap;

        Win32.Shell32.SHCreateItemFromParsingName(FilePath, IntPtr.Zero, Win32.Shell32.IID_IShellItem, out ShellItem);
        var ImageFactory = (Win32.IShellItemImageFactory)ShellItem;
        ImageFactory.GetImage(new Shell32.SIZE() { cx = 32, cy = 32 }, Win32.Shell32.SIIGBF.SIIGBF_ICONONLY | Shell32.SIIGBF.SIIGBF_BIGGERSIZEOK, out HBitmap);

        var renderable = Rendering.RenderableFactory.RenderableFromHBitmap(HBitmap);

        Gdi32.DeleteObject(HBitmap);

        return renderable.Render();
      }
    }

    System.Windows.Media.ImageSource ImageConcept.Access(int Width, int Height)
    {
      if (Environment.OSVersion.Platform == PlatformID.Win32NT &&
          Environment.OSVersion.Version.Major < 6)
      {
        using (var FileIcon = System.Drawing.Icon.ExtractAssociatedIcon(FilePath))
        {
          var Result = System.Windows.Interop.Imaging.CreateBitmapSourceFromHIcon(FileIcon.Handle, System.Windows.Int32Rect.Empty, System.Windows.Media.Imaging.BitmapSizeOptions.FromEmptyOptions());

          var renderable = Rendering.RenderableFactory.RenderableFromImage(Result);
          if (renderable.GetWidth() > Width || renderable.GetHeight() > Height)
            renderable.ResampleIntoBoundingBox(Width, Height);

          return renderable.Render();
        }
      }
      else
      {
        IShellItem ShellItem;
        IntPtr HBitmap;

        Win32.Shell32.SHCreateItemFromParsingName(FilePath, IntPtr.Zero, Win32.Shell32.IID_IShellItem, out ShellItem);
        var ImageFactory = (Win32.IShellItemImageFactory)ShellItem;
        ImageFactory.GetImage(new Shell32.SIZE() { cx = Width, cy = Height }, Shell32.SIIGBF.SIIGBF_ICONONLY | Shell32.SIIGBF.SIIGBF_BIGGERSIZEOK, out HBitmap);

        var renderable = Rendering.RenderableFactory.RenderableFromHBitmap(HBitmap);

        // Make sure it fits within the bounding box the user requested
        if (renderable.GetWidth() > Width || renderable.GetHeight() > Height)
          renderable.ResampleIntoBoundingBox(Width, Height);

        Gdi32.DeleteObject(HBitmap);

        return renderable.Render();
      }
    }
  }
}
