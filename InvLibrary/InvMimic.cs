﻿/*! 1 !*/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inv
{
  public abstract class Mimic<T>
    where T : class
  {
    protected T Base 
    {
      get
      {
        Debug.Assert(BaseField != null, "Base has not been set.");

        return BaseField;
      }
      set
      {
        Debug.Assert(BaseField == null, "Base must only be set once.");

        this.BaseField = value;
      }
    }

    public static implicit operator T(Mimic<T> Self)
    {
      return Self != null ? Self.Base : null;
    }

    private T BaseField;
  }
}
