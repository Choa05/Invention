﻿/*! 3 !*/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv.Resource
{
  public static class Foundation
  {
    static Foundation()
    {
      Governor = new Governor();
    }

    public static void Import(Type PackageType, string PackagePath)
    {
      var TypeInfo = PackageType.GetReflectionInfo();
      //if (!TypeInfo.IsClass || !TypeInfo.IsAbstract || !TypeInfo.IsSealed)
      //  throw new Exception("Package class must be a static type.");

      var Package = new Package();
      using (var PackageStream = TypeInfo.Assembly.GetManifestResourceStream(PackagePath))
      {
        if (PackageStream == null)
        {
          var ResourceArray = TypeInfo.Assembly.GetManifestResourceNames();
          throw new Exception(string.Format("Resource package '{0}' was not found in the embedded resources.\nPerhaps you meant one of these?\n{1}", PackagePath, ResourceArray.Take(20).AsSeparatedText(Environment.NewLine)));
        }

        if (PackageStream != null)
          Governor.Load(Package, PackageStream);
      }

      if (Package.DirectoryList == null)
      {
        foreach (var FieldInfo in TypeInfo.GetReflectionFields())
        {
          var FieldValue = FieldInfo.FieldType.ReflectionCreate();
          FieldInfo.SetValue(null, FieldValue);
        }
      }
      else
      {
        foreach (var Directory in Package.DirectoryList)
        {
          var FieldInfo = TypeInfo.GetReflectionFields().Find(P => P.Name == Directory.Name);

          if (FieldInfo != null)
          {
            var FieldValue = FieldInfo.FieldType.ReflectionCreate();
            FieldInfo.SetValue(null, FieldValue);

            if (Directory.FileList.Count > 0)
            {
              var DirectoryInfo = FieldInfo.FieldType.GetReflectionInfo();

              foreach (var File in Directory.FileList)
              {
                var FileInfo = DirectoryInfo.GetReflectionFields().Find(F => F.Name == File.Name);

                if (FileInfo != null)
                {
                  switch (File.Format)
                  {
                    case Format.Text:
                      FileInfo.SetValue(FieldValue, System.Text.Encoding.UTF8.GetString(File.Content.GetBuffer(), 0, (int)File.Content.GetLength()));
                      break;

                    case Format.Sound:
                      FileInfo.SetValue(FieldValue, new Inv.Sound(File.Content));
                      break;

                    case Format.Image:
                      FileInfo.SetValue(FieldValue, new Inv.Image(File.Content));
                      break;

                    case Format.Binary:
                      FileInfo.SetValue(FieldValue, new Inv.Binary(File.Content));
                      break;

                    default:
                      throw new Exception("Format not handled: " + File.Format);
                  }
                }
              }
            }
          }
        }
      }
    }

    private static Governor Governor;
  }
  
  public sealed class Governor
  {
    public Governor()
    {
      this.Base = new Inv.Persist.Governor();

      Base.Register<Package>();
      Base.Register<Directory>();
      Base.Register<File>();

#if DEBUG
      Base.Validate<Package>();
#endif
    }

    public void Save(Package Package, Stream Stream)
    {
      Base.Save(Package, Stream);
    }
    public void Load(Package Package, Stream Stream)
    {
      Base.Load(Package, Stream);
    }

    private Inv.Persist.Governor Base;
  }

  public sealed class Package
  {
    public Package() { }

    public Inv.DistinctList<Directory> DirectoryList { get; set; }
  }

  public sealed class Directory
  {
    public Directory() { }

    public string Name { get; set; }
    public Inv.DistinctList<File> FileList { get; set; }
  }

  public sealed class File
  {
    public File() { }

    public string Name { get; set; }
    public Format Format { get; set; }
    public Inv.Binary Content { get; set; }
  }

  public enum Format
  {
    Text,
    Image,
    Sound,
    Binary
  }
}
