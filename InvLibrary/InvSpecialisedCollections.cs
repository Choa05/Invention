﻿/*! 1 !*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

namespace Inv.Collections
{
  /**
   *  A b+tree with snapshot semantics for readers.
   *  
   *  Write performance is in or above the ballpark of SortedDictionary whilst providing reasonable
   *  concurrency guarantees and deadlock prevention. Probe performance is approximately 25-33% of
   *  SortedDictionary - sadly, I can't break the laws of physics. Yet.
   *  
   *  Vaguely wasteful in memory terms - each node encompasses three statically sized arrays rather than
   *  the normal approach of using regular [] arrays and new()ing them up as necessary; since large numbers
   *  of nodes are created as part of filling the tree, profiling shows we spend a substantial amount
   *  of time in GC allocations, and the more we can reduce that hit, the better.
   *  
   *  Taking an enumerator effectively creates a read-only snapshot representing writes started up to the point
   *  where the enumerator was created; the tree will not mutate internal nodes for any writes started
   *  while one or more enumerators are live. Try not to leak them - insertion and removal performance suffers
   *  by around 30% while nodes are in the internally immutable state, and GC pressure will be higher.
   *  
   *  Using structs for K is extremely advisable, and for V is merely a good idea.
   *  
   *  Approximately O(log n) complexity in time and memory usage, modulo the GC doing its thing.
   *  Accordingly, insertion time remains consistent into the hundreds of millions of KVPs.
   *  Count() is O(log n) rather than O(1) as bookkeeping information is not propagated up the tree.
   *  
   *  = Why would I use this instead of Dictionary<TKey, TValue>? =
   *  - If you want consistent, gracefully degrading I/U/D performance scaling logarithmically with item count.
   *  - If you want large to enormous numbers of items but can't spare a large contiguous block of memory 
   *    for a Dictionary's internal array.
   *  - If you want the memory usage of the data structure to closely track the number of actual items in it.
   *  - If you want concurrency but don't want the overhead of manually locking or using ConcurrentDictionary or
   *    its immutable friends (you don't - System.Collections.Immutable are generally slow as shit)
   *  - If you want the freedom to modify a collection while enumerating a snapshot of it without exceptions.
   *  
   *  = Why would I *not* use this? =
   *  - If you only have a few thousand items, it's of questionable value.
   *  - If you can guarantee single-thread, single-reader, single-writer.
   *  
   *  Usage notes:
   *  - .Set(key, value) is slightly faster than the indexer access [key] = value. Use it if you're loading
   *    large numbers of items
   *  
   *  Implementation notes:
   *  We use lock crabbing where possible to improve concurrency - this is simply manually locking one node ahead in the
   *  tree we're attempting to modify, and unlocking behind us as we traverse down the tree. This means the root node
   *  is the one with the highest contention, but Monitor.Enter/Exit seem sufficient for fairly large insertion
   *  rates.
   *  
   *  We proactively split/merge nodes during traversal to guarantee changes to leaf nodes don't propagate
   *  up the tree (ie, traversal is one-way, which is required given the nature of lock crabbing).
   *  
   *  TODO:
   *  - Add an ApproximateCount() that traverses down a few randomly chosen branches to estimate cardinality quickly.
   *    Important for sufficiently gigantic trees.
   *  - Add a bulk-load operation that ingests a sorted input and constructs the tree bottom-up.
   *  - Further optimisations - remove is relatively unoptimised at this point.
   *  - Add on-disk support. We've come this far with what's essentialy a regulation database format, why not go
   *    whole hog?
   *  - Improve immutability - flag at each node, set on clone/enumeration? Too slow?  
   *  - CopyTo() is unimplemented right now.
   */
  public sealed class BTree<TKey, TValue> : IEnumerable<KeyValuePair<TKey, TValue>>, IDictionary<TKey, TValue>
    where TKey : IComparable<TKey>
  {
    public static BTree<TKey, TValue> Empty { get { return new BTree<TKey, TValue>() { RootNode = new Node() { Leaf = true } }; } }

    [Conditional("DEBUG")]
    public void Dump()
    {
      var sb = new StringBuilder();
      dumpNode(sb, RootNode);
      Debug.WriteLine(sb);
    }

    /**
     * Remove the specified key from the BTree, returning true if an item was removed.    
     */
    [MethodImpl(methodImplOptions: MethodImplOptions.AggressiveInlining)]
    public bool Remove(TKey Key)
    {
      if (RootNode.Used.Cardinality == 0)
        return false;

      Monitor.Enter(RootLock);
      Interlocked.Increment(ref WriterCount);
      try
      {
        var HasReaders = (Interlocked.Read(ref ReaderCount) > 0);

        // Behave immutably if we have one or more readers open
        var NewRoot = RootNode;
        if (HasReaders) NewRoot = Node.Clone(RootNode);

        Monitor.Enter(NewRoot);

        // Merge the root with its immediate children if we need to + can
        if (!NewRoot.Leaf && NewRoot.Used.Cardinality < MIN_CHILDREN)
        {
          int Sum = 0;
          bool AllLeaves = true;

          // Figure out if it's safe to merge, and whether or not we're becoming a leaf node
          for (int i = 0; i < NewRoot.Used.Cardinality; i++)
          {
            Sum += NewRoot.Children[i + 1].Used.Cardinality;
            AllLeaves &= NewRoot.Children[i + 1].Leaf;
          }

          if (Sum < MAX_CHILDREN)
          {
            int RootIndex = 0;
            var TempRoot = NewRoot;
            NewRoot = new Node() { Leaf = AllLeaves };
            // Copy the children of the old root's children into the new root
            // (ie. delete the old root and merge all of its children into a single new root)
            for (int ChildIndex = 0; ChildIndex < TempRoot.Used.Cardinality; ChildIndex++)
              for (int GrandchildIndex = 0; GrandchildIndex < TempRoot.Children[ChildIndex + 1].Used.Cardinality; GrandchildIndex++)
              {
                NewRoot.Keys[RootIndex] = TempRoot.Children[ChildIndex + 1].Keys[GrandchildIndex];
                NewRoot.Values[RootIndex] = TempRoot.Children[ChildIndex + 1].Values[GrandchildIndex];
                NewRoot.Used[RootIndex] = TempRoot.Children[ChildIndex + 1].Used[GrandchildIndex];
                NewRoot.Children[RootIndex + 1] = TempRoot.Children[ChildIndex + 1].Children[GrandchildIndex + 1];
                RootIndex++;
              }

            // Shuffle lock ownership
            Monitor.Enter(NewRoot);
            Monitor.Exit(TempRoot);
          }
        }

        // Descend into the tree to do the actual removal
        var result = NewRoot.Remove(RootLock, Key, !HasReaders);
        Interlocked.Exchange(ref RootNode, NewRoot);
        return result;
      }
      finally
      {
        Interlocked.Decrement(ref WriterCount);
      }
    }

    /**
     * Set the value in the BTree with the specified key. Returns true if the item was inserted, false if it was
     * already present in the BTree and was updated to the new value.
     */
    [MethodImpl(methodImplOptions: MethodImplOptions.AggressiveInlining)]
    public bool Set(TKey Key, TValue Value)
    {
      Node NewRootLeft, NewRootRight, NewRoot, OldRoot;

      Monitor.Enter(RootLock);
      Interlocked.Increment(ref WriterCount);
      try
      {
        // Split the root node if it's getting too big
        var SplitRequired = checkSplit(RootNode, out NewRootLeft, out NewRootRight);
        NewRoot = null;
        if (SplitRequired)
        {
          NewRoot = new Node();
          NewRoot.Keys.Item00 = NewRootLeft.Keys[0];
          NewRoot.Children.Item01 = NewRootLeft;
          NewRoot.Used[0] = true;
          NewRoot.Keys.Item01 = NewRootRight.Keys[0];
          NewRoot.Children.Item02 = NewRootRight;
          NewRoot.Used[1] = true;
        }

        if (Interlocked.Read(ref ReaderCount) > 0)
        {
          // Need to be immutable; if we didn't split then the root needs cloning
          if (!SplitRequired)
            NewRoot = Node.Clone(RootNode);

          Monitor.Enter(NewRoot);
          var updated = NewRoot.Set(null, Key, Value, false);
          Interlocked.Exchange<Node>(ref RootNode, NewRoot);
          Monitor.Exit(RootLock);
          return updated;
        }
        else
        {
          Monitor.Enter(RootNode);
          if (SplitRequired)
          {
            OldRoot = RootNode;
            RootNode = NewRoot;
            Monitor.Enter(NewRoot);
            Monitor.Exit(OldRoot);
          }
          var updated = RootNode.Set(RootLock, Key, Value, true);

          return updated;
        }
      }
      finally
      {
        Interlocked.Decrement(ref WriterCount);
      }
    }

    private void dumpNode(StringBuilder sb, Node n)
    {
      sb.Append("{");
      for (int i = 0; i < n.Used.Cardinality; i++)
      {
        sb.AppendFormat(" {0} ", n.Keys[i]);
        if (n.Children[i + 1] != null)
        {
          sb.Append("-> ");
          dumpNode(sb, n.Children[i + 1]);
        }
      }
      sb.Append("}");
    }

    // Check and split a node if necessary, copying children to the new left and right nodes
    [MethodImpl(methodImplOptions: MethodImplOptions.AggressiveInlining)]
    private static bool checkSplit(Node Current, out Node NewLeft, out Node NewRight)
    {
      var NodeCardinality = Current.Used.Cardinality;
      if (NodeCardinality == MAX_CHILDREN)
      {
        NewLeft = new Node();
        NewLeft.Leaf = Current.Leaf;
        NewRight = new Node();
        NewRight.Leaf = Current.Leaf;

        // Unrolling these loops is a non-negligible performance improvement for >400k insertions.
        // A few percent, but everything we can shave off potential startup insertion time is probably
        // worth it.
        NewLeft.Keys.Item00 = Current.Keys.Item00;
        NewLeft.Keys.Item01 = Current.Keys.Item01;
        NewLeft.Keys.Item02 = Current.Keys.Item02;
        NewLeft.Keys.Item03 = Current.Keys.Item03;
        NewLeft.Keys.Item04 = Current.Keys.Item04;
        NewLeft.Keys.Item05 = Current.Keys.Item05;
        NewLeft.Keys.Item06 = Current.Keys.Item06;

        NewLeft.Values.Item00 = Current.Values.Item00;
        NewLeft.Values.Item01 = Current.Values.Item01;
        NewLeft.Values.Item02 = Current.Values.Item02;
        NewLeft.Values.Item03 = Current.Values.Item03;
        NewLeft.Values.Item04 = Current.Values.Item04;
        NewLeft.Values.Item05 = Current.Values.Item05;
        NewLeft.Values.Item06 = Current.Values.Item06;

        NewLeft.Children.Item01 = Current.Children.Item01;
        NewLeft.Children.Item02 = Current.Children.Item02;
        NewLeft.Children.Item03 = Current.Children.Item03;
        NewLeft.Children.Item04 = Current.Children.Item04;
        NewLeft.Children.Item05 = Current.Children.Item05;
        NewLeft.Children.Item06 = Current.Children.Item06;
        NewLeft.Children.Item07 = Current.Children.Item07;

        NewLeft.Used.Value = 0x007f;
        NewLeft.Used.CachedCardinality = 7;
        NewLeft.Used.Dirty = false;

        NewRight.Keys.Item00 = Current.Keys.Item07;
        NewRight.Keys.Item01 = Current.Keys.Item08;
        NewRight.Keys.Item02 = Current.Keys.Item09;
        NewRight.Keys.Item03 = Current.Keys.Item10;
        NewRight.Keys.Item04 = Current.Keys.Item11;
        NewRight.Keys.Item05 = Current.Keys.Item12;
        NewRight.Keys.Item06 = Current.Keys.Item13;
        NewRight.Keys.Item07 = Current.Keys.Item14;

        NewRight.Values.Item00 = Current.Values.Item07;
        NewRight.Values.Item01 = Current.Values.Item08;
        NewRight.Values.Item02 = Current.Values.Item09;
        NewRight.Values.Item03 = Current.Values.Item10;
        NewRight.Values.Item04 = Current.Values.Item11;
        NewRight.Values.Item05 = Current.Values.Item12;
        NewRight.Values.Item06 = Current.Values.Item13;
        NewRight.Values.Item07 = Current.Values.Item14;

        NewRight.Children.Item01 = Current.Children.Item08;
        NewRight.Children.Item02 = Current.Children.Item09;
        NewRight.Children.Item03 = Current.Children.Item10;
        NewRight.Children.Item04 = Current.Children.Item11;
        NewRight.Children.Item05 = Current.Children.Item12;
        NewRight.Children.Item06 = Current.Children.Item13;
        NewRight.Children.Item07 = Current.Children.Item14;
        NewRight.Children.Item08 = Current.Children.Item15;

        NewRight.Used.Value = 0x00FF;
        NewRight.Used.CachedCardinality = 8;
        NewRight.Used.Dirty = false;

        // more readable, slower, less fun, blah blah
        //for (int i = 0; i < MIN_CHILDREN; i++)
        //{
        //  newLeft.keys[i] = current.keys[i];
        //  newLeft.values[i] = current.values[i];
        //  newLeft.children[i + 1] = current.children[i + 1];
        //  newLeft.used[i] = true;
        //}
        //
        //for (int i = MIN_CHILDREN; i < MAX_CHILDREN; i++)
        //{
        //  newRight.keys[i - MIN_CHILDREN] = current.keys[i];
        //  newRight.values[i - MIN_CHILDREN] = current.values[i];
        //  newRight.children[i - MIN_CHILDREN + 1] = current.children[i + 1];
        //  newRight.used[i - MIN_CHILDREN] = true;
        //}

        return true;
      }

      // Nothing to do if the current node wasn't too big.
      NewRight = null;
      NewLeft = null;
      return false;
    }

    // Check and either steal keys/children/values from one node to another or merge them entirely
    // to balance the key count.
    [MethodImpl(methodImplOptions: MethodImplOptions.AggressiveInlining)]
    private static bool stealOrMerge(Node Left, Node Right, out Node NewLeft, out Node NewRight)
    {
      // order the nodes
      if (Left.Keys[0].CompareTo(Right.Keys[0]) > 0)
      {
        Node tmp = Right;
        Right = Left;
        Left = tmp;
      }

      // Grab these into locals so we're not invoking as many functions.
      int LeftCardinality = Left.Used.Cardinality;
      int RightCardinality = Right.Used.Cardinality;

      // Do we need to do anything?
      if (LeftCardinality > MIN_CHILDREN && RightCardinality > MIN_CHILDREN)
      {
        NewLeft = Left; NewRight = Right;
        return false;
      }

      NewLeft = null;
      NewRight = null;

      // do we steal or merge?
      if (LeftCardinality + RightCardinality > MAX_CHILDREN)
      {
        // steal; merging would make a single node too large
        // steal enough to balance the two nodes
        var StealCount = Math.Abs(LeftCardinality - RightCardinality) / 2;

        // If they're balanced within 1, don't bother; otherwise you'll end up oscillating pointlessly
        if (StealCount == 0)
        {
          NewLeft = Left;
          NewRight = Right;
          return false;
        }

        if (LeftCardinality > RightCardinality)
        {
          NewLeft = new Node();
          NewRight = new Node();
          NewLeft.Leaf = Left.Leaf;
          NewRight.Leaf = Right.Leaf;

          // stealing from left tail, adding to right head
          int LeftIndex = LeftCardinality - StealCount;
          int RightIndex = 0;

          // blit left into newLeft up to the steal point
          for (int i = 0; i < LeftIndex; i++)
          {
            NewLeft.Keys[i] = Left.Keys[i];
            NewLeft.Values[i] = Left.Values[i];
            NewLeft.Children[i + 1] = Left.Children[i + 1];
            NewLeft.Used[i] = Left.Used[i];
          }

          NewLeft.Used.Value = (ushort)((1 << LeftIndex) - 1);
          NewLeft.Used.Dirty = true;

          // copy the stolen part of left into the head of right
          for (int i = 0; i < StealCount; i++, LeftIndex++, RightIndex++)
          {
            NewRight.Keys[RightIndex] = Left.Keys[LeftIndex];
            NewRight.Children[RightIndex + 1] = Left.Children[LeftIndex + 1];
            NewRight.Values[RightIndex] = Left.Values[LeftIndex];
            NewRight.Used[RightIndex] = Left.Used[LeftIndex];
          }

          // copy the remainder of right into the tail of right
          for (int i = 0; i < RightCardinality; i++, RightIndex++)
          {
            NewRight.Keys[RightIndex] = Right.Keys[i];
            NewRight.Values[RightIndex] = Right.Values[i];
            NewRight.Children[RightIndex + 1] = Right.Children[i + 1];
            NewRight.Used[RightIndex] = Right.Used[i];
          }

          NewRight.Used.Value = (ushort)((1 << RightIndex) - 1);
          NewRight.Used.Dirty = true;

          Debug.Assert(NewLeft.Used.Cardinality + NewRight.Used.Cardinality == LeftCardinality + RightCardinality);
        }
        else
        {
          // stealing from right head, adding to left tail
          NewLeft = Node.Clone(Left);
          NewRight = new Node();
          NewRight.Leaf = Right.Leaf;

          int LeftIndex = LeftCardinality;
          int RightIndex = 0;
          for (int i = 0; i < StealCount; i++, LeftIndex++, RightIndex++)
          {
            NewLeft.Keys[LeftIndex] = Right.Keys[RightIndex];
            NewLeft.Values[LeftIndex] = Right.Values[RightIndex];
            NewLeft.Children[LeftIndex + 1] = Right.Children[RightIndex + 1];
            NewLeft.Used[LeftIndex] = Right.Used[RightIndex];
          }

          NewLeft.Used.Value = (ushort)((1 << LeftIndex) - 1);
          NewLeft.Used.Dirty = true;

          for (int i = 0; RightIndex < RightCardinality; i++, RightIndex++)
          {
            NewRight.Keys[i] = Right.Keys[RightIndex];
            NewRight.Values[i] = Right.Values[RightIndex];
            NewRight.Children[i + 1] = Right.Children[RightIndex + 1];
            NewRight.Used[i] = Right.Used[RightIndex];
          }

          Debug.Assert(NewLeft.Used.Cardinality + NewRight.Used.Cardinality == LeftCardinality + RightCardinality);
        }

        // since we didn't merge nodes, only shuffle them, return false
        return false;
      }
      else
      {
        // merge into a new node in newLeft
        // copy the left side
        NewLeft = Node.Clone(Left);

        int LeftIndex = NewLeft.Used.Cardinality;

        for (int i = 0; i < Right.Used.Cardinality; i++, LeftIndex++)
        {
          NewLeft.Keys[LeftIndex] = Right.Keys[i];
          NewLeft.Values[LeftIndex] = Right.Values[i];
          NewLeft.Children[LeftIndex + 1] = Right.Children[i + 1];
        }

        NewLeft.Used.Value = (ushort)((1 << LeftIndex) - 1);
        NewLeft.Used.Dirty = true;

        Debug.Assert(NewLeft.Used.Cardinality == Left.Used.Cardinality + Right.Used.Cardinality);
        return true;
      }
    }

    // Depth-first tree traversal to find + fetch a key
    private bool SearchInternal(Node Node, TKey Key, out TValue Value)
    {
      var NodeCardinality = Node.Used.Cardinality;
      int cmp = int.MaxValue;

      for (int NodeIndex = 0; NodeIndex < NodeCardinality; NodeIndex++)
      {
        cmp = Key.CompareTo(Node.Keys[NodeIndex]);
        if (cmp <= 0)
        {
          if (Node.Leaf && cmp == 0)
          {
            Value = Node.Values[NodeIndex];
            return true;
          }
          else if (!Node.Leaf && cmp < 0 && NodeIndex > 0)
            return SearchInternal(Node.Children[NodeIndex], Key, out Value);
        }
      }
      if (!Node.Leaf)
        return SearchInternal(Node.Children[NodeCardinality], Key, out Value);

      Value = default(TValue);
      return false;
    }

    // Depth-first traversal counting leaf node children
    private int CountInternal(Node Node)
    {
      var Sum = 0;
      if (Node == null) return 0;

      if (!Node.Leaf)
      {
        for (int i = 0; i < Node.Used.Cardinality; i++)
          Sum += CountInternal(Node.Children[i + 1]);

        return Sum;
      }

      return Node.Used.Cardinality;
    }

    private object RootLock = new object();
    private Node RootNode;

    private long ReaderCount;
    private long WriterCount;

    private const int MIN_CHILDREN = 7; // If you change this, make sure to build an ArrayX and TinyBitArrayX large enough for it.
    private const int MAX_CHILDREN = (2 * MIN_CHILDREN) + 1;

    #region IDictionary<K,V> et al
    public ICollection<TKey> Keys { get { return new BTreeKeyCollection(this); } }

    public ICollection<TValue> Values { get { return new BTreeValueCollection(this); } }

    public int Count
    {
      get
      {
        Interlocked.Increment(ref ReaderCount);
        try
        {
          var CountNode = RootNode;
          var Sum = CountInternal(CountNode);
          return Sum;
        }
        finally
        {
          Interlocked.Decrement(ref ReaderCount);
        }
      }
    }

    public bool IsReadOnly { get { return false; } }

    public TValue this[TKey key]
    {
      [MethodImpl(methodImplOptions: MethodImplOptions.AggressiveInlining)]
      get
      {
        TValue result;
        if (TryGetValue(key, out result))
          return result;

        throw new KeyNotFoundException();
      }

      [MethodImpl(methodImplOptions: MethodImplOptions.AggressiveInlining)]
      set
      {
        Set(key, value);
      }
    }

    [MethodImpl(methodImplOptions: MethodImplOptions.AggressiveInlining)]
    public bool TryGetValue(TKey Key, out TValue Value)
    {
      Interlocked.Increment(ref ReaderCount);
      try
      {
        var Root = RootNode;
        return SearchInternal(Root, Key, out Value);
      }
      finally
      {
        Interlocked.Decrement(ref ReaderCount);
      }
    }

    public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
    {
      Interlocked.Increment(ref ReaderCount);
      return new BTreeEnumerator(this);
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      Interlocked.Increment(ref ReaderCount);
      return new BTreeEnumerator(this);
    }

    public bool ContainsKey(TKey key)
    {
      TValue tmp;
      return TryGetValue(key, out tmp);
    }

    [MethodImpl(methodImplOptions: MethodImplOptions.AggressiveInlining)]
    public void Add(TKey Key, TValue Value)
    {
      Set(Key, Value);
    }

    [MethodImpl(methodImplOptions: MethodImplOptions.AggressiveInlining)]
    bool IDictionary<TKey, TValue>.Remove(TKey Key)
    {
      return Remove(Key);
    }

    [MethodImpl(methodImplOptions: MethodImplOptions.AggressiveInlining)]
    public void Add(KeyValuePair<TKey, TValue> Item)
    {
      Set(Item.Key, Item.Value);
    }

    public void Clear()
    {
      Monitor.Enter(RootLock);
      RootNode = new Node();
      Monitor.Exit(RootLock);
    }

    [MethodImpl(methodImplOptions: MethodImplOptions.AggressiveInlining)]
    public bool Contains(KeyValuePair<TKey, TValue> Item)
    {
      TValue tmp;
      return TryGetValue(Item.Key, out tmp) && tmp.Equals(Item.Value);
    }

    public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
    {
      throw new NotImplementedException();
    }

    [MethodImpl(methodImplOptions: MethodImplOptions.AggressiveInlining)]
    public bool Remove(KeyValuePair<TKey, TValue> Item)
    {
      return Remove(Item.Key);
    }
    #endregion

    #region ICollection for keys/values
    private sealed class BTreeKeyCollection : ICollection<TKey>
    {
      public BTreeKeyCollection(BTree<TKey, TValue> tree)
      {
        this.Tree = tree;
      }

      BTree<TKey, TValue> Tree;
      public int Count { get { return Tree.Count; } }
      public bool IsReadOnly { get { return true; } }
      public void Add(TKey Item) { throw new NotSupportedException(); }
      public void Clear() { throw new NotSupportedException(); }
      public bool Contains(TKey Item) { return Tree.ContainsKey(Item); }

      public void CopyTo(TKey[] array, int arrayIndex)
      {
        throw new NotImplementedException();
      }

      public IEnumerator<TKey> GetEnumerator() { return new BTreeEnumerator.BTreeKeyEnumerator(Tree); }

      public bool Remove(TKey Item) { throw new NotSupportedException(); }

      IEnumerator IEnumerable.GetEnumerator() { return new BTreeEnumerator.BTreeKeyEnumerator(Tree); }
    }

    private sealed class BTreeValueCollection : ICollection<TValue>
    {
      public BTreeValueCollection(BTree<TKey, TValue> tree)
      {
        this.Tree = tree;
      }

      BTree<TKey, TValue> Tree;
      public int Count { get { return Tree.Count; } }
      public bool IsReadOnly { get { return true; } }
      public void Add(TValue Item) { throw new NotSupportedException(); }
      public void Clear() { throw new NotSupportedException(); }
      public bool Contains(TValue Item) { return (new BTreeEnumerator.BTreeValueEnumerator(Tree) as IEnumerable<TValue>).Any(x => x.Equals(Item)); }

      public void CopyTo(TValue[] array, int arrayIndex)
      {
        throw new NotImplementedException();
      }

      public IEnumerator<TValue> GetEnumerator() { return new BTreeEnumerator.BTreeValueEnumerator(Tree); }

      public bool Remove(TValue Item) { throw new NotSupportedException(); }

      IEnumerator IEnumerable.GetEnumerator() { return new BTreeEnumerator.BTreeValueEnumerator(Tree); }
    }
    #endregion

    #region Enumerator
    private class BTreeEnumerator : IEnumerator<KeyValuePair<TKey, TValue>>, IEnumerator
    {
      internal sealed class BTreeKeyEnumerator : BTreeEnumerator, IEnumerator<TKey>
      {
        internal BTreeKeyEnumerator(BTree<TKey, TValue> Tree) : base(Tree) { }

        TKey IEnumerator<TKey>.Current { get { return CurrentNodePath.Node.Keys[CurrentNodePath.Index]; } }
        object IEnumerator.Current { get { return CurrentNodePath.Node.Keys[CurrentNodePath.Index]; } }
      }

      internal sealed class BTreeValueEnumerator : BTreeEnumerator, IEnumerator<TValue>
      {
        internal BTreeValueEnumerator(BTree<TKey, TValue> tree) : base(tree) { }

        TValue IEnumerator<TValue>.Current { get { return CurrentNodePath.Node.Values[CurrentNodePath.Index]; } }
        object IEnumerator.Current { get { return CurrentNodePath.Node.Values[CurrentNodePath.Index]; } }
      }

      internal BTreeEnumerator(BTree<TKey, TValue> Tree)
      {
        this.Tree = Tree;
        CurrentNodePath = new NodePath() { Node = this.Tree.RootNode, Index = -1 };
      }
      struct NodePath
      {
        internal Node Node;
        internal int Index;
      }
      internal BTree<TKey, TValue> Tree;
      NodePath CurrentNodePath;
      Stack<NodePath> Path = new Stack<NodePath>();

      public KeyValuePair<TKey, TValue> Current
      {
        get
        {
          if (CurrentNodePath.Index < 0)
            throw new InvalidOperationException();
          return new KeyValuePair<TKey, TValue>(CurrentNodePath.Node.Keys[CurrentNodePath.Index], CurrentNodePath.Node.Values[CurrentNodePath.Index]);
        }
      }

      object IEnumerator.Current
      {
        get
        {
          if (CurrentNodePath.Index < 0)
            throw new InvalidOperationException();
          return new KeyValuePair<TKey, TValue>(CurrentNodePath.Node.Keys[CurrentNodePath.Index], CurrentNodePath.Node.Values[CurrentNodePath.Index]);
        }
      }

      public bool MoveNext()
      {
        CurrentNodePath.Index++;

        while (!CurrentNodePath.Node.Leaf || CurrentNodePath.Index >= CurrentNodePath.Node.Used.Cardinality)
        {
          if (CurrentNodePath.Index >= CurrentNodePath.Node.Used.Cardinality)
          {
            if (Path.Count == 0)
              return false;

            CurrentNodePath = Path.Pop();
          }
          else
          {
            Path.Push(CurrentNodePath);
            CurrentNodePath = new NodePath() { Node = CurrentNodePath.Node.Children[CurrentNodePath.Index + 1], Index = -1 };
          }
          CurrentNodePath.Index++;
        }

        return true;
      }

      public void Reset()
      {
        CurrentNodePath = new NodePath() { Node = Tree.RootNode, Index = -1 };
        MoveNext();
      }

      #region IDisposable Support
      private bool disposedValue = false; // To detect redundant calls

      protected virtual void Dispose(bool disposing)
      {
        if (!disposedValue)
        {
          if (disposing)
          {
            Interlocked.Decrement(ref Tree.ReaderCount);
            Tree = BTree<TKey, TValue>.Empty;
          }
          disposedValue = true;
        }
      }

      // This code added to correctly implement the disposable pattern.
      public void Dispose()
      {
        // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        Dispose(true);
      }
      #endregion
    }

    #endregion

    // This is a class not a struct solely because, since Array16<T> is a struct,
    // if Node were a struct it'd be self-referential and infinitely large. In all
    // other respects it's treated as a dumb struct.
    private sealed class Node
    {
      internal Node() { }

      [MethodImpl(methodImplOptions: MethodImplOptions.AggressiveInlining)]
      internal static Node Clone(Node other)
      {
        var node = new Node();
        node.Keys = other.Keys;
        node.Values = other.Values;
        node.Children = other.Children;
        node.Used = other.Used;
        node.Leaf = other.Leaf;
        return node;
      }

      internal bool Set(object UpstreamLock, TKey Key, TValue Value, bool Mutable)
      {
        var NodeCardinality = Used.Cardinality;
        Debug.Assert(NodeCardinality < MAX_CHILDREN, "SetInternal on full node");

        // Find the key index
        int KeyIndex, cmp = int.MaxValue;
        for (KeyIndex = 0; KeyIndex < NodeCardinality; KeyIndex++)
        {
          cmp = Key.CompareTo(Keys[KeyIndex]);
          if (cmp <= 0)
            break;
        }

        if (Leaf)
        {
          // We're in a leaf node so either insert or update the value
          if (cmp != 0)
          {
            // insert a value here
            Keys.Insert(KeyIndex, Key);
            Values.Insert(KeyIndex, Value);
            Children.Insert(KeyIndex, null);
            Used.Insert(KeyIndex, true);

            if (UpstreamLock != null)
              Monitor.Exit(UpstreamLock);
            Monitor.Exit(this);
            return true;
          }
          else
          {
            Values[KeyIndex] = Value;
            if (UpstreamLock != null)
              Monitor.Exit(UpstreamLock);
            Monitor.Exit(this);
            return false;
          }
        }
        else
        {
          if (cmp <= 0 || KeyIndex == NodeCardinality)
          {
            Node NewChildLeft, NewChildRight;
            // if we're lower than the minimum key of this node we belong in the first child
            if (KeyIndex == 0)
            {
              Keys.Item00 = Key;
              KeyIndex++;
            }
            else if (cmp == 0)
            {
              // We belong in the next child over if we're equal to keys[keyIndex]
              KeyIndex++;
            }

            // See if we need to split
            var SplitChild = checkSplit(Children[KeyIndex], out NewChildLeft, out NewChildRight);

            if (SplitChild)
            {
              // We split, so shift the freshly created nodes into the current node's children
              Children[KeyIndex] = NewChildLeft;
              Keys[KeyIndex - 1] = NewChildLeft.Keys[0];
              Keys.Insert(KeyIndex, NewChildRight.Keys[0]);
              Children.Insert(KeyIndex + 1, NewChildRight);
              Used.Insert(KeyIndex, true);
              Values.Insert(KeyIndex, default(TValue));

              Node DescendNode;

              // Figure out which side of the split we need to descend into and do it
              if (Key.CompareTo(Keys[KeyIndex]) < 0)
                DescendNode = Children[KeyIndex];
              else
                DescendNode = Children[KeyIndex + 1];

              Monitor.Enter(DescendNode);
              if (UpstreamLock != null)
                Monitor.Exit(UpstreamLock);

              return DescendNode.Set(this, Key, Value, Mutable);
            }
            else
            {
              // No split, so just lock, (optionally) clone and descend
              var Child = Children[KeyIndex];
              Monitor.Enter(Children[KeyIndex]);

              if (!Mutable)
              {
                var OldChild = Children[KeyIndex];
                Child = Clone(Child);
                Children[KeyIndex] = Child;
                Monitor.Enter(Child);
                Monitor.Exit(OldChild);
              }
              if (UpstreamLock != null)
                Monitor.Exit(UpstreamLock);

              return Child.Set(this, Key, Value, Mutable);
            }
          }
        }

        // If this happens the world has gone horribly wrong
        Debug.Assert(false, "how did we get here?");
        if (UpstreamLock != null)
          Monitor.Exit(UpstreamLock);
        Monitor.Exit(this);
        return false;
      }

      internal bool Remove(object UpstreamLock, TKey Key, bool Mutable)
      {
        var NodeCardinality = Used.Cardinality;
        if (NodeCardinality == 0) return false;

        // Find the key index
        int KeyIndex, cmp = int.MaxValue;
        for (KeyIndex = 0; KeyIndex < NodeCardinality; KeyIndex++)
        {
          cmp = Key.CompareTo(Keys[KeyIndex]);
          if (cmp <= 0)
            break;
        }

        if (Leaf)
        {
          // cmp == 0 if we found a match to remove, otherwise the key isn't in this node so return false
          if (cmp != 0 || KeyIndex == NodeCardinality)
          {
            if (UpstreamLock != null)
              Monitor.Exit(UpstreamLock);
            Monitor.Exit(this);
            return false;
          }
          else
          {
            // Remove it and return true
            Values.Remove(KeyIndex);
            Keys.Remove(KeyIndex);
            Used.Remove(KeyIndex);
            Children.Remove(KeyIndex + 1);
            if (UpstreamLock != null)
              Monitor.Exit(UpstreamLock);
            Monitor.Exit(this);
            return true;
          }
        }
        else
        {
          // Grab the relevant child
          var ChildIndex = Math.Min(Math.Max(1, KeyIndex + (cmp == 0 ? 1 : 0)), NodeCardinality);
          var Child = Children[ChildIndex];

          // Descend into the child if we don't need to try merging it.
          if (Child.Used.Cardinality > MIN_CHILDREN || NodeCardinality == 1)
          {
            // lock and descend
            Monitor.Enter(Child);
            if (!Mutable)
            {
              var NewChild = Clone(Child);
              var OldChild = Child;
              Child = NewChild;
              Children[ChildIndex] = NewChild;
              Monitor.Enter(NewChild);
              Monitor.Exit(OldChild);
            }

            if (UpstreamLock != null)
              Monitor.Exit(UpstreamLock);

            return Child.Remove(this, Key, Mutable);
          }
          else
          {
            int LeftIndex = -1, RightIndex = -1;
            Node NewLeft, NewRight;

            // Need to consider merging
            // Try merge left if we can
            if (KeyIndex >= 2)
              LeftIndex = KeyIndex - 1;
            else if (KeyIndex <= Used.Cardinality)
              LeftIndex = KeyIndex;

            // Clamp to the allowable range of child nodes
            LeftIndex = Math.Min(Math.Max(1, LeftIndex + (cmp == 0 ? 1 : 0)), NodeCardinality - 1);
            RightIndex = LeftIndex + 1;

            // Hand off to the helper to do the actual work
            if (stealOrMerge(Children[LeftIndex], Children[RightIndex], out NewLeft, out NewRight))
            {
              // nodes were merged into a single node (newLeft); shuffle the current node
              Children[LeftIndex] = NewLeft;
              Keys[LeftIndex - 1] = NewLeft.Keys[0];
              Values[LeftIndex - 1] = default(TValue);

              // Remove the one that got everything merged out of it
              Children.Remove(LeftIndex + 1);
              Keys.Remove(LeftIndex);
              Values.Remove(LeftIndex);
              Used.Remove(LeftIndex);

              // lock, descend
              Monitor.Enter(NewLeft);
              if (UpstreamLock != null)
                Monitor.Exit(UpstreamLock);

              return NewLeft.Remove(this, Key, Mutable);
            }
            else
            {
              // Nodes had zero or more keys shuffled from one to the other
              // Make sure we keep track of them in this node
              Children[LeftIndex] = NewLeft;
              Keys[LeftIndex - 1] = NewLeft.Keys[0];
              Values[LeftIndex - 1] = default(TValue);

              Children[RightIndex] = NewRight;
              Keys[RightIndex - 1] = NewRight.Keys[0];
              Values[RightIndex - 1] = default(TValue);// newRight.values[0];

              // Pick a side, lock, and descend
              Node DescendNode;
              if (Key.CompareTo(Keys[RightIndex - 1]) < 0)
                DescendNode = NewLeft;
              else
                DescendNode = NewRight;

              Monitor.Enter(DescendNode);
              if (UpstreamLock != null)
                Monitor.Exit(UpstreamLock);

              return DescendNode.Remove(this, Key, Mutable);
            }
          }
        }
      }

      // TODO: Maybe refactor into IndexNode and LeafNode with only one array each.
      // May need to see whether the added overhead of typed nodes in time outweighs
      // the reduced overhead in memory usage.

      // These are all fields not properties because we don't want to pay the cost of 
      // function invocations when we're touching them hundreds of thousands of times
      // per second
      internal Array16<TKey> Keys;
      internal Array16<TValue> Values;

      internal Array16<Node> Children;

      internal TinyBitArray16 Used;
      internal bool Leaf;
    }
  }

  #region Data type helpers
  // Fixed-size array with struct semantics to minimise GC allocs per node. It's a little gross,
  // though it means we save three extra allocs per node and should reduce heap fragmentation slightly.
  struct Array16<T>
  {
    internal T Item00, Item01, Item02, Item03, Item04, Item05, Item06, Item07,
               Item08, Item09, Item10, Item11, Item12, Item13, Item14, Item15;

    [MethodImpl(methodImplOptions: MethodImplOptions.AggressiveInlining)]
    public void CopyTo(ref Array16<T> other)
    {
      other = this;
    }

    // As this is a fixed size array, Insert() effectively pushes the 16th item (if it exists)off the end and into the bin.
    [MethodImpl(methodImplOptions: MethodImplOptions.AggressiveInlining)]
    public void Insert(int index, T value)
    {
      for (int i = 15; i > index; i--)
        this[i] = this[i - 1];

      this[index] = value;
    }

    [MethodImpl(methodImplOptions: MethodImplOptions.AggressiveInlining)]
    public void Remove(int index)
    {
      for (int i = index; i < 15; i++)
        this[i] = this[i + 1];

      this[15] = default(T);
    }

    public T this[int index]
    {
      [MethodImpl(methodImplOptions: MethodImplOptions.AggressiveInlining)]
      get
      {
        switch (index)
        {
          case 0: return Item00;
          case 1: return Item01;
          case 2: return Item02;
          case 3: return Item03;
          case 4: return Item04;
          case 5: return Item05;
          case 6: return Item06;
          case 7: return Item07;
          case 8: return Item08;
          case 9: return Item09;
          case 10: return Item10;
          case 11: return Item11;
          case 12: return Item12;
          case 13: return Item13;
          case 14: return Item14;
          case 15: return Item15;
          default: throw new IndexOutOfRangeException();
        }
      }

      [MethodImpl(methodImplOptions: MethodImplOptions.AggressiveInlining)]
      set
      {
        switch (index)
        {
          case 0: Item00 = value; break;
          case 1: Item01 = value; break;
          case 2: Item02 = value; break;
          case 3: Item03 = value; break;
          case 4: Item04 = value; break;
          case 5: Item05 = value; break;
          case 6: Item06 = value; break;
          case 7: Item07 = value; break;
          case 8: Item08 = value; break;
          case 9: Item09 = value; break;
          case 10: Item10 = value; break;
          case 11: Item11 = value; break;
          case 12: Item12 = value; break;
          case 13: Item13 = value; break;
          case 14: Item14 = value; break;
          case 15: Item15 = value; break;
          default: throw new IndexOutOfRangeException();
        }
      }
    }
  }

  // Like a BCL BitArray, but fixed size and as a struct rather than allocating a child
  // array to store the bits.
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  struct TinyBitArray16
  {
    internal ushort Value;
    internal bool Dirty;
    internal ushort CachedCardinality;

    public bool this[int index]
    {
      [MethodImpl(methodImplOptions: MethodImplOptions.AggressiveInlining)]
      get
      {
        if (index < 0 || index > 15)
          throw new IndexOutOfRangeException();

        return (Value & (1 << (int)index)) != 0;
      }

      [MethodImpl(methodImplOptions: MethodImplOptions.AggressiveInlining)]
      set
      {
        if (index < 0 || index > 15)
          throw new IndexOutOfRangeException();
        Dirty = true;
        Value ^= (ushort)(((value ? -1 : 0) ^ Value) & (1 << (int)index));
      }
    }

    [MethodImpl(methodImplOptions: MethodImplOptions.AggressiveInlining)]
    public void Insert(int index, bool value)
    {
      if (index < 0 || index > 15)
        throw new IndexOutOfRangeException();

      Dirty = true;
      // mask bits up to the specified index
      var mask = (1 << (index + 1)) - 1;
      // value & mask | value & ~mask << 1 creates the empty space, then OR it with the value bitshifted to the right location
      Value = (ushort)(((Value & mask) | ((Value << 1) & ~mask)) | (value ? 1 : 0) << index);
    }

    [MethodImpl(methodImplOptions: MethodImplOptions.AggressiveInlining)]
    public void Remove(int index)
    {
      if (index < 0 || index > 15)
        throw new IndexOutOfRangeException();

      Dirty = true;
      var mask = (1 << index) - 1;

      // Excise bit at specified index
      Value = (ushort)((Value & mask) | ((Value >> 1) & ~mask));
    }

    public int Cardinality
    {
      [MethodImpl(methodImplOptions: MethodImplOptions.AggressiveInlining)]
      get { if (!Dirty) return CachedCardinality; else { CachedCardinality = (ushort)CountSetBits(Value); return CachedCardinality; } }
    }

    [MethodImpl(methodImplOptions: MethodImplOptions.AggressiveInlining)]
    private int CountSetBits(int i)
    {
      // voodoo bitshifting math. Way faster than a loop because of zero branching.
      i = i - ((i >> 1) & 0x55555555);
      i = (i & 0x33333333) + ((i >> 2) & 0x33333333);
      return (((i + (i >> 4)) & 0x0F0F0F0F) * 0x01010101) >> 24;
    }
  }
  #endregion
}