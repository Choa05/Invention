﻿/*! 2 !*/
using System;
using UIKit;
using CoreGraphics;
using Inv.Support;
using System.Diagnostics;

namespace Inv
{
  internal sealed class iOSDateTimePicker : UIViewController
  {
    public iOSDateTimePicker(UIViewController Parent, string HeaderText)
    {
      this.HeaderBackgroundColor = UIColor.DarkGray;
      this.HeaderTextColor = UIColor.White;
      this.HeaderText = HeaderText;
      this.Parent = Parent;
      this.PickDate = true;
      this.PickTime = true;
      this.ModalPresentationStyle = UIKit.UIModalPresentationStyle.Custom;
    }

    public UIColor HeaderBackgroundColor { get; set; }
    public UIColor HeaderTextColor { get; set; }
    public string HeaderText { get; set; }
    public bool PickDate { get; set; }
    public bool PickTime { get; set; }
    public DateTime Value { get; set; }
    public event Action DoneEvent;
    public event Action CancelEvent;

    public override void ViewDidLoad()
    {
      base.ViewDidLoad();

      View.BackgroundColor = UIColor.Clear;

      this.EmbedView = new UIView();
      EmbedView.BackgroundColor = HeaderBackgroundColor;

      this.HeaderLabel = new UILabel(new CGRect(0, 0, 320 / 2, 44));
      HeaderLabel.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;
      HeaderLabel.BackgroundColor = HeaderBackgroundColor;
      HeaderLabel.TextColor = HeaderTextColor;
      HeaderLabel.TextAlignment = UITextAlignment.Center;
      HeaderLabel.Text = HeaderText + (PickDate ? " Date" : "") + (PickTime ? " Time" : "");

      this.CancelButton = UIButton.FromType(UIButtonType.System);
      CancelButton.SetTitleColor(HeaderTextColor, UIControlState.Normal);
      CancelButton.BackgroundColor = UIColor.Clear;
      CancelButton.SetTitle("Cancel", UIControlState.Normal);
      CancelButton.TouchUpInside += (Sender, Event) =>
      {
        DismissViewController(true, null);
        if (CancelEvent != null)
          CancelEvent();
      };

      this.DoneButton = UIButton.FromType(UIButtonType.System);
      DoneButton.SetTitleColor(HeaderTextColor, UIControlState.Normal);
      DoneButton.BackgroundColor = UIColor.Clear;
      DoneButton.SetTitle("Done", UIControlState.Normal);
      DoneButton.TouchUpInside += (Sender, Event) =>
      {
        // TODO: does this do timezones correctly?

        if (PickDate)
          this.Value = DatePicker.Date.NSDateToDateTime().Date;
        else
          this.Value = DateTime.MinValue.Date;

        if (PickTime)
          this.Value += TimePicker.Date.NSDateToDateTime().TimeOfDay.TruncateSeconds(); // seconds are not editable in UIDatePicker.

        DismissViewController(true, null);
        if (DoneEvent != null)
          DoneEvent();
      };

      if (PickDate)
      {
        this.DatePicker = new UIDatePicker();
        DatePicker.BackgroundColor = UIColor.White;
        DatePicker.Mode = UIDatePickerMode.Date;
        DatePicker.Date = (Foundation.NSDate)Value.ToLocalTime();
        EmbedView.AddSubview(DatePicker);
      }

      if (PickTime)
      {
        this.TimePicker = new UIDatePicker();
        TimePicker.BackgroundColor = UIColor.White;
        TimePicker.Mode = UIDatePickerMode.Time;
        TimePicker.Date = (Foundation.NSDate)Value.ToLocalTime();
        EmbedView.AddSubview(TimePicker);
      }

      EmbedView.AddSubview(HeaderLabel);
      EmbedView.AddSubview(CancelButton);
      EmbedView.AddSubview(DoneButton);

      Add(EmbedView);
    }
    public override void ViewWillAppear(bool animated)
    {
      base.ViewDidAppear(animated);

      Show(false);
    }
    public override void DidRotate(UIInterfaceOrientation fromInterfaceOrientation)
    {
      base.DidRotate(fromInterfaceOrientation);

      Show(true);
      View.SetNeedsDisplay();
    }

    private const float HeaderBarHeight = 40.0F;
    private CGSize ButtonSize = new CGSize(71, 30);

    private void Show(bool Rotating)
    {
      Debug.Assert(DatePicker != null || TimePicker != null);

      if (DatePicker != null && TimePicker == null)
        ArrangeSingle(DatePicker, Rotating);
      else if (TimePicker != null && DatePicker == null)
        ArrangeSingle(TimePicker, Rotating);
      else if (DatePicker != null && TimePicker != null)
        ArrangeMultiple(Rotating);

      HeaderLabel.Frame = new CGRect(20 + ButtonSize.Width, 4, Parent.View.Frame.Width - (40 + 2 * ButtonSize.Width), 35);
      DoneButton.Frame = new CGRect(EmbedView.Frame.Width - ButtonSize.Width - 10, 7, ButtonSize.Width, ButtonSize.Height);
      CancelButton.Frame = new CGRect(10, 7, ButtonSize.Width, ButtonSize.Height);
    }
    private void ArrangeSingle(UIDatePicker Picker, bool Rotating)
    {
      var FrameWidth = Parent.View.Frame.Width;

      var EmbedViewSize = new CGSize(FrameWidth, Picker.Frame.Height + HeaderBarHeight);

      var EmbedViewRect = new CGRect(0, View.Frame.Height - EmbedViewSize.Height, EmbedViewSize.Width, EmbedViewSize.Height);
      EmbedView.Frame = EmbedViewRect;

      Picker.Frame = new CGRect(Picker.Frame.X, HeaderBarHeight, EmbedViewRect.Width, Picker.Frame.Height);
    }
    private void ArrangeMultiple(bool Rotating)
    {
      var FrameWidth = Parent.View.Frame.Width;

      CGRect EmbedViewRect;

      if (FrameWidth < 568)
      {
        // stack date and time on top of each other (iPhone portrait).
        var EmbedViewSize = new CGSize(FrameWidth, DatePicker.Frame.Height + TimePicker.Frame.Height + HeaderBarHeight);
        EmbedViewRect = new CGRect(0, View.Frame.Height - EmbedViewSize.Height, EmbedViewSize.Width, EmbedViewSize.Height);

        DatePicker.Frame = new CGRect(0, HeaderBarHeight, EmbedViewRect.Width, DatePicker.Frame.Height);
        TimePicker.Frame = new CGRect(0, HeaderBarHeight + DatePicker.Frame.Height, EmbedViewRect.Width, TimePicker.Frame.Height);
      }
      else
      {
        // date and time pickers are side-by-side when there's enough width to split in half.
        var EmbedViewSize = new CGSize(FrameWidth, Math.Max(DatePicker.Frame.Height, TimePicker.Frame.Height) + HeaderBarHeight);
        EmbedViewRect = new CGRect(0, View.Frame.Height - EmbedViewSize.Height, EmbedViewSize.Width, EmbedViewSize.Height);

        var SplitWidth = EmbedViewRect.Width / 2;
        DatePicker.Frame = new CGRect(0, HeaderBarHeight, SplitWidth, DatePicker.Frame.Height);
        TimePicker.Frame = new CGRect(SplitWidth, HeaderBarHeight, SplitWidth, TimePicker.Frame.Height);
      }

      EmbedView.Frame = EmbedViewRect;
    }

    private UILabel HeaderLabel;
    private UIButton DoneButton;
    private UIButton CancelButton;
    private UIViewController Parent;
    private UIView EmbedView;
    private UIDatePicker DatePicker;
    private UIDatePicker TimePicker;
  }
}
