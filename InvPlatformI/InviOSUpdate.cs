﻿/*! 1 !*/
using Foundation;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using UIKit;

namespace Inv
{
  public sealed class iOSSelfUpdate
  {
    public iOSSelfUpdate(string LocalVersionText, string RemoteHostAddress, string RemoteVersionResource, string RemoteManifestResource)
    {
      this.LocalVersionText = LocalVersionText;
      this.RemoteHostAddress = RemoteHostAddress;
      this.RemoteVersionResource = RemoteVersionResource;
      this.RemoteManifestResource = RemoteManifestResource;
    }

    public string LocalVersionText { get; private set; }
    public string RemoteHostAddress { get; private set; }
    public string RemoteVersionResource { get; private set; }
    public string RemoteManifestResource { get; private set; }
  }

  internal sealed class UpdateAppDelegate : UIKit.UIApplicationDelegate
  {
    public override UIKit.UIWindow Window { get; set; }

    public override void FinishedLaunching(UIApplication Application)
    {
      this.Window = new UIKit.UIWindow();
      Window.MakeKeyAndVisible();

      var Url = new NSUrl("itms-services://?action=download-manifest&url=" + iOSShell.UpgradeSelfUpdate.RemoteHostAddress + iOSShell.UpgradeSelfUpdate.RemoteManifestResource);

      var Result = Application.OpenUrl(Url);

      if (!Result)
      {
        Debug.WriteLine("Url did not open.");

        // TODO: display a message to indicate the self-ugprade failed.
      }
    }
  }
}
