/*! 2 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Foundation;
using UIKit;

#if DEBUG
namespace Inv
{
  public static class iOSDebugNative
  {
    public static void Run(params string[] args)
    {
      UIApplication.Main(args, null, "iOSDebugNativeAppDelegate");
    }
  }

  [Register("iOSDebugNativeAppDelegate")]
  internal class iOSDebugNativeAppDelegate : UIApplicationDelegate
  {
    public override UIWindow Window { get; set; }

    public override bool FinishedLaunching(UIApplication application, NSDictionary launchOptions)
    {
      Window = new UIWindow(UIScreen.MainScreen.Bounds);

      var iOSRootController = new UIKit.UINavigationController();
      Window.RootViewController = iOSRootController;
      iOSRootController.NavigationBarHidden = true;

      var LayoutController = NativeTests.Layouts();
      iOSRootController.PushViewController(LayoutController, false);

      Window.MakeKeyAndVisible();

      return true;
    }
  }

  internal static class NativeTests
  {
    public static UIViewController Layouts()
    {
      var Result = new iOSLayoutController();
      //Result.View.BackgroundColor = UIColor.Gray;
      Result.LoadEvent += () =>
      {
        Result.View.BackgroundColor = UIColor.Gray;

        var LogoImage = (UIImage)null;// UIKit.UIImage.LoadFromData(Foundation.NSData.FromArray(InvTest.Resources.Images.PhoenixLogo960x540.GetBuffer()), 3.0F);

        var Surface = new iOSLayoutContainer();
        Result.SetContentElement(Surface);

        var Overlay = new iOSLayoutOverlay();
        Surface.SetContentElement(Overlay);

        var LogoContainer = new iOSLayoutContainer();
        LogoContainer.SetContentWidth(400);
        LogoContainer.SetContentHeight(400);
        LogoContainer.SetContentAlignment(iOSLayoutVertical.Stretch, iOSLayoutHorizontal.Left);

        var Logo = new iOSLayoutGraphic();
        LogoContainer.SetContentElement(Logo);
        Logo.Image = LogoImage;
        Logo.BackgroundColor = UIColor.Orange;

        var Container = new iOSLayoutContainer();
        Container.SetContentAlignment(iOSLayoutVertical.Stretch, iOSLayoutHorizontal.Center);

        Overlay.ComposeElements(new[] { LogoContainer, Container });

        var Dock = new iOSLayoutDock();
        Container.SetContentElement(Dock);
        Dock.SetOrientation(iOSLayoutOrientation.Vertical);

        var LabelContainer = new iOSLayoutContainer();
        LabelContainer.SetContentMinimumHeight(100);
        LabelContainer.LayoutMargins = new UIEdgeInsets(20, 20, 20, 20);
        //LabelContainer.SetContentVisiblity(false);

        var Label1 = new iOSLayoutLabel();
        LabelContainer.SetContentElement(Label1);
        Label1.Text = "Label 1";
        Label1.BackgroundColor = UIColor.Green;
        Label1.LayoutMargins = new UIEdgeInsets(20, 20, 20, 20);

        var Label2 = new iOSLayoutLabel();
        Label2.Text = "Label 2";
        Label2.LayoutMargins = new UIEdgeInsets(20, 20, 20, 20);
        Label2.BackgroundColor = UIColor.Blue;

        var Label3 = new iOSLayoutLabel();
        Label3.Text = "Label 3";
        Label3.BackgroundColor = UIColor.Red;

        var Graphic1 = new iOSLayoutGraphic();
        Graphic1.Image = LogoImage;
        Graphic1.BackgroundColor = UIColor.Purple;

        Dock.ComposeElements(new iOSLayoutElement[] { LabelContainer, Label2 }, new[] { Graphic1 }, new[] { Label3 });

        var SideContainer = new iOSLayoutContainer();
        Overlay.ComposeElements(new[] { SideContainer });
        SideContainer.LayoutMargins = new UIEdgeInsets(50, 50, 50, 50);

        var Scroll = new iOSLayoutScroll();
        SideContainer.SetContentElement(Scroll);
        SideContainer.SetContentAlignment(iOSLayoutVertical.Stretch, iOSLayoutHorizontal.Right);

        var Stack = new iOSLayoutStack();
        Scroll.SetContentElement(Stack);
        Stack.SetOrientation(iOSLayoutOrientation.Vertical);
        Scroll.BackgroundColor = UIColor.White;

        var Label4 = new iOSLayoutLabel();
        Label4.Text = "L4";
        Label4.Font = UIFont.SystemFontOfSize(250);
        Label4.BackgroundColor = UIColor.Green;

        var Label5 = new iOSLayoutLabel();
        Label5.Text = "L5";
        Label5.Font = UIFont.SystemFontOfSize(250);
        Label5.BackgroundColor = UIColor.Blue;

        var Label6 = new iOSLayoutLabel();
        Label6.Text = "L6";
        Label6.Font = UIFont.SystemFontOfSize(250);
        Label6.BackgroundColor = UIColor.Red;

        Stack.ComposeElements(new[] { Label4, Label5, Label6 });
      };

      return Result;
    }
  }
}
#endif