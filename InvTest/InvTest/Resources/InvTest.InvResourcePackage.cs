/*! 10 !*/

namespace InvTest
{
  public static class Resources
  {
    static Resources()
    {
      global::Inv.Resource.Foundation.Import(typeof(Resources), "Resources.InvTest.InvResourcePackage.rs");
    }

    public static readonly ResourcesDocuments Documents;
    public static readonly ResourcesImages Images;
    public static readonly ResourcesSounds Sounds;
  }

  public sealed class ResourcesDocuments
  {
    public ResourcesDocuments() { }

    ///<Summary>200.7 KB</Summary>
    public readonly global::Inv.Binary PhoenixLogoDoc;
    ///<Summary>67.9 KB</Summary>
    public readonly global::Inv.Binary PhoenixPlan;
  }

  public sealed class ResourcesImages
  {
    public ResourcesImages() { }

    ///<Summary>74.1 KB</Summary>
    public readonly global::Inv.Image PhoenixLogo960x540;
    ///<Summary>2.8 KB</Summary>
    public readonly global::Inv.Image SoundOff256x256;
    ///<Summary>3.3 KB</Summary>
    public readonly global::Inv.Image SoundOn256x256;
    ///<Summary>15.9 KB</Summary>
    public readonly global::Inv.Image Water128x128;
    ///<Summary>193.5 KB</Summary>
    public readonly global::Inv.Image Waves;
  }

  public sealed class ResourcesSounds
  {
    public ResourcesSounds() { }

    ///<Summary>19.2 KB</Summary>
    public readonly global::Inv.Sound Clang;
  }
}