﻿/*! 7 !*/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Reflection;
using Windows.UI;
using Inv.Support;

namespace Inv
{
  public sealed partial class UwaMainPage : Windows.UI.Xaml.Controls.Page
  {
    public UwaMainPage()
    {
      this.InitializeComponent();

      //RenderTest();
      //WarningTest();
      Inv.UwaShell.Attach(this, InvTest.Shell.Install);
    }

    /*
    private void WarningTest()
    {
      var Message = "1. This is one line of text\n2. This is two lines of text\n3. This is three lines of text\n4. This is four lines of text\n5. This is the last line of text";

      var Border = new Windows.UI.Xaml.Controls.Border();
      this.Content = Border;
      Border.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Center;
      Border.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Center;

      var RichBox = new Windows.UI.Xaml.Controls.RichEditBox();
      //Border.Child = RichBox;
      //RichBox.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Center;
      //RichBox.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Center;
      RichBox.AcceptsReturn = true;
      RichBox.TextWrapping = Windows.UI.Xaml.TextWrapping.Wrap;
      RichBox.Document.SetText(Windows.UI.Text.TextSetOptions.None, Message);
      RichBox.IsReadOnly = true;

      var TextBox = new Windows.UI.Xaml.Controls.TextBox();
      Border.Child = TextBox;
      TextBox.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Center;
      TextBox.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Center;
      TextBox.IsReadOnly = true;
      TextBox.TextWrapping = Windows.UI.Xaml.TextWrapping.Wrap;
      TextBox.Text = Message;

      var TextBlock = new Windows.UI.Xaml.Controls.TextBlock();
      //Border.Child = TextBlock;
      //TextBlock.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Center;
      //TextBlock.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Center;
      TextBlock.TextWrapping = Windows.UI.Xaml.TextWrapping.Wrap;
      TextBlock.Text = Message;
    }
    private void RenderTest()
    {
      var CanvasControl = new Microsoft.Graphics.Canvas.UI.Xaml.CanvasControl();
      this.Content = CanvasControl;

      var FPSLastTick = 0;
      var FPSLastCount = 0;
      var FPSCurrentCount = 0;

      Windows.UI.Xaml.Media.CompositionTarget.Rendering += (Sender, Event) =>
      {
        CanvasControl.Invalidate();

        var CurrentTick = System.Environment.TickCount;
        var Difference = CurrentTick - FPSLastTick;

        if (Difference < 0 || Difference >= 1000)
        {
          FPSLastCount = FPSCurrentCount;
          FPSCurrentCount = 0;
          FPSLastTick = System.Environment.TickCount;
        }

        FPSCurrentCount++;
      };

      Inv.Image WaterImage;

      var Assembly = typeof(UwaMainPage).GetTypeInfo().Assembly;
      using (var Stream = Assembly.GetManifestResourceStream("InvTestUwa.Water.png"))
      {
        var Buffer = new byte[Stream.Length];
        Stream.Read(Buffer, 0, Buffer.Length);

        WaterImage = new Inv.Image(Buffer, ".png");
      }

      var GraphicsRect = new Windows.Foundation.Rect();
      var ImageSourceRect = new Windows.Foundation.Rect(0, 0, 128, 128);
      var TileSize = 16;
      var CanvasBitmapDictionary = new Dictionary<Inv.Image, Microsoft.Graphics.Canvas.CanvasBitmap>();

      CanvasControl.Draw += (Sender, Event) =>
      {
        var Canvas = Event.DrawingSession;

        var CanvasWidth = (int)CanvasControl.ActualWidth;
        var CanvasHeight = (int)CanvasControl.ActualHeight;

        Canvas.DrawRectangle(0, 0, CanvasWidth, CanvasHeight, Colors.Black);

        var BoardWidth = CanvasWidth / TileSize;
        var BoardHeight = CanvasHeight / TileSize;

        GraphicsRect.Height = TileSize;
        GraphicsRect.Width = TileSize;

        GraphicsRect.Y = 0;

        for (var Y = 0; Y <= BoardHeight; Y++)
        {
          GraphicsRect.X = 0;

          for (var X = 0; X <= BoardWidth; X++)
          {
            var GraphicsWaterBitmap = CanvasBitmapDictionary.GetOrAdd(WaterImage, K => LoadCanvasBitmap(Canvas, K));

            Canvas.DrawImage(GraphicsWaterBitmap, GraphicsRect, ImageSourceRect, 1.0F);

            GraphicsRect.X += TileSize;
          }

          GraphicsRect.Y += TileSize;
        }

        Canvas.DrawText("Win2D (" + BoardWidth + " by " + BoardHeight + " @ " + TileSize + "x" + TileSize + ") " + FPSLastCount + " fps", 8, CanvasHeight - 32, Colors.White);
      };
    }

    
    private Microsoft.Graphics.Canvas.CanvasBitmap LoadCanvasBitmap(Microsoft.Graphics.Canvas.CanvasDrawingSession DS, Inv.Image Image)
    {
      var Task = LoadCanvasBitmapTask(DS, Image);

      Task.Wait();

      return Task.Result;
    }

    private async Task<Microsoft.Graphics.Canvas.CanvasBitmap> LoadCanvasBitmapTask(Microsoft.Graphics.Canvas.CanvasDrawingSession DS, Inv.Image Image)
    {
      using (var MemoryStream = new Windows.Storage.Streams.InMemoryRandomAccessStream())
      {
        using (var DataWriter = new Windows.Storage.Streams.DataWriter(MemoryStream))
        {
          // Write the bytes to the stream
          DataWriter.WriteBytes(Image.GetBuffer());

          // Store the bytes to the MemoryStream
          await DataWriter.StoreAsync();

          // Not necessary, but do it anyway
          await DataWriter.FlushAsync();

          // Detach from the Memory stream so we don't close it
          DataWriter.DetachStream();
        }

        MemoryStream.Seek(0);

        //return await Microsoft.Graphics.Canvas.CanvasBitmap.LoadAsync(DS, MemoryStream, 96);

        var LoadTask = Microsoft.Graphics.Canvas.CanvasBitmap.LoadAsync(DS, MemoryStream, 96).AsTask();
        LoadTask.Wait();
        return LoadTask.Result;
      }
    }
    */
  }
}