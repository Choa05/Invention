﻿/*! 1 !*/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Diagnostics;
using System.Reflection;
using System.Threading;
using Inv.Support;

namespace Inv
{
  public sealed class WpfDeviceEmulation
  {
    private WpfDeviceEmulation(string Name, int Width, int Height)
    {
      this.Name = Name;
      this.Width = Width;
      this.Height = Height;
    }

    public string Name { get; private set; }
    public int Width { get; private set; }
    public int Height { get; private set; }

    public static readonly WpfDeviceEmulation iPhone5 = new WpfDeviceEmulation("Apple iPhone 5", 320, 568);
    public static readonly WpfDeviceEmulation iPhone6 = new WpfDeviceEmulation("Apple iPhone 6", 375, 667);
    public static readonly WpfDeviceEmulation iPhone6Plus = new WpfDeviceEmulation("Apple iPhone 6+", 414, 736);
    public static readonly WpfDeviceEmulation iPad2 = new WpfDeviceEmulation("Apple iPad 2", 1024, 768);
    public static readonly WpfDeviceEmulation ZteZ752C = new WpfDeviceEmulation("Zte Z752C", 320, 533);
    public static readonly WpfDeviceEmulation Nexus5 = new WpfDeviceEmulation("Google Nexus 5", 360, 592);
    public static readonly WpfDeviceEmulation Nexus7 = new WpfDeviceEmulation("Google Nexus 7", 888, 600);
    public static readonly WpfDeviceEmulation Nexus9 = new WpfDeviceEmulation("Google Nexus 9", 1024, 768);
    public static readonly WpfDeviceEmulation GalaxyNote4 = new WpfDeviceEmulation("Samsung Galaxy Note 4", 450, 800); // 5.7"
    public static readonly WpfDeviceEmulation GalaxyS10 = new WpfDeviceEmulation("Samsung Galaxy Tab S 10\"", 1280, 800);
    public static readonly WpfDeviceEmulation Surface3 = new WpfDeviceEmulation("Microsoft Surface 3", 1600, 1064); // TODO: is this right?
    public static readonly WpfDeviceEmulation VaioDuo11 = new WpfDeviceEmulation("Sony Vaio Duo 11", 1920, 1080); // TODO: is this right?

    public static readonly WpfDeviceEmulation[] Array;

    static WpfDeviceEmulation()
    {
      var List = new DistinctList<WpfDeviceEmulation>();

      var TypeInfo = typeof(WpfDeviceEmulation).GetReflectionInfo();

      foreach (var ReflectionField in TypeInfo.GetReflectionFields().Where(F => F.IsStatic && F.FieldType == typeof(WpfDeviceEmulation)))
        List.Add((WpfDeviceEmulation)ReflectionField.GetValue(null));

      Array = List.ToArray();
    }
  }
}
