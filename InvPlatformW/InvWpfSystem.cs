﻿/*! 1 !*/
using System;
  using System.Runtime.InteropServices;

namespace Inv
{
  /// <summary>
  /// Provides detailed information about the host operating system.
  /// </summary>
  internal static class WpfSystem
  {
    /// <summary>
    /// Determines if the current application is 32 or 64-bit.
    /// </summary>
    public static int Bits
    {
      get
      {
        return IntPtr.Size * 8;
      }
    }
    /// <summary>
    /// Gets the edition of the operating system running on this computer.
    /// </summary>
    public static string Edition
    {
      get
      {
        if (s_Edition != null)
          return s_Edition; 

        var edition = string.Empty;

        var osVersion = Environment.OSVersion;
        var osVersionInfo = new OSVERSIONINFOEX();
        osVersionInfo.dwOSVersionInfoSize = Marshal.SizeOf(typeof(OSVERSIONINFOEX));

        if (GetVersionEx(ref osVersionInfo))
        {
          int majorVersion = osVersion.Version.Major;
          int minorVersion = osVersion.Version.Minor;
          byte productType = osVersionInfo.wProductType;
          short suiteMask = osVersionInfo.wSuiteMask;

          #region VERSION 4
          if (majorVersion == 4)
          {
            if (productType == VER_NT_WORKSTATION)
            {
              // Windows NT 4.0 Workstation
              edition = "Workstation";
            }
            else if (productType == VER_NT_SERVER)
            {
              if ((suiteMask & VER_SUITE_ENTERPRISE) != 0)
              {
                // Windows NT 4.0 Server Enterprise
                edition = "Enterprise Server";
              }
              else
              {
                // Windows NT 4.0 Server
                edition = "Standard Server";
              }
            }
          }
          #endregion VERSION 4

          #region VERSION 5
          else if (majorVersion == 5)
          {
            if (productType == VER_NT_WORKSTATION)
            {
              if ((suiteMask & VER_SUITE_PERSONAL) != 0)
              {
                // Windows XP Home Edition
                edition = "Home";
              }
              else
              {
                // Windows XP / Windows 2000 Professional
                edition = "Professional";
              }
            }
            else if (productType == VER_NT_SERVER)
            {
              if (minorVersion == 0)
              {
                if ((suiteMask & VER_SUITE_DATACENTER) != 0)
                {
                  // Windows 2000 Datacenter Server
                  edition = "Datacenter Server";
                }
                else if ((suiteMask & VER_SUITE_ENTERPRISE) != 0)
                {
                  // Windows 2000 Advanced Server
                  edition = "Advanced Server";
                }
                else
                {
                  // Windows 2000 Server
                  edition = "Server";
                }
              }
              else
              {
                if ((suiteMask & VER_SUITE_DATACENTER) != 0)
                {
                  // Windows Server 2003 Datacenter Edition
                  edition = "Datacenter";
                }
                else if ((suiteMask & VER_SUITE_ENTERPRISE) != 0)
                {
                  // Windows Server 2003 Enterprise Edition
                  edition = "Enterprise";
                }
                else if ((suiteMask & VER_SUITE_BLADE) != 0)
                {
                  // Windows Server 2003 Web Edition
                  edition = "Web Edition";
                }
                else
                {
                  // Windows Server 2003 Standard Edition
                  edition = "Standard";
                }
              }
            }
          }
          #endregion VERSION 5

          #region VERSION 6
          else if (majorVersion == 6)
          {
            int ed;
            if (GetProductInfo(majorVersion, minorVersion,
              osVersionInfo.wServicePackMajor, osVersionInfo.wServicePackMinor,
              out ed))
            {
              // TODO: more editions here: https://msdn.microsoft.com/en-us/library/windows/desktop/ms724358(v=vs.85).aspx

              switch (ed)
              {
                case PRODUCT_BUSINESS:
                  edition = "Business";
                  break;
                case PRODUCT_BUSINESS_N:
                  edition = "Business N";
                  break;
                case PRODUCT_CLUSTER_SERVER:
                  edition = "HPC Edition";
                  break;
                case PRODUCT_DATACENTER_SERVER:
                  edition = "Datacenter Server";
                  break;
                case PRODUCT_DATACENTER_SERVER_CORE:
                  edition = "Datacenter Server (core installation)";
                  break;
                case PRODUCT_ENTERPRISE:
                  edition = "Enterprise";
                  break;
                case PRODUCT_ENTERPRISE_N:
                  edition = "Enterprise N";
                  break;
                case PRODUCT_ENTERPRISE_SERVER:
                  edition = "Enterprise Server";
                  break;
                case PRODUCT_ENTERPRISE_SERVER_CORE:
                  edition = "Enterprise Server (core installation)";
                  break;
                case PRODUCT_ENTERPRISE_SERVER_CORE_V:
                  edition = "Enterprise Server without Hyper-V (core installation)";
                  break;
                case PRODUCT_ENTERPRISE_SERVER_IA64:
                  edition = "Enterprise Server for Itanium-based Systems";
                  break;
                case PRODUCT_ENTERPRISE_SERVER_V:
                  edition = "Enterprise Server without Hyper-V";
                  break;
                case PRODUCT_HOME_BASIC:
                  edition = "Home Basic";
                  break;
                case PRODUCT_HOME_BASIC_N:
                  edition = "Home Basic N";
                  break;
                case PRODUCT_HOME_PREMIUM:
                  edition = "Home Premium";
                  break;
                case PRODUCT_HOME_PREMIUM_N:
                  edition = "Home Premium N";
                  break;
                case PRODUCT_HYPERV:
                  edition = "Microsoft Hyper-V Server";
                  break;
                case PRODUCT_MEDIUMBUSINESS_SERVER_MANAGEMENT:
                  edition = "Windows Essential Business Management Server";
                  break;
                case PRODUCT_MEDIUMBUSINESS_SERVER_MESSAGING:
                  edition = "Windows Essential Business Messaging Server";
                  break;
                case PRODUCT_MEDIUMBUSINESS_SERVER_SECURITY:
                  edition = "Windows Essential Business Security Server";
                  break;
                case PRODUCT_SERVER_FOR_SMALLBUSINESS:
                  edition = "Windows Essential Server Solutions";
                  break;
                case PRODUCT_SERVER_FOR_SMALLBUSINESS_V:
                  edition = "Windows Essential Server Solutions without Hyper-V";
                  break;
                case PRODUCT_SMALLBUSINESS_SERVER:
                  edition = "Windows Small Business Server";
                  break;
                case PRODUCT_STANDARD_SERVER:
                  edition = "Standard Server";
                  break;
                case PRODUCT_STANDARD_SERVER_CORE:
                  edition = "Standard Server (core installation)";
                  break;
                case PRODUCT_STANDARD_SERVER_CORE_V:
                  edition = "Standard Server without Hyper-V (core installation)";
                  break;
                case PRODUCT_STANDARD_SERVER_V:
                  edition = "Standard Server without Hyper-V";
                  break;
                case PRODUCT_STARTER:
                  edition = "Starter";
                  break;
                case PRODUCT_STORAGE_ENTERPRISE_SERVER:
                  edition = "Enterprise Storage Server";
                  break;
                case PRODUCT_STORAGE_EXPRESS_SERVER:
                  edition = "Express Storage Server";
                  break;
                case PRODUCT_STORAGE_STANDARD_SERVER:
                  edition = "Standard Storage Server";
                  break;
                case PRODUCT_STORAGE_WORKGROUP_SERVER:
                  edition = "Workgroup Storage Server";
                  break;
                case PRODUCT_UNDEFINED:
                  edition = "Unknown product";
                  break;
                case PRODUCT_ULTIMATE:
                  edition = "Ultimate";
                  break;
                case PRODUCT_ULTIMATE_N:
                  edition = "Ultimate N";
                  break;
                case PRODUCT_WEB_SERVER:
                  edition = "Web Server";
                  break;
                case PRODUCT_WEB_SERVER_CORE:
                  edition = "Web Server (core installation)";
                  break;
                case PRODUCT_PROFESSIONAL:
                  edition = "Professional";
                  break;

                default:
                  edition = "Unknown Edition";
                  break;
              }
            }
          }
          #endregion VERSION 6
        }

        s_Edition = edition;
        return edition;
      }
    }
    /// <summary>
    /// Gets the name of the operating system running on this computer.
    /// </summary>
    public static string Name
    {
      get
      {
        if (s_Name != null)
          return s_Name;  

        var name = "unknown";

        var osVersion = Environment.OSVersion;
        var osVersionInfo = new OSVERSIONINFOEX();
        osVersionInfo.dwOSVersionInfoSize = Marshal.SizeOf(typeof(OSVERSIONINFOEX));

        if (GetVersionEx(ref osVersionInfo))
        {
          var majorVersion = osVersion.Version.Major;
          var minorVersion = osVersion.Version.Minor;

          switch (osVersion.Platform)
          {
            case PlatformID.Win32Windows:
              {
                if (majorVersion == 4)
                {
                  string csdVersion = osVersionInfo.szCSDVersion;
                  switch (minorVersion)
                  {
                    case 0:
                      if (csdVersion == "B" || csdVersion == "C")
                        name = "Windows 95 OSR2";
                      else
                        name = "Windows 95";
                      break;
                    case 10:
                      if (csdVersion == "A")
                        name = "Windows 98 Second Edition";
                      else
                        name = "Windows 98";
                      break;
                    case 90:
                      name = "Windows Me";
                      break;
                  }
                }
                break;
              }

            case PlatformID.Win32NT:
              {
                switch (majorVersion)
                {
                  case 6:
                    switch (minorVersion)
                    {
                      case 0:
                        name = "Windows Vista";
                        break;
                      case 1:
                        name = "Windows 7";
                        break;
                      case 2:
                        name = "Windows 8";
                        break;
                      case 3:
                        // TODO: https://msdn.microsoft.com/en-us/library/windows/desktop/dn481241(v=vs.85).aspx
                        name = "Windows 8.1";
                        break;
                    }
                    break;
                  case 10:
                    name = "Windows 10";
                    break;
                }
                break;
              }
          }
        }

        s_Name = name;
        return name;
      }
    }
    /// <summary>
    /// Gets the service pack information of the operating system running on this computer.
    /// </summary>
    public static string ServicePack
    {
      get
      {
        var servicePack = String.Empty;
        var osVersionInfo = new OSVERSIONINFOEX();
        osVersionInfo.dwOSVersionInfoSize = Marshal.SizeOf(typeof(OSVERSIONINFOEX));

        if (GetVersionEx(ref osVersionInfo))
          servicePack = osVersionInfo.szCSDVersion;

        return servicePack;
      }
    }
    /// <summary>
    /// Gets the build version number of the operating system running on this computer.
    /// </summary>
    public static int BuildVersion
    {
      get
      {
        return Environment.OSVersion.Version.Build;
      }
    }
    /// <summary>
    /// Gets the full version string of the operating system running on this computer.
    /// </summary>
    public static string VersionString
    {
      get
      {
        return Environment.OSVersion.Version.ToString();
      }
    }
    /// <summary>
    /// Gets the full version of the operating system running on this computer.
    /// </summary>
    public static Version Version
    {
      get
      {
        return Environment.OSVersion.Version;
      }
    }
    /// <summary>
    /// Gets the major version number of the operating system running on this computer.
    /// </summary>
    public static int MajorVersion
    {
      get
      {
        return Environment.OSVersion.Version.Major;
      }
    }
    /// <summary>
    /// Gets the minor version number of the operating system running on this computer.
    /// </summary>
    public static int MinorVersion
    {
      get
      {
        return Environment.OSVersion.Version.Minor;
      }
    }
    /// <summary>
    /// Gets the revision version number of the operating system running on this computer.
    /// </summary>
    public static int RevisionVersion
    {
      get
      {
        return Environment.OSVersion.Version.Revision;
      }
    }

    public static double PhysicalWidthInches
    {
      get
      {
        if (s_PhysicalWidthInches == double.NaN)
          LoadDeviceDimensions();

        return s_PhysicalWidthInches;
      }
    }
    public static double PhysicalHeightInches
    {
      get
      {
        if (s_PhysicalHeightInches == double.NaN)
          LoadDeviceDimensions();

        return s_PhysicalHeightInches;
      }
    }

    public static bool HasTouchInput()
    {
      // detect device is a touch screen, not how many touches (i.e. single touch vs multi-touch).
      foreach (System.Windows.Input.TabletDevice tabletDevice in System.Windows.Input.Tablet.TabletDevices)
      {
        if (tabletDevice.Type == System.Windows.Input.TabletDeviceType.Touch)
          return true;
      }

      return false;
    }

    private static void LoadDeviceDimensions()
    {
      const double OneCmInInches = 0.393700787;
      var DesktopHandle = System.Drawing.Graphics.FromHwnd(IntPtr.Zero).GetHdc();
      var PhysicalWidthMms = GetDeviceCaps(DesktopHandle, (int)WpfDeviceCap.HORZSIZE);
      var PhysicalHeightMms = GetDeviceCaps(DesktopHandle, (int)WpfDeviceCap.VERTSIZE);
      s_PhysicalWidthInches = PhysicalWidthMms * OneCmInInches / 10;
      s_PhysicalHeightInches = PhysicalHeightMms * OneCmInInches / 10;
    }

    [System.Runtime.InteropServices.DllImport("gdi32.dll", ExactSpelling = true)]
    private static extern int GetDeviceCaps(IntPtr hDC, int nIndex);

    private static double s_PhysicalWidthInches = double.NaN;
    private static double s_PhysicalHeightInches = double.NaN;
    private static string s_Edition;
    private static string s_Name;

    [DllImport("Kernel32.dll")]
    private static extern bool GetProductInfo(
      int osMajorVersion,
      int osMinorVersion,
      int spMajorVersion,
      int spMinorVersion,
      out int edition);

    [DllImport("kernel32.dll")]
    private static extern bool GetVersionEx(ref OSVERSIONINFOEX osVersionInfo);

    [StructLayout(LayoutKind.Sequential)]
    private struct OSVERSIONINFOEX
    {
      public int dwOSVersionInfoSize;
      public int dwMajorVersion;
      public int dwMinorVersion;
      public int dwBuildNumber;
      public int dwPlatformId;
      [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
      public string szCSDVersion;
      public short wServicePackMajor;
      public short wServicePackMinor;
      public short wSuiteMask;
      public byte wProductType;
      public byte wReserved;
    }

    private const int PRODUCT_UNDEFINED = 0x00000000;
    private const int PRODUCT_ULTIMATE = 0x00000001;
    private const int PRODUCT_HOME_BASIC = 0x00000002;
    private const int PRODUCT_HOME_PREMIUM = 0x00000003;
    private const int PRODUCT_ENTERPRISE = 0x00000004;
    private const int PRODUCT_HOME_BASIC_N = 0x00000005;
    private const int PRODUCT_BUSINESS = 0x00000006;
    private const int PRODUCT_STANDARD_SERVER = 0x00000007;
    private const int PRODUCT_DATACENTER_SERVER = 0x00000008;
    private const int PRODUCT_SMALLBUSINESS_SERVER = 0x00000009;
    private const int PRODUCT_ENTERPRISE_SERVER = 0x0000000A;
    private const int PRODUCT_STARTER = 0x0000000B;
    private const int PRODUCT_DATACENTER_SERVER_CORE = 0x0000000C;
    private const int PRODUCT_STANDARD_SERVER_CORE = 0x0000000D;
    private const int PRODUCT_ENTERPRISE_SERVER_CORE = 0x0000000E;
    private const int PRODUCT_ENTERPRISE_SERVER_IA64 = 0x0000000F;
    private const int PRODUCT_BUSINESS_N = 0x00000010;
    private const int PRODUCT_WEB_SERVER = 0x00000011;
    private const int PRODUCT_CLUSTER_SERVER = 0x00000012;
    private const int PRODUCT_HOME_SERVER = 0x00000013;
    private const int PRODUCT_STORAGE_EXPRESS_SERVER = 0x00000014;
    private const int PRODUCT_STORAGE_STANDARD_SERVER = 0x00000015;
    private const int PRODUCT_STORAGE_WORKGROUP_SERVER = 0x00000016;
    private const int PRODUCT_STORAGE_ENTERPRISE_SERVER = 0x00000017;
    private const int PRODUCT_SERVER_FOR_SMALLBUSINESS = 0x00000018;
    private const int PRODUCT_SMALLBUSINESS_SERVER_PREMIUM = 0x00000019;
    private const int PRODUCT_HOME_PREMIUM_N = 0x0000001A;
    private const int PRODUCT_ENTERPRISE_N = 0x0000001B;
    private const int PRODUCT_ULTIMATE_N = 0x0000001C;
    private const int PRODUCT_WEB_SERVER_CORE = 0x0000001D;
    private const int PRODUCT_MEDIUMBUSINESS_SERVER_MANAGEMENT = 0x0000001E;
    private const int PRODUCT_MEDIUMBUSINESS_SERVER_SECURITY = 0x0000001F;
    private const int PRODUCT_MEDIUMBUSINESS_SERVER_MESSAGING = 0x00000020;
    private const int PRODUCT_SERVER_FOR_SMALLBUSINESS_V = 0x00000023;
    private const int PRODUCT_STANDARD_SERVER_V = 0x00000024;
    private const int PRODUCT_ENTERPRISE_SERVER_V = 0x00000026;
    private const int PRODUCT_STANDARD_SERVER_CORE_V = 0x00000028;
    private const int PRODUCT_ENTERPRISE_SERVER_CORE_V = 0x00000029;
    private const int PRODUCT_PROFESSIONAL = 0x00000030;
    private const int PRODUCT_HYPERV = 0x0000002A;

    private const int VER_NT_WORKSTATION = 1;
    private const int VER_NT_DOMAIN_CONTROLLER = 2;
    private const int VER_NT_SERVER = 3;
    private const int VER_SUITE_SMALLBUSINESS = 1;
    private const int VER_SUITE_ENTERPRISE = 2;
    private const int VER_SUITE_TERMINAL = 16;
    private const int VER_SUITE_DATACENTER = 128;
    private const int VER_SUITE_SINGLEUSERTS = 256;
    private const int VER_SUITE_PERSONAL = 512;
    private const int VER_SUITE_BLADE = 1024;
  }

  internal enum WpfDeviceCap
  {
    #region http://pinvoke.net/default.aspx/gdi32/GetDeviceCaps.html
    /// <summary>
    /// Device driver version
    /// </summary>
    DRIVERVERSION = 0,
    /// <summary>
    /// Device classification
    /// </summary>
    TECHNOLOGY = 2,
    /// <summary>
    /// Horizontal size in millimeters
    /// </summary>
    HORZSIZE = 4,
    /// <summary>
    /// Vertical size in millimeters
    /// </summary>
    VERTSIZE = 6,
    /// <summary>
    /// Horizontal width in pixels
    /// </summary>
    HORZRES = 8,
    /// <summary>
    /// Vertical height in pixels
    /// </summary>
    VERTRES = 10,
    /// <summary>
    /// Number of bits per pixel
    /// </summary>
    BITSPIXEL = 12,
    /// <summary>
    /// Number of planes
    /// </summary>
    PLANES = 14,
    /// <summary>
    /// Number of brushes the device has
    /// </summary>
    NUMBRUSHES = 16,
    /// <summary>
    /// Number of pens the device has
    /// </summary>
    NUMPENS = 18,
    /// <summary>
    /// Number of markers the device has
    /// </summary>
    NUMMARKERS = 20,
    /// <summary>
    /// Number of fonts the device has
    /// </summary>
    NUMFONTS = 22,
    /// <summary>
    /// Number of colors the device supports
    /// </summary>
    NUMCOLORS = 24,
    /// <summary>
    /// Size required for device descriptor
    /// </summary>
    PDEVICESIZE = 26,
    /// <summary>
    /// Curve capabilities
    /// </summary>
    CURVECAPS = 28,
    /// <summary>
    /// Line capabilities
    /// </summary>
    LINECAPS = 30,
    /// <summary>
    /// Polygonal capabilities
    /// </summary>
    POLYGONALCAPS = 32,
    /// <summary>
    /// Text capabilities
    /// </summary>
    TEXTCAPS = 34,
    /// <summary>
    /// Clipping capabilities
    /// </summary>
    CLIPCAPS = 36,
    /// <summary>
    /// Bitblt capabilities
    /// </summary>
    RASTERCAPS = 38,
    /// <summary>
    /// Length of the X leg
    /// </summary>
    ASPECTX = 40,
    /// <summary>
    /// Length of the Y leg
    /// </summary>
    ASPECTY = 42,
    /// <summary>
    /// Length of the hypotenuse
    /// </summary>
    ASPECTXY = 44,
    /// <summary>
    /// Shading and Blending caps
    /// </summary>
    SHADEBLENDCAPS = 45,

    /// <summary>
    /// Logical pixels inch in X
    /// </summary>
    LOGPIXELSX = 88,
    /// <summary>
    /// Logical pixels inch in Y
    /// </summary>
    LOGPIXELSY = 90,

    /// <summary>
    /// Number of entries in physical palette
    /// </summary>
    SIZEPALETTE = 104,
    /// <summary>
    /// Number of reserved entries in palette
    /// </summary>
    NUMRESERVED = 106,
    /// <summary>
    /// Actual color resolution
    /// </summary>
    COLORRES = 108,

    // Printing related DeviceCaps. These replace the appropriate Escapes
    /// <summary>
    /// Physical Width in device units
    /// </summary>
    PHYSICALWIDTH = 110,
    /// <summary>
    /// Physical Height in device units
    /// </summary>
    PHYSICALHEIGHT = 111,
    /// <summary>
    /// Physical Printable Area x margin
    /// </summary>
    PHYSICALOFFSETX = 112,
    /// <summary>
    /// Physical Printable Area y margin
    /// </summary>
    PHYSICALOFFSETY = 113,
    /// <summary>
    /// Scaling factor x
    /// </summary>
    SCALINGFACTORX = 114,
    /// <summary>
    /// Scaling factor y
    /// </summary>
    SCALINGFACTORY = 115,

    /// <summary>
    /// Current vertical refresh rate of the display device (for displays only) in Hz
    /// </summary>
    VREFRESH = 116,
    /// <summary>
    /// Vertical height of entire desktop in pixels
    /// </summary>
    DESKTOPVERTRES = 117,
    /// <summary>
    /// Horizontal width of entire desktop in pixels
    /// </summary>
    DESKTOPHORZRES = 118,
    /// <summary>
    /// Preferred blt alignment
    /// </summary>
    BLTALIGNMENT = 119

    #endregion
  }
}