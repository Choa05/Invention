﻿/*! 5 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inv.Manual
{
  public class Program
  {
    [STAThread]
    static void Main(string[] args)
    {
      Inv.WpfShell.CheckRequirements(() =>
      {
        Inv.WpfShell.DeviceEmulation = Inv.WpfDeviceEmulation.iPad2;
        Inv.WpfShell.DeviceRotation = false;
        Inv.WpfShell.FullScreenMode = false;
        Inv.WpfShell.DefaultWindowWidth = 1920;
        Inv.WpfShell.DefaultWindowHeight = 1080;

        Inv.WpfShell.Run(A => Inv.Manual.Shell.Install(A));
      });
    }
  }
}