﻿/*! 3 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv.Manual
{
  internal sealed class AboutSurface : Inv.Mimic<Inv.Surface>
  {
    public AboutSurface(Application Application)
    {
      this.Base = Application.NewSurface();

      Base.Background.Colour = Theme.SheetColour;
      
      var Dialog = new DialogPanel(Base);
      Base.Content = Dialog;

      var OuterDock = Base.NewVerticalDock();
      Dialog.AddLayer(OuterDock);

      var NavigatePanel = new NavigatePanel(Base);
      OuterDock.AddFooter(NavigatePanel);
      NavigatePanel.BackIsEnabled = false;
      NavigatePanel.BackText = Resources.Texts.Version ?? "?VERSION?"; // NOTE: doesn't work in Play.
      NavigatePanel.NextIsEnabled = true;
      NavigatePanel.NextText = "MANUAL >";
      NavigatePanel.TitleIsEnabled = false;
#if DEBUG
      NavigatePanel.TitleText = Base.Window.Width + " x " + Base.Window.Height;
#endif
      NavigatePanel.NextEvent += () => Application.NextIntroduction();
      
      var Scroll = Base.NewVerticalScroll();
      OuterDock.AddClient(Scroll);

      var Dock = Base.NewVerticalDock();
      Scroll.Content = Dock;

      var SubjectPanel = new SubjectPanel(Base);
      Dock.AddHeader(SubjectPanel);
      SubjectPanel.Text = "About";

      this.Document = new DocumentPanel(Base);
      Dock.AddHeader(Document);

      var ControlPanel = new ControlPanel(Base, "");
      Dock.AddFooter(ControlPanel);
      ControlPanel.Margin.Set(Theme.DocumentGap, 0, Theme.DocumentGap, Theme.DocumentGap);
      
      var UpdatesButton = ControlPanel.AddButton("UPDATES");
      UpdatesButton.SelectEvent += () =>
      {
        var Flyout = Dialog.NewFlyoutPanel();

        var Frame = Base.NewFrame();
        Flyout.Content = Frame;
        Frame.Border.Set(4);
        Frame.Padding.Set(4);
        Frame.Border.Colour = Inv.Colour.DimGray;
        Frame.Background.Colour = Theme.SheetColour;
        Frame.Margin.Set(44);
        Frame.Alignment.Center();

        var Code = new CodePanel(Base);
        Frame.Content = Code;
        Code.Text = Resources.Texts.Updates ?? "?UPDATES?";

        Flyout.Show();
      };

      var LicenseButton = ControlPanel.AddButton("LICENSE");
      LicenseButton.SelectEvent += () =>
      {
        var Flyout = Dialog.NewFlyoutPanel();

        var Frame = Base.NewFrame();
        Flyout.Content = Frame;
        Frame.Border.Set(4);
        Frame.Border.Colour = Inv.Colour.DimGray;
        Frame.Background.Colour = Theme.SheetColour;
        Frame.Padding.Set(4);
        Frame.Margin.Set(44);
        Frame.Alignment.Center();

        var Code = new CodePanel(Base);
        Frame.Content = Code;

        using (var LicenseStream = typeof(AboutSurface).GetReflectionInfo().Assembly.GetManifestResourceStream("LICENSE"))
        {
          if (LicenseStream == null)
          {
            Code.Text = "https://gitlab.com/hodgskin-callan/Invention/blob/master/LICENSE";
          }
          else
          {
            using (var LicenseReader = new StreamReader(LicenseStream))
              Code.Text = LicenseReader.ReadToEnd();
          }
        }

        Flyout.Show();
      };

      var RateButton = ControlPanel.AddButton("RATE & REVIEW");
      RateButton.SelectEvent += () =>
      {
        Base.Window.Application.Market.Browse("1195335633", "com.x10host.invention.manual", "808CallanHodgskin.InventionManual_ax6pb6c2q0eqa");
      };

      Base.GestureBackwardEvent += () => NavigatePanel.Next();
      Base.ArrangeEvent += () =>
      {
        ControlPanel.Arrange(Base.Window.Width - (Theme.DocumentGap * 2));
      };
    }

    public DocumentPanel Document { get; private set; }
  }
}
