/*! 8 !*/

namespace Inv.Material
{
  public static class Resources
  {
    static Resources()
    {
      global::Inv.Resource.Foundation.Import(typeof(Resources), "Resources.InvMaterial.InvResourcePackage.rs");
    }

    public static readonly ResourcesImages Images;
  }

  public sealed class ResourcesImages
  {
    public ResourcesImages() { }

    ///<Summary>0.6 KB</Summary>
    public readonly global::Inv.Image AccessibilityBlack;
    ///<Summary>0.6 KB</Summary>
    public readonly global::Inv.Image AccessibilityWhite;
    ///<Summary>1.8 KB</Summary>
    public readonly global::Inv.Image AccessibleBlack;
    ///<Summary>1.8 KB</Summary>
    public readonly global::Inv.Image AccessibleWhite;
    ///<Summary>0.5 KB</Summary>
    public readonly global::Inv.Image AccountBalanceBlack;
    ///<Summary>0.9 KB</Summary>
    public readonly global::Inv.Image AccountBalanceWalletBlack;
    ///<Summary>1.0 KB</Summary>
    public readonly global::Inv.Image AccountBalanceWalletWhite;
    ///<Summary>0.6 KB</Summary>
    public readonly global::Inv.Image AccountBalanceWhite;
    ///<Summary>1.2 KB</Summary>
    public readonly global::Inv.Image AccountBoxBlack;
    ///<Summary>1.2 KB</Summary>
    public readonly global::Inv.Image AccountBoxWhite;
    ///<Summary>2.3 KB</Summary>
    public readonly global::Inv.Image AccountCircleBlack;
    ///<Summary>2.4 KB</Summary>
    public readonly global::Inv.Image AccountCircleWhite;
    ///<Summary>1.1 KB</Summary>
    public readonly global::Inv.Image AddAlertBlack;
    ///<Summary>1.1 KB</Summary>
    public readonly global::Inv.Image AddAlertWhite;
    ///<Summary>1.7 KB</Summary>
    public readonly global::Inv.Image AddShoppingCartBlack;
    ///<Summary>1.8 KB</Summary>
    public readonly global::Inv.Image AddShoppingCartWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image AddToQueueBlack;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image AddToQueueWhite;
    ///<Summary>0.8 KB</Summary>
    public readonly global::Inv.Image AirplayBlack;
    ///<Summary>0.8 KB</Summary>
    public readonly global::Inv.Image AirplayWhite;
    ///<Summary>2.8 KB</Summary>
    public readonly global::Inv.Image AlarmAddBlack;
    ///<Summary>3.0 KB</Summary>
    public readonly global::Inv.Image AlarmAddWhite;
    ///<Summary>3.1 KB</Summary>
    public readonly global::Inv.Image AlarmBlack;
    ///<Summary>2.7 KB</Summary>
    public readonly global::Inv.Image AlarmOffBlack;
    ///<Summary>2.8 KB</Summary>
    public readonly global::Inv.Image AlarmOffWhite;
    ///<Summary>3.0 KB</Summary>
    public readonly global::Inv.Image AlarmOnBlack;
    ///<Summary>3.2 KB</Summary>
    public readonly global::Inv.Image AlarmOnWhite;
    ///<Summary>3.3 KB</Summary>
    public readonly global::Inv.Image AlarmWhite;
    ///<Summary>2.1 KB</Summary>
    public readonly global::Inv.Image AlbumBlack;
    ///<Summary>2.2 KB</Summary>
    public readonly global::Inv.Image AlbumWhite;
    ///<Summary>2.0 KB</Summary>
    public readonly global::Inv.Image AllOutBlack;
    ///<Summary>2.0 KB</Summary>
    public readonly global::Inv.Image AllOutWhite;
    ///<Summary>1.3 KB</Summary>
    public readonly global::Inv.Image AndroidBlack;
    ///<Summary>1.3 KB</Summary>
    public readonly global::Inv.Image AndroidWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image AnnouncementBlack;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image AnnouncementWhite;
    ///<Summary>0.8 KB</Summary>
    public readonly global::Inv.Image ArtTrackBlack;
    ///<Summary>0.8 KB</Summary>
    public readonly global::Inv.Image ArtTrackWhite;
    ///<Summary>0.6 KB</Summary>
    public readonly global::Inv.Image AspectRatioBlack;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image AspectRatioWhite;
    ///<Summary>0.6 KB</Summary>
    public readonly global::Inv.Image AssessmentBlack;
    ///<Summary>0.6 KB</Summary>
    public readonly global::Inv.Image AssessmentWhite;
    ///<Summary>0.9 KB</Summary>
    public readonly global::Inv.Image AssignmentBlack;
    ///<Summary>1.4 KB</Summary>
    public readonly global::Inv.Image AssignmentIndBlack;
    ///<Summary>1.5 KB</Summary>
    public readonly global::Inv.Image AssignmentIndWhite;
    ///<Summary>0.9 KB</Summary>
    public readonly global::Inv.Image AssignmentLateBlack;
    ///<Summary>0.9 KB</Summary>
    public readonly global::Inv.Image AssignmentLateWhite;
    ///<Summary>1.0 KB</Summary>
    public readonly global::Inv.Image AssignmentReturnBlack;
    ///<Summary>1.0 KB</Summary>
    public readonly global::Inv.Image AssignmentReturnedBlack;
    ///<Summary>1.1 KB</Summary>
    public readonly global::Inv.Image AssignmentReturnedWhite;
    ///<Summary>1.1 KB</Summary>
    public readonly global::Inv.Image AssignmentReturnWhite;
    ///<Summary>1.2 KB</Summary>
    public readonly global::Inv.Image AssignmentTurnedInBlack;
    ///<Summary>1.2 KB</Summary>
    public readonly global::Inv.Image AssignmentTurnedInWhite;
    ///<Summary>0.9 KB</Summary>
    public readonly global::Inv.Image AssignmentWhite;
    ///<Summary>1.7 KB</Summary>
    public readonly global::Inv.Image AutorenewBlack;
    ///<Summary>1.9 KB</Summary>
    public readonly global::Inv.Image AutorenewWhite;
    ///<Summary>2.4 KB</Summary>
    public readonly global::Inv.Image AvTimerBlack;
    ///<Summary>2.5 KB</Summary>
    public readonly global::Inv.Image AvTimerWhite;
    ///<Summary>1.4 KB</Summary>
    public readonly global::Inv.Image BackupBlack;
    ///<Summary>1.5 KB</Summary>
    public readonly global::Inv.Image BackupWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image BookBlack;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image BookmarkBlack;
    ///<Summary>0.9 KB</Summary>
    public readonly global::Inv.Image BookmarkBorderBlack;
    ///<Summary>0.9 KB</Summary>
    public readonly global::Inv.Image BookmarkBorderWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image BookmarkWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image BookWhite;
    ///<Summary>1.0 KB</Summary>
    public readonly global::Inv.Image BugReportBlack;
    ///<Summary>1.1 KB</Summary>
    public readonly global::Inv.Image BugReportWhite;
    ///<Summary>1.2 KB</Summary>
    public readonly global::Inv.Image BuildBlack;
    ///<Summary>1.3 KB</Summary>
    public readonly global::Inv.Image BuildWhite;
    ///<Summary>1.6 KB</Summary>
    public readonly global::Inv.Image CachedBlack;
    ///<Summary>1.7 KB</Summary>
    public readonly global::Inv.Image CachedWhite;
    ///<Summary>1.6 KB</Summary>
    public readonly global::Inv.Image CameraEnhanceBlack;
    ///<Summary>1.7 KB</Summary>
    public readonly global::Inv.Image CameraEnhanceWhite;
    ///<Summary>1.7 KB</Summary>
    public readonly global::Inv.Image CardGiftcardBlack;
    ///<Summary>1.8 KB</Summary>
    public readonly global::Inv.Image CardGiftcardWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image CardMembershipBlack;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image CardMembershipWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image CardTravelBlack;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image CardTravelWhite;
    ///<Summary>0.9 KB</Summary>
    public readonly global::Inv.Image ChangeHistoryBlack;
    ///<Summary>1.0 KB</Summary>
    public readonly global::Inv.Image ChangeHistoryWhite;
    ///<Summary>1.8 KB</Summary>
    public readonly global::Inv.Image CheckCircleBlack;
    ///<Summary>1.7 KB</Summary>
    public readonly global::Inv.Image CheckCircleWhite;
    ///<Summary>0.6 KB</Summary>
    public readonly global::Inv.Image ChromeReaderModeBlack;
    ///<Summary>0.6 KB</Summary>
    public readonly global::Inv.Image ChromeReaderModeWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image ClassBlack;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image ClassWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image ClosedCaptionBlack;
    ///<Summary>0.8 KB</Summary>
    public readonly global::Inv.Image ClosedCaptionWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image CodeBlack;
    ///<Summary>0.8 KB</Summary>
    public readonly global::Inv.Image CodeWhite;
    ///<Summary>0.5 KB</Summary>
    public readonly global::Inv.Image CompareArrowsBlack;
    ///<Summary>0.5 KB</Summary>
    public readonly global::Inv.Image CompareArrowsWhite;
    ///<Summary>3.1 KB</Summary>
    public readonly global::Inv.Image CopyrightBlack;
    ///<Summary>3.2 KB</Summary>
    public readonly global::Inv.Image CopyrightWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image CreditCardBlack;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image CreditCardWhite;
    ///<Summary>0.1 KB</Summary>
    public readonly global::Inv.Image DashboardBlack;
    ///<Summary>0.1 KB</Summary>
    public readonly global::Inv.Image DashboardWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image DateRangeBlack;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image DateRangeWhite;
    ///<Summary>0.5 KB</Summary>
    public readonly global::Inv.Image DeleteBlack;
    ///<Summary>0.5 KB</Summary>
    public readonly global::Inv.Image DeleteWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image DescriptionBlack;
    ///<Summary>0.8 KB</Summary>
    public readonly global::Inv.Image DescriptionWhite;
    ///<Summary>0.8 KB</Summary>
    public readonly global::Inv.Image DnsBlack;
    ///<Summary>0.8 KB</Summary>
    public readonly global::Inv.Image DnsWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image DoneAllBlack;
    ///<Summary>0.9 KB</Summary>
    public readonly global::Inv.Image DoneAllWhite;
    ///<Summary>0.4 KB</Summary>
    public readonly global::Inv.Image DoneBlack;
    ///<Summary>0.5 KB</Summary>
    public readonly global::Inv.Image DoneWhite;
    ///<Summary>2.2 KB</Summary>
    public readonly global::Inv.Image DonutLargeBlack;
    ///<Summary>2.2 KB</Summary>
    public readonly global::Inv.Image DonutLargeWhite;
    ///<Summary>1.8 KB</Summary>
    public readonly global::Inv.Image DonutSmallBlack;
    ///<Summary>1.8 KB</Summary>
    public readonly global::Inv.Image DonutSmallWhite;
    ///<Summary>4.8 KB</Summary>
    public readonly global::Inv.Image Edit;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image EjectBlack;
    ///<Summary>0.6 KB</Summary>
    public readonly global::Inv.Image EjectWhite;
    ///<Summary>0.1 KB</Summary>
    public readonly global::Inv.Image EqualizerBlack;
    ///<Summary>0.1 KB</Summary>
    public readonly global::Inv.Image EqualizerWhite;
    ///<Summary>1.5 KB</Summary>
    public readonly global::Inv.Image ErrorBlack;
    ///<Summary>2.5 KB</Summary>
    public readonly global::Inv.Image ErrorOutlineBlack;
    ///<Summary>2.6 KB</Summary>
    public readonly global::Inv.Image ErrorOutlineWhite;
    ///<Summary>1.6 KB</Summary>
    public readonly global::Inv.Image ErrorWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image EventBlack;
    ///<Summary>0.5 KB</Summary>
    public readonly global::Inv.Image EventSeatBlack;
    ///<Summary>0.5 KB</Summary>
    public readonly global::Inv.Image EventSeatWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image EventWhite;
    ///<Summary>0.8 KB</Summary>
    public readonly global::Inv.Image ExitToAppBlack;
    ///<Summary>0.8 KB</Summary>
    public readonly global::Inv.Image ExitToAppWhite;
    ///<Summary>0.6 KB</Summary>
    public readonly global::Inv.Image ExplicitBlack;
    ///<Summary>0.6 KB</Summary>
    public readonly global::Inv.Image ExplicitWhite;
    ///<Summary>2.3 KB</Summary>
    public readonly global::Inv.Image ExploreBlack;
    ///<Summary>2.5 KB</Summary>
    public readonly global::Inv.Image ExploreWhite;
    ///<Summary>1.2 KB</Summary>
    public readonly global::Inv.Image ExtensionBlack;
    ///<Summary>1.3 KB</Summary>
    public readonly global::Inv.Image ExtensionWhite;
    ///<Summary>2.5 KB</Summary>
    public readonly global::Inv.Image FaceBlack;
    ///<Summary>2.7 KB</Summary>
    public readonly global::Inv.Image FaceWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image FastForwardBlack;
    ///<Summary>0.8 KB</Summary>
    public readonly global::Inv.Image FastForwardWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image FastRewindBlack;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image FastRewindWhite;
    ///<Summary>1.5 KB</Summary>
    public readonly global::Inv.Image FavoriteBlack;
    ///<Summary>2.5 KB</Summary>
    public readonly global::Inv.Image FavoriteBorderBlack;
    ///<Summary>2.6 KB</Summary>
    public readonly global::Inv.Image FavoriteBorderWhite;
    ///<Summary>1.5 KB</Summary>
    public readonly global::Inv.Image FavoriteWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image FeedbackBlack;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image FeedbackWhite;
    ///<Summary>1.3 KB</Summary>
    public readonly global::Inv.Image FiberDvrBlack;
    ///<Summary>1.3 KB</Summary>
    public readonly global::Inv.Image FiberDvrWhite;
    ///<Summary>1.2 KB</Summary>
    public readonly global::Inv.Image FiberManualRecordBlack;
    ///<Summary>1.2 KB</Summary>
    public readonly global::Inv.Image FiberManualRecordWhite;
    ///<Summary>1.0 KB</Summary>
    public readonly global::Inv.Image FiberNewBlack;
    ///<Summary>1.0 KB</Summary>
    public readonly global::Inv.Image FiberNewWhite;
    ///<Summary>1.0 KB</Summary>
    public readonly global::Inv.Image FiberPinBlack;
    ///<Summary>1.0 KB</Summary>
    public readonly global::Inv.Image FiberPinWhite;
    ///<Summary>1.9 KB</Summary>
    public readonly global::Inv.Image FiberSmartRecordBlack;
    ///<Summary>1.9 KB</Summary>
    public readonly global::Inv.Image FiberSmartRecordWhite;
    ///<Summary>1.6 KB</Summary>
    public readonly global::Inv.Image FindInPageBlack;
    ///<Summary>1.7 KB</Summary>
    public readonly global::Inv.Image FindInPageWhite;
    ///<Summary>1.6 KB</Summary>
    public readonly global::Inv.Image FindReplaceBlack;
    ///<Summary>1.7 KB</Summary>
    public readonly global::Inv.Image FindReplaceWhite;
    ///<Summary>5.2 KB</Summary>
    public readonly global::Inv.Image FingerprintBlack;
    ///<Summary>4.9 KB</Summary>
    public readonly global::Inv.Image FingerprintWhite;
    ///<Summary>1.2 KB</Summary>
    public readonly global::Inv.Image FlightLandBlack;
    ///<Summary>1.3 KB</Summary>
    public readonly global::Inv.Image FlightLandWhite;
    ///<Summary>1.4 KB</Summary>
    public readonly global::Inv.Image FlightTakeoffBlack;
    ///<Summary>1.5 KB</Summary>
    public readonly global::Inv.Image FlightTakeoffWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image FlipToBackBlack;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image FlipToBackWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image FlipToFrontBlack;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image FlipToFrontWhite;
    ///<Summary>2.2 KB</Summary>
    public readonly global::Inv.Image Forward10black;
    ///<Summary>2.3 KB</Summary>
    public readonly global::Inv.Image Forward10white;
    ///<Summary>2.4 KB</Summary>
    public readonly global::Inv.Image Forward30black;
    ///<Summary>2.5 KB</Summary>
    public readonly global::Inv.Image Forward30white;
    ///<Summary>2.0 KB</Summary>
    public readonly global::Inv.Image Forward5black;
    ///<Summary>2.2 KB</Summary>
    public readonly global::Inv.Image Forward5white;
    ///<Summary>0.4 KB</Summary>
    public readonly global::Inv.Image GamesBlack;
    ///<Summary>0.4 KB</Summary>
    public readonly global::Inv.Image GamesWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image GavelBlack;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image GavelWhite;
    ///<Summary>0.3 KB</Summary>
    public readonly global::Inv.Image GetAppBlack;
    ///<Summary>0.3 KB</Summary>
    public readonly global::Inv.Image GetAppWhite;
    ///<Summary>0.4 KB</Summary>
    public readonly global::Inv.Image GifBlack;
    ///<Summary>0.4 KB</Summary>
    public readonly global::Inv.Image GifWhite;
    ///<Summary>1.7 KB</Summary>
    public readonly global::Inv.Image GradeBlack;
    ///<Summary>1.7 KB</Summary>
    public readonly global::Inv.Image GradeWhite;
    ///<Summary>1.9 KB</Summary>
    public readonly global::Inv.Image GroupWorkBlack;
    ///<Summary>2.1 KB</Summary>
    public readonly global::Inv.Image GroupWorkWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image HdBlack;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image HdWhite;
    ///<Summary>2.9 KB</Summary>
    public readonly global::Inv.Image HearingBlack;
    ///<Summary>3.0 KB</Summary>
    public readonly global::Inv.Image HearingWhite;
    ///<Summary>2.2 KB</Summary>
    public readonly global::Inv.Image HelpBlack;
    ///<Summary>3.0 KB</Summary>
    public readonly global::Inv.Image HelpOutlineBlack;
    ///<Summary>3.2 KB</Summary>
    public readonly global::Inv.Image HelpOutlineWhite;
    ///<Summary>2.3 KB</Summary>
    public readonly global::Inv.Image HelpWhite;
    ///<Summary>2.8 KB</Summary>
    public readonly global::Inv.Image HighlightOffBlack;
    ///<Summary>2.9 KB</Summary>
    public readonly global::Inv.Image HighlightOffWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image HighQualityBlack;
    ///<Summary>0.8 KB</Summary>
    public readonly global::Inv.Image HighQualityWhite;
    ///<Summary>2.3 KB</Summary>
    public readonly global::Inv.Image HistoryBlack;
    ///<Summary>2.3 KB</Summary>
    public readonly global::Inv.Image HistoryWhite;
    ///<Summary>0.6 KB</Summary>
    public readonly global::Inv.Image HomeBlack;
    ///<Summary>0.6 KB</Summary>
    public readonly global::Inv.Image HomeWhite;
    ///<Summary>0.4 KB</Summary>
    public readonly global::Inv.Image HourglassEmptyBlack;
    ///<Summary>0.4 KB</Summary>
    public readonly global::Inv.Image HourglassEmptyWhite;
    ///<Summary>0.4 KB</Summary>
    public readonly global::Inv.Image HourglassFullBlack;
    ///<Summary>0.5 KB</Summary>
    public readonly global::Inv.Image HourglassFullWhite;
    ///<Summary>0.3 KB</Summary>
    public readonly global::Inv.Image HttpBlack;
    ///<Summary>1.4 KB</Summary>
    public readonly global::Inv.Image HttpsBlack;
    ///<Summary>1.4 KB</Summary>
    public readonly global::Inv.Image HttpsWhite;
    ///<Summary>0.4 KB</Summary>
    public readonly global::Inv.Image HttpWhite;
    ///<Summary>0.2 KB</Summary>
    public readonly global::Inv.Image IcEventBlack12dp;
    ///<Summary>0.3 KB</Summary>
    public readonly global::Inv.Image IcEventBlack16dp;
    ///<Summary>0.2 KB</Summary>
    public readonly global::Inv.Image IcEventSeatBlack16dp;
    ///<Summary>0.2 KB</Summary>
    public readonly global::Inv.Image IcEventSeatWhite16dp;
    ///<Summary>0.2 KB</Summary>
    public readonly global::Inv.Image IcEventWhite12dp;
    ///<Summary>0.3 KB</Summary>
    public readonly global::Inv.Image IcEventWhite16dp;
    ///<Summary>0.4 KB</Summary>
    public readonly global::Inv.Image IcLockBlack12dp;
    ///<Summary>0.3 KB</Summary>
    public readonly global::Inv.Image IcLockWhite12dp;
    ///<Summary>0.2 KB</Summary>
    public readonly global::Inv.Image IcOpenInNewBlack12dp;
    ///<Summary>0.3 KB</Summary>
    public readonly global::Inv.Image IcOpenInNewWhite12dp;
    ///<Summary>0.4 KB</Summary>
    public readonly global::Inv.Image IcShoppingCartBlack12dp;
    ///<Summary>0.4 KB</Summary>
    public readonly global::Inv.Image IcShoppingCartWhite12dp;
    ///<Summary>0.3 KB</Summary>
    public readonly global::Inv.Image IcTrendingDownBlack16dp;
    ///<Summary>0.3 KB</Summary>
    public readonly global::Inv.Image IcTrendingDownWhite16dp;
    ///<Summary>0.2 KB</Summary>
    public readonly global::Inv.Image IcTrendingFlatBlack16dp;
    ///<Summary>0.2 KB</Summary>
    public readonly global::Inv.Image IcTrendingFlatWhite16dp;
    ///<Summary>0.3 KB</Summary>
    public readonly global::Inv.Image IcTrendingUpBlack16dp;
    ///<Summary>0.3 KB</Summary>
    public readonly global::Inv.Image IcTrendingUpWhite16dp;
    ///<Summary>0.4 KB</Summary>
    public readonly global::Inv.Image IcVerifiedUserBlack12dp;
    ///<Summary>0.4 KB</Summary>
    public readonly global::Inv.Image IcVerifiedUserWhite12dp;
    ///<Summary>1.6 KB</Summary>
    public readonly global::Inv.Image IcWarningBlack96dp;
    ///<Summary>1.9 KB</Summary>
    public readonly global::Inv.Image IcWarningWhite96dp;
    ///<Summary>1.1 KB</Summary>
    public readonly global::Inv.Image ImportantDevicesBlack;
    ///<Summary>1.1 KB</Summary>
    public readonly global::Inv.Image ImportantDevicesWhite;
    ///<Summary>1.5 KB</Summary>
    public readonly global::Inv.Image InfoBlack;
    ///<Summary>2.5 KB</Summary>
    public readonly global::Inv.Image InfoOutlineBlack;
    ///<Summary>2.6 KB</Summary>
    public readonly global::Inv.Image InfoOutlineWhite;
    ///<Summary>1.6 KB</Summary>
    public readonly global::Inv.Image InfoWhite;
    ///<Summary>0.8 KB</Summary>
    public readonly global::Inv.Image InputBlack;
    ///<Summary>0.8 KB</Summary>
    public readonly global::Inv.Image InputWhite;
    ///<Summary>1.7 KB</Summary>
    public readonly global::Inv.Image InvertColorsBlack;
    ///<Summary>1.8 KB</Summary>
    public readonly global::Inv.Image InvertColorsWhite;
    ///<Summary>0.9 KB</Summary>
    public readonly global::Inv.Image LabelBlack;
    ///<Summary>1.2 KB</Summary>
    public readonly global::Inv.Image LabelOutlineBlack;
    ///<Summary>1.2 KB</Summary>
    public readonly global::Inv.Image LabelOutlineWhite;
    ///<Summary>1.0 KB</Summary>
    public readonly global::Inv.Image LabelWhite;
    ///<Summary>3.0 KB</Summary>
    public readonly global::Inv.Image LanguageBlack;
    ///<Summary>3.2 KB</Summary>
    public readonly global::Inv.Image LanguageWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image LaunchBlack;
    ///<Summary>0.8 KB</Summary>
    public readonly global::Inv.Image LaunchWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image LibraryAddBlack;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image LibraryAddWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image LibraryBooksBlack;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image LibraryBooksWhite;
    ///<Summary>1.0 KB</Summary>
    public readonly global::Inv.Image LibraryMusicBlack;
    ///<Summary>1.0 KB</Summary>
    public readonly global::Inv.Image LibraryMusicWhite;
    ///<Summary>1.6 KB</Summary>
    public readonly global::Inv.Image LightbulbOutlineBlack;
    ///<Summary>1.7 KB</Summary>
    public readonly global::Inv.Image LightbulbOutlineWhite;
    ///<Summary>0.1 KB</Summary>
    public readonly global::Inv.Image LineStyleBlack;
    ///<Summary>0.1 KB</Summary>
    public readonly global::Inv.Image LineStyleWhite;
    ///<Summary>0.1 KB</Summary>
    public readonly global::Inv.Image LineWeightBlack;
    ///<Summary>0.1 KB</Summary>
    public readonly global::Inv.Image LineWeightWhite;
    ///<Summary>0.1 KB</Summary>
    public readonly global::Inv.Image ListBlack;
    ///<Summary>0.1 KB</Summary>
    public readonly global::Inv.Image ListWhite;
    ///<Summary>1.4 KB</Summary>
    public readonly global::Inv.Image LockBlack;
    ///<Summary>1.3 KB</Summary>
    public readonly global::Inv.Image LockOpenBlack;
    ///<Summary>1.3 KB</Summary>
    public readonly global::Inv.Image LockOpenWhite;
    ///<Summary>1.3 KB</Summary>
    public readonly global::Inv.Image LockOutlineBlack;
    ///<Summary>1.3 KB</Summary>
    public readonly global::Inv.Image LockOutlineWhite;
    ///<Summary>1.4 KB</Summary>
    public readonly global::Inv.Image LockWhite;
    ///<Summary>1.7 KB</Summary>
    public readonly global::Inv.Image LoopBlack;
    ///<Summary>1.9 KB</Summary>
    public readonly global::Inv.Image LoopWhite;
    ///<Summary>1.4 KB</Summary>
    public readonly global::Inv.Image LoyaltyBlack;
    ///<Summary>1.6 KB</Summary>
    public readonly global::Inv.Image LoyaltyWhite;
    ///<Summary>0.6 KB</Summary>
    public readonly global::Inv.Image MarkunreadMailboxBlack;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image MarkunreadMailboxWhite;
    ///<Summary>1.5 KB</Summary>
    public readonly global::Inv.Image MicBlack;
    ///<Summary>1.7 KB</Summary>
    public readonly global::Inv.Image MicNoneBlack;
    ///<Summary>1.8 KB</Summary>
    public readonly global::Inv.Image MicNoneWhite;
    ///<Summary>1.5 KB</Summary>
    public readonly global::Inv.Image MicOffBlack;
    ///<Summary>1.7 KB</Summary>
    public readonly global::Inv.Image MicOffWhite;
    ///<Summary>1.6 KB</Summary>
    public readonly global::Inv.Image MicWhite;
    ///<Summary>1.9 KB</Summary>
    public readonly global::Inv.Image MotorcycleBlack;
    ///<Summary>1.9 KB</Summary>
    public readonly global::Inv.Image MotorcycleWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image MovieBlack;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image MovieWhite;
    ///<Summary>1.0 KB</Summary>
    public readonly global::Inv.Image MusicVideoBlack;
    ///<Summary>0.9 KB</Summary>
    public readonly global::Inv.Image MusicVideoWhite;
    ///<Summary>1.1 KB</Summary>
    public readonly global::Inv.Image NavigateBackBlack;
    ///<Summary>3.1 KB</Summary>
    public readonly global::Inv.Image NavigateBackWhite;
    ///<Summary>1.8 KB</Summary>
    public readonly global::Inv.Image NewReleasesBlack;
    ///<Summary>1.9 KB</Summary>
    public readonly global::Inv.Image NewReleasesWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image NoteAddBlack;
    ///<Summary>0.8 KB</Summary>
    public readonly global::Inv.Image NoteAddWhite;
    ///<Summary>2.5 KB</Summary>
    public readonly global::Inv.Image NotInterestedBlack;
    ///<Summary>2.6 KB</Summary>
    public readonly global::Inv.Image NotInterestedWhite;
    ///<Summary>1.7 KB</Summary>
    public readonly global::Inv.Image OfflinePinBlack;
    ///<Summary>1.8 KB</Summary>
    public readonly global::Inv.Image OfflinePinWhite;
    ///<Summary>1.7 KB</Summary>
    public readonly global::Inv.Image OpacityBlack;
    ///<Summary>1.7 KB</Summary>
    public readonly global::Inv.Image OpacityWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image OpenInBrowserBlack;
    ///<Summary>0.8 KB</Summary>
    public readonly global::Inv.Image OpenInBrowserWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image OpenInNewBlack;
    ///<Summary>0.8 KB</Summary>
    public readonly global::Inv.Image OpenInNewWhite;
    ///<Summary>0.6 KB</Summary>
    public readonly global::Inv.Image OpenWithBlack;
    ///<Summary>0.6 KB</Summary>
    public readonly global::Inv.Image OpenWithWhite;
    ///<Summary>1.5 KB</Summary>
    public readonly global::Inv.Image PageviewBlack;
    ///<Summary>1.6 KB</Summary>
    public readonly global::Inv.Image PageviewWhite;
    ///<Summary>1.2 KB</Summary>
    public readonly global::Inv.Image PanToolBlack;
    ///<Summary>1.2 KB</Summary>
    public readonly global::Inv.Image PanToolWhite;
    ///<Summary>0.1 KB</Summary>
    public readonly global::Inv.Image PauseBlack;
    ///<Summary>1.5 KB</Summary>
    public readonly global::Inv.Image PauseCircleFilledBlack;
    ///<Summary>1.7 KB</Summary>
    public readonly global::Inv.Image PauseCircleFilledWhite;
    ///<Summary>2.5 KB</Summary>
    public readonly global::Inv.Image PauseCircleOutlineBlack;
    ///<Summary>2.7 KB</Summary>
    public readonly global::Inv.Image PauseCircleOutlineWhite;
    ///<Summary>0.1 KB</Summary>
    public readonly global::Inv.Image PauseWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image PaymentBlack;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image PaymentWhite;
    ///<Summary>1.7 KB</Summary>
    public readonly global::Inv.Image PermCameraMicBlack;
    ///<Summary>1.7 KB</Summary>
    public readonly global::Inv.Image PermCameraMicWhite;
    ///<Summary>1.2 KB</Summary>
    public readonly global::Inv.Image PermContactCalendarBlack;
    ///<Summary>1.2 KB</Summary>
    public readonly global::Inv.Image PermContactCalendarWhite;
    ///<Summary>1.7 KB</Summary>
    public readonly global::Inv.Image PermDataSettingBlack;
    ///<Summary>1.8 KB</Summary>
    public readonly global::Inv.Image PermDataSettingWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image PermDeviceInformationBlack;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image PermDeviceInformationWhite;
    ///<Summary>1.6 KB</Summary>
    public readonly global::Inv.Image PermIdentityBlack;
    ///<Summary>1.7 KB</Summary>
    public readonly global::Inv.Image PermIdentityWhite;
    ///<Summary>1.3 KB</Summary>
    public readonly global::Inv.Image PermMediaBlack;
    ///<Summary>1.3 KB</Summary>
    public readonly global::Inv.Image PermMediaWhite;
    ///<Summary>1.6 KB</Summary>
    public readonly global::Inv.Image PermPhoneMsgBlack;
    ///<Summary>1.6 KB</Summary>
    public readonly global::Inv.Image PermPhoneMsgWhite;
    ///<Summary>1.5 KB</Summary>
    public readonly global::Inv.Image PermScanWifiBlack;
    ///<Summary>1.7 KB</Summary>
    public readonly global::Inv.Image PermScanWifiWhite;
    ///<Summary>1.9 KB</Summary>
    public readonly global::Inv.Image PetsBlack;
    ///<Summary>1.9 KB</Summary>
    public readonly global::Inv.Image PetsWhite;
    ///<Summary>0.6 KB</Summary>
    public readonly global::Inv.Image PictureInPictureAltBlack;
    ///<Summary>0.6 KB</Summary>
    public readonly global::Inv.Image PictureInPictureAltWhite;
    ///<Summary>0.6 KB</Summary>
    public readonly global::Inv.Image PictureInPictureBlack;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image PictureInPictureWhite;
    ///<Summary>0.5 KB</Summary>
    public readonly global::Inv.Image PlayArrowBlack;
    ///<Summary>0.6 KB</Summary>
    public readonly global::Inv.Image PlayArrowWhite;
    ///<Summary>1.7 KB</Summary>
    public readonly global::Inv.Image PlayCircleFilledBlack;
    ///<Summary>1.8 KB</Summary>
    public readonly global::Inv.Image PlayCircleFilledWhite;
    ///<Summary>2.6 KB</Summary>
    public readonly global::Inv.Image PlayCircleOutlineBlack;
    ///<Summary>2.8 KB</Summary>
    public readonly global::Inv.Image PlayCircleOutlineWhite;
    ///<Summary>1.0 KB</Summary>
    public readonly global::Inv.Image PlayForWorkBlack;
    ///<Summary>1.2 KB</Summary>
    public readonly global::Inv.Image PlayForWorkWhite;
    ///<Summary>0.1 KB</Summary>
    public readonly global::Inv.Image PlaylistAddBlack;
    ///<Summary>0.5 KB</Summary>
    public readonly global::Inv.Image PlaylistAddCheckBlack;
    ///<Summary>0.5 KB</Summary>
    public readonly global::Inv.Image PlaylistAddCheckWhite;
    ///<Summary>0.1 KB</Summary>
    public readonly global::Inv.Image PlaylistAddWhite;
    ///<Summary>0.3 KB</Summary>
    public readonly global::Inv.Image PlaylistPlayBlack;
    ///<Summary>0.3 KB</Summary>
    public readonly global::Inv.Image PlaylistPlayWhite;
    ///<Summary>1.4 KB</Summary>
    public readonly global::Inv.Image PolymerBlack;
    ///<Summary>1.5 KB</Summary>
    public readonly global::Inv.Image PolymerWhite;
    ///<Summary>2.0 KB</Summary>
    public readonly global::Inv.Image PowerSettingsNewBlack;
    ///<Summary>2.1 KB</Summary>
    public readonly global::Inv.Image PowerSettingsNewWhite;
    ///<Summary>0.8 KB</Summary>
    public readonly global::Inv.Image PregnantWomanBlack;
    ///<Summary>0.8 KB</Summary>
    public readonly global::Inv.Image PregnantWomanWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image PrintBlack;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image PrintWhite;
    ///<Summary>2.7 KB</Summary>
    public readonly global::Inv.Image QueryBuilderBlack;
    ///<Summary>2.8 KB</Summary>
    public readonly global::Inv.Image QueryBuilderWhite;
    ///<Summary>0.6 KB</Summary>
    public readonly global::Inv.Image QuestionAnswerBlack;
    ///<Summary>0.6 KB</Summary>
    public readonly global::Inv.Image QuestionAnswerWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image QueueBlack;
    ///<Summary>0.6 KB</Summary>
    public readonly global::Inv.Image QueueMusicBlack;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image QueueMusicWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image QueuePlayNextBlack;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image QueuePlayNextWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image QueueWhite;
    ///<Summary>1.4 KB</Summary>
    public readonly global::Inv.Image RadioBlack;
    ///<Summary>1.5 KB</Summary>
    public readonly global::Inv.Image RadioWhite;
    ///<Summary>0.3 KB</Summary>
    public readonly global::Inv.Image ReceiptBlack;
    ///<Summary>0.3 KB</Summary>
    public readonly global::Inv.Image ReceiptWhite;
    ///<Summary>0.9 KB</Summary>
    public readonly global::Inv.Image RecentActorsBlack;
    ///<Summary>0.9 KB</Summary>
    public readonly global::Inv.Image RecentActorsWhite;
    ///<Summary>2.1 KB</Summary>
    public readonly global::Inv.Image RecordVoiceOverBlack;
    ///<Summary>2.0 KB</Summary>
    public readonly global::Inv.Image RecordVoiceOverWhite;
    ///<Summary>1.7 KB</Summary>
    public readonly global::Inv.Image RedeemBlack;
    ///<Summary>1.8 KB</Summary>
    public readonly global::Inv.Image RedeemWhite;
    ///<Summary>1.3 KB</Summary>
    public readonly global::Inv.Image RemoveCircleGray;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image RemoveFromQueueBlack;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image RemoveFromQueueWhite;
    ///<Summary>0.1 KB</Summary>
    public readonly global::Inv.Image ReorderBlack;
    ///<Summary>0.1 KB</Summary>
    public readonly global::Inv.Image ReorderWhite;
    ///<Summary>0.4 KB</Summary>
    public readonly global::Inv.Image RepeatBlack;
    ///<Summary>0.5 KB</Summary>
    public readonly global::Inv.Image RepeatOneBlack;
    ///<Summary>0.5 KB</Summary>
    public readonly global::Inv.Image RepeatOneWhite;
    ///<Summary>0.4 KB</Summary>
    public readonly global::Inv.Image RepeatWhite;
    ///<Summary>2.2 KB</Summary>
    public readonly global::Inv.Image Replay10black;
    ///<Summary>2.3 KB</Summary>
    public readonly global::Inv.Image Replay10white;
    ///<Summary>2.4 KB</Summary>
    public readonly global::Inv.Image Replay30black;
    ///<Summary>2.5 KB</Summary>
    public readonly global::Inv.Image Replay30white;
    ///<Summary>2.0 KB</Summary>
    public readonly global::Inv.Image Replay5black;
    ///<Summary>2.2 KB</Summary>
    public readonly global::Inv.Image Replay5white;
    ///<Summary>1.7 KB</Summary>
    public readonly global::Inv.Image ReplayBlack;
    ///<Summary>1.8 KB</Summary>
    public readonly global::Inv.Image ReplayWhite;
    ///<Summary>0.9 KB</Summary>
    public readonly global::Inv.Image ReportProblemBlack;
    ///<Summary>1.0 KB</Summary>
    public readonly global::Inv.Image ReportProblemWhite;
    ///<Summary>2.3 KB</Summary>
    public readonly global::Inv.Image RestoreBlack;
    ///<Summary>2.4 KB</Summary>
    public readonly global::Inv.Image RestoreWhite;
    ///<Summary>1.7 KB</Summary>
    public readonly global::Inv.Image RoomBlack;
    ///<Summary>1.7 KB</Summary>
    public readonly global::Inv.Image RoomWhite;
    ///<Summary>0.6 KB</Summary>
    public readonly global::Inv.Image RoundedCornerBlack;
    ///<Summary>0.6 KB</Summary>
    public readonly global::Inv.Image RoundedCornerWhite;
    ///<Summary>1.4 KB</Summary>
    public readonly global::Inv.Image RowingBlack;
    ///<Summary>1.4 KB</Summary>
    public readonly global::Inv.Image RowingWhite;
    ///<Summary>2.7 KB</Summary>
    public readonly global::Inv.Image ScheduleBlack;
    ///<Summary>2.6 KB</Summary>
    public readonly global::Inv.Image ScheduleWhite;
    ///<Summary>1.8 KB</Summary>
    public readonly global::Inv.Image SearchBlack;
    ///<Summary>1.9 KB</Summary>
    public readonly global::Inv.Image SearchWhite;
    ///<Summary>1.7 KB</Summary>
    public readonly global::Inv.Image SettingsApplicationsBlack;
    ///<Summary>1.8 KB</Summary>
    public readonly global::Inv.Image SettingsApplicationsWhite;
    ///<Summary>2.3 KB</Summary>
    public readonly global::Inv.Image SettingsBackupRestoreBlack;
    ///<Summary>2.4 KB</Summary>
    public readonly global::Inv.Image SettingsBackupRestoreWhite;
    ///<Summary>2.1 KB</Summary>
    public readonly global::Inv.Image SettingsBlack;
    ///<Summary>1.0 KB</Summary>
    public readonly global::Inv.Image SettingsBluetoothBlack;
    ///<Summary>1.1 KB</Summary>
    public readonly global::Inv.Image SettingsBluetoothWhite;
    ///<Summary>1.0 KB</Summary>
    public readonly global::Inv.Image SettingsBrightnessBlack;
    ///<Summary>1.0 KB</Summary>
    public readonly global::Inv.Image SettingsBrightnessWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image SettingsCellBlack;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image SettingsCellWhite;
    ///<Summary>1.6 KB</Summary>
    public readonly global::Inv.Image SettingsEthernetBlack;
    ///<Summary>1.8 KB</Summary>
    public readonly global::Inv.Image SettingsEthernetWhite;
    ///<Summary>2.6 KB</Summary>
    public readonly global::Inv.Image SettingsInputAntennaBlack;
    ///<Summary>2.8 KB</Summary>
    public readonly global::Inv.Image SettingsInputAntennaWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image SettingsInputComponentBlack;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image SettingsInputComponentWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image SettingsInputCompositeBlack;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image SettingsInputCompositeWhite;
    ///<Summary>0.6 KB</Summary>
    public readonly global::Inv.Image SettingsInputHdmiBlack;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image SettingsInputHdmiWhite;
    ///<Summary>3.1 KB</Summary>
    public readonly global::Inv.Image SettingsInputSvideoBlack;
    ///<Summary>3.3 KB</Summary>
    public readonly global::Inv.Image SettingsInputSvideoWhite;
    ///<Summary>1.2 KB</Summary>
    public readonly global::Inv.Image SettingsOverscanBlack;
    ///<Summary>1.2 KB</Summary>
    public readonly global::Inv.Image SettingsOverscanWhite;
    ///<Summary>1.4 KB</Summary>
    public readonly global::Inv.Image SettingsPhoneBlack;
    ///<Summary>1.5 KB</Summary>
    public readonly global::Inv.Image SettingsPhoneWhite;
    ///<Summary>1.9 KB</Summary>
    public readonly global::Inv.Image SettingsPowerBlack;
    ///<Summary>2.0 KB</Summary>
    public readonly global::Inv.Image SettingsPowerWhite;
    ///<Summary>1.7 KB</Summary>
    public readonly global::Inv.Image SettingsRemoteBlack;
    ///<Summary>1.8 KB</Summary>
    public readonly global::Inv.Image SettingsRemoteWhite;
    ///<Summary>1.5 KB</Summary>
    public readonly global::Inv.Image SettingsVoiceBlack;
    ///<Summary>1.7 KB</Summary>
    public readonly global::Inv.Image SettingsVoiceWhite;
    ///<Summary>2.2 KB</Summary>
    public readonly global::Inv.Image SettingsWhite;
    ///<Summary>0.8 KB</Summary>
    public readonly global::Inv.Image ShopBlack;
    ///<Summary>1.7 KB</Summary>
    public readonly global::Inv.Image ShoppingBasketBlack;
    ///<Summary>1.8 KB</Summary>
    public readonly global::Inv.Image ShoppingBasketWhite;
    ///<Summary>1.2 KB</Summary>
    public readonly global::Inv.Image ShoppingCartBlack;
    ///<Summary>1.5 KB</Summary>
    public readonly global::Inv.Image ShoppingCartWhite;
    ///<Summary>0.9 KB</Summary>
    public readonly global::Inv.Image ShopTwoBlack;
    ///<Summary>0.9 KB</Summary>
    public readonly global::Inv.Image ShopTwoWhite;
    ///<Summary>0.8 KB</Summary>
    public readonly global::Inv.Image ShopWhite;
    ///<Summary>0.8 KB</Summary>
    public readonly global::Inv.Image ShuffleBlack;
    ///<Summary>1.0 KB</Summary>
    public readonly global::Inv.Image ShuffleWhite;
    ///<Summary>0.5 KB</Summary>
    public readonly global::Inv.Image SkipNextBlack;
    ///<Summary>0.6 KB</Summary>
    public readonly global::Inv.Image SkipNextWhite;
    ///<Summary>0.6 KB</Summary>
    public readonly global::Inv.Image SkipPreviousBlack;
    ///<Summary>0.6 KB</Summary>
    public readonly global::Inv.Image SkipPreviousWhite;
    ///<Summary>2.5 KB</Summary>
    public readonly global::Inv.Image SlowMotionVideoBlack;
    ///<Summary>2.5 KB</Summary>
    public readonly global::Inv.Image SlowMotionVideoWhite;
    ///<Summary>3.1 KB</Summary>
    public readonly global::Inv.Image SnoozeBlack;
    ///<Summary>3.2 KB</Summary>
    public readonly global::Inv.Image SnoozeWhite;
    ///<Summary>1.8 KB</Summary>
    public readonly global::Inv.Image SortByAlphaBlack;
    ///<Summary>2.0 KB</Summary>
    public readonly global::Inv.Image SortByAlphaWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image SpeakerNotesBlack;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image SpeakerNotesWhite;
    ///<Summary>1.5 KB</Summary>
    public readonly global::Inv.Image SpellcheckBlack;
    ///<Summary>1.7 KB</Summary>
    public readonly global::Inv.Image SpellcheckWhite;
    ///<Summary>2.4 KB</Summary>
    public readonly global::Inv.Image StarsBlack;
    ///<Summary>2.6 KB</Summary>
    public readonly global::Inv.Image StarsWhite;
    ///<Summary>0.1 KB</Summary>
    public readonly global::Inv.Image StopBlack;
    ///<Summary>0.1 KB</Summary>
    public readonly global::Inv.Image StopWhite;
    ///<Summary>0.3 KB</Summary>
    public readonly global::Inv.Image StoreBlack;
    ///<Summary>0.4 KB</Summary>
    public readonly global::Inv.Image StoreWhite;
    ///<Summary>0.1 KB</Summary>
    public readonly global::Inv.Image SubjectBlack;
    ///<Summary>0.1 KB</Summary>
    public readonly global::Inv.Image SubjectWhite;
    ///<Summary>0.9 KB</Summary>
    public readonly global::Inv.Image SubscriptionsBlack;
    ///<Summary>0.9 KB</Summary>
    public readonly global::Inv.Image SubscriptionsWhite;
    ///<Summary>0.6 KB</Summary>
    public readonly global::Inv.Image SubtitlesBlack;
    ///<Summary>0.6 KB</Summary>
    public readonly global::Inv.Image SubtitlesWhite;
    ///<Summary>1.4 KB</Summary>
    public readonly global::Inv.Image SupervisorAccountBlack;
    ///<Summary>1.5 KB</Summary>
    public readonly global::Inv.Image SupervisorAccountWhite;
    ///<Summary>2.2 KB</Summary>
    public readonly global::Inv.Image SurroundSoundBlack;
    ///<Summary>2.3 KB</Summary>
    public readonly global::Inv.Image SurroundSoundWhite;
    ///<Summary>0.5 KB</Summary>
    public readonly global::Inv.Image SwapHorizBlack;
    ///<Summary>0.6 KB</Summary>
    public readonly global::Inv.Image SwapHorizWhite;
    ///<Summary>0.6 KB</Summary>
    public readonly global::Inv.Image SwapVertBlack;
    ///<Summary>1.7 KB</Summary>
    public readonly global::Inv.Image SwapVerticalCircleBlack;
    ///<Summary>1.8 KB</Summary>
    public readonly global::Inv.Image SwapVerticalCircleWhite;
    ///<Summary>0.6 KB</Summary>
    public readonly global::Inv.Image SwapVertWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image SystemUpdateAltBlack;
    ///<Summary>0.8 KB</Summary>
    public readonly global::Inv.Image SystemUpdateAltWhite;
    ///<Summary>0.6 KB</Summary>
    public readonly global::Inv.Image TabBlack;
    ///<Summary>0.6 KB</Summary>
    public readonly global::Inv.Image TabUnselectedBlack;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image TabUnselectedWhite;
    ///<Summary>0.6 KB</Summary>
    public readonly global::Inv.Image TabWhite;
    ///<Summary>0.1 KB</Summary>
    public readonly global::Inv.Image TheatersBlack;
    ///<Summary>0.1 KB</Summary>
    public readonly global::Inv.Image TheatersWhite;
    ///<Summary>1.0 KB</Summary>
    public readonly global::Inv.Image ThumbDownBlack;
    ///<Summary>1.0 KB</Summary>
    public readonly global::Inv.Image ThumbDownWhite;
    ///<Summary>1.2 KB</Summary>
    public readonly global::Inv.Image ThumbsUpDownBlack;
    ///<Summary>1.3 KB</Summary>
    public readonly global::Inv.Image ThumbsUpDownWhite;
    ///<Summary>1.0 KB</Summary>
    public readonly global::Inv.Image ThumbUpBlack;
    ///<Summary>1.1 KB</Summary>
    public readonly global::Inv.Image ThumbUpWhite;
    ///<Summary>1.2 KB</Summary>
    public readonly global::Inv.Image TimelineBlack;
    ///<Summary>1.2 KB</Summary>
    public readonly global::Inv.Image TimelineWhite;
    ///<Summary>0.1 KB</Summary>
    public readonly global::Inv.Image TocBlack;
    ///<Summary>0.1 KB</Summary>
    public readonly global::Inv.Image TocWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image TodayBlack;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image TodayWhite;
    ///<Summary>2.7 KB</Summary>
    public readonly global::Inv.Image TollBlack;
    ///<Summary>2.8 KB</Summary>
    public readonly global::Inv.Image TollWhite;
    ///<Summary>1.5 KB</Summary>
    public readonly global::Inv.Image TouchAppBlack;
    ///<Summary>1.5 KB</Summary>
    public readonly global::Inv.Image TouchAppWhite;
    ///<Summary>4.0 KB</Summary>
    public readonly global::Inv.Image TrackChangesBlack;
    ///<Summary>4.0 KB</Summary>
    public readonly global::Inv.Image TrackChangesWhite;
    ///<Summary>1.8 KB</Summary>
    public readonly global::Inv.Image TranslateBlack;
    ///<Summary>1.9 KB</Summary>
    public readonly global::Inv.Image TranslateWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image TrendingDownBlack;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image TrendingDownWhite;
    ///<Summary>0.3 KB</Summary>
    public readonly global::Inv.Image TrendingFlatBlack;
    ///<Summary>0.3 KB</Summary>
    public readonly global::Inv.Image TrendingFlatWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image TrendingUpBlack;
    ///<Summary>0.8 KB</Summary>
    public readonly global::Inv.Image TrendingUpWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image TurnedInBlack;
    ///<Summary>0.9 KB</Summary>
    public readonly global::Inv.Image TurnedInNotBlack;
    ///<Summary>0.9 KB</Summary>
    public readonly global::Inv.Image TurnedInNotWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image TurnedInWhite;
    ///<Summary>2.5 KB</Summary>
    public readonly global::Inv.Image UpdateBlack;
    ///<Summary>2.6 KB</Summary>
    public readonly global::Inv.Image UpdateWhite;
    ///<Summary>1.4 KB</Summary>
    public readonly global::Inv.Image VerifiedUserBlack;
    ///<Summary>1.6 KB</Summary>
    public readonly global::Inv.Image VerifiedUserWhite;
    ///<Summary>0.5 KB</Summary>
    public readonly global::Inv.Image VideocamBlack;
    ///<Summary>0.8 KB</Summary>
    public readonly global::Inv.Image VideocamOffBlack;
    ///<Summary>0.9 KB</Summary>
    public readonly global::Inv.Image VideocamOffWhite;
    ///<Summary>0.6 KB</Summary>
    public readonly global::Inv.Image VideocamWhite;
    ///<Summary>0.8 KB</Summary>
    public readonly global::Inv.Image VideoLibraryBlack;
    ///<Summary>0.8 KB</Summary>
    public readonly global::Inv.Image VideoLibraryWhite;
    ///<Summary>0.5 KB</Summary>
    public readonly global::Inv.Image ViewAgendaBlack;
    ///<Summary>0.5 KB</Summary>
    public readonly global::Inv.Image ViewAgendaWhite;
    ///<Summary>0.1 KB</Summary>
    public readonly global::Inv.Image ViewArrayBlack;
    ///<Summary>0.1 KB</Summary>
    public readonly global::Inv.Image ViewArrayWhite;
    ///<Summary>0.1 KB</Summary>
    public readonly global::Inv.Image ViewCarouselBlack;
    ///<Summary>0.1 KB</Summary>
    public readonly global::Inv.Image ViewCarouselWhite;
    ///<Summary>0.1 KB</Summary>
    public readonly global::Inv.Image ViewColumnBlack;
    ///<Summary>0.1 KB</Summary>
    public readonly global::Inv.Image ViewColumnWhite;
    ///<Summary>0.4 KB</Summary>
    public readonly global::Inv.Image ViewDayBlack;
    ///<Summary>0.4 KB</Summary>
    public readonly global::Inv.Image ViewDayWhite;
    ///<Summary>0.1 KB</Summary>
    public readonly global::Inv.Image ViewHeadlineBlack;
    ///<Summary>0.1 KB</Summary>
    public readonly global::Inv.Image ViewHeadlineWhite;
    ///<Summary>0.1 KB</Summary>
    public readonly global::Inv.Image ViewListBlack;
    ///<Summary>0.1 KB</Summary>
    public readonly global::Inv.Image ViewListWhite;
    ///<Summary>0.1 KB</Summary>
    public readonly global::Inv.Image ViewModuleBlack;
    ///<Summary>0.1 KB</Summary>
    public readonly global::Inv.Image ViewModuleWhite;
    ///<Summary>0.1 KB</Summary>
    public readonly global::Inv.Image ViewQuiltBlack;
    ///<Summary>0.1 KB</Summary>
    public readonly global::Inv.Image ViewQuiltWhite;
    ///<Summary>0.1 KB</Summary>
    public readonly global::Inv.Image ViewStreamBlack;
    ///<Summary>0.1 KB</Summary>
    public readonly global::Inv.Image ViewStreamWhite;
    ///<Summary>0.4 KB</Summary>
    public readonly global::Inv.Image ViewWeekBlack;
    ///<Summary>0.4 KB</Summary>
    public readonly global::Inv.Image ViewWeekWhite;
    ///<Summary>2.2 KB</Summary>
    public readonly global::Inv.Image VisibilityBlack;
    ///<Summary>2.2 KB</Summary>
    public readonly global::Inv.Image VisibilityOffBlack;
    ///<Summary>2.4 KB</Summary>
    public readonly global::Inv.Image VisibilityOffWhite;
    ///<Summary>2.4 KB</Summary>
    public readonly global::Inv.Image VisibilityWhite;
    ///<Summary>0.6 KB</Summary>
    public readonly global::Inv.Image VolumeDownBlack;
    ///<Summary>0.8 KB</Summary>
    public readonly global::Inv.Image VolumeDownWhite;
    ///<Summary>0.3 KB</Summary>
    public readonly global::Inv.Image VolumeMuteBlack;
    ///<Summary>0.3 KB</Summary>
    public readonly global::Inv.Image VolumeMuteWhite;
    ///<Summary>1.7 KB</Summary>
    public readonly global::Inv.Image VolumeOffBlack;
    ///<Summary>1.9 KB</Summary>
    public readonly global::Inv.Image VolumeOffWhite;
    ///<Summary>1.6 KB</Summary>
    public readonly global::Inv.Image VolumeUpBlack;
    ///<Summary>1.8 KB</Summary>
    public readonly global::Inv.Image VolumeUpWhite;
    ///<Summary>0.9 KB</Summary>
    public readonly global::Inv.Image WarningBlack;
    ///<Summary>1.0 KB</Summary>
    public readonly global::Inv.Image WarningWhite;
    ///<Summary>1.7 KB</Summary>
    public readonly global::Inv.Image WatchLaterBlack;
    ///<Summary>1.7 KB</Summary>
    public readonly global::Inv.Image WatchLaterWhite;
    ///<Summary>0.6 KB</Summary>
    public readonly global::Inv.Image WebAssetBlack;
    ///<Summary>0.6 KB</Summary>
    public readonly global::Inv.Image WebAssetWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image WebBlack;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image WebWhite;
    ///<Summary>0.7 KB</Summary>
    public readonly global::Inv.Image WorkBlack;
    ///<Summary>0.8 KB</Summary>
    public readonly global::Inv.Image WorkWhite;
    ///<Summary>1.9 KB</Summary>
    public readonly global::Inv.Image YoutubeSearchedForBlack;
    ///<Summary>2.0 KB</Summary>
    public readonly global::Inv.Image YoutubeSearchedForWhite;
    ///<Summary>1.8 KB</Summary>
    public readonly global::Inv.Image ZoomInBlack;
    ///<Summary>2.0 KB</Summary>
    public readonly global::Inv.Image ZoomInWhite;
    ///<Summary>1.8 KB</Summary>
    public readonly global::Inv.Image ZoomOutBlack;
    ///<Summary>1.9 KB</Summary>
    public readonly global::Inv.Image ZoomOutWhite;
    ///<Summary>2.8 KB</Summary>
    public readonly global::Inv.Image _3dRotationBlack;
    ///<Summary>2.9 KB</Summary>
    public readonly global::Inv.Image _3dRotationWhite;
  }
}