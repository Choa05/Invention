﻿/*! 36 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv.Material
{
  public sealed class Application
  {
    public Application(Theme Theme)
      : this (new Inv.Application(), Theme)
    {
    }
    public Application(Inv.Application Base, Theme Theme)
    {
      this.Base = Base;
      this.Theme = Theme;
    }

    public Theme Theme { get; private set; }
    public string Title
    {
      get { return Base.Title; }
      set { Base.Title = value; }
    }
    public Inv.Directory Directory
    {
      get { return Base.Directory; }
    }
    public Inv.Audio Audio
    {
      get { return Base.Audio; }
    }
    public Inv.Location Location
    {
      get { return Base.Location; }
    }
    public Inv.Process Process
    {
      get { return Base.Process; }
    }
    public Inv.Window Window
    {
      get { return Base.Window; }
    }
    public event Action StartEvent
    {
      add { Base.StartEvent += value; }
      remove { Base.StartEvent -= value; }
    }
    public event Action StopEvent
    {
      add { Base.StopEvent += value; }
      remove { Base.StopEvent -= value; }
    }
    public event Action SuspendEvent
    {
      add { Base.SuspendEvent += value; }
      remove { Base.SuspendEvent -= value; }
    }
    public event Action ResumeEvent
    {
      add { Base.ResumeEvent += value; }
      remove { Base.ResumeEvent -= value; }
    }
    public event Action<Exception> HandleExceptionEvent
    {
      add { Base.HandleExceptionEvent += value; }
      remove { Base.HandleExceptionEvent -= value; }
    }
    public event Func<bool> ExitQuery
    {
      add { Base.ExitQuery += value; }
      remove { Base.ExitQuery -= value; }
    }

    public void Exit()
    {
      Base.Exit();
    }

    public ConsoleSurface NewConsoleSurface()
    {
      return new ConsoleSurface(this);
    }
    public NavigationSurface NewNavigationSurface()
    {
      return new NavigationSurface(this);
    }

    internal Inv.Application Base { get; private set; }

    public static implicit operator Inv.Application(Application Application)
    {
      return Application != null ? Application.Base : null;
    }
  }

  public sealed class Theme
  {
    public Theme(Inv.Colour Colour)
    {
      this.Colour = Colour;
    }

    public Inv.Colour Colour { get; private set; }
  }

  public sealed class NavigationSurface
  {
    internal NavigationSurface(Application Application)
    {
      this.Application = Application;
      this.Base = Application.Window.NewSurface();

      var MainOverlay = Base.NewOverlay();
      Base.Content = MainOverlay;

      var MainDock = Base.NewHorizontalDock();
      MainOverlay.AddPanel(MainDock);
      MainDock.Background.Colour = Inv.Colour.WhiteSmoke;

      var DrawerDock = Base.NewVerticalDock();
      DrawerDock.Alignment.StretchLeft();

      this.TitleLabel = Base.NewLabel();
      DrawerDock.AddHeader(TitleLabel);
      TitleLabel.Padding.Set(16, 0, 16, 0);
      TitleLabel.Background.Colour = Inv.Colour.WhiteSmoke;
      TitleLabel.Size.SetHeight(56);
      TitleLabel.Font.Medium();
      TitleLabel.Font.Size = 28;
      TitleLabel.Font.Colour = Inv.Colour.DimGray;

      var TitleDivider = Base.NewFrame();
      DrawerDock.AddHeader(TitleDivider);
      TitleDivider.Size.SetHeight(1);
      TitleDivider.Background.Colour = Inv.Colour.LightGray;

      this.Drawer = new Drawer(Application, Base);
      DrawerDock.AddClient(Drawer);

      var DrawerDivider = Base.NewFrame();
      DrawerDivider.Size.SetWidth(1);
      DrawerDivider.Background.Colour = Inv.Colour.LightGray;

      var ClientOverlay = Base.NewOverlay();
      MainDock.AddClient(ClientOverlay);

      this.ClientFrame = Base.NewFrame();
      ClientOverlay.AddPanel(ClientFrame);
      ClientFrame.Margin.Set(0, 56, 0, 0);

      // toolbar is 'on top' of client frame.
      this.Toolbar = new Toolbar(Application, Base);
      ClientOverlay.AddPanel(Toolbar);
      Toolbar.Alignment.TopStretch();
      
      this.MenuIcon = Toolbar.AddLeadingIcon();
      MenuIcon.Image = Inv.Material.Resources.Images.ViewHeadlineWhite;
      MenuIcon.SingleTapEvent += () =>
      {
        if (!IsWide())
        {
          this.MenuScrim = new Scrim(Base);
          MenuScrim.AddContent(DrawerDock);
          MenuScrim.DismissEvent += () =>
          {
            if (MenuScrim != null)
            {
              MenuScrim.RemoveContent(DrawerDock);
              MainOverlay.RemovePanel(MenuScrim);
              
              this.MenuScrim = null;
            }
          };

          MainOverlay.AddPanel(MenuScrim);
        }
      };

      Base.ArrangeEvent += () =>
      {
        var ArrangeWide = IsWide();

        DrawerDock.Size.SetWidth(Math.Min(300, Application.Window.Width - 64));

        MenuIcon.Visibility.Set(!ArrangeWide);
        MenuIcon.Background.Colour = Toolbar.BackgroundColour;

        MainDock.RemoveHeaders();

        if (ArrangeWide)
        {
          if (MenuScrim != null)
            MenuScrim.Dismiss();

          MainDock.AddHeader(DrawerDock);
          MainDock.AddHeader(DrawerDivider);
        }

        Toolbar.Padding.Set(ArrangeWide ? 52 : 0, 0, ArrangeWide ? 16 : 0, 0);
      };
    }

    public string Title
    {
      get { return TitleLabel.Text; }
      set { TitleLabel.Text = value; }
    }
    public Toolbar Toolbar { get; private set; }
    public Drawer Drawer { get; private set; }
    public event Action ArrangeEvent
    {
      add { Base.ArrangeEvent += value; }
      remove { Base.ArrangeEvent -= value; }
    }
    public event Action ComposeEvent
    {
      add { Base.ComposeEvent += value; }
      remove { Base.ComposeEvent -= value; }
    }
    public event Action GestureBackwardEvent
    {
      add { Base.GestureBackwardEvent += value; }
      remove { Base.GestureBackwardEvent -= value; }
    }
    public event Action GestureForwardEvent
    {
      add { Base.GestureForwardEvent += value; }
      remove { Base.GestureForwardEvent -= value; }
    }
    public event Action<Inv.Keystroke> KeystrokeEvent
    {
      add { Base.KeystrokeEvent += value; }
      remove { Base.KeystrokeEvent -= value; }
    }

    public void Rearrange()
    {
      Base.Rearrange();
    }
    public void GestureBackward()
    {
      Base.GestureBackward();
    }
    public void GestureForward()
    {
      Base.GestureForward();
    }
    public void SetClient(Inv.Panel Panel)
    {
      if (MenuScrim != null)
        MenuScrim.Dismiss();

      ClientFrame.Content = Panel;
    }

    public static implicit operator Inv.Surface(NavigationSurface Surface)
    {
      return Surface != null ? Surface.Base : null;
    }

    internal Inv.Surface Base { get; private set; }

    private bool IsWide()
    {
      return Application.Window.Width > 1000;
    }

    private Application Application;
    private IconButton MenuIcon;
    private Label TitleLabel;
    private Frame ClientFrame;
    private Scrim MenuScrim;
  }

  public sealed class Toolbar
  {
    public Toolbar(Application Application, Inv.Surface Surface)
    {
      this.Base = Surface.NewHorizontalDock();
      Base.Background.Colour = Application.Theme.Colour;
      Base.Size.SetHeight(56);
      Base.Elevation.Set(2);

      this.LeadingStack = Surface.NewHorizontalStack();
      Base.AddHeader(LeadingStack);

      this.TitleLabel = Surface.NewLabel();
      Base.AddClient(TitleLabel);
      TitleLabel.Font.Colour = Inv.Colour.White;
      TitleLabel.Font.Size = 20;
      TitleLabel.Font.Medium();
      TitleLabel.Alignment.CenterLeft();

      this.TrailingStack = Surface.NewHorizontalStack();
      Base.AddFooter(TrailingStack);

      this.IconButtonList = new DistinctList<IconButton>();
    }

    public string Title
    {
      get { return TitleLabel.Text; }
      set { TitleLabel.Text = value; }
    }
    public Inv.Colour BackgroundColour
    {
      get { return Base.Background.Colour; }
      set 
      { 
        Base.Background.Colour = value;
        foreach (var IconButton in IconButtonList)
          IconButton.Background.Colour = value;
      }
    }
    public Inv.Colour ForegroundColour
    {
      get { return TitleLabel.Font.Colour; }
      set { TitleLabel.Font.Colour = value; }
    }

    public Inv.Material.IconButton AddLeadingIcon(Inv.Image Image = null)
    {
      var Result = new Inv.Material.IconButton(Base.Surface);
      Result.Image = Image;
      Result.Background.Colour = BackgroundColour;

      LeadingStack.AddPanel(Result);

      return Result;
    }
    public Inv.Material.IconButton AddTrailingIcon(Inv.Image Image = null)
    {
      var Result = new Inv.Material.IconButton(Base.Surface);
      Result.Image = Image;
      Result.Background.Colour = BackgroundColour;

      TrailingStack.AddPanel(Result);

      return Result;
    }

    internal Inv.Alignment Alignment
    {
      get { return Base.Alignment; }
    }
    internal Inv.Edge Padding
    {
      get { return Base.Padding; }
    }

    public static implicit operator Inv.Panel(Toolbar Toolbar)
    {
      return Toolbar != null ? Toolbar.Base : null;
    }

    private Inv.Dock Base;
    private Inv.Label TitleLabel;
    private Inv.Stack LeadingStack;
    private Inv.Stack TrailingStack;
    private Inv.DistinctList<IconButton> IconButtonList;
  }

  public sealed class FloatingActionButton
  {
    public FloatingActionButton(Application Application, Inv.Surface Surface)
    {
      this.Base = Surface.NewButton();
      Base.Size.Set(56, 56);
      Base.Corner.Set(28);
      Base.Elevation.Set(2);
      Base.Background.Colour = Application.Theme.Colour;

      this.Graphic = Surface.NewGraphic();
      Base.Content = Graphic;
      Graphic.Alignment.Center();
      Graphic.Size.Set(24, 24);
    }

    public Inv.Colour Colour
    {
      get { return Base.Background.Colour; }
      set { Base.Background.Colour = value; }
    }
    public Inv.Image Image
    {
      get { return Graphic.Image; }
      set { Graphic.Image = value; }
    }
    public event Action SingleTapEvent
    {
      add { Base.SingleTapEvent += value; }
      remove { Base.SingleTapEvent -= value; }
    }

    public static implicit operator Inv.Panel(FloatingActionButton Button)
    {
      return Button != null ? Button.Base : null;
    }

    private Inv.Button Base;
    private Graphic Graphic;
  }

  public sealed class RaisedButton
  {
    public RaisedButton(Inv.Surface Surface)
    {
      this.Base = Surface.NewButton();
      Base.Size.SetHeight(36);
      Base.Padding.Set(16, 0, 16, 0);
      Base.Elevation.Set(2);

      this.Label = Surface.NewLabel();
      Base.Content = Label;
      Label.JustifyCenter();
      Label.Font.Colour = Inv.Colour.White;
      Label.Font.Size = 14;
      Label.Font.Medium();
    }

    public Inv.Colour Colour
    {
      get { return Base.Background.Colour; }
      set { Base.Background.Colour = value; }
    }
    public string Caption
    {
      get { return Label.Text; }
      set { Label.Text = value.NullAsEmpty().ToUpper(); }
    }
    public event Action SingleTapEvent
    {
      add { Base.SingleTapEvent += value; }
      remove { Base.SingleTapEvent -= value; }
    }

    public static implicit operator Inv.Panel(RaisedButton Button)
    {
      return Button != null ? Button.Base : null;
    }

    private Inv.Button Base;
    private Inv.Label Label;
  }

  public sealed class FlatButton
  {
    public FlatButton(Inv.Surface Surface)
    {
      this.Base = Surface.NewButton();
      Base.Size.SetHeight(36);
      Base.Padding.Set(16, 0, 16, 0);

      this.Label = Surface.NewLabel();
      Base.Content = Label;
      Label.JustifyCenter();
      Label.Font.Size = 14;
      Label.Font.Medium();
    }

    public Inv.Colour Colour
    {
      get { return Label.Font.Colour; }
      set { Label.Font.Colour = value; }
    }
    public string Caption
    {
      get { return Label.Text; }
      set { Label.Text = value.NullAsEmpty().ToUpper(); }
    }
    public event Action SingleTapEvent
    {
      add { Base.SingleTapEvent += value; }
      remove { Base.SingleTapEvent -= value; }
    }

    public static implicit operator Inv.Panel(FlatButton Button)
    {
      return Button != null ? Button.Base : null;
    }

    internal Inv.Edge Margin
    {
      get { return Base.Margin; }
    }
    internal Inv.Background Background
    {
      get { return Base.Background; }
    }

    private Inv.Button Base;
    private Inv.Label Label;
  }

  public sealed class IconButton
  {
    public IconButton(Inv.Surface Surface)
    {
      this.Base = Surface.NewButton();
      Base.Size.Set(36, 36);
      Base.Margin.Set(10, 0, 10, 0);

      this.Graphic = Surface.NewGraphic();
      Base.Content = Graphic;
      Graphic.Size.Set(24, 24);
      Graphic.Alignment.Center();
    }

    public Inv.Image Image
    {
      get { return Graphic.Image; }
      set { Graphic.Image = value; }
    }
    public event Action SingleTapEvent
    {
      add { Base.SingleTapEvent += value; }
      remove { Base.SingleTapEvent -= value; }
    }

    public static implicit operator Inv.Panel(IconButton Button)
    {
      return Button != null ? Button.Base : null;
    }

    internal Inv.Alignment Alignment
    {
      get { return Base.Alignment; }
    }
    internal Inv.Edge Margin
    {
      get { return Base.Margin; }
    }
    internal Inv.Background Background
    {
      get { return Base.Background; }
    }
    internal Inv.Visibility Visibility
    {
      get { return Base.Visibility; }
    }

    private Inv.Button Base;
    private Inv.Graphic Graphic;
  }

  public sealed class Scrim
  {
    internal Scrim(Inv.Surface Surface)
    {
      this.Base = Surface.NewOverlay();
      Base.Background.Colour = Inv.Colour.Black.Opacity(1.0F / 3.0F);

      var Shade = Surface.NewButton();
      Base.AddPanel(Shade);
      Shade.SingleTapEvent += () => Dismiss();
      Shade.Background.Colour = Inv.Colour.Transparent;
    }

    public event Action DismissEvent;

    public void Dismiss()
    {
      if (DismissEvent != null)
        DismissEvent();
    }

    internal Inv.Surface Surface
    {
      get { return Base.Surface; }
    }

    internal void AddContent(Inv.Panel Panel)
    {
      Base.AddPanel(Panel);
    }
    internal void RemoveContent(Inv.Panel Panel)
    {
      Base.RemovePanel(Panel);
    }

    public static implicit operator Inv.Panel(Scrim Scrim)
    {
      return Scrim != null ? Scrim.Base : null;
    }

    private Inv.Overlay Base;
  }

  public sealed class Dialog
  {
    public Dialog(Inv.Surface Surface)
    {
      this.Base = new Scrim(Surface);
      Base.DismissEvent += () => Hide();

      this.Dock = Surface.NewVerticalDock();
      Base.AddContent(Dock);
      Dock.Background.Colour = Inv.Colour.White;
      Dock.Padding.Set(0, 24, 0, 0);
      Dock.Alignment.Center();
      Dock.Elevation.Set(24);

      this.TitleLabel = Surface.NewLabel();
      Dock.AddHeader(TitleLabel);
      TitleLabel.Margin.Set(24, 0, 24, 20);
      TitleLabel.JustifyLeft();
      TitleLabel.Font.Colour = Inv.Colour.Black;
      TitleLabel.Font.Size = 20;
      TitleLabel.Font.Medium();

      this.ContentFrame = Surface.NewFrame();
      Dock.AddClient(ContentFrame);
      ContentFrame.Margin.Set(24, 0, 24, 24);

      this.Strip = new Strip(Surface);
      Dock.AddFooter(Strip);
      Strip.Margin.Set(24, 4, 4, 4);
      Strip.BackgroundColour = Dock.Background.Colour;
      Strip.SelectEvent += () => Hide();
    }

    public string Title
    {
      get { return TitleLabel.Text; }
      set 
      { 
        TitleLabel.Text = value;
        TitleLabel.Visibility.Set(value != null);
      }
    }
    public Inv.Panel Content
    {
      get { return ContentFrame.Content; }
      set { ContentFrame.Content = value; }
    }
    public Strip Strip { get; private set; }
    public event Action HideEvent;

    public void SetContentText(string Text)
    {
      var Label = Dock.Surface.NewLabel();
      Content = Label;
      Label.JustifyLeft();
      Label.Font.Colour = Inv.Colour.DimGray;
      Label.Font.Size = 16;
      Label.Text = Text;
    }
    public void Hide()
    {
      if (HideEvent != null)
        HideEvent();
    }

    public static implicit operator Inv.Panel(Dialog Dialog)
    {
      return Dialog != null ? Dialog.Base : null;
    }

    internal Inv.Surface Surface
    {
      get { return Base.Surface; }
    }

    private Scrim Base;
    private Inv.Dock Dock;
    private Inv.Label TitleLabel;
    private Inv.Frame ContentFrame;
  }

  public sealed class Strip
  {
    public Strip(Inv.Surface Surface)
    {
      this.Base = Surface.NewHorizontalDock();

      // TODO: vertical orientation on narrow displays?

      this.ActionList = new DistinctList<StripAction>();
      this.IconList = new DistinctList<StripIcon>();
    }

    public StripAction AddDismissiveAction(string Caption = null)
    {
      var Result = NewAction(Caption);

      Base.AddHeader(Result);

      return Result;
    }
    public StripAction AddAffirmitiveAction(string Caption = null)
    {
      var Result = NewAction(Caption);

      Base.AddFooter(Result);

      return Result;
    }
    public StripIcon AddDismissiveIcon(Inv.Image Image = null)
    {
      var Result = NewIcon(Image);

      Base.AddHeader(Result);

      return Result;
    }
    public StripIcon AddAffirmitiveIcon(Inv.Image Image = null)
    {
      var Result = NewIcon(Image);

      Base.AddFooter(Result);

      return Result;
    }

    internal Inv.Surface Surface
    {
      get { return Base.Surface; }
    }
    internal Inv.Edge Margin
    {
      get { return Base.Margin; }
    }
    internal Inv.Colour BackgroundColour
    {
      get { return BackgroundColourField; }
      set
      {
        if (BackgroundColourField != value)
        {
          this.BackgroundColourField = value;

          foreach (var Action in ActionList)
            Action.Background.Colour = value;
        }
      }
    }
    internal event Action SelectEvent;

    internal void SelectInvoke()
    {
      if (SelectEvent != null)
        SelectEvent();
    }

    private StripAction NewAction(string Caption)
    {
      var Result = new StripAction(this);
      Result.Caption = Caption;

      ActionList.Add(Result);

      return Result;
    }
    private StripIcon NewIcon(Inv.Image Image)
    {
      var Result = new StripIcon(this);
      Result.Image = Image;

      IconList.Add(Result);

      return Result;
    }

    public static implicit operator Inv.Panel(Strip Strip)
    {
      return Strip != null ? Strip.Base : null;
    }

    private Inv.Dock Base;
    private Inv.DistinctList<StripAction> ActionList;
    private Inv.DistinctList<StripIcon> IconList;
    private Inv.Colour BackgroundColourField;
  }

  public sealed class StripAction
  {
    public StripAction(Strip Strip)
    {
      this.Base = new FlatButton(Strip.Surface);
      Base.Margin.Set(4);
      Base.Background.Colour = Strip.BackgroundColour;
      Base.SingleTapEvent += () => Strip.SelectInvoke();
    }

    public string Caption
    {
      get { return Base.Caption; }
      set { Base.Caption = value; }
    }
    public Inv.Colour Colour
    {
      get { return Base.Colour; }
      set { Base.Colour = value; }
    }
    public event Action SingleTapEvent
    {
      add { Base.SingleTapEvent += value; }
      remove { Base.SingleTapEvent -= value; }
    }

    internal Inv.Background Background
    {
      get { return Base.Background; }
    }

    public static implicit operator Inv.Panel(StripAction Action)
    {
      return Action != null ? Action.Base : null;
    }

    private FlatButton Base;
  }

  public sealed class StripIcon
  {
    public StripIcon(Strip Strip)
    {
      this.Base = new IconButton(Strip.Surface);
      Base.Margin.Set(4);
      Base.Background.Colour = Strip.BackgroundColour;
      Base.SingleTapEvent += () => Strip.SelectInvoke();
    }

    public Inv.Image Image
    {
      get { return Base.Image; }
      set { Base.Image = value; }
    }
    public event Action SingleTapEvent
    {
      add { Base.SingleTapEvent += value; }
      remove { Base.SingleTapEvent -= value; }
    }

    internal Inv.Background Background
    {
      get { return Base.Background; }
    }

    public static implicit operator Inv.Panel(StripIcon Icon)
    {
      return Icon != null ? Icon.Base : null;
    }

    private IconButton Base;
  }

  public sealed class Chip
  {
    public Chip(Inv.Surface Surface)
    {
      this.Base = Surface.NewButton();
      Base.Size.SetHeight(32);
      Base.Corner.Set(16);
      Base.Background.Colour = Inv.Colour.LightGray;
      //Base.Elevation.Set(2, Inv.Colour.DimGray);

      var Dock = Surface.NewHorizontalDock();
      Base.Content = Dock;

      this.IconGraphic = Surface.NewGraphic();
      Dock.AddHeader(IconGraphic);
      IconGraphic.Size.Set(32, 32);
      IconGraphic.Corner.Set(16);

      this.Label = Surface.NewLabel();
      Dock.AddClient(Label);
      Label.Font.Regular();
      Label.Font.Size = 14;
      Label.Font.Colour = Inv.Colour.Black.Opacity(0.87F);

      this.RemoveGraphic = Surface.NewGraphic();
      Dock.AddFooter(RemoveGraphic);
      RemoveGraphic.Size.Set(24, 24);
      RemoveGraphic.Corner.Set(12);
      RemoveGraphic.Image = Resources.Images.RemoveCircleGray;
      RemoveGraphic.Opacity.Set(0.87F);
      RemoveGraphic.Margin.Set(4, 0, 4, 0);

      Arrange();
    }

    public Inv.Image Icon
    {
      get { return IconGraphic.Image; }
      set 
      { 
        IconGraphic.Image = value;
        Arrange();
      }
    }
    public string Text
    {
      get { return Label.Text; }
      set { Label.Text = value; }
    }
    public event Action RemoveEvent
    {
      add
      {
        this.RemoveAction += value;
        Arrange();
      }
      remove
      {
        this.RemoveAction -= value;
        Arrange();
      }
    }

    public static implicit operator Inv.Panel(Chip Chip)
    {
      return Chip != null ? Chip.Base : null;
    }

    private void Arrange()
    {
      IconGraphic.Visibility.Set(IconGraphic.Image != null);
      RemoveGraphic.Visibility.Set(this.RemoveAction != null);

      var LeftMargin = IconGraphic.Visibility.Get() ? 8 : 12;
      var RightMargin = RemoveGraphic.Visibility.Get() ? 0 : 12;

      Label.Margin.Set(LeftMargin, 0, RightMargin, 0);
    }

    private Inv.Button Base;
    private Inv.Graphic IconGraphic;
    private Inv.Label Label;
    private Inv.Graphic RemoveGraphic;
    private Action RemoveAction;
  }

  public sealed class Snackbar
  {
    public Snackbar(Inv.Surface Surface)
    {
      this.Base = Surface.NewButton();
      Base.Size.SetHeight(48);
      Base.Alignment.BottomStretch();
      //Base.Corner.Set(2); // desktop only?

      var Dock = Surface.NewHorizontalDock();
      Base.Content = Dock;
      Dock.Background.Colour = Inv.Colour.FromArgb(255, 50, 50, 50);
      Dock.Padding.Set(24, 0, 24, 0);

      this.Label = Surface.NewLabel();
      Dock.AddClient(Label);
      Label.Font.Regular();
      Label.Font.Size = 14;
      Label.Font.Colour = Inv.Colour.White;

      this.Button = new FlatButton(Surface);
      Dock.AddFooter(Button);
      Button.Background.Colour = Dock.Background.Colour;
      Button.Margin.Set(24, 0, 0, 0);
    }

    public string Text
    {
      get { return Label.Text; }
      set { Label.Text = value; }
    }
    public FlatButton Button { get; private set; }

    public static implicit operator Inv.Panel(Snackbar Snackbar)
    {
      return Snackbar != null ? Snackbar.Base : null;
    }
 
    private Button Base;
    private Label Label;
  }

  public sealed class Popup
  {
    public Popup(Inv.Surface Surface)
    {
      this.Base = Surface.NewVerticalScroll();
      Base.Background.Colour = Inv.Colour.White;
      Base.Border.Set(1);
      Base.Border.Colour = Inv.Colour.DarkGray;
      Base.Corner.Set(2);
      Base.Elevation.Set(4);

      this.Stack = Surface.NewVerticalStack();
      Base.Content = Stack;
      Stack.Padding.Set(0, 8, 0, 8);

      this.ItemList = new DistinctList<PopupItem>();
    }

    public PopupItem AddItem()
    {
      var Result = new PopupItem(this);
      ItemList.Add(Result);

      Stack.AddPanel(Result);

      return Result;
    }
    public PopupItem AddItem(string Text)
    {
      var Result = AddItem();
      Result.Caption = Text;
      return Result;
    }
    public PopupItem AddItem(Inv.Image Icon, string Text)
    {
      var Result = AddItem();
      Result.Icon = Icon;
      Result.Caption = Text;
      return Result;
    }
    public PopupItem AddItem(string Text, string Shortcut)
    {
      var Result = AddItem();
      Result.Caption = Text;
      Result.Shortcut = Shortcut;
      return Result;
    }
    public PopupItem AddItem(Inv.Image Icon, string Text, string Shortcut)
    {
      var Result = AddItem();
      Result.Icon = Icon;
      Result.Caption = Text;
      Result.Shortcut = Shortcut;
      return Result;
    }
    public Divider AddDivider()
    {
      var Result = new Divider(Surface);

      Stack.AddPanel(Result);

      return Result;
    }

    public static implicit operator Inv.Panel(Popup Popup)
    {
      return Popup != null ? Popup.Base : null;
    }

    internal Inv.Surface Surface
    {
      get { return Base.Surface; }
    }

    private Inv.Scroll Base;
    private Inv.Stack Stack;
    private Inv.DistinctList<PopupItem> ItemList;
  }

  public sealed class Divider
  {
    internal Divider(Inv.Surface Surface)
    {
      this.Base = Surface.NewFrame();
      Base.Size.SetHeight(1);
      Base.Background.Colour = Inv.Colour.WhiteSmoke;
      Base.Margin.Set(0, 8, 0, 8);
    }

    public static implicit operator Inv.Panel(Divider Divider)
    {
      return Divider != null ? Divider.Base : null;
    }

    private Frame Base;
  }

  public sealed class Subheading
  {
    internal Subheading(Inv.Surface Surface)
    {
      this.Base = Surface.NewLabel();
      Base.Padding.Set(16, 0, 16, 16);
      Base.Font.Colour = Inv.Colour.DimGray;
      Base.Font.Size = 14;
      Base.Font.Medium();
      Base.JustifyLeft();
      Base.Size.SetHeight(48);
    }

    public string Text
    {
      get { return Base.Text; }
      set { Base.Text = value; }
    }
    public Inv.Colour Colour
    {
      get { return Base.Font.Colour; }
      set { Base.Font.Colour = value; }
    }

    public static implicit operator Inv.Panel(Subheading Subheading)
    {
      return Subheading != null ? Subheading.Base : null;
    }

    private Inv.Label Base;
  }

  public sealed class PopupItem
  {
    internal PopupItem(Popup Popup)
    {
      this.Popup = Popup;

      this.Base = Popup.Surface.NewButton();
      Base.Padding.Set(24, 0, 24, 0);
      Base.Background.Colour = Inv.Colour.White;
      Base.Size.SetHeight(48);

      var Dock = Popup.Surface.NewHorizontalDock();
      Base.Content = Dock;

      this.IconGraphic = Popup.Surface.NewGraphic();
      Dock.AddHeader(IconGraphic);
      IconGraphic.Size.Set(24, 24);
      IconGraphic.Opacity.Set(0.54F);

      this.CaptionLabel = Popup.Surface.NewLabel();
      Dock.AddClient(CaptionLabel);
      CaptionLabel.Font.Regular();
      CaptionLabel.Font.Size = 16;
      CaptionLabel.Font.Colour = Inv.Colour.Black;
      CaptionLabel.Margin.Set(16, 0, 0, 0);

      this.ShortcutLabel = Popup.Surface.NewLabel();
      Dock.AddFooter(ShortcutLabel);
      ShortcutLabel.Font.Regular();
      ShortcutLabel.Font.Size = 16;
      ShortcutLabel.Font.Colour = Inv.Colour.Black;
      ShortcutLabel.Margin.Set(16, 0, 0, 0);
    }

    public Inv.Image Icon
    {
      get { return IconGraphic.Image; }
      set { IconGraphic.Image = value; }
    }
    public string Caption
    {
      get { return CaptionLabel.Text; }
      set { CaptionLabel.Text = value; }
    }
    public string Shortcut
    {
      get { return ShortcutLabel.Text; }
      set { ShortcutLabel.Text = value; }
    }
    public event Action SingleTapEvent
    {
      add { Base.SingleTapEvent += value; }
      remove { Base.SingleTapEvent -= value; }
    }

    public static implicit operator Inv.Panel(PopupItem PopupItem)
    {
      return PopupItem != null ? PopupItem.Base : null;
    }

    private Popup Popup;
    private Inv.Button Base;
    private Inv.Label CaptionLabel;
    private Inv.Graphic IconGraphic;
    private Inv.Label ShortcutLabel;
  }

  public sealed class Card
  {
    public Card(Inv.Surface Surface)
    {
      this.Base = Surface.NewVerticalDock();
      Base.Background.Colour = Inv.Colour.White;
      Base.Corner.Set(2);
      Base.Elevation.Set(2);

      this.Header = new Header(Surface);
      Base.AddHeader(Header);

      this.Stack = Surface.NewVerticalStack();
      Base.AddClient(Stack);

      this.Strip = new Strip(Surface);
      Base.AddFooter(Strip);
      Strip.Margin.Set(8);
      Strip.BackgroundColour = Base.Background.Colour;
    }

    public Header Header { get; private set; }
    public Strip Strip { get; private set; }
    public Inv.Colour BackgroundColour
    {
      get { return Base.Background.Colour; }
      set
      {
        Base.Background.Colour = value;
        Strip.BackgroundColour = value;
      }
    }

    public CardSubject AddSubject(string Text = null)
    {
      var Result = new CardSubject(this);
      Result.Text = Text;

      Stack.AddPanel(Result);

      return Result;
    }
    public CardMedia AddMedia(Inv.Image Image = null)
    {
      var Result = new CardMedia(this);
      Result.Image = Image;

      Stack.AddPanel(Result);

      return Result;
    }
    public CardSupport AddSupport(string Text = null)
    {
      var Result = new CardSupport(this);
      Result.Text = Text;

      Stack.AddPanel(Result);

      return Result;
    }

    internal Inv.Surface Surface
    {
      get { return Base.Surface; }
    }

    public static implicit operator Inv.Panel(Card Card)
    {
      return Card != null ? Card.Base : null;
    }

    private Inv.Dock Base;
    private Inv.Stack Stack;
  }

  public sealed class CardMedia
  {
    internal CardMedia(Card Card)
    {
      this.Base = Card.Surface.NewGraphic();
    }

    public Inv.Image Image
    {
      get { return Base.Image; }
      set { Base.Image = value; }
    }

    public static implicit operator Inv.Panel(CardMedia CardMedia)
    {
      return CardMedia != null ? CardMedia.Base : null;
    }

    private Inv.Graphic Base;
  }

  public sealed class CardSubject
  {
    internal CardSubject(Card Card)
    {
      this.Base = Card.Surface.NewLabel();
      Base.Font.Colour = Inv.Colour.Black;
      Base.Font.Size = 24;
      Base.Margin.Set(16, 24, 16, 0);
    }

    public string Text
    {
      get { return Base.Text; }
      set { Base.Text = value; }
    }

    public static implicit operator Inv.Panel(CardSubject CardSubject)
    {
      return CardSubject != null ? CardSubject.Base : null;
    }

    private Inv.Label Base;
  }

  public sealed class CardSupport
  {
    internal CardSupport(Card Card)
    {
      this.Base = Card.Surface.NewLabel();
      Base.Font.Colour = Inv.Colour.DimGray;
      Base.Font.Size = 14;
      Base.Margin.Set(16, 16, 16, 0);
    }

    public string Text
    {
      get { return Base.Text; }
      set { Base.Text = value; }
    }

    public static implicit operator Inv.Panel(CardSupport CardSupport)
    {
      return CardSupport != null ? CardSupport.Base : null;
    }

    private Inv.Label Base;
  }

  public sealed class Header
  {
    public Header(Inv.Surface Surface)
    {
      this.Base = Surface.NewHorizontalDock();

      this.AvatarGraphic = Surface.NewGraphic();
      Base.AddHeader(AvatarGraphic);
      AvatarGraphic.Size.Set(40, 40);
      AvatarGraphic.Corner.Set(20);
      AvatarGraphic.Margin.Set(0, 0, 8, 0);

      var HeaderStack = Surface.NewVerticalStack();
      Base.AddClient(HeaderStack);
      HeaderStack.Alignment.CenterStretch();

      this.TitleLabel = Surface.NewLabel();
      HeaderStack.AddPanel(TitleLabel);

      this.SubtitleLabel = Surface.NewLabel();
      HeaderStack.AddPanel(SubtitleLabel);
      SubtitleLabel.Font.Size = 14;
      SubtitleLabel.Font.Colour = Inv.Colour.DimGray;

      Refresh();
    }

    public Inv.Image Avatar
    {
      get { return AvatarGraphic.Image; }
      set
      {
        AvatarGraphic.Image = value;
        Refresh();
      }
    }
    public string Title
    {
      get { return TitleLabel.Text; }
      set
      {
        TitleLabel.Text = value;
        Refresh();
      }
    }
    public string Subtitle
    {
      get { return SubtitleLabel.Text; }
      set
      {
        SubtitleLabel.Text = value;
        Refresh();
      }
    }
    public Inv.Colour BackgroundColour
    {
      get { return Base.Background.Colour; }
      set 
      { 
        Base.Background.Colour = value;
        AvatarGraphic.Background.Colour = value != null ? value.Lighten(0.25F) : (Inv.Colour)null;
      }
    }
    public Inv.Colour ForegroundColour
    {
      get { return TitleLabel.Font.Colour; }
      set
      {
        TitleLabel.Font.Colour = value;
        SubtitleLabel.Font.Colour = value != null ? value.Darken(0.25F) : (Inv.Colour)null;
      }
    }

    private void Refresh()
    {
      var HasTitle = Title != null;
      var HasSubtitle = Subtitle != null;
      var HasIcon = Avatar != null;

      AvatarGraphic.Visibility.Set(HasIcon);
      TitleLabel.Visibility.Set(HasTitle);
      SubtitleLabel.Visibility.Set(HasSubtitle);

      if (HasTitle && HasSubtitle && HasIcon)
      {
        Base.Padding.Set(16);
        TitleLabel.Font.Size = 14;
      }
      else if (HasTitle && HasSubtitle)
      {
        Base.Padding.Set(16);
        TitleLabel.Font.Size = 24;
      }
      else if (HasTitle || HasIcon || HasSubtitle)
      {
        Base.Padding.Set(24);
        TitleLabel.Font.Size = 24;
      }
      else
      {
        Base.Padding.Clear();
      }
    }

    public static implicit operator Inv.Panel(Header Header)
    {
      return Header != null ? Header.Base : null;
    }

    private Inv.Label TitleLabel;
    private Inv.Label SubtitleLabel;
    private Inv.Graphic AvatarGraphic;
    private Inv.Dock Base;
  }

  public sealed class Drawer
  {
    public Drawer(Application Application, Inv.Surface Surface)
    {
      this.Application = Application;

      this.Base = Surface.NewFlow();
      Base.Padding.Set(0, 8, 0, 8);
      Base.Background.Colour = Inv.Colour.WhiteSmoke;

      this.Section = Base.AddSection();
      Section.ItemQuery += (Item) => ItemList[Item];

      this.ItemList = new DistinctList<DrawerItem>();
    }

    internal Inv.Surface Surface
    {
      get { return Base.Surface; }
    }

    public DrawerItem AddItem()
    {
      var Result = new DrawerItem(Application, Base.Surface);
      ItemList.Add(Result);
      Result.BackgroundColour = Base.Background.Colour;

      Section.SetItemCount(ItemList.Count);

      return Result;
    }
    public DrawerItem AddItem(string Caption)
    {
      var Result = AddItem();
      Result.Caption = Caption;
      return Result;
    }
    public void ScrollItem(DrawerItem Item)
    {
      Section.ScrollToItemAtIndex(ItemList.IndexOf(Item));
    }

    public static implicit operator Inv.Panel(Drawer Drawer)
    {
      return Drawer != null ? Drawer.Base : null;
    }

    private Application Application;
    private Inv.Flow Base;
    private Inv.DistinctList<DrawerItem> ItemList;
    private FlowSection Section;
  }

  public sealed class DrawerItem
  {
    internal DrawerItem(Application Application, Inv.Surface Surface)
    {
      this.Application = Application;

      this.Base = Surface.NewButton();
      Base.Padding.Set(16, 0, 16, 0);
      Base.Size.SetHeight(48);
      Base.IsFocusable = false;

      this.CaptionLabel = Surface.NewLabel();
      Base.Content = CaptionLabel;
      CaptionLabel.Font.Regular();
      CaptionLabel.Font.Size = 16;
      CaptionLabel.Font.Colour = Inv.Colour.Black;
      CaptionLabel.Margin.Set(16, 0, 0, 0);
    }

    public string Caption
    {
      get { return CaptionLabel.Text; }
      set { CaptionLabel.Text = value; }
    }
    public bool IsHighlighted
    {
      get { return IsHighlightedField; }
      set
      {
        if (IsHighlightedField != value)
        {
          this.IsHighlightedField = value;

          if (IsHighlightedField)
          {
            CaptionLabel.Font.Colour = Application.Theme.Colour;
            CaptionLabel.Font.Medium();
          }
          else
          {
            CaptionLabel.Font.Colour = Inv.Colour.Black;
            CaptionLabel.Font.Regular();
          }
        }
      }
    }
    public event Action SingleTapEvent
    {
      add { Base.SingleTapEvent += value; }
      remove { Base.SingleTapEvent -= value; }
    }

    public void SingleTap()
    {
      Base.SingleTap();
    }

    internal Inv.Colour BackgroundColour
    {
      get { return Base.Background.Colour; }
      set { Base.Background.Colour = value ?? Inv.Colour.WhiteSmoke; }
    }

    public static implicit operator Inv.Panel(DrawerItem DrawerItem)
    {
      return DrawerItem != null ? DrawerItem.Base : null;
    }

    private Application Application;
    private Inv.Button Base;
    private Inv.Label CaptionLabel;
    private bool IsHighlightedField;
  }

  public sealed class List
  {
    public List(Application Application, Inv.Surface Surface)
    {
      this.Application = Application;
      this.Base = Surface.NewVerticalScroll();
      
      this.Stack = Surface.NewVerticalStack();
      Base.Content = Stack;
      Stack.Padding.Set(0, 8, 0, 8);

      this.ItemList = new DistinctList<ListItem>();

      this.Colour = Inv.Colour.White;
    }

    public ListItem AddItem()
    {
      var Result = new ListItem(Application, Base.Surface);
      ItemList.Add(Result);
      Result.BackgroundColour = Colour;

      Stack.AddPanel(Result);

      return Result;
    }
    public ListItem AddItem(string Text)
    {
      var Result = AddItem();
      Result.Caption = Text;
      return Result;
    }
    public ListItem AddItem(Inv.Image Avatar, string Text)
    {
      var Result = AddItem();
      Result.Avatar = Avatar;
      Result.Caption = Text;
      return Result;
    }
    public Divider AddDivider()
    {
      var Result = new Divider(Surface);

      Stack.AddPanel(Result);

      return Result;
    }
    public Subheading AddSubheading(string Text = null)
    {
      var Result = new Subheading(Surface);
      Result.Text = Text;

      Stack.AddPanel(Result);

      return Result;
    }

    internal Application Application { get; private set; }
    internal Inv.Surface Surface
    {
      get { return Base.Surface; }
    }
    internal Inv.Colour Colour
    {
      get { return Base.Background.Colour; }
      set
      {
        Base.Background.Colour = value;
        foreach (var Item in ItemList)
          Item.BackgroundColour = value;
      }
    }

    public static implicit operator Inv.Panel(List List)
    {
      return List != null ? List.Base : null;
    }

    private Inv.Scroll Base;
    private Inv.Stack Stack;
    private Inv.DistinctList<ListItem> ItemList;
  }

  public sealed class ListItem
  {
    internal ListItem(Application Application, Inv.Surface Surface)
    {
      this.Application = Application;

      this.Base = Surface.NewHorizontalDock();

      this.Button = Surface.NewButton();
      Base.AddClient(Button);
      Button.Padding.Set(16, 0, 16, 0);
      Button.Size.SetHeight(48);

      this.Dock = Surface.NewHorizontalDock();
      Button.Content = Dock;

      this.AvatarGraphic = Surface.NewGraphic();
      Dock.AddHeader(AvatarGraphic);
      AvatarGraphic.Size.Set(40, 40);
      AvatarGraphic.Corner.Set(20);
      AvatarGraphic.Opacity.Set(0.75F);
      AvatarGraphic.Visibility.Collapse();

      this.CaptionLabel = Surface.NewLabel();
      Dock.AddClient(CaptionLabel);
      CaptionLabel.Font.Regular();
      CaptionLabel.Font.Size = 16;
      CaptionLabel.Font.Colour = Inv.Colour.Black;
      CaptionLabel.Margin.Set(16, 0, 0, 0);
    }

    public Inv.Image Avatar
    {
      get { return AvatarGraphic.Image; }
      set 
      { 
        AvatarGraphic.Image = value;
        AvatarGraphic.Visibility.Set(value != null);
      }
    }
    public string Caption
    {
      get { return CaptionLabel.Text; }
      set { CaptionLabel.Text = value; }
    }
    public event Action SingleTapEvent
    {
      add { Button.SingleTapEvent += value; }
      remove { Button.SingleTapEvent -= value; }
    }
    public bool IsHighlighted
    {
      get { return IsHighlightedField; }
      set
      {
        if (IsHighlightedField != value)
        {
          this.IsHighlightedField = value;

          if (IsHighlightedField)
          {
            CaptionLabel.Font.Colour = Application.Theme.Colour;
            CaptionLabel.Font.Medium();
          }
          else
          {
            CaptionLabel.Font.Colour = Inv.Colour.Black;
            CaptionLabel.Font.Regular();
          }
        }
      }
    }

    public void SingleTap()
    {
      Button.SingleTap();
    }
    public IconButton AddIcon(Inv.Image Image = null)
    {
      var Result = new IconButton(Button.Surface);
      Result.Image = Image;
      //Result.Margin.Clear();
      Result.Alignment.TopRight();
      Result.Background.Colour = Inv.Colour.White;

      Base.AddFooter(Result);

      return Result;
    }

    internal Inv.Colour BackgroundColour
    {
      get { return Base.Background.Colour; }
      set
      {
        Button.Background.Colour = value ?? Inv.Colour.White;
        AvatarGraphic.Background.Colour = value != null ? value.Darken(0.25F) : Inv.Colour.LightGray;
      }
    }

    public static implicit operator Inv.Panel(ListItem ListItem)
    {
      return ListItem != null ? ListItem.Base : null;
    }

    private Application Application;
    private Inv.Dock Base;
    private Inv.Button Button;
    private Inv.Label CaptionLabel;
    private Inv.Graphic AvatarGraphic;
    private Inv.Dock Dock;
    private bool IsHighlightedField;
  }
}