﻿/*! 9 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv.Material
{
  internal static class ConsoleFoundation
  {
    internal const int ChromeSize = 44;
    internal const int TileSize = 256;
    internal const int ShadowSize = 2;
    internal const int TabWidth = 200;
  }

  public sealed class ConsoleSurface
  {
    internal ConsoleSurface(Application Application)
    {
      this.Application = Application;

      this.LocationList = new Inv.DistinctList<ConsoleLocation>();
      this.ScreenList = new Inv.DistinctList<ConsoleScreen>();
      this.TabList = new Inv.DistinctList<ConsoleTab>();

      this.Base = Application.Window.NewSurface();
      Base.Background.Colour = Inv.Colour.WhiteSmoke;
      Base.ArrangeEvent += () =>
      {
        if (ActiveScreen != null)
          ActiveScreen.ArrangeInvoke();

        if (IsMore())
          CloseMore();

        Refresh();
      };
      Base.ComposeEvent += () =>
      {
        if (ActiveScreen != null)
          ActiveScreen.ComposeInvoke();
      };

      var Overlay = Base.NewOverlay();
      Base.Content = Overlay;

      this.LocationFrame = Base.NewFrame();
      Overlay.AddPanel(LocationFrame);
      LocationFrame.Margin.Set(0, 0, 0, ConsoleFoundation.ChromeSize + ConsoleFoundation.ShadowSize); // tab height + 2 for the border.

      this.MoreShade = Base.NewButton();
      Overlay.AddPanel(MoreShade);
      MoreShade.Visibility.Collapse();
      MoreShade.Background.Colour = Inv.Colour.Black.Opacity(0.50F);
      MoreShade.SingleTapEvent += () => CloseMore();

      var ClientDock = Base.NewVerticalDock();
      Overlay.AddPanel(ClientDock);

      this.MoreStack = Base.NewVerticalStack();
      ClientDock.AddFooter(MoreStack);
      MoreStack.Margin.Set(ConsoleFoundation.ChromeSize, 0, 0, 0);

      this.TabDock = Base.NewHorizontalDock();
      ClientDock.AddFooter(TabDock);
      TabDock.Background.Colour = Inv.Colour.Black;
      TabDock.Border.Set(0, ConsoleFoundation.ShadowSize, 0, 0);
      TabDock.Border.Colour = Inv.Colour.DarkGray;

      this.MenuLaunch = new ConsoleLaunch(Base);
      TabDock.AddHeader(MenuLaunch);
      MenuLaunch.Symbol = '☰';
      MenuLaunch.Size.Set(ConsoleFoundation.ChromeSize, ConsoleFoundation.ChromeSize);
      MenuLaunch.SelectEvent += () =>
      {
        CloseMore();

        if (Menu.Visibility.Get())
          CloseMenu();
        else
          OpenMenu();

        Refresh();
      };

      this.MoreLaunch = new ConsoleLaunch(Base);
      TabDock.AddFooter(MoreLaunch);
      MoreLaunch.Symbol = '⋮';
      MoreLaunch.Size.Set(ConsoleFoundation.ChromeSize, ConsoleFoundation.ChromeSize);
      MoreLaunch.SelectEvent += () =>
      {
        if (IsMore())
          CloseMore();
        else
          OpenMore();

        Refresh();
      };

      this.Menu = new ConsoleMenu(Base, Application.Theme);
      ClientDock.AddClient(Menu);

      OpenMenu();
      Refresh();
    }

    public ConsoleMenu Menu { get; private set; }
    public ConsoleScreen ActiveScreen { get; private set; }
    public event Action RefreshEvent;

    public ConsoleLocation GetLocation(string Title)
    {
      return LocationList.Find(L => L.Title == Title);
    }
    public ConsoleLocation AddLocation(string Title, Inv.Image Image)
    {
      var Result = new ConsoleLocation(Title, Image);
      LocationList.Add(Result);

      var Tile = Menu.AddTile(Title, Image);
      Tile.SelectEvent += () =>
      {
        NavigateLocation(Result);
      };

      return Result;
    }
    public void NavigateLocation(ConsoleLocation Location)
    {
      CloseMenu();

      var Screen = new ConsoleScreen(Base, Location);
      ScreenList.Add(Screen);
      Screen.Tab.SelectEvent += () =>
      {
        if (TabCount == 1 && ActiveScreen == Screen && !IsMenu())
          OpenMore();
        else
          OpenScreen(Screen);

        Refresh();
      };
      Screen.Tab.CloseEvent += () =>
      {
        CloseScreen(Screen);
        Refresh();
      };

      OpenScreen(Screen);
      Refresh();
    }
    public void NavigateTab(ConsoleTab Tab)
    {
      CloseMenu();
      OpenScreen(Tab.Screen);
    }
    public IEnumerable<ConsoleTab> GetTabs()
    {
      return TabList;
    }

    private void OpenMore()
    {
      CloseMenu();

      if (ScreenList.Count > 1)
        MoreShade.Visibility.Show();
    }
    private void CloseMore()
    {
      MoreShade.Visibility.Collapse();
    }
    private void OpenScreen(ConsoleScreen Screen)
    {
      this.ActiveScreen = Screen;

      if (ActiveScreen == null)
      {
        OpenMenu();
      }
      else
      {
        CloseMore();
        CloseMenu();

        ActiveScreen.ArrangeInvoke();
      }
    }
    private void CloseScreen(ConsoleScreen Screen)
    {
      var Index = ScreenList.IndexOf(Screen);

      if (Index >= 0)
      {
        ScreenList.RemoveAt(Index);

        if (ActiveScreen == Screen)
        {
          if (ScreenList.Count > 0)
            OpenScreen(ScreenList[Math.Min(Math.Max(0, Index), ScreenList.Count - 1)]);
          else
            OpenScreen(null);
        }
      }

      if (ScreenList.Count <= 1)
        CloseMore();
    }
    private void OpenMenu()
    {
      Menu.Visibility.Show();
    }
    private void CloseMenu()
    {
      Menu.Visibility.Collapse();
    }
    private bool IsMenu()
    {
      return Menu.Visibility.Get();
    }
    private bool IsMore()
    {
      return MoreShade.Visibility.Get();
    }
    private void Refresh()
    {
      if (Menu.Visibility.Get())
        Menu.Arrange();

      MenuLaunch.Colour = IsMenu() ? Application.Theme.Colour : Inv.Colour.Black;

      this.TabCount = (Base.Window.Width - ConsoleFoundation.ChromeSize) / ConsoleFoundation.TabWidth;

      MoreStack.RemovePanels();
      TabDock.RemoveClients();
      TabList.Clear();

      if (TabCount == 1)
      {
        if (ActiveScreen != null)
        {
          TabList.Add(ActiveScreen.Tab);
          TabDock.AddClient(ActiveScreen.Tab);
        }
      }
      else
      {
        for (var TabIndex = 0; TabIndex < TabCount; TabIndex++)
        {
          if (TabIndex < ScreenList.Count)
          {
            var Screen = ScreenList[TabIndex];
            var Tab = Screen.Tab;

            TabList.Add(Tab);
            TabDock.AddClient(Tab);
          }
          else
          {
            var Placeholder = Base.NewFrame();
            TabDock.AddClient(Placeholder);
          }
        }
      }

      foreach (var Screen in ScreenList)
      {
        var IsActive = !IsMenu() && ActiveScreen == Screen;

        Screen.Tab.Colour = IsActive ? Application.Theme.Colour : Inv.Colour.Black;
        Screen.Tab.CloseAllowed = IsActive;
      }

      if (IsMore())
      {
        foreach (var Tab in ScreenList.Select(S => S.Tab).Except(TabList))
        {
          Tab.CloseAllowed = true;

          MoreStack.AddPanel(Tab);
        }
      }

      MoreLaunch.Visibility.Set(TabCount > 1 && ScreenList.Count > TabCount);
      MoreLaunch.Colour = !IsMenu() && !IsMore() && ActiveScreen != null && !TabList.Contains(ActiveScreen.Tab) ? Application.Theme.Colour : Inv.Colour.Black;

      if (ActiveScreen == null)
      {
        // TODO: big icon?

        var TitleLabel = Base.NewLabel();
        LocationFrame.Content = TitleLabel;
        TitleLabel.Alignment.CenterStretch();
        TitleLabel.Text = "There are no open tabs." + Environment.NewLine + Environment.NewLine + "Tap ☰ below to use the menu.";
        TitleLabel.Font.Colour = Inv.Colour.Black;
        TitleLabel.Font.Size = 24;
        TitleLabel.Font.Thin();
        TitleLabel.JustifyCenter();
      }
      else if (ActiveScreen.Content == null)
      {
        var TitleLabel = Base.NewLabel();
        LocationFrame.Content = TitleLabel;
        TitleLabel.JustifyCenter();
        TitleLabel.Alignment.CenterStretch();
        TitleLabel.Font.Colour = Inv.Colour.Black;
        TitleLabel.Font.Size = 24;
        TitleLabel.Font.Thin();
        TitleLabel.Text = ActiveScreen.Location.Title + Environment.NewLine + "This screen has not been specified.";
      }
      else
      {
        LocationFrame.Content = ActiveScreen.Content;
      }

      if (RefreshEvent != null)
        RefreshEvent();
    }

    public static implicit operator Inv.Surface(ConsoleSurface Self)
    {
      return Self != null ? Self.Base : null;
    }

    private Application Application;
    private Inv.Surface Base;
    private Inv.DistinctList<ConsoleLocation> LocationList;
    private Inv.DistinctList<ConsoleScreen> ScreenList;
    private Inv.DistinctList<ConsoleTab> TabList;
    private ConsoleLaunch MenuLaunch;
    private Inv.Frame LocationFrame;
    private Dock TabDock;
    private int TabCount;
    private Inv.Button MoreShade;
    private Inv.Stack MoreStack;
    private ConsoleLaunch MoreLaunch;
  }

  public sealed class ConsoleMenu
  {
    internal ConsoleMenu(Inv.Surface Surface, Theme Theme)
    {
      this.TileList = new Inv.DistinctList<ConsoleTile>();

      this.Base = Surface.NewVerticalDock();
      Base.Background.Colour = Inv.Colour.WhiteSmoke;

      // TODO: labels. icons. switch users.
      var TitleDock = Surface.NewHorizontalDock();
      Base.AddHeader(TitleDock);
      TitleDock.Padding.Set(8, 8, 0, 0);
      TitleDock.Background.Colour = Inv.Colour.Black;
      TitleDock.Border.Set(0, 0, 0, ConsoleFoundation.ShadowSize);
      TitleDock.Border.Colour = Inv.Colour.DarkGray;

      this.Switch = new ConsoleSwitch(Surface);
      TitleDock.AddFooter(Switch);
      Switch.Colour = Theme.Colour;

      this.TitleLabel = Surface.NewLabel();
      TitleDock.AddHeader(TitleLabel);
      TitleLabel.Font.Colour = Inv.Colour.White;
      TitleLabel.Font.Size = 40;

      var Scroll = Surface.NewVerticalScroll();
      Base.AddClient(Scroll);

      this.TileTable = Surface.NewTable();
      Scroll.Content = TileTable;
      TileTable.Padding.Set(0, 0, 8, 8);
    }

    public ConsoleSwitch Switch { get; private set; }
    public string Title
    {
      get { return TitleLabel.Text; }
      set { TitleLabel.Text = value; }
    }

    internal Inv.Visibility Visibility
    {
      get { return Base.Visibility; }
    }

    internal ConsoleTile AddTile(string Title, Inv.Image Icon)
    {
      var Result = new ConsoleTile(Base.Surface);
      TileList.Add(Result);
      Result.Text = Title;
      Result.Icon = Icon;

      return Result;
    }
    internal void Arrange()
    {
      var Width = Base.Surface.Window.Width;
      var Height = Base.Surface.Window.Height;

      var Columns = (int)Math.Ceiling(Width / (float)ConsoleFoundation.TileSize);
      var Rows = (int)Math.Ceiling(Height / (float)ConsoleFoundation.TileSize);

      var RowsUsed = (int)Math.Ceiling((float)Columns / (float)TileList.Count);

      TileTable.RemovePanels();

      for (var Column = 0; Column < Columns; Column++)
      {
        var TableColumn = TileTable.AddColumn();
        TableColumn.Star();
      }

      TileTable.AddRow();

      var ColumnIndex = 0;
      var RowIndex = 0;

      foreach (var Tile in TileList)
      {
        var TableCell = TileTable.GetCell(ColumnIndex, RowIndex);
        TableCell.Content = Tile;

        ColumnIndex++;

        if (ColumnIndex >= Columns)
        {
          ColumnIndex = 0;
          RowIndex++;

          TileTable.AddRow();
        }
      }
    }

    public static implicit operator Inv.Panel(ConsoleMenu Self)
    {
      return Self != null ? Self.Base : null;
    }

    private Inv.Dock Base;
    private Inv.DistinctList<ConsoleTile> TileList;
    private Inv.Table TileTable;
    private Inv.Label TitleLabel;
  }

  public sealed class ConsoleLocation
  {
    internal ConsoleLocation(string Title, Inv.Image Icon)
    {
      this.Title = Title;
      this.Icon = Icon;
    }

    public string Title { get; private set; }
    public Inv.Image Icon { get; private set; }
    public event Action<ConsoleScreen> NavigateEvent;

    internal void Navigate(ConsoleScreen Screen)
    {
      if (NavigateEvent != null)
        NavigateEvent(Screen);
    }
  }

  public sealed class ConsoleScreen
  {
    internal ConsoleScreen(Inv.Surface Surface, ConsoleLocation Location)
    {
      this.Surface = Surface;
      this.Location = Location;

      this.Tab = new ConsoleTab(this, Surface);
      Tab.Text = Location.Title;

      Location.Navigate(this);
    }

    public Inv.Surface Surface { get; private set; }
    public ConsoleLocation Location { get; private set; }
    public ConsoleTab Tab { get; private set; }
    public Inv.Panel Content { get; set; }
    public event Action ArrangeEvent;
    public event Action ComposeEvent;

    internal void ArrangeInvoke()
    {
      if (ArrangeEvent != null)
        ArrangeEvent();
    }
    internal void ComposeInvoke()
    {
      if (ComposeEvent != null)
        ComposeEvent();
    }
  }

  public sealed class ConsoleSwitch
  {
    internal ConsoleSwitch(Inv.Surface Surface)
    {
      this.Base = Surface.NewButton();
      Base.Alignment.BottomRight();
      Base.Padding.Set(8);
      Base.Corner.Set(8, 8, 0, 0);

      this.Label = Surface.NewLabel();
      Base.Content = Label;
      Label.JustifyRight();
      Label.Font.Colour = Inv.Colour.White;
      Label.Font.Size = 16;
      Label.LineWrapping = false;
    }

    public Inv.Colour Colour
    {
      get { return Base.Background.Colour; }
      set { Base.Background.Colour = value; }
    }
    public event Action SelectEvent
    {
      add { Base.SingleTapEvent += value; }
      remove { Base.SingleTapEvent -= value; }
    }
    public string Text
    {
      get { return Label.Text; }
      set { Label.Text = value; }
    }

    public static implicit operator Inv.Panel(ConsoleSwitch Self)
    {
      return Self != null ? Self.Base : null;
    }

    private Inv.Button Base;
    private Inv.Label Label;
  }

  public sealed class ConsoleLaunch
  {
    internal ConsoleLaunch(Inv.Surface Surface)
    {
      this.Base = Surface.NewButton();
      Base.Background.Colour = Inv.Colour.Black;
      Base.Alignment.BottomStretch();

      this.Label = Surface.NewLabel();
      Base.Content = Label;
      Label.Alignment.Center();
      Label.Font.Colour = Inv.Colour.White;
      Label.Font.Size = 24;
    }

    public char Symbol
    {
      get { return Label.Text.Length > 0 ? Label.Text[0] : '\0'; }
      set { Label.Text = value.ToString(); }
    }
    public Inv.Colour Colour
    {
      get { return Base.Background.Colour; }
      set { Base.Background.Colour = value; }
    }
    public event Action SelectEvent
    {
      add { Base.SingleTapEvent += value; }
      remove { Base.SingleTapEvent -= value; }
    }

    internal Inv.Visibility Visibility
    {
      get { return Base.Visibility; }
    }
    internal Inv.Size Size
    {
      get { return Base.Size; }
    }

    public static implicit operator Inv.Panel(ConsoleLaunch Self)
    {
      return Self != null ? Self.Base : null;
    }

    private Inv.Button Base;
    private Label Label;
  }

  public sealed class ConsoleTile
  {
    internal ConsoleTile(Inv.Surface Surface)
    {
      this.Base = Surface.NewButton();
      Base.Background.Colour = Inv.Colour.LightGray;
      Base.Border.Colour = Inv.Colour.DarkGray;
      Base.Border.Set(1);
      Base.Margin.Set(8, 8, 0, 0);
      Base.Padding.Set(8);

      this.MainDock = Surface.NewVerticalDock();
      Base.Content = MainDock;

      this.MainGraphic = Surface.NewGraphic();
      MainDock.AddClient(MainGraphic);
      MainGraphic.Margin.Set(8);
      MainGraphic.Size.Set(64, 64);
      MainGraphic.Elevation.Set(2);

      this.MainLabel = Surface.NewLabel();
      MainDock.AddFooter(MainLabel);
      MainLabel.Font.Colour = Inv.Colour.Black;
      MainLabel.Font.Size = 16;
      MainLabel.JustifyCenter();
    }

    public Inv.Image Icon
    {
      get { return MainGraphic.Image; }
      set { MainGraphic.Image = value; }
    }
    public string Text
    {
      get { return MainLabel.Text; }
      set { MainLabel.Text = value; }
    }
    public Inv.Margin Margin
    {
      get { return Base.Margin; }
    }
    public event Action SelectEvent
    {
      add { Base.SingleTapEvent += value; }
      remove { Base.SingleTapEvent -= value; }
    }

    public static implicit operator Inv.Panel(ConsoleTile Tile)
    {
      return Tile != null ? Tile.Base : null;
    }

    private Inv.Button Base;
    private Inv.Dock MainDock;
    private Inv.Label MainLabel;
    private Inv.Graphic MainGraphic;
  }

  public sealed class ConsoleTab
  {
    internal ConsoleTab(ConsoleScreen Screen, Inv.Surface Surface)
    {
      this.Screen = Screen;
      this.Base = Surface.NewHorizontalDock();

      this.MainButton = Surface.NewButton();
      Base.AddClient(MainButton);
      MainButton.Background.Colour = Inv.Colour.Black;

      this.MainLabel = Surface.NewLabel();
      MainButton.Content = MainLabel;
      MainLabel.Font.Colour = Inv.Colour.White;
      MainLabel.Font.Size = 18;
      MainLabel.LineWrapping = false;
      MainLabel.Padding.Set(2, 0, 2, 0);
      MainLabel.JustifyCenter();

      this.CloseButton = Surface.NewButton();
      Base.AddFooter(CloseButton);
      CloseButton.Background.Colour = Inv.Colour.Black;
      CloseButton.Alignment.BottomStretch();
      CloseButton.Size.Set(ConsoleFoundation.ChromeSize, ConsoleFoundation.ChromeSize);

      var CloseLabel = Surface.NewLabel();
      CloseButton.Content = CloseLabel;
      CloseLabel.JustifyCenter();
      CloseLabel.Font.Colour = Inv.Colour.White;
      CloseLabel.Font.Size = 18;
      CloseLabel.Font.Bold();
      CloseLabel.Text = "✕";
    }

    public ConsoleScreen Screen { get; private set; }
    public Inv.Colour Colour
    {
      get { return MainButton.Background.Colour; }
      set 
      { 
        MainButton.Background.Colour = value;
        CloseButton.Background.Colour = value;
      }
    }
    public string Text
    {
      get { return MainLabel.Text; }
      set { MainLabel.Text = value; }
    }
    public Inv.Margin Margin
    {
      get { return Base.Margin; }
    }
    public Inv.Visibility Visibility
    {
      get { return Base.Visibility; }
    }
    public event Action SelectEvent
    {
      add { MainButton.SingleTapEvent += value; }
      remove { MainButton.SingleTapEvent -= value; }
    }
    public bool CloseAllowed
    {
      get { return CloseButton.Visibility.Get(); }
      set 
      { 
        CloseButton.Visibility.Set(value);
        MainButton.Padding.Set(0, 0, value ? 0 : ConsoleFoundation.ChromeSize, 0);
      }
    }
    public event Action CloseEvent
    {
      add { CloseButton.SingleTapEvent += value; }
      remove { CloseButton.SingleTapEvent -= value; }
    }

    public static implicit operator Inv.Panel(ConsoleTab Tab)
    {
      return Tab != null ? Tab.Base : null;
    }

    private Inv.Dock Base;
    private Inv.Button MainButton;
    private Inv.Label MainLabel;
    private Inv.Button CloseButton;
  }
}
