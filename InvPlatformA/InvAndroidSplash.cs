/*! 1 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Views.Animations;
using Android.Graphics;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;
using Inv.Support;

namespace Inv
{
  public sealed class AndroidSelfUpdate
  {
    public AndroidSelfUpdate(string LocalVersionText, string RemoteHostAddress, string RemoteVersionResource, string RemotePackageResource)
    {
      this.LocalVersionText = LocalVersionText;
      this.RemoteHostAddress = RemoteHostAddress;
      this.RemoteVersionResource = RemoteVersionResource;
      this.RemotePackageResource = RemotePackageResource;
    }

    public string LocalVersionText { get; private set; }
    public string RemoteHostAddress { get; private set; }
    public string RemoteVersionResource { get; private set; }
    public string RemotePackageResource { get; private set; }
  }

  public abstract class AndroidSplashActivity : Android.App.Activity
  {
    public int ImageResourceId { get; protected set; }
    public AndroidSelfUpdate SelfUpdate { get; protected set; }
    public Type LaunchActivity { get; protected set; }

    protected override void OnCreate(Android.OS.Bundle bundle)
    {
      Debug.Assert(ImageResourceId != 0, "ImageResourceId must be specified before base.OnCreate() is called.");
      // SelfUpdate is optional.
      Debug.Assert(LaunchActivity != null, "LaunchActivity must be specified before base.OnCreate() is called.");

      RequestWindowFeature(WindowFeatures.NoTitle);
      base.OnCreate(bundle);

      var RelativeLayout = new RelativeLayout(this);
      SetContentView(RelativeLayout);
      RelativeLayout.LayoutParameters = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MatchParent, FrameLayout.LayoutParams.MatchParent)
      {
        Gravity = GravityFlags.Fill
      };
      var ImageView = new ImageView(this);
      RelativeLayout.AddView(ImageView);
      ImageView.SetImageResource(ImageResourceId);
      var ImageLP = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WrapContent, RelativeLayout.LayoutParams.WrapContent);
      ImageView.LayoutParameters = ImageLP;
      ImageLP.AddRule(LayoutRules.CenterInParent);

      var fadeIn = new AlphaAnimation(0, 1);
      fadeIn.Interpolator = new DecelerateInterpolator();
      fadeIn.Duration = 1000;

      var fadeOut = new AlphaAnimation(1, 0);
      fadeOut.Interpolator = new AccelerateInterpolator();
      fadeOut.StartOffset = 1000;
      fadeOut.Duration = 1000;

      RelativeLayout.Animation = fadeIn;

      if (SelfUpdate == null)
      {
        Task.Run(() =>
        {
          System.Threading.Thread.Sleep(1000);

          StartActivity(LaunchActivity);
        });        
      }
      else
      {
        var LabelView = new TextView(this);
        RelativeLayout.AddView(LabelView);
        LabelView.Text = "checking for updates...";
        LabelView.TextSize = 20;
        LabelView.TextAlignment = TextAlignment.Center;
        LabelView.SetTextColor(Color.White);
        var LabelLP = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WrapContent, RelativeLayout.LayoutParams.WrapContent);
        LabelView.LayoutParameters = LabelLP;
        LabelLP.AddRule(LayoutRules.CenterHorizontal);

        Task.Run(() =>
        {
#if DEBUG
          System.Net.ServicePointManager.ServerCertificateValidationCallback += (Sender, Certificate, Chain, PolicyErrors) => true;
#endif

          try
          {
            var WebBroker = new Inv.WebBroker(SelfUpdate.RemoteHostAddress);

            var LocalVersion = SelfUpdate.LocalVersionText;
            var RemoteVersion = WebBroker.GetTextJsonObject<string>(SelfUpdate.RemoteVersionResource);

            var SelfUpgrade = LocalVersion != RemoteVersion;

            if (SelfUpgrade)
            {
              ImageView.Post(() => LabelView.Text = "downloading update...");

              var WebDownload = WebBroker.GetDownload(SelfUpdate.RemotePackageResource);

              var AppName = this.ApplicationInfo.LoadLabel(this.PackageManager).Replace(" ", "");
              var TempFile = Java.IO.File.CreateTempFile(AppName + RemoteVersion, ".apk", ExternalCacheDir);
              var TempUri = Android.Net.Uri.FromFile(TempFile);

              using (var FileStream = new FileStream(TempUri.Path, FileMode.Create))
              {
                var BufferSize = 16 * 1024; // 16KB
                var Buffer = new byte[BufferSize];

                var WriteLength = 0L;
                var ReadLength = WebDownload.Stream.Length;

                int BufferLength;
                while ((BufferLength = WebDownload.Stream.Read(Buffer, 0, BufferSize)) > 0)
                {
                  if (this.IsFinishing)
                    break;

                  FileStream.Write(Buffer, 0, BufferLength);

                  WriteLength += BufferLength;

                  var Progress = ReadLength > 0 ? (int)Math.Ceiling((double)WriteLength / (double)ReadLength * 100) : -1;

                  string ProgressText;
                  if (Progress < 0)
                    ProgressText = Math.Round(((double)WriteLength / 1024 / 1024), 1).ToString("N1") + " MB";
                  else
                    ProgressText = Progress + "%";

                  ImageView.Post(() => LabelView.Text = "downloading update... " + ProgressText);
                }
              }

              ImageView.Post(() =>
              {
                LabelView.Text = "installing update.";
                RelativeLayout.Animation = fadeOut;
              });

              System.Threading.Thread.Sleep(1000);

              var UpgradeIntent = new Android.Content.Intent(Android.Content.Intent.ActionView);
              UpgradeIntent.SetDataAndType(TempUri, "application/vnd.android.package-archive");
              UpgradeIntent.AddFlags(Android.Content.ActivityFlags.NewTask);
              StartActivity(UpgradeIntent);
            }
            else
            {
              ImageView.Post(() =>
              {
                LabelView.Text = "up to date.";
                RelativeLayout.Animation = fadeOut;
              });

              System.Threading.Thread.Sleep(1000);

              StartActivity(LaunchActivity);
            }
          }
          catch (Exception Exception)
          {
            Debug.WriteLine(Exception.Message);

            ImageView.Post(() =>
            {
              LabelView.Text = Exception.Message + Environment.NewLine + Environment.NewLine + Exception.StackTrace;
            });

            System.Threading.Thread.Sleep(10000);
          }

          Finish();
        });
      }
    }
  }
}