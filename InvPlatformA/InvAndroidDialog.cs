/*! 2 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Inv.Support;

namespace Inv
{
#if ANDROID_SUPPORT
  internal sealed class DateTimePagerAdapter : Android.Support.V4.View.PagerAdapter
  {
    public DateTimePagerAdapter(Context Context, DateTime SelectedDate)
      : base()
    {
      this.Context = Context;
      this.SelectedDateField = SelectedDate;
    }

    public DateTime SelectedDate
    {
      get
      {
        var Year = SelectedDateField.Year;
        var Month = SelectedDateField.Month;
        var Day = SelectedDateField.Day;
        var Hour = SelectedDateField.Hour;
        var Minute = SelectedDateField.Minute;

        if (DatePicker != null)
        {
          Year = DatePicker.Year;
          Month = DatePicker.Month + 1;
          Day = DatePicker.DayOfMonth;
        }

        if (TimePicker != null)
        {
          Hour = TimePicker.Hour;
          Minute = TimePicker.Minute;
        }

        return new DateTime(Year, Month, Day, Hour, Minute, 0);
      }
    }
    public override int Count { get { return 2; } }

    public const int DatePosition = 0;
    public const int TimePosition = 1;
    
    public override bool IsViewFromObject(View View, Java.Lang.Object ObjectValue)
    {
      return View == ObjectValue;
    }
    public override Java.Lang.Object InstantiateItem(ViewGroup Container, int Position)
    {
      switch (Position)
      {
        case DatePosition:
          this.DatePicker = new DatePicker(Context);
          DatePicker.Init(SelectedDateField.Year, SelectedDateField.Month - 1, SelectedDateField.Day, null);
          Container.AddView(DatePicker);
          return DatePicker;

        case TimePosition:
          this.TimePicker = new TimePicker(Context);
          TimePicker.Hour = SelectedDateField.Hour;
          TimePicker.Minute = SelectedDateField.Minute;
          Container.AddView(TimePicker);
          return TimePicker;

        default:
          return null;
      }
    }
    public override void DestroyItem(ViewGroup Container, int Position, Java.Lang.Object ObjectValue)
    {
      var View = ObjectValue as View;
      if (View != null)
        Container.RemoveView(View);

      switch (Position)
      {
        case DatePosition:
          DatePicker = null;
          break;

        case TimePosition:
          TimePicker = null;
          break;
      }
    }
    public override Java.Lang.ICharSequence GetPageTitleFormatted(int Position)
    {
      var Result = string.Empty;

      switch (Position)
      {
        case DatePosition:
          Result = "Date";
          break;

        case TimePosition:
          Result = "Time";
          break;
      }

      return new Java.Lang.String(Result);
    }

    private Context Context;
    private DateTime SelectedDateField;
    private DatePicker DatePicker;
    private TimePicker TimePicker;
  }

  internal sealed class AndroidDateTimePickerFragment : Android.App.DialogFragment, Android.Support.Design.Widget.TabLayout.IOnTabSelectedListener, Android.Support.V4.View.ViewPager.IOnPageChangeListener
  {
    public AndroidDateTimePickerFragment()
    {
      var Now = DateTime.Now;

      this.SelectedDate = new DateTime(Now.Year, Now.Month, Now.Day, Now.Hour, Now.Minute, 0);
    }

    public static readonly string TAG = "X:" + typeof(AndroidDateTimePickerFragment).Name.ToUpper();

    public DateTime SelectedDate { get; set; }
    public event Action SelectEvent;
    public event Action CancelEvent;

    public void OnTabReselected(Android.Support.Design.Widget.TabLayout.Tab Tab) { }
    public void OnTabSelected(Android.Support.Design.Widget.TabLayout.Tab Tab)
    {
      if (Tab == DateTab)
        Pager.CurrentItem = DateTimePagerAdapter.DatePosition;
      else if (Tab == TimeTab)
        Pager.CurrentItem = DateTimePagerAdapter.TimePosition;
    }
    public void OnTabUnselected(Android.Support.Design.Widget.TabLayout.Tab Tab) { }
    public void OnPageScrollStateChanged(int State) { }
    public void OnPageScrolled(int Position, float PositionOffset, int PositionOffsetPixels) { }
    public void OnPageSelected(int Position)
    {
      switch (Position)
      {
        case DateTimePagerAdapter.DatePosition:
          DateTab.Select();
          break;

        case DateTimePagerAdapter.TimePosition:
          TimeTab.Select();
          break;
      }
    }

    public override Dialog OnCreateDialog(Bundle savedInstanceState)
    {
      this.DismissEventInvoked = false;

      this.Adapter = new DateTimePagerAdapter(Activity, SelectedDate);

      this.Pager = new Android.Support.V4.View.ViewPager(Activity);
      Pager.Adapter = Adapter;
      Pager.AddOnPageChangeListener(this);

      var TabLayout = new Android.Support.Design.Widget.TabLayout(Activity);
      this.DateTab = TabLayout.NewTab().SetText("Date");
      this.TimeTab = TabLayout.NewTab().SetText("Time");
      TabLayout.AddTab(DateTab);
      TabLayout.AddTab(TimeTab);
      TabLayout.SetOnTabSelectedListener(this);

      var Layout = new LinearLayout(Activity);
      Layout.Orientation = Orientation.Vertical;
      Layout.AddView(TabLayout);
      Layout.AddView(Pager);

      var Builder = new AlertDialog.Builder(Activity);
      Builder.SetView(Layout);
      Builder.SetNegativeButton("Cancel", delegate { CancelInvoke(); });
      Builder.SetPositiveButton("OK", delegate { SelectInvoke(); });

      return Builder.Create();
    }
    public override void OnDismiss(IDialogInterface Dialog)
    {
      base.OnDismiss(Dialog);
      
      CancelInvoke();
    }

    private void SelectInvoke()
    {
      SelectedDate = Adapter.SelectedDate;

      if (!DismissEventInvoked)
      {
        if (SelectEvent != null)
          SelectEvent();

        DismissEventInvoked = true;
      }
    }
    private void CancelInvoke()
    {
      if (!DismissEventInvoked)
      {
        if (CancelEvent != null)
          CancelEvent();

        DismissEventInvoked = true;
      }
    }

    private bool DismissEventInvoked;
    private Android.Support.V4.View.ViewPager Pager;
    private DateTimePagerAdapter Adapter;
    private Android.Support.Design.Widget.TabLayout.Tab DateTab;
    private Android.Support.Design.Widget.TabLayout.Tab TimeTab;
  }
#endif

  // SOURCE: http://developer.xamarin.com/guides/android/user_interface/date_picker/
  internal sealed class AndroidDatePickerFragment : Android.App.DialogFragment, AndroidDatePickerDialog.IOnDateSetListener
  {
    public AndroidDatePickerFragment()
    {
      this.SelectedDate = DateTime.Now.Date;
    }

    // TAG can be any string of your choice.
    public static readonly string TAG = "X:" + typeof(AndroidDatePickerFragment).Name.ToUpper();

    // Initialize this value to prevent NullReferenceExceptions.
    public DateTime SelectedDate { get; set; }
    public event Action SelectEvent;
    public event Action CancelEvent;

    public override Android.App.Dialog OnCreateDialog(Android.OS.Bundle savedInstanceState)
    {
      // NOTE: monthOfYear is a value between 0 and 11.
      var Result = new AndroidDatePickerDialog(Activity, this, SelectedDate.Year, SelectedDate.Month - 1, SelectedDate.Day);
      Result.CancelEvent += () => CancelInvoke();
      Result.DismissEvent += () => CancelInvoke();
      return Result;
    }

    private void CancelInvoke()
    {
      if (CancelEvent != null)
        CancelEvent();
    }

    void Android.App.DatePickerDialog.IOnDateSetListener.OnDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
    {
      // NOTE: monthOfYear is a value between 0 and 11.
      this.SelectedDate = new DateTime(year, monthOfYear + 1, dayOfMonth);

      if (SelectEvent != null)
        SelectEvent();
    }
  }

  internal sealed class AndroidDatePickerDialog : Android.App.DatePickerDialog
  {
    public AndroidDatePickerDialog(Context context, AndroidDatePickerDialog.IOnDateSetListener listener, int year, int monthOfYear, int dayOfMonth)
      : base(context, listener, year, monthOfYear, dayOfMonth)
    {
    }
    public new event Action CancelEvent;
    public new event Action DismissEvent;
    public override void Cancel()
    {
      base.Cancel();

      if (CancelEvent != null)
        CancelEvent();
    }
    public override void Dismiss()
    {
      base.Dismiss();

      if (DismissEvent != null)
        CancelEvent();
    }
  }

  internal sealed class AndroidTimePickerFragment : Android.App.DialogFragment, AndroidTimePickerDialog.IOnTimeSetListener
  {
    public AndroidTimePickerFragment()
    {
      this.SelectedTime = DateTime.Now.Date;
    }

    // TAG can be any string of your choice.
    public static readonly string TAG = "X:" + typeof(AndroidTimePickerFragment).Name.ToUpper();

    // Initialize this value to prevent NullReferenceExceptions.
    public DateTime SelectedTime { get; set; }
    public event Action SelectEvent;
    public event Action CancelEvent;

    public override Android.App.Dialog OnCreateDialog(Android.OS.Bundle savedInstanceState)
    {
      // NOTE: monthOfYear is a value between 0 and 11.
      var Result = new AndroidTimePickerDialog(Activity, this, SelectedTime.Hour, SelectedTime.Minute);
      Result.CancelEvent += () => CancelInvoke();
      Result.DismissEvent += () => CancelInvoke();
      return Result;
    }

    private void CancelInvoke()
    {
      if (CancelEvent != null)
        CancelEvent();
    }

    void Android.App.TimePickerDialog.IOnTimeSetListener.OnTimeSet(TimePicker view, int hourOfDay, int minute)
    {
      // NOTE: monthOfYear is a value between 0 and 11.
      this.SelectedTime = DateTime.MinValue + new TimeSpan(hourOfDay, minute, 0);

      if (SelectEvent != null)
        SelectEvent();
    }
  }

  internal sealed class AndroidTimePickerDialog : Android.App.TimePickerDialog
  {
    public AndroidTimePickerDialog(Context context, AndroidTimePickerDialog.IOnTimeSetListener listener, int hourOfDay, int minuteOfHour)
      : base(context, listener, hourOfDay, minuteOfHour, true)
    {
    }
    public new event Action CancelEvent;
    public new event Action DismissEvent;
    public override void Cancel()
    {
      base.Cancel();

      if (CancelEvent != null)
        CancelEvent();
    }
    public override void Dismiss()
    {
      base.Dismiss();

      if (DismissEvent != null)
        CancelEvent();
    }
  }
}